package com.sensichips.sensiplus.level0.driver.winux;

import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.driver.winux.com_ble.EventListenerBLE;
import com.sensichips.sensiplus.level0.driver.winux.com_ble.HCI;
import com.sensichips.sensiplus.level0.driver.winux.com_ble.Receiver;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;


import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverListener;

import java.util.ArrayList;


public class SPDriverWINUX_COMBLE extends SPDriver implements  EventListenerBLE {

    public static String NIX_EXPR = "nux";
    public static String NIX = "*nix";
    public static String WINDOWS = "Windows";
    public static String OP_SYSTEM = NIX;

	private SerialPort com;
	private Receiver receiver;
	private SPDriverListener driverListener;
	public static final String DRIVERNAME = "WINUX_COMBLE";
	private String driverKey;
	private Integer TIME_OUT = 2000;

	//Synchronization
	private boolean freeToWrite = true;
	private byte[] receivedData = null;
	private int baudRate = -1;
	private String comPort;

	public SPDriverWINUX_COMBLE(ArrayList<String> driverParameters) throws SPDriverException {
        String osName = System.getProperty("os.name");
        //System.out.println(osName);
        OP_SYSTEM = osName.contains(NIX_EXPR) ? NIX : WINDOWS;

        if (OP_SYSTEM == NIX){
            comPort = driverParameters.get(0);
        } else {
            comPort = driverParameters.get(1);
        }

        baudRate = Integer.parseInt(driverParameters.get(2));

        driverKey = DRIVERNAME + "_" + comPort;

    }

    @Override
    public long getSPDriverTimeOut(){
		return 30000;
	}


    public String getDriverKey(){
	    return driverKey;
    }

    private void setBaudRate(int baudRate){
		this.baudRate = baudRate;
	}

	private void setComPort(String comPort) throws SPDriverException {
		if (comPort == null){
            throw new SPDriverException("Com port cannot be null!");
        }
        this.comPort = comPort;
    }
	@Override
	public void openConnection() throws SPDriverException {

		if (comPort == null || baudRate < 0){
			throw new SPDriverException("SPDriverWINUX_COMBLE: port or baudRate non specified");
		}
		try{
			com = new SerialPort(this.comPort);
			com.openPort();
			//Open serial comPort
			com.setParams(baudRate,
					SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);//Set params. Also you can set params by this string: serialPort.setParams(9600, 8, 1, 0);
		} catch (SerialPortException spe){
			SerialPortList spl;
			String[] portNames = SerialPortList.getPortNames();
			String mes = "";
			for(int i = 0; i < portNames.length; i++){
				mes += portNames[i] + (i < (portNames.length - 1)?", ":"");
			}

			throw new SPDriverException("SPDriverCOM_BLE in open comPort: " + spe.getMessage() + "\nAvailable ports: " + mes);
		}

		//this.com = sp;
		receiver = new Receiver(com);
		receiver.addEventListener(this);
		this.driverListener = driverListener;


		String irk_csrk = "0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00";
		try {

			this.com.writeBytes(HCI.GAP_DeviceInit("0x08", "0x05", irk_csrk, irk_csrk, "0x01 0x00 0x00 0x00"));
		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			this.com.writeBytes(HCI.GAP_DeviceDiscoveryRequest("0x03", "0x01", "0x00"));
		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			throw new SPDriverException(e.getMessage());
		}
		
	}

    @Override
    public void setParameters(ArrayList<String> driverParameters) throws SPDriverException {

    }


	public void establishLink() {
		//System.out.println("establishLink");
		String peerAddr = "0xBA 0x20 0x5E 0x16 0x4A 0x54";
		String peerAddrSensiplusDemo = "0x99 0xE6 0x79 0x18 0xEB 0x84";
		try {
			
			this.com.writeBytes(HCI.GAP_EstablishLinkRequest("0x00", "0x00", "0x00", peerAddrSensiplusDemo));
		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void enableBLENotification() {
		try {
			this.com.writeBytes(HCI.GATT_WriteCharValue(2, "0x00 0x00", "0x2F 0x00", "0x01 0x00"));
		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("\n\nSEND - Write using uuid: "+bufferTX);
	}
	
	private synchronized void newData(byte[] data){


        receivedData = new byte[data.length];
        System.arraycopy(data, 0, receivedData, 0, data.length);

        

        notifyAll();
    }

	public synchronized int read(byte[] output) throws SPDriverException {
		// TODO Auto-generated method stub
		
		if (receivedData == null){

            try {
               
                wait(TIME_OUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return 0;
            }
        }
		
		/*int i;
        for(i=0; i<receivedData.length; i++) {
            //String s_appo = Integer.toHexString(b).toUpperCase();
            if(receivedData[i]==(byte)('\n'))
                break;
        }*/

		if (receivedData == null){
			throw new SPDriverException("Time out during read operation din SPDriverWINUX_COMBLE!");
		}

        int numByte = receivedData[0];
		//output = new byte[numByte];
        System.arraycopy(receivedData, 0, output, 0, receivedData.length);
		
        receivedData = null;
        freeToWrite = true;

        //System.out.println("Value Read: "+HCI.bytesToHexStrings(output));

        notify();

        return output.length;
	}

	public synchronized int write(byte[] toWrite) throws SPDriverException {
		// TODO Auto-generated method stub
		while(!freeToWrite){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		freeToWrite = false;
		
		try {
			byte[] packet = HCI.GATT_WriteCharValue(toWrite.length, "0x00 0x00", "0x32 0x00", HCI.bytesToHexStrings(toWrite));
			this.com.writeBytes(packet);
		} catch (SerialPortException e) {
			// TODO Auto-generated catch block
			freeToWrite = true;
			throw new SPDriverException(e.getMessage());
			
		}
		
		
		
		return toWrite.length;
	}



	public void GAPDeviceInformation(String deviceAddress) {
		// TODO Auto-generated method stub
		System.out.println(deviceAddress);
	}



	public void GAPDiscoveryDone() {
		// TODO Auto-generated method stub
	
		System.out.println("Discovery done");
		this.establishLink();
		
	}



	public void GAPLinkEstablished() {
		// TODO Auto-generated method stub
		
		System.out.println("Link Established");
		this.enableBLENotification();
		
		//Notifica il driver listener che la connessione 
		//con il ble-device � stata stabilita
		//this.driverListener.connectionUp();
	
	}



	public void ATTReadByTypeResponse(String dataRead) {
		// TODO Auto-generated method stub
		
	}



	public void ATTWriteResponse(String status) {
		// TODO Auto-generated method stub
		System.out.println("SPMeasurementStatus send: "+status);
	}



	
	
	public void ATTHandleValueNotification(byte[] values) {

		this.newData(values);
	}


    public void ATTNotificationEnabled() {
        System.out.println("Notification Enabled");
		driverListener.connectionUp();
    }

	public void GAPTerminateLink() {
		System.out.println("Link Terminate");
		try {
			receiver.setStopThread(true);
			this.com.closePort();
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
	}

    public void closeConnection() throws SPDriverException {
        try {

			this.com.writeBytes(HCI.GAP_TerminateLinkRequest("0x00 0x00"));

        } catch (SerialPortException e) {
            throw new SPDriverException("SPDriverWINUX_COMUSB closeConnection(): " + e.getMessage());
        }
		
	}

	public String getDriverName() {
		// TODO Auto-generated method stub
		return DRIVERNAME;
	}

	public boolean isConnected() throws SPDriverException {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean equals(SPDriver spDriver){
        return spDriver.getDriverName().equals(this.getDriverName()) && this.comPort.equals(((SPDriverWINUX_COMBLE)spDriver).comPort);
    }



}
