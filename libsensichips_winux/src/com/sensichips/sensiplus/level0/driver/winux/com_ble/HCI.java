package com.sensichips.sensiplus.level0.driver.winux.com_ble;

import jssc.SerialPortException;


public class HCI {
	
	//SerialPort sp;
	//byte[] TXBuffer;
	
	/*public HCI(SerialPort sp){
		this.sp = sp;
	}
	*/
	
	private static byte[] generateBuff(String s){
    	String[] hexStrings = s.split(" ");
    	byte[] buff = new byte[hexStrings.length];
    	
    	for(int i=0; i<hexStrings.length; i++){
    		buff[i] = Integer.decode(hexStrings[i]).byteValue();
    	}
    	
    	return buff;
    }
	
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String byteToHexString(byte b) {
        char[] hexChars = new char[4];

        int v = b & 0xFF;

		hexChars[0] = '0';
		hexChars[1] = 'x';

        hexChars[2] = hexArray[v >>> 4];
        hexChars[3] = hexArray[v & 0x0F];

        return new String(hexChars);
    }
    
    public static String bytesToHexStrings(byte[] buff){
    	String hex="";
    	for(int i=0; i<buff.length; i++){
    		hex=hex+byteToHexString(buff[i])+" ";
    	}
    	
		return hex;
    	
    }
	
    
	public static byte[] GAP_DeviceInit(String profileRole, String maxScanResponses, String IRK, String CSRK, String signCounter) throws SerialPortException{

		String total = "0x01 0x00 0xFE 0x26 "+profileRole+" "+maxScanResponses+" "+IRK+" "+CSRK+" "+signCounter;
		
		//TXBuffer = generateBuff(total);
		
		//sp.writeBytes(TXBuffer);		

		//Restituisce il buffer di byte inviati sottoforma di stringa
		return generateBuff(total);
		
	}
	
	
	public static byte[] GAP_DeviceDiscoveryRequest(String mode, String activeScan, String whiteList) throws SerialPortException{
		String total = "0x01 0x04 0xFE 0x03 "+mode+" "+activeScan+" "+whiteList;
		//TXBuffer = generateBuff(total);
		
		//sp.writeBytes(TXBuffer);


		return generateBuff(total);		
	}
	
	
	public static byte[] GAP_EstablishLinkRequest(String highDutyCycle, String whiteList, String addrTypePeer, String peerAddr) throws SerialPortException{
		String total = "0x01 0x09 0xFE 0x09 "+highDutyCycle+" "+whiteList+" "+addrTypePeer+" "+peerAddr;
		
		//TXBuffer = generateBuff(total);
				
		//sp.writeBytes(TXBuffer);
		


		return generateBuff(total);		
	}
	
	
	public static byte[] GATT_ReadUsingCharUUID(String connHandle, String startHandle, String endHandle, String type) throws SerialPortException{
		String total = "0x01 0xB4 0xFD 0x08 "+connHandle+" "+startHandle+" "+endHandle+" "+type;
		//TXBuffer = generateBuff(total);
		
		//sp.writeBytes(TXBuffer);
		
		
		
		
		return  generateBuff(total);
	}
	
	
	public static byte[] GATT_WriteCharValue(int numOfByte,String connHandle, String Handle,String values) throws SerialPortException{
		int datalen = numOfByte+4;
		
		String total = "0x01 0x92 0xFD "+byteToHexString((byte)datalen)+" "+connHandle+" "+Handle+" "+values;
		
		//TXBuffer = generateBuff(total);
		
		//sp.writeBytes(TXBuffer);
		
		return generateBuff(total);
	}

	public static byte[] GAP_TerminateLinkRequest(String connHandle){

		String total = "0x01 0x0A 0xFE 0x03 "+connHandle+" 0x13";

		return generateBuff(total);

	}
	

}
