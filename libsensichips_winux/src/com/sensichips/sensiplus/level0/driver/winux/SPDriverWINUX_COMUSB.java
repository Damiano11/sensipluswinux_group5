package com.sensichips.sensiplus.level0.driver.winux;

import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

import java.util.ArrayList;

/**
 * Created by mario on 03/12/2015.
 */
public class SPDriverWINUX_COMUSB extends SPDriver {

    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L0;

    public static final String DRIVERNAME = "WINUX_COMUSB";
    //private SPDriverListener driverListener;

    public static String NIX_EXPR = "nux";
    public static String NIX = "*nix";
    public static String WINDOWS = "Windows";
    public static String OP_SYSTEM = NIX;


    private SerialPort serialPort;

    private boolean connected;
    private int baudRate = SerialPort.BAUDRATE_115200;
    private String comPort;
    private String driverKey;


    public SPDriverWINUX_COMUSB(ArrayList<String> driverParameters) throws SPDriverException {
        //String osName = System.getProperty("os.name");
        //System.out.println(osName);
        //OP_SYSTEM = osName.contains(NIX_EXPR) ? NIX : WINDOWS;

        //if (OP_SYSTEM == NIX){
        comPort = driverParameters.get(0);
        //} else {
        //    comPort = driverParameters.get(1);
        //}

        baudRate = Integer.parseInt(driverParameters.get(1));
        String secondPartOfKey = "";
        for(int i = 2; i < driverParameters.size(); i++){
            secondPartOfKey += driverParameters.get(i) + ((i < driverParameters.size())? "" : "_");
        }
        driverKey = DRIVERNAME + "_" + comPort + "_" + secondPartOfKey;
    }

    public String getDriverKey(){
        return driverKey;
    }

    public void setBaudRate(int baudRate){
        this.baudRate = baudRate;
    }

    public void setComPort(String comPort) throws SPDriverException {
        if (comPort == null){
            throw new SPDriverException("Com port cannot be null!");
        }
        this.comPort = comPort;
    }


    @Override
    public void openConnection() throws SPDriverException {

        if (comPort == null || baudRate < 0){
            throw new SPDriverException("SPDriverWINUX_COMBLE: port or baudRate non specified");
        }
        try{
            if (!connected){
                serialPort = new SerialPort(comPort);
                this.serialPort.openPort();
                //Open serial port
                this.serialPort.setParams(baudRate,
                        SerialPort.DATABITS_8,
                        SerialPort.STOPBITS_1,
                        SerialPort.PARITY_NONE);//Set params. Also you can set params by this string: serialPort.setParams(9600, 8, 1, 0);

                connected = true;
            }
        } catch (SerialPortException spe){
            SerialPortList spl;
            String[] portNames = SerialPortList.getPortNames();
            String mes = "";
            for(int i = 0; i < portNames.length; i++){
                mes += portNames[i] + (i < (portNames.length - 1)?", ":"");
            }

            throw new SPDriverException("SPDriverWINUX_COMUSB in open port: " + spe.getMessage() + "\nAvailable ports: " + mes);
        }
    }

    @Override
    public String getDriverName() {
        return DRIVERNAME;
    }


    @Override
    public boolean isConnected(){
        return connected;
    }

    /**
     * Receive bytes on USB at the low possible level
     *
     * @param buf with this buf(byte)0xFE,r the function return the received bytes to the calling function.
     * @return the number received bytes
     */
    @Override
    public synchronized int read(byte[] buf) throws SPDriverException {
        int numByteReceived = 0;
        try {
            byte[] recvd = serialPort.readBytes();
            if (recvd != null){
                //System.out.println(recvd.length);
                numByteReceived = recvd.length;
                System.arraycopy(recvd, 0, buf, 0, recvd.length);
            } else {
                //System.out.println("Ricevuto null ...");
            }
        } catch (SerialPortException spe){
            SPLogger.getLogInterface().d(LOG_MESSAGE, "SPDriverWINUX_COMUSB read. " + spe.getMessage(), DEBUG_LEVEL);
            throw new SPDriverException(LOG_MESSAGE + " SPDriverWINUX_COMUSB read. " + spe.getMessage());
        }
        return numByteReceived;
    }

    /**
     * Send bytes on USB at the low possible level
     *
     * @param buf all the bytes into te buf array will be send on the USB channel
     * @return the number of bytes ef(byte)0xFE,ctively sent
     */
    @Override
    public synchronized int write(byte[] buf) throws SPDriverException {
        try{
            boolean ok = serialPort.writeBytes(buf);
            if (ok) {
                return buf.length;
            } else {
                return 0;
            }
        } catch (SerialPortException spe){
            throw new SPDriverException("SPDriverWINUX_COMUSB send. " + spe.getMessage());
        }
    }

    @Override
    public long getSPDriverTimeOut(){
        return 3000;
    }

    @Override
    public void closeConnection() throws SPDriverException {
        try {
            connected = false;
            serialPort.closePort();
        } catch (SerialPortException e) {
            throw new SPDriverException("SPDriverWINUX_COMUSB closeConnection(): " + e.getMessage());
        }
    }


    @Override
    public boolean equals(SPDriver spDriver){
        return spDriver.getDriverName().equals(this.getDriverName()) && this.comPort.equals(((SPDriverWINUX_COMUSB)spDriver).comPort);
    }



    @Override
    public void setParameters(ArrayList<String> driverParameters) throws SPDriverException {

    }


    public static void main(String args[]) throws Exception {

        byte[] resp = new byte[256];

        ArrayList<String> parameters = new ArrayList<>();
        parameters.add("/dev/ttyUSB0");
        parameters.add("COM15");
        parameters.add("" + SerialPort.BAUDRATE_115200);

        SPDriverWINUX_COMUSB port = new SPDriverWINUX_COMUSB(parameters);
        port.openConnection();

        byte[] buf = new byte[]{
                (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00,
                (byte)0x00, (byte)0x00, (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00};
                //(byte)0x03, (byte)0x67, (byte)0x80, (byte)0x45, (byte)0x4D, (byte)0x0F};

        port.write(buf);

        int numByteReceived = 0;
        int numByteReceivedTot = 0;
        int numByteExpected = 5;
        int counter = 0;
        while(numByteReceivedTot != numByteExpected) {
            while (numByteReceived == 0) {
                numByteReceived = port.read(resp);
            }
            numByteReceivedTot += numByteReceived;
            for (int i = 0; i < resp.length; i++) {
                if(resp[i] != 0 && resp[i] != 1){
                    counter++;
                    System.out.print("" + Integer.toHexString(resp[i]).replace("ffffff", "") + " "); // + "(" + (char)resp[i] + ")");
                    if (counter % 8 == 0) {
                        System.out.println(" - " + numByteReceivedTot);
                    }
                }
            }
            numByteReceived = 0;

        }
        System.out.println("\n*******************************************************************");

        /*******************************************************************/


        buf = new byte[]{(byte)0x03, (byte)0x67, (byte)0x80, (byte)0x45, (byte)0x4D, (byte)0x0F};
        port.write(buf);

        numByteReceived = 0;
        numByteReceivedTot = 0;
        numByteExpected = 10;
        counter = 0;
        while(numByteReceivedTot != numByteExpected) {
            while (numByteReceived == 0) {
                numByteReceived = port.read(resp);
            }
            numByteReceivedTot += numByteReceived;
            for (int i = 0; i < resp.length; i++) {
                if(resp[i] != 0 && resp[i] != 1){
                    counter++;
                    System.out.print("" + Integer.toHexString(resp[i]).replace("ffffff", "") + " "); // + "(" + (char)resp[i] + ")");
                    if (counter % 8 == 0) {
                        System.out.println(" - " + numByteReceivedTot);
                    }
                }
            }
            numByteReceived = 0;
        }
        System.out.println("\n*******************************************************************");


        buf = new byte[]{(byte)0xE0,
                (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xFE, (byte)0xFE, (byte)0xF8, (byte)0xFE, (byte)0xFE, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xF8, (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE,
                (byte)0xE0,
                (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xF8, (byte)0xF8, (byte)0xFE, (byte)0xFE, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xFE, (byte)0xF8, (byte)0xFE, (byte)0xF8, (byte)0xFE, (byte)0xF8, (byte)0xFE, (byte)0xF8,
                (byte)0xE0,
                (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xF8, (byte)0xF8, (byte)0xFE, (byte)0xFE, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xFE,
                (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE,
                (byte)0xE0,
                (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xF8, (byte)0xF8, (byte)0xFE, (byte)0xFE, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xFE, (byte)0xF8, (byte)0xFE, (byte)0xF8, (byte)0xFE, (byte)0xF8, (byte)0xFE, (byte)0xF8,
                (byte)0xE0,
                (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xF8,
                (byte)0xF8, (byte)0xF8, (byte)0xFE, (byte)0xFE, (byte)0xF8, (byte)0xF8, (byte)0xF8, (byte)0xFE,
                (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE, (byte)0xFE};

        port.write(buf);


        numByteReceived = 0;
        numByteReceivedTot = 0;
        numByteExpected = buf.length;
        counter = 0;
        while(numByteReceivedTot < numByteExpected) {
            resp = new byte[256];
            while (numByteReceived == 0) {
                numByteReceived = port.read(resp);
            }
            numByteReceivedTot += numByteReceived;
            for (int i = 0; i < resp.length; i++) {
                if(resp[i] != 0 && resp[i] != 1){
                    counter++;
                    System.out.print("" + Integer.toHexString(resp[i]).replace("ffffff", "") + " "); // + "(" + (char)resp[i] + ")");
                    if (counter % 8 == 0) {
                        System.out.println(" - " + numByteReceivedTot);
                    }
                }
            }
            numByteReceived = 0;
            Thread.sleep(1);
        }
        System.out.println("\n*******************************************************************");
    }



}
