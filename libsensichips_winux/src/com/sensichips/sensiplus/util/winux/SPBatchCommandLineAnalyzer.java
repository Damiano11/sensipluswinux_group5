package com.sensichips.sensiplus.util.winux;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.util.batch.SPBatchParameters;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import org.apache.commons.cli.*;

public class SPBatchCommandLineAnalyzer {

    public static String LOG_MESSAGE = "SPBatchCommandLineAnalyzer";

    private SPBatchParameters spBatchParameters = new SPBatchParameters();

    public SPBatchCommandLineAnalyzer(String args[]) throws SPException {

        CommandLine cmd = commandLine(args);

        if (cmd == null){
            return;
        }

        System.out.println("***************************************");
        System.out.println("Input parameters: ");
        String msg1 = "";
        String msg2 = "";

        for(int i = 0; i < cmd.getOptions().length; i++){
            msg1 = cmd.getOptions()[i].getLongOpt();
            msg2 = "";
            if (cmd.getOptions()[i].getValues() != null){
                for(int j = 0; j < cmd.getOptions()[i].getValues().length; j++){
                    msg2 = cmd.getOptions()[i].getValues()[j] + " ";
                }
            }
            System.out.format("%-20s %s\n", msg1, msg2);
        }
        System.out.println("***************************************");
        spBatchParameters.valid = true;
        spBatchParameters.layoutConfigurationFileXML = cmd.getOptionValue(SPBatchParameters.layoutConfigurationName) == null ? spBatchParameters.layoutConfigurationFileXML :cmd.getOptionValue(SPBatchParameters.layoutConfigurationName);
        spBatchParameters.outputDir = cmd.getOptionValue(SPBatchParameters.outputDirName) == null ? spBatchParameters.outputDir :cmd.getOptionValue(SPBatchParameters.outputDirName);
        spBatchParameters.batchFile = cmd.getOptionValue(SPBatchParameters.batchFileName) == null ? spBatchParameters.batchFile :cmd.getOptionValue(SPBatchParameters.batchFileName);
        spBatchParameters.gui = cmd.hasOption(SPBatchParameters.guiName);
        spBatchParameters.currentConfiguration = cmd.getOptionValue(SPBatchParameters.currentConfiguratinoName) == null ? spBatchParameters.currentConfiguration : cmd.getOptionValue(SPBatchParameters.currentConfiguratinoName);
        spBatchParameters.host = cmd.getOptionValue(SPBatchParameters.hostName) == null ? spBatchParameters.host : cmd.getOptionValue(SPBatchParameters.hostName);
        spBatchParameters.port = Integer.parseInt(cmd.getOptionValue(SPBatchParameters.portName) == null ? ("" + spBatchParameters.port) : cmd.getOptionValue(SPBatchParameters.portName));


        SPLogger.DEBUG_TH =  cmd.hasOption(SPBatchParameters.debugLevel0Name) ? SPLoggerInterface.DEBUG_VERBOSITY_L0 : 0;
        SPLogger.DEBUG_TH |= cmd.hasOption(SPBatchParameters.debugLevel1Name) ? SPLoggerInterface.DEBUG_VERBOSITY_L1 : 0;
        SPLogger.DEBUG_TH |= cmd.hasOption(SPBatchParameters.debugLevel2Name) ? SPLoggerInterface.DEBUG_VERBOSITY_L2 : 0;

    }

    public SPBatchParameters getBatchParameters(){
        return spBatchParameters;
    }

    private CommandLine commandLine(String args[]) throws SPException {
        Options options = new Options();

        Option sc = new Option(SPBatchParameters.currentConfiguratinoName, "currentConfiguration", true, "default configuration used by no-gui application");
        sc.setRequired(true);
        options.addOption(sc);

        Option gui = new Option(SPBatchParameters.guiName, "gui", false, "start application with graphical user interface");
        gui.setRequired(false);
        options.addOption(gui);

        Option conf_dir = new Option(SPBatchParameters.layoutConfigurationName, "layoutConfigurations", true, "file containing chips configurations");
        conf_dir.setRequired(true);
        options.addOption(conf_dir);

        Option output_dir = new Option(SPBatchParameters.outputDirName, "outputDir", true, "output directory where data are saved");
        output_dir.setRequired(true);
        options.addOption(output_dir);

        Option batch_file = new Option(SPBatchParameters.batchFileName, "batchFile", true, "batch file to execute");
        batch_file.setRequired(true);
        options.addOption(batch_file);

        Option debug_level0 = new Option(SPBatchParameters.debugLevel0Name, "debug_level0", false, "show level 0 debug info");
        debug_level0.setRequired(false);
        options.addOption(debug_level0);

        Option debug_level1 = new Option(SPBatchParameters.debugLevel1Name, "debug_level1", false, "show level 1 debug info");
        debug_level1.setRequired(false);
        options.addOption(debug_level1);

        Option debug_level2 = new Option(SPBatchParameters.debugLevel2Name, "debug_level2", false, "show level 2 debug info");
        debug_level2.setRequired(false);
        options.addOption(debug_level2);

        Option host = new Option(SPBatchParameters.debugLevel2Name, "host", true, "configuration hosts");
        host.setRequired(false);
        options.addOption(host);

        Option port = new Option(SPBatchParameters.debugLevel2Name, "port", true, "configuration port");
        port.setRequired(false);
        options.addOption(port);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
            return cmd;
        } catch (ParseException e) {
            System.out.println("Argument errors: " + e.getMessage());
            formatter.printHelp(LOG_MESSAGE, options);
            return null;
        }
    }

}
