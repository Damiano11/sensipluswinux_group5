package com.sensichips.xmlclientconfiguration;

import java.io.Serializable;

/**
 * Created by mario on 08/03/18.
 */

public class Person implements Serializable {

    public static final long serialVersionUID = 42L;

    private String nome;
    private String cognome;

    public Person(String nome, String cognome) {
        this.nome = nome;
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String toString(){
        return "Nome: " + nome + ", cognome: " + cognome;
    }

}
