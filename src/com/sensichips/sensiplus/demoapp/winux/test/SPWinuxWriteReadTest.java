package com.sensichips.sensiplus.demoapp.winux.test;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolListener;
import com.sensichips.sensiplus.level1.protocols.cc2541.SPProtocolCC2541_SPI;
import com.sensichips.sensiplus.level1.protocols.ft232rl.SPProtocolFT232RL_BUSPIRATE;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.decoder.SPDecoderGenericInstruction;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.util.batch.SPBatchExperiment;
import jssc.SerialPortList;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class SPWinuxWriteReadTest implements SPProtocolListener, Runnable {

    public static String LOG_MESSAGE = "SPWinuxRegisterDump";

    private int DEBUG_LEVEL = 2;

    private static String SENSIPLUS_INIT = "SENSIPLUS_Init_RUN4.txt";

    private NumberFormat formatter = new DecimalFormat("#0.00");

    private String outDir = ".\\";
    private String COMPort;
    private String initFileDir = ".";

    // High level and low level
    private SPMeasurement dhl;
    private SPProtocol dll;
    private SPDecoder decoder;


    private static SPWinuxWriteReadTest frontEndLevel;

    public static void main(String[] args) throws Exception {

        System.out.println("COM PORT AVAILABLE:");
        String[] portNames = SerialPortList.getPortNames();
        if (portNames.length > 0){
            for(int i = 0; i < portNames.length; i++){
                System.out.println("\t" + (i + 1) + ". " + portNames[i]);
            }
        } else {
            System.out.println("\n\tANY DEVICE FOUND, THE PROGRAM WILL EXIT!!");
            System.exit(-1);
        }
        System.out.println("------------------------------------");

        frontEndLevel = new SPWinuxWriteReadTest();

        if (args.length == 0){
            frontEndLevel.initFileDir = "./";
            frontEndLevel.COMPort = "/dev/ttyUSB1";
            frontEndLevel.outDir = ".";
        } else if (args.length == 2) {
            frontEndLevel.initFileDir = args[0];
            frontEndLevel.COMPort = args[1];
            frontEndLevel.outDir = ".";
        } else {
            System.exit(-1);
        }

        System.out.println("PARAMETERS:");
        System.out.println("initFileDir:\t" + frontEndLevel.initFileDir);
        System.out.println("outDir:\t\t\t" + frontEndLevel.outDir);
        System.out.println("COMPort:\t\t" + frontEndLevel.COMPort);
        System.out.println("-------------------------------------");


        // Initialize decoder from init file (this file define also the protocol (SPI, I2C, etc.)
        frontEndLevel.decoder = null; //SPInitialization.decoderFactory(new FileInputStream(new File(frontEndLevel.initFileDir + layout_configuration)));


        String controllerName = null;
        SPDriver spDriver = null;


        if ("COM_USB".equals("COM_USB")) {
            spDriver = null; //new SPDriverWINUX_COMUSB(frontEndLevel.COMPort, SerialPort.BAUDRATE_115200);
            controllerName = SPProtocolFT232RL_BUSPIRATE.MCU;


        } else if ("COM_USB".equals("COM_BLE")){
            spDriver = null; //new SPDriverWINUX_COMBLE(frontEndLevel.COMPort);
            controllerName = SPProtocolCC2541_SPI.MCU;

        } else {
            throw new SPException("Unknown COM_MODE: " + "COM_USB" + ". Valid values: (COM_USB/COM_BLE");

        }
        // Create low level channel (on serial port)
        frontEndLevel.dll = null; // new SPProtocolDelegated(spDriver, frontEndLevel, SPConfigurationManager.getCurrentConfiguration(), SPProtocol.FULL_ADDRESS);
        //SPBatchParameters.protocol.setSPProtocol(controllerName, "");// + SPConfiguration.getInterfaceType());


    }

    @Override
    public void connectionUp(SPProtocol protocol) {

        /*
        try{
            SPCluster cluster = null;//SPInitialization.getSPChipCluster("SENSIBUS", "RUN4");

            SPBatchParameters.spProtocol = new SPMnemonic(protocol, SPBatchParameters.decoder);
            frontEndLevel.dhl = null; // = SPMeasurement.apiFactory(SPBatchParameters.spProtocol, cluster, outDir);  // default decoder
        } catch (SPException e){
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during connectionUp(): " + e, DEBUG_LEVEL);
            System.exit(-1);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Connection up, ready to start measurement...", DEBUG_LEVEL);
        new Thread(this).start();
        */
    }

    @Override
    public void message(String s) {

    }

    @Override
    public void connectionDown() {

    }

    @Override
    public void interrupt(byte b) {

    }

    @Override
    public void fatalError(String s) {

    }


    private boolean generaNuovoFile = true;
    private SPBatchExperiment SPBatchExperiment;

    @Override
    public void run(){

        ArrayList<String> comando = new ArrayList<String>();
        String rispostaAttesa = null;
        ArrayList<String[]> fase    = new ArrayList<String[]>();
        ArrayList<String[]> quad          = new ArrayList<String[]>();
        try {
            SPCluster cluster = null; //SPInitialization.getSPChipCluster("SENSIBUS", "RUN4");

            comando.add("WRITE DIG_CONFIG M 0X0904");              // 0x1A 0x04 0x09
            //comando.add("READ  DIG_CONFIG M 2");

            comando.add("WRITE INT_CONFIG S 0x01");                // 0x08 0x01
            //comando.add("READ  INT_CONFIG S 1");

            comando.add("WRITE ANA_CONFIG+1 S 0x04");              // 0x14 0x04
            //comando.add("READ  ANA_CONFIG+1 S 1");

            // SET EIS
            comando.add("WRITE CHA_SELECT S 0x5c");                // 0x48 0x5C
            //comando.add("READ  CHA_SELECT S 1");

            comando.add("WRITE DSS_SELECT M 0x073c");              // 0x92 0x3C 0x07
            //comando.add("READ  DSS_SELECT M 2");

            comando.add("WRITE DSS_DIVIDER M 0x0002");             // 0x72 0x02 0x00
            //comando.add("READ  DSS_DIVIDER M 2");

            comando.add("WRITE DAS_CONFIG M 0x7000");              // 0x8A 0x00 0x70
            //comando.add("READ  DAS_CONFIG M 2");

            comando.add("WRITE CHA_FILTER M 0x01E1");              // 0x42 0xE1 0x01
            //comando.add("READ  CHA_FILTER M 2");

            comando.add("WRITE CHA_CONDIT M 0x0340");              // 0x3A 0x40 0x03
            //comando.add("READ  CHA_CONDIT M 2");

            comando.add("WRITE ANA_CONFIG S 0x3F");                // 0x10 0x3F
            //comando.add("READ  ANA_CONFIG S 1");


            // SET ADC
            comando.add("WRITE CHA_DIVIDER M 0Xfc62");             // 0x22 0x62 0xFC
            //comando.add("READ  CHA_DIVIDER M 2");

            comando.add("WRITE CHA_SELECT+1 S 0Xf0");              // 0x4C 0xF0
            //comando.add("READ  CHA_SELECT+1 S 1");


            comando.add("WRITE CHA_CONFIG S 0x21");                // 0x28 0x21
            //comando.add("READ  CHA_CONFIG S 1");

            // GET EIS
            //comando.add("WRITE CHA_FILTER S 0x21");                // 0x40 0x21
            //comando.add("READ  CHA_FILTER S 1");


            for(int i = 0; i < 10; i++){
                comando.add("WRITE CHA_FILTER S 0x21");
                comando.add("WRITE COMMAND S 0x20");
                comando.add("READ CHA_FIFOL M 2");
                //-- 0xC3 r r

                comando.add("WRITE CHA_FILTER S 0x29");
                comando.add("WRITE COMMAND S 0x20");
                comando.add("READ CHA_FIFOL M 2");
            }


            String response[];
            String bytes[];
            int contatore = 0;
            for (int i = 0; i < comando.size(); i++) {
                String command = comando.get(i);
                SPDecoderGenericInstruction instruction = new SPDecoderGenericInstruction(command);
                response = frontEndLevel.dhl.getSPProtocol().sendInstruction(command, null).recValues;
                //SPDelay.delay(SPDelay.DELAY_LOW);
                //response = frontEndLevel.dhl.getSPMnemonic().read();
                for(int k = 0; k < response.length; k++){
                    if (command.startsWith("READ CHA_FIFOL")){
                        response[k] = response[k].replace("]","").replace("[", "");
                        bytes = new String[2];
                        String tokens[] = response[k].split(" ");
                        bytes[0] = tokens[0].trim();
                        bytes[1] = tokens[1].trim();
                        System.out.println("Response: " + bytes[0] + ", " + bytes[1]);
                        if (contatore % 2 == 0){
                            fase.add(bytes);
                        } else {
                            quad.add(bytes);
                        }
                        contatore++;
                    }
                }

            }

            int faseInt, quadInt;
            for(int i = 0; i < fase.size(); i++){
                byte[] b = new byte[2];
                b[0] = (byte) Integer.parseInt(fase.get(i)[0].replace("0x", ""), 16);
                b[1] = (byte) Integer.parseInt(fase.get(i)[1].replace("0x", ""), 16);
                faseInt = toInt(b);
                b[0] = (byte) Integer.parseInt(quad.get(i)[0].replace("0x", ""), 16);
                b[1] = (byte) Integer.parseInt(quad.get(i)[1].replace("0x", ""), 16);
                quadInt = toInt(b);
                System.out.println("" + i + ". " + fase.get(i)[0] + " " + fase.get(i)[1] + " => " + faseInt + ", " + quad.get(i)[0] + " " + quad.get(i)[1] + " => " + quadInt);
            }

            // TODO: RIPRISTINARE
            //dll.closeAll();

        } catch (SPException e) {
            e.printStackTrace();
        }
    }

    public static int toInt(byte[] data){
        int maschera = 255;

        int x = ((int) data[1]) << 8;   // Sfhift to left with sign extension
        int y = data[0];
        y = y & maschera;
        return x | y;
    }


}
