package com.sensichips.sensiplus.demoapp.winux.test;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.util.batch.SPBatchParameters;

import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;


public class AvailableFrequencies {

    public Integer FRD;
    public Float OSM;
    public Integer DSS_DIVIDER;
    public Float availableFrequency;


    private NumberFormat formatter = new DecimalFormat("#0.000");

    public String toString(){
        return "" + (int)OSM.doubleValue() + ";" + DSS_DIVIDER + ";" +  FRD + ";" +  availableFrequency + ";";
    }


    private static float log(float x, float base)
    {
        return (float) Math.log(x) / (float) Math.log(base);
    }

    private final static int limits1 = 1000; // Divisione in tre aree: f < 1000, 1000 <= f <= 78125 e f > 78125
    private final static int limits2 = 78125;

    private final static float freqErrThresholdPerc = 5; // Percentuale di errore ammesso rispoetto alla frequenza richiesta
    private final static int DSS_MAX = 32767;


    private static int NUM_OF_DECIMAL = 2;
    private static float MYROUND = (float)Math.pow(10,NUM_OF_DECIMAL);

    /*
    Valori degli step per f > 78125
        104166:			OSM=110 FRD=2/3
        156250:  		OSM=110 FRD=1
        208333:			OSM=101 FRD=2/3
        312500:			OSM=101	FRD=1
        416666:			OSM=100	FRD=2/3
        625000:			OSM=100	FRD=1
        833333:			OSM=011 FRD=2/3
        1250000:		OSM=011 FRD=1
        1666666:		OSM=010 FRD=2/3
        2500000:		OSM=010 FRD=1
        3333333:		OSM=001 FRD=2/3
        5000000:		OSM=001 FRD=1
    */
    private static float[] F_FRA = {104166, 208333, 416666, 833333, 1666666, 3333333};
    private static float[] F_INT = {156250, 312500, 625000, 1250000, 2500000, 5000000};



    public static void availableFrequencies(SPConfiguration conf, Float frequencyVal, AvailableFrequencies frequencyCalc) throws SPException {



        if (frequencyVal < 0){
            throw new SPException("Negative frequency are not allowed: " + frequencyVal);
        }

        double SYS_CLOCK_FIELD = 10000000;
        if (conf != null){
            //SYS_CLOCK_FIELD = conf.getSYS_CLOCK();
        }

        if (frequencyVal == 0) {
            frequencyCalc.FRD = 0;
            frequencyCalc.OSM = (float) 0;
            frequencyCalc.DSS_DIVIDER = 10;     // 0x14 DSS_DIVIDER PER RUN 4; TODO: Verificare il valore per RUN5
            frequencyCalc.availableFrequency = (float) 0;

            // Required frequency > 0
        } else {
            if (frequencyVal < limits1){
                // 0  < frequencyVal  < 1000
                // Old manner
                float DSS_DIVIDER_int;
                float DSS_DIVIDER_fraz;
                float FREQUENCY_int;
                float FREQUENCY_fraz;
                float FRD_fraz = 1;

                frequencyCalc.OSM = (float) 7.0;

                DSS_DIVIDER_int = Math.round(SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * frequencyVal)));
                DSS_DIVIDER_int = DSS_DIVIDER_int > DSS_MAX ? DSS_MAX: DSS_DIVIDER_int;


                FREQUENCY_int = (float) (SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * DSS_DIVIDER_int)));

                DSS_DIVIDER_fraz = Math.round((SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * frequencyVal))) * 2 / 3);
                DSS_DIVIDER_fraz = DSS_DIVIDER_fraz > DSS_MAX ? DSS_MAX: DSS_DIVIDER_fraz;

                FREQUENCY_fraz = (float) (SYS_CLOCK_FIELD / ((Math.pow(2, frequencyCalc.OSM) * DSS_DIVIDER_fraz))) * 2 / 3;


                if (Math.abs(FREQUENCY_int - frequencyVal) < Math.abs(FREQUENCY_fraz - frequencyVal)) {
                    frequencyCalc.DSS_DIVIDER = (int) DSS_DIVIDER_int;
                    frequencyCalc.FRD = 0;
                    FRD_fraz = 1;
                } else {
                    frequencyCalc.DSS_DIVIDER = (int) DSS_DIVIDER_fraz;
                    frequencyCalc.FRD = 1;
                    FRD_fraz = (float) 2 / (float) 3;
                }

                frequencyCalc.availableFrequency = (float) (FRD_fraz * SYS_CLOCK_FIELD / ((float) frequencyCalc.DSS_DIVIDER * Math.pow(2, frequencyCalc.OSM)));


            } else if (frequencyVal <= limits2){
                // 1000  <= frequencyVal  <= 78125
                // LUT
                float OSM[] = {128,64,32,16,8,4};
                double Fa, Fa_2_3;
                boolean freqErrThreshold_INT = false;
                boolean freqErrThreshold_FRAZ = false;
                double DSS_Fa, DSS_Fa_2_3, distFa, distFa_2_3;
                double threshold;

                int i = 0;
                while(i < OSM.length && !freqErrThreshold_INT && !freqErrThreshold_FRAZ){

                    threshold = frequencyVal * (freqErrThresholdPerc/100);

                    DSS_Fa = Math.round(SYS_CLOCK_FIELD/(frequencyVal * OSM[i]));
                    DSS_Fa = DSS_Fa > DSS_MAX ? DSS_MAX: DSS_Fa;
                    Fa = SYS_CLOCK_FIELD/(DSS_Fa * OSM[i]);
                    distFa = Math.abs(frequencyVal - Fa);
                    if (distFa < threshold){
                        freqErrThreshold_INT = true;
                        frequencyCalc.OSM = log(OSM[i], 2);
                        frequencyCalc.DSS_DIVIDER = (int)DSS_Fa;
                        frequencyCalc.FRD = 0;
                        frequencyCalc.availableFrequency = (float) Fa;
                    }

                    DSS_Fa_2_3 = Math.round((SYS_CLOCK_FIELD * 2)/(3 * frequencyVal * OSM[i]));
                    DSS_Fa_2_3 = DSS_Fa_2_3 > DSS_MAX ? DSS_MAX: DSS_Fa_2_3;
                    Fa_2_3 = (SYS_CLOCK_FIELD * 2)/(3 * DSS_Fa_2_3 * OSM[i]);
                    distFa_2_3 = Math.abs(frequencyVal - Fa_2_3);
                    if (distFa_2_3 < distFa && distFa_2_3 < threshold){
                        freqErrThreshold_FRAZ = true;
                        frequencyCalc.OSM = log(OSM[i], 2);
                        frequencyCalc.DSS_DIVIDER = (int)DSS_Fa_2_3;
                        frequencyCalc.FRD = 1;
                        frequencyCalc.availableFrequency = (float) Fa_2_3;
                    }
                    i++;
                }


            } else {

                double dist, minDist;

                // Initialize min values
                frequencyCalc.OSM = (float)7.0;
                frequencyCalc.FRD = 1;
                frequencyCalc.DSS_DIVIDER = 1;
                frequencyCalc.availableFrequency = (float) limits2;
                minDist = Math.abs(frequencyVal - frequencyCalc.availableFrequency);

                int i = 0;
                float OSM = 6;
                while(i < F_FRA.length){
                    dist = Math.abs(frequencyVal - F_FRA[i]);
                    if (dist < minDist){
                        frequencyCalc.OSM = OSM;
                        frequencyCalc.DSS_DIVIDER = 1;
                        frequencyCalc.FRD = 1;
                        frequencyCalc.availableFrequency = F_FRA[i];
                        minDist = dist;
                    }
                    dist = Math.abs(frequencyVal - F_INT[i]);
                    if (dist < minDist){
                        frequencyCalc.OSM = OSM;
                        frequencyCalc.DSS_DIVIDER = 1;
                        frequencyCalc.FRD = 0;
                        frequencyCalc.availableFrequency = F_INT[i];
                        minDist = dist;
                    }
                    OSM--;
                    i++;
                }
            }

        }

        frequencyCalc.availableFrequency = (float) Math.round(frequencyCalc.availableFrequency * MYROUND)/MYROUND;

    }


    public static Integer evaluateADCConversionRate(float freqIn, int min, int max) {
        //boolean flag = false;
        int adcFreq;
        int freq = (int)freqIn;
        if (freq <= 0){
            return min;
        } else if (freq < min){
            int multiplier = (int)Math.ceil((double)min/freq);
            adcFreq = multiplier * freq;
        } else if (freq < max){
            List<Integer> factors = primeFactors(freq);
            adcFreq = minFactorization(factors, min);

        } else {
            adcFreq = min;
        }
        return adcFreq;
    }

    private static int minFactorization(List<Integer> factors, int min){
        //boolean flag = false;
        int p = 1;
        int n = factors.size();
        int v_i[] = new int[p];
        //int v_i_min[] = new int[p];
        int prodMin = Integer.MAX_VALUE;
        int prod = 1;
        boolean flagToReturn = false;
        for(p = 1; p <= n; p++){
            v_i = new int[p];
            //v_i_min = new int[p];
            for(int i = 0; i < p; i++){
                v_i[i] = i;
            }
            int k = p - 1;

            while (v_i[k] < n) {
                // produttoria
                prod = 1;

                for (int i = 0; i < v_i.length; i++) {
                    prod *= factors.get(v_i[i]);
                    //System.out.print(factors.get(v_i[i]) + ", ");
                }
                //System.out.println(prod);
                if (prod >= min){
                    flagToReturn = true;
                    if (prod < prodMin){
                        prodMin = prod;
                        //System.arraycopy(v_i, 0, v_i_min, 0, v_i.length);
                    }
                }
                if (flagToReturn){
                    return prodMin;
                }
                v_i[k]++;
            }

            v_i[k]--;
            k = p - 2;
            while (k >= 0 && v_i[k] >= (v_i[k + 1] - 1)) {
                k--;
            }
            if (k >= 0) {
                v_i[k]++;
                for (int i = k + 1; i < (p - 1); i++) {
                    v_i[i] = v_i[i - 1] + 1;
                }
            }
        }
        return prodMin;
    }


    private static List<Integer> primeFactors(int numbers) {
        int n = numbers;
        List<Integer> factors = new ArrayList<Integer>();
        for (int i = 2; i <= n / i; i++) {
            while (n % i == 0) {
                factors.add(i);
                n /= i;
            }
        }
        if (n > 1) {
            factors.add(n);
        }
        return factors;
    }


    public static void main(String args[]) throws FileNotFoundException, SPException {
        SPBatchParameters.decoder = null;//SPInitialization.decoderFactory(new FileInputStream(new File("/home/mario/Scrivania/Sensichips/Sensichip-SerialPort/SensiplusWinux/SENSIPLUS_Init_RUN4.txt")));

        AvailableFrequencies freq = new AvailableFrequencies();
        AvailableFrequencies lastFreq = null;


        try {

            PrintWriter out = new PrintWriter(new FileOutputStream(new File("test.csv")));
            out.println("Start;End;OSM;DSS_DIVIDER;FRD;Favailable;MaxError;MaxErrorPerc;Conversion_Rate;");
            System.out.println("Start;End;OSM;DSS_DIVIDER;FRD;Favailable;MaxError;MaxErrorPerc;Conversion_Rate;");
            int lastFreqRequested = 0;
            float maxError = 0;
            float Conversion_Rate;
            float FRD;
            float currentError = Float.MIN_VALUE;
            Integer cutOffMin = 50;
            Integer cutOffMax = 100;
            int maxFreq = 5000002;
            for(int i = 0; i < maxFreq; i++){
                AvailableFrequencies.availableFrequencies(null, (float)i, freq);
                currentError = Math.round(Math.abs(i - freq.availableFrequency)*100)/(float)100;

                if (i == (maxFreq - 1) || (lastFreq != null && freq.availableFrequency.floatValue() != lastFreq.availableFrequency.floatValue())){
                    out.print(("" + lastFreqRequested + ";" + (i - 1) + ";" + lastFreq + maxError + ";" + (maxError/lastFreqRequested) + ";").replace(".", ","));
                    System.out.print(("" + lastFreqRequested + ";" + (i - 1) + ";" + lastFreq + maxError + ";" + (maxError/lastFreqRequested) + ";").replace(".", ","));
                    //out.println(("" + (lastFreqRequested + 1) + ";" + i + ";" + freq + Math.abs(i - freq.availableFrequency) + ";").replace(".", ","));
                    lastFreqRequested = i;
                    maxError = 0;

                    // Evaluate the conversion rate
                    Conversion_Rate = AvailableFrequencies.evaluateADCConversionRate(lastFreqRequested, cutOffMin, cutOffMax);
                    out.println((Conversion_Rate + ";").replace(".", ","));
                    System.out.println((Conversion_Rate + ";").replace(".", ","));
                }

                lastFreq = new AvailableFrequencies();
                lastFreq.availableFrequency = freq.availableFrequency;
                lastFreq.DSS_DIVIDER = freq.DSS_DIVIDER;
                lastFreq.FRD = freq.FRD;
                lastFreq.OSM = freq.OSM;
                if (currentError > maxError){
                    maxError = currentError;
                }
            }
            out.close();

        } catch (SPException e) {
            e.printStackTrace();
        }
    }
}