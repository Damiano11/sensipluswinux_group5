package com.sensichips.sensiplus.demoapp.winux.test;

/**
 * Created by mario on 19/01/17.
 */
public class SPMeasurementAdapter {

    private int numOfChips = 1;
    private int buffer = 10;
    private int counter = 0;
    private double mean = 0;
    private double sum = 0;
    private double bias[];


    public SPMeasurementAdapter(int numOfChips, int buffer){
        this.numOfChips = numOfChips;
        this.buffer = buffer;
        bias = new double[numOfChips];
    }

    public double[] adaptMeasure(double[] measures){
        if (counter < buffer){
            counter++;
            for(int i = 0; i < measures.length; i++){
                sum += measures[i];
            }
            mean = sum/(counter * measures.length);
            for(int i = 0; i < measures.length; i++){
                bias[i] = measures[i] - mean;
            }
        }
        for(int i = 0; i < measures.length; i++){
            measures[i] = measures[i] - bias[i];
        }

        return measures;
    }


}
