package com.sensichips.sensiplus.demoapp.winux.test;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMBLE;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMUSB;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterActuator;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 * Created by mario on 09/06/17.
 */
public class SPTestConfigurationManager {

    public static void main(String args[]) throws Exception {
        SPConfigurationManager.loadConfigurationsFromFile(new FileInputStream(new File("layoutConfiguration.xml")));

        ArrayList<String> lista = SPConfigurationManager.getConfigurationNames();

        for(int i = 0; i < lista.size(); i++){
            //System.out.println(SPConfigurationManager.getSPConfiguration(lista.get(i)).toString());
            System.out.println(lista.get(i));
        }

        SPConfiguration configuration = SPConfigurationManager.getSPConfiguration("WINUX_COMUSB-CLUSTERID_0X10-FT232RL_SENSIBUS-ShortAddress-ALL_SENSORS(RUN5)");
        //configuration.startConfiguration(null);

        SPConfigurationManager.setDefaultSPConfiguration(configuration);


        SPConfiguration sp = SPConfigurationManager.getSPConfigurationDefault();

        SPCluster cluster = sp.getCluster();
        ArrayList<String> deactivationList = new ArrayList<>();
        deactivationList.add("0X0000004405");

        SPCluster newCluster = cluster.generateTempCluster(deactivationList);

        SPMeasurement spMeasurement = sp.getSPMeasurement("EIS");


        SPMeasurementParameterActuator actuator = new SPMeasurementParameterActuator(sp);
        actuator.setTemporaryCluster(newCluster);
        actuator.setPort(SPMeasurementParameterActuator.ACTUATOR_PORT_RDY);
        actuator.setMode(SPMeasurementParameterActuator.ON);
        spMeasurement.setACTUATOR(actuator);

        actuator.setPort(SPMeasurementParameterActuator.ACTUATOR_PORT_INT);
        actuator.setMode(SPMeasurementParameterActuator.ON);
        spMeasurement.setACTUATOR(actuator);


        System.exit(-1);

    }
}
