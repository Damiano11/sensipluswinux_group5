package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.datareader.SPDataReader;
import com.sensichips.sensiplus.datareader.SPDataReaderConf;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.*;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterPOT;
import com.sensichips.sensiplus.util.log.SPLogger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.swing.*;
import java.text.NumberFormat;
import java.util.function.UnaryOperator;
import java.util.prefs.Preferences;

public class InstrumentsPOTController implements TabControllerInterface, ConfigConnectionListener {

    public static int DEBUG_LEVEL = 0;
    private static String LOG_MESSAGE = "InstrumentsPOTController";
    ObservableList<String> PortList = FXCollections.observableArrayList("PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3","PORT_HP");
    ObservableList<String> TypeList = FXCollections.observableArrayList("LINEAR_SWEEP", "STAIRCASE", "SQUAREWAVE", "NORMAL_PULSE", "DIFFERENTIAL_PULSE", "POTENTIOMETRIC", "CURRENT");
    ObservableList<String> RSenceList = FXCollections.observableArrayList("50", "500", "5000", "50000");
    ObservableList<String> InGainList = FXCollections.observableArrayList("1", "12", "20", "40");
    ObservableList<String> ContactsList = FXCollections.observableArrayList("TWO", "THREE");

    UnaryOperator<TextFormatter.Change> filter = new UnaryOperator<TextFormatter.Change>() {
        @Override
        public TextFormatter.Change apply(TextFormatter.Change t) {
            if (t.isReplaced()) {
                if ((t.getText().equals("-"))){
                    t.setText("-");
                }
                else if ((t.getText().equals("."))) {
                    t.setText(".");
                }
                else if (t.getText().matches("[^0-9]")) {
                    t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));
                }
            }

            if (t.isAdded()) {
                if (t.getControlText().contains(".") && t.getText().equals(".")) {
                    t.setText("");
                } else if (t.getControlText().contains("-") && t.getText().equals("-")){
                    t.setText("");
                }
                else if ((t.getText().equals("-"))){
                    t.setText("-");
                }
                else if ((!t.getText().equals(".") && t.getText().matches("[^0-9]"))) {
                    t.setText("");
                }


            }
            return t;
        }
    };

    private Controller mainController;

    @FXML
    private ChoiceBox Port;

    @FXML
    private ChoiceBox Type;

    @FXML
    private ChoiceBox RSence;

    @FXML
    private ChoiceBox InGain;

    @FXML
    private ChoiceBox Contacts;

    @FXML
    private Slider InitialPotSlider;

    @FXML
    private Label InitialPot_labelMin;

    @FXML
    private Label InitialPot_labelMax;

    @FXML
    private Slider FinalPotSlider;

    @FXML
    private Slider StepSlider;

    @FXML
    private Slider AmplitudeSlider;

    @FXML
    private Slider PeriodSlider;

    @FXML
    private TextField InitialPotField;

    @FXML
    private TextField FinalPotField;

    @FXML
    private TextField StepField;

    @FXML
    private TextField AmplitudeField;

    @FXML
    private TextField PeriodField;

    @FXML
    private Button DefaultSettings;

    @FXML
    private Button SaveSettings;

    @FXML
    private Button LoadSettings;

    @FXML
    private Button Startmeasure;

    @FXML
    private Button Plot;

    @FXML
    private TextArea TextArea;

    private boolean alreadyInitialized = false;
    private SPMeasurementParameterPOT defaultParam;
    private float InitialPotPos = 0;

    public InstrumentsPOTController() {
    }

    public void setMainController(Controller mainController) {
        System.out.println("Main controller passed");
        this.mainController = mainController;
        this.mainController.addConfigConnectionListener(this);
    }

    @FXML
    private AnchorPane tabPOT;

    @Override
    public void initTab() {
        if (!alreadyInitialized) {

            //System.out.println("Try to initialize POT");
            try {
                SPConfigurationManager.getSPConfigurationDefault();
                setParameterToUI(createDefaultParam());
                alreadyInitialized = true;
            } catch (SPException e) {
                SPLogger.getLogInterface().d("InstrumentsPOTController", "Default configuration not available. Please open a connection", DEBUG_LEVEL);
            }
        } else {
            System.out.println("POT already initialized.");

        }
    }

    @FXML
    private void initialize() {
        Port.setItems(PortList);

        Type.setItems(TypeList);
        RSence.setItems(RSenceList);
        InGain.setItems(InGainList);
        Contacts.setItems(ContactsList);
        Plot.setDisable(true);

        // CONSTRAINS (CIRO)
        Type.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {

                InitialPotSlider.setDisable(false);
                InitialPotField.setDisable(false);
                FinalPotSlider.setDisable(false);
                FinalPotField.setDisable(false);
                StepSlider.setDisable(false);
                StepField.setDisable(false);
                AmplitudeSlider.setDisable(false);
                AmplitudeField.setDisable(false);
                PeriodSlider.setDisable(false);
                PeriodField.setDisable(false);


                if(Type.getValue()=="CURRENT" || Type.getValue()=="POTENTIOMETRIC") {
                    InitialPotSlider.setDisable(true);
                    InitialPotField.setDisable(true);
                    FinalPotSlider.setDisable(true);
                    FinalPotField.setDisable(true);
                    StepSlider.setDisable(true);
                    StepField.setDisable(true);
                    AmplitudeSlider.setDisable(true);
                    AmplitudeField.setDisable(true);
                    PeriodSlider.setDisable(true);
                    PeriodField.setDisable(true);

                }

                else if(Type.getValue()=="LINEAR_SWEEP"){
                    StepSlider.setDisable(true);
                    StepField.setDisable(true);
                    AmplitudeSlider.setDisable(true);
                    AmplitudeField.setDisable(true);
                    PeriodSlider.setDisable(true);
                    PeriodField.setDisable(true);

                }

                else if(Type.getValue()=="STAIRCASE"){
                    AmplitudeSlider.setDisable(true);
                    AmplitudeField.setDisable(true);

                }

                else if(Type.getValue()=="SQUAREWAVE"){
                    StepSlider.setValue(0);
                    StepSlider.setDisable(true);
                    StepField.setDisable(true);

                }


            }

        });

        InitialPotSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable,
                                Number oldValue, Number newValue) {

                if(InitialPotSlider.getValue()> FinalPotSlider.getValue()){
                    FinalPotSlider.setValue(InitialPotSlider.getValue());
               }
            }
        });

        FinalPotSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable,
                                Number oldValue, Number newValue) {
                if(FinalPotSlider.getValue()< InitialPotSlider.getValue()){
                    InitialPotSlider.setValue(FinalPotSlider.getValue());
                }

            }
        });

        InitialPotField.setTextFormatter(new TextFormatter<Object>(filter));
        InitialPotField.textProperty().bindBidirectional(InitialPotSlider.valueProperty(), NumberFormat.getIntegerInstance());


        FinalPotField.setTextFormatter(new TextFormatter<Object>(filter));
        FinalPotField.textProperty().bindBidirectional(FinalPotSlider.valueProperty(), NumberFormat.getIntegerInstance());

        StepField.setTextFormatter(new TextFormatter<Object>(filter));
        StepField.textProperty().bindBidirectional(StepSlider.valueProperty(), NumberFormat.getIntegerInstance());

        AmplitudeField.setTextFormatter(new TextFormatter<Object>(filter));
        AmplitudeField.textProperty().bindBidirectional(AmplitudeSlider.valueProperty(), NumberFormat.getIntegerInstance());

        PeriodField.setTextFormatter(new TextFormatter<Object>(filter));
        PeriodField.textProperty().bindBidirectional(PeriodSlider.valueProperty(), NumberFormat.getIntegerInstance());
    }

    private SPMeasurementParameterPOT createDefaultParam() throws SPException {

        try {
            defaultParam = new SPMeasurementParameterPOT();

            Preferences prefs = Preferences.userNodeForPackage(InstrumentsPOTController.class);
            String defaultValues = prefs.get("default values", null);
            if (defaultValues == null) {

                defaultParam = new SPMeasurementParameterPOT(SPConfigurationManager.getListSPConfiguration().get(0));

                defaultParam.setPort(SPMeasurementParameterPOT.PortLabels[0]);
                defaultParam.setType(SPMeasurementParameterPOT.typeLabels[1]);
                defaultParam.setRsense(SPMeasurementParameterPOT.rsenseLabels[2]);
                defaultParam.setInGain(SPMeasurementParameterPOT.ingainLabels[3]);
                defaultParam.setContacts(SPMeasurementParameterPOT.ContactsLabels[4]);

            } else {
                String parameter[] = defaultValues.split("/");

                defaultParam.setPort(parameter[0]);
                defaultParam.setType(parameter[1]);
                defaultParam.setRsense(parameter[2]);
                defaultParam.setInGain(parameter[3]);
                defaultParam.setContacts(parameter[4]);
                defaultParam.setInitialPotential(Integer.parseInt(parameter[5]));
                defaultParam.setFinalPotential(Integer.parseInt(parameter[6]));
                defaultParam.setStep(Integer.parseInt(parameter[7]));
                defaultParam.setPulseAmplitude(Integer.parseInt(parameter[8]));
                defaultParam.setPulsePeriod(Integer.parseInt(parameter[9]));

            }


        } catch (SPException e) {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during default loading!\n" + e.getMessage() + "\n", DEBUG_LEVEL);
            throw e;
        }

        return defaultParam;
    }

//    @FXML
//    private void buttonSetPOTClick (ActionEvent e){
//
//        if(Plot.getText().equals("Plot")){
//            Plot.setDisable(true);
//        }
//
//
//        ;
//
//
//    }

    @FXML
    private void buttonSetPOTClick (ActionEvent e){
        /*/
        if(Plot.getText().equals("Plot")){
            Plot.setDisable(true);
        }
        /*/
    }

    @FXML
    private void startMeasureClick(ActionEvent e){

        // VISUALIZZAZIONE TEXT AREA PARAMETRI
//        try {
//            TextArea.appendText(getParameterFromUI().getPort().toString());
//            TextArea.appendText(getParameterFromUI().PortLabels.toString());
//            TextArea.appendText(getParameterFromUI().getPort().toString());
//        } catch (SPException e1) {
//            e1.printStackTrace();
//        }
        if (Startmeasure.getText().equals("Start measure")) {

            Startmeasure.setText("Stop");
            Plot.setDisable(false);



        }else {

            Startmeasure.setText("Start measure");
            Plot.setDisable(true);

        }
    }

    @FXML
    private void plotButtonClick(ActionEvent e){
        if(Plot.getText().equals("Plot")){
            Plot.setDisable(true);
        }
    }

    @Override
    public void open(String info) {
        System.out.println("InstrumentsPOTController received opening connection info");
        tabPOT.setDisable(false);
    }

    @Override
    public void close(String info) {
        System.out.println("InstrumentsPOTController received closing connection info");
        tabPOT.setDisable(true);
    }

    @FXML
    private void LoadSettingsOnAction() throws SPException {

        setParameterToUI(createDefaultParam());
    }

    @FXML
    private void DefaultSettingsOnAction() throws SPException {

        Port.setValue(null);
        Type.setValue(null);
        RSence.setValue(null);
        InGain.setValue(null);
        Contacts.setValue(null);
        InitialPotSlider.setValue(0);
        FinalPotSlider.setValue(0);
        StepSlider.setValue(0);
        AmplitudeSlider.setValue(0);
        PeriodSlider.setValue(0);
        StepField.setText("0");
        AmplitudeField.setText("0");
        PeriodField.setText("0");

    }


    private void setParameterToUI(SPMeasurementParameterPOT spMeasurementParameterPOT) throws SPException {

        Port.setValue(SPMeasurementParameterPOT.PortLabels[UtilityGUI.searchVector(SPMeasurementParameterPOT.PortValues, spMeasurementParameterPOT.getPort())]);
        Type.setValue(SPMeasurementParameterPOT.typeLabels[UtilityGUI.searchVector(SPMeasurementParameterPOT.typeValues, spMeasurementParameterPOT.getTypeVoltammetry())]);
        RSence.setValue(SPMeasurementParameterPOT.rsenseLabels[UtilityGUI.searchVector(SPMeasurementParameterPOT.rsenseValues, spMeasurementParameterPOT.getRsense())]);
        InGain.setValue(SPMeasurementParameterPOT.ingainLabels[UtilityGUI.searchVector(SPMeasurementParameterPOT.ingainValues, spMeasurementParameterPOT.getInGain())]);
        Contacts.setValue(SPMeasurementParameterPOT.ContactsLabels[UtilityGUI.searchVector(SPMeasurementParameterPOT.ContactsValues, spMeasurementParameterPOT.getContacts())]);

        InitialPotSlider.setValue(spMeasurementParameterPOT.getInitialPotential());
        FinalPotSlider.setValue(spMeasurementParameterPOT.getFinalPotential());
        StepSlider.setValue(spMeasurementParameterPOT.getStep());
        AmplitudeSlider.setValue(spMeasurementParameterPOT.getPulseAmplitude());
        PeriodSlider.setValue(spMeasurementParameterPOT.getPulsePeriod());

    }

    @FXML
    public void setParametersChoice() {
        int dialogButton = JOptionPane.YES_NO_OPTION;
        JOptionPane.showConfirmDialog(null, "Are you sure to set this parameters?", "WARNING", dialogButton);
        if (dialogButton == JOptionPane.YES_OPTION) {
            String defaultParam = null;
            defaultParam =
                    Port.getValue().toString() + "/" +
                            Type.getValue().toString() + "/" +
                            RSence.getValue().toString() + "/" +
                            InGain.getValue().toString() + "/" +
                            Contacts.getValue().toString() + "/" +
                            Math.round(InitialPotSlider.getValue()) + "/" +
                            Math.round(FinalPotSlider.getValue()) + "/" +
                            Math.round(StepSlider.getValue()) + "/" +
                            Math.round(AmplitudeSlider.getValue()) + "/" +
                            Math.round(PeriodSlider.getValue()) + "/";

            Preferences prefs2 = Preferences.userNodeForPackage(InstrumentsPOTController.class);
            prefs2.put("default values", defaultParam);

        } else if (dialogButton == JOptionPane.NO_OPTION) {

        }
    }

    private SPMeasurementParameterPOT getParameterFromUI() throws SPException {


      SPMeasurementParameterPOT param = new SPMeasurementParameterPOT(SPConfigurationManager.getSPConfigurationDefault());

        param.setPort(Port.getValue().toString());
        param.setType(Type.getValue().toString());
        param.setRsense(RSence.getValue().toString());
        param.setInGain(InGain.getValue().toString());
        param.setContacts(Contacts.getValue().toString());
        param.setInitialPotential(Integer.parseInt(InitialPotField.getText().toString()));
        param.setFinalPotential(Integer.parseInt(FinalPotField.getText().toString()));
        param.setStep(Integer.parseInt(InitialPotField.getText().toString()));
        param.setPulseAmplitude(Integer.parseInt(AmplitudeField.getText().toString()));
        param.setPulsePeriod(Integer.parseInt(PeriodField.getText().toString()));

        return param;
   }


}




