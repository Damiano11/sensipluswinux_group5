package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMUSB;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.util.batch.SPBatchParameters;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import jssc.SerialPortList;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

public class ConfigCreatorController {

    public static final String STOP_ON_ERROR_BANDGAP_OPERATION = "Stop on error";
    public static final String DISABLE_CHIPS_BANDGAP_OPERATION = "Disable if problems";

    /*@FXML
    private TextField descriptionTextFieldID;
*/
    @FXML
    private Label clusterInfoID;

    @FXML
    private ComboBox property1ComboBoxID;

    @FXML
    private ComboBox property2ComboBoxID;

    @FXML
    private Label property1_LabelID;

    @FXML
    private Label property2_LabelID;


    @FXML
    private TextField vccTextFieldID;

    @FXML
    private ComboBox driverComboBoxID;

    @FXML
    private ComboBox clusterComboBoxID;

    @FXML
    private ComboBox mcuComboBoxID;

    @FXML
    private ComboBox protocolComboBoxID;

    @FXML
    private ComboBox addressingComboBoxID;

    @FXML
    private ComboBox hostControllerComboBoxID;

    @FXML
    private ComboBox apiOwnerComboBoxID;


    private Stage stage;

    @FXML
    private CheckBox ledBlinkCheckBoxID;

    @FXML
    private CheckBox multicastCheckBoxID;

    @FXML
    private CheckBox detectionCheckBoxID;

    @FXML
    private CheckBox detectionStopOnErrorCheckBoxID;

    @FXML
    private CheckBox restartBandgapCheckBoxID;

    @FXML
    private CheckBox restartBandgapStopOnErrorCheckBoxID;

    @FXML
    private ComboBox configurationSelectionComboBoxID;

    private boolean flagConfigurationSelected = false;

    @FXML
    private ComboBox comboBoxBandgapOperationID;


    //private boolean anyConfigurationChanges = false;

    public ConfigCreatorController() {

    }

    private SPConfigurationManager.SPDriverGenerator spDriverGenerator;

    public void setSPDriverGenerator(SPConfigurationManager.SPDriverGenerator spDriverGenerator){
        this.spDriverGenerator = spDriverGenerator;
    }


    private ConfigCreator configCreator;
    public void setConfigCreator(ConfigCreator configCreator){
        this.configCreator = configCreator;
        loadUIPreferences();
    }



    public ConfigCreator getConfigCreator(){
        return configCreator;
    }

    // Led is available only if multicast and chip detection has been selected



    @FXML
    public void initialize() {
        try {
            for (int i = 0; i < SPConfigurationManager.getListSPConfigurationSize(); i++) {
                configurationSelectionComboBoxID.getItems().add(SPConfigurationManager.getConfigurationNames().get(i));
            }

            if(SPConfigurationManager.getListSPConfigurationSize()>0){
                //configurationSelectionComboBoxID.getSelectionModel().select(0);
                //configCreator.setDefaultSPConfigurationName(SPConfigurationManager.getConfigurationNames().get(0));
            }
        }catch (SPException spE){
            //TODO: Exception configuration initialization
        }

        configurationSelectionComboBoxID.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            configCreator.setDefaultSPConfigurationName((String)newValue);
            flagConfigurationSelected = true;
            loadUIPreferences();
        });



        driverComboBoxID.getItems().addAll("WINUX_COMUSB", "WINUX_IP", "WINUX_COMBLE");
        driverComboBoxID.getSelectionModel().select(0);

        mcuComboBoxID.getItems().addAll("ESP8266","FT232RL", "STM32L0");
        mcuComboBoxID.getSelectionModel().select(0);

        protocolComboBoxID.getItems().addAll("SENSIBUS","SPI", "I2C");
        protocolComboBoxID.getSelectionModel().select(0);

        addressingComboBoxID.getItems().addAll("NoAddress", "ShortAddress", "FullAddress");
        addressingComboBoxID.getSelectionModel().select(0);

        hostControllerComboBoxID.getItems().addAll("PC");
        hostControllerComboBoxID.getSelectionModel().select(0);

        apiOwnerComboBoxID.getItems().addAll("PC","MCU");
        apiOwnerComboBoxID.getSelectionModel().select(0);

        List<SPCluster> clusterList = SPConfigurationManager.getSPClusterList();

        for(int i = 0; i < clusterList.size(); i++){
            clusterComboBoxID.getItems().addAll(clusterList.get(i).getClusterID());
        }
        clusterComboBoxID.getSelectionModel().select(0);
        //Added Listner to measureTechniqueBox selection change.
        clusterComboBoxID.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    String clusterID = (String) newValue;

                    if (SPConfigurationManager.getSPClusterList().size() > 0){

                        boolean found = false;
                        int i = 0;
                        while(i < SPConfigurationManager.getSPClusterList().size() && !found){
                            found = SPConfigurationManager.getSPClusterList().get(i).getClusterID().equalsIgnoreCase(clusterID);
                            if (!found){
                                i++;
                            }
                        }

                        SPCluster cluster = SPConfigurationManager.getSPClusterList().get(i);
                        clusterInfoID.setText("# " + cluster.getActiveSPChipList().size()  + "chips, Fam. " + cluster.getFamilyOfChips().getId());
                    }
                });



        driverSelected((String)mcuComboBoxID.getSelectionModel().getSelectedItem());
        //Added Listner to measureTechniqueBox selection change.
        driverComboBoxID.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    driverSelected((String)driverComboBoxID.getSelectionModel().getSelectedItem());
                });

        ledBlinkCheckBoxID.setSelected(true);
        multicastCheckBoxID.setSelected(true);
        detectionCheckBoxID.setSelected(true);
        detectionStopOnErrorCheckBoxID.setSelected(true);
        restartBandgapCheckBoxID.setSelected(true);

        //restartBandgapStopOnErrorCheckBoxID.setSelected(true);

        comboBoxBandgapOperationID.getItems().addAll(STOP_ON_ERROR_BANDGAP_OPERATION,DISABLE_CHIPS_BANDGAP_OPERATION);


        detectionCheckBoxID.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov,
                                Boolean old_val, Boolean new_val) {
                if (new_val){
                    System.out.println("detection selected");
                    detectionStopOnErrorCheckBoxID.setDisable(false);
                } else {
                    detectionStopOnErrorCheckBoxID.setDisable(true);
                    restartBandgapCheckBoxID.setSelected(false);
                    System.out.println("detection unselected");

                }
            }
        });

        /*restartBandgapCheckBoxID.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov,
                                Boolean old_val, Boolean new_val) {
                if (new_val){
                    System.out.println("restart selected");
                    restartBandgapStopOnErrorCheckBoxID.setDisable(false);
                    detectionCheckBoxID.setSelected(true);
                } else {
                    restartBandgapStopOnErrorCheckBoxID.setDisable(true);
                    System.out.println("restart unselected");

                }
            }
        });*/




    }

    private void driverSelected(String name){
        property1ComboBoxID.getItems().clear();
        property2ComboBoxID.getItems().clear();

        if (name.contains("IP")) {
            property1_LabelID.setText("Host");
            property2_LabelID.setText("Port");
            property1ComboBoxID.getItems().addAll("192.168.1.100");
            property2ComboBoxID.getItems().addAll("8999");
        } else {
            property1_LabelID.setText("Serial port");
            property2_LabelID.setText("Speed");
            String osName = System.getProperty("os.name");
            //System.out.println(osName);
            String OP_SYSTEM = osName.contains(SPDriverWINUX_COMUSB.NIX_EXPR) ? SPDriverWINUX_COMUSB.NIX : SPDriverWINUX_COMUSB.WINDOWS;

            String[] portNames = SerialPortList.getPortNames();
            for(int i = 0; i < portNames.length; i++){
                property1ComboBoxID.getItems().add(portNames[i]);
            }

            property2ComboBoxID.getItems().add("115200");

            if (((String)mcuComboBoxID.getSelectionModel().getSelectedItem()).contains("ESP8266")){
                property2ComboBoxID.getItems().add("460800");
            }
        }

        property1ComboBoxID.getSelectionModel().select(0);
        property2ComboBoxID.getSelectionModel().select(0);

    }

    @FXML
    public void okButtonAction() {
        if(!flagConfigurationSelected){
            Controller.alertShow("ERROR",new SPException("Please select a configuration before clicking OK"));
        }
        else{

            try {

                String oldConfigurationName = configCreator.getDefaultSPConfigurationName();

                SPConfiguration appoConfig = SPConfigurationManager.getSPConfiguration(oldConfigurationName);

                appoConfig.setDriverName((String) driverComboBoxID.getSelectionModel().getSelectedItem());
                appoConfig.getDriverParameters().set(0,(String)property1ComboBoxID.getSelectionModel().getSelectedItem());

                appoConfig.getDriverParameters().set(1,(String)property2ComboBoxID.getSelectionModel().getSelectedItem());

                appoConfig.setHostController((String) hostControllerComboBoxID.getSelectionModel().getSelectedItem());
                appoConfig.setApiOwner((String) apiOwnerComboBoxID.getSelectionModel().getSelectedItem());
                appoConfig.setMcu((String) mcuComboBoxID.getSelectionModel().getSelectedItem());
                appoConfig.setProtocol((String) protocolComboBoxID.getSelectionModel().getSelectedItem());


                appoConfig.setAddressingType((String) addressingComboBoxID.getSelectionModel().getSelectedItem());
                appoConfig.setVCC(vccTextFieldID.getText());



                int i=0;
                while(i<SPConfigurationManager.getSPClusterList().size() && !SPConfigurationManager.getSPClusterList().get(i).getClusterID().equals(clusterComboBoxID.getSelectionModel().getSelectedItem())){
                    i++;
                }
                if(i<SPConfigurationManager.getSPClusterList().size()){
                    appoConfig.setCluster(SPConfigurationManager.getSPClusterList().get(i));
                }

                //SPConfigurationManager.modifyConfigurationInFile(oldConfigurationName,configCreator.getSPConfiguration());



                configCreator.setDefaultSPConfigurationName(appoConfig.getConfigurationName());
                SPConfigurationManager.updateConfigurationInXMLwithJAXB(oldConfigurationName,appoConfig,
                        new File(SPBatchParameters.layoutConfigurationFileXML),new File(SPBatchParameters.layoutConfigurationFileXSD));


                configCreator.setLedBlink(ledBlinkCheckBoxID.isSelected());
                configCreator.setMulticast(multicastCheckBoxID.isSelected());
                configCreator.setDetection(detectionCheckBoxID.isSelected());
                configCreator.setDetectionStopOnError(detectionStopOnErrorCheckBoxID.isSelected());
                configCreator.setDetectionStopOnError(detectionStopOnErrorCheckBoxID.isSelected());
                configCreator.setRestartBandgap(restartBandgapCheckBoxID.isSelected());

                if(comboBoxBandgapOperationID.getSelectionModel().getSelectedItem().toString().equals(STOP_ON_ERROR_BANDGAP_OPERATION)){
                    configCreator.setRestartBandgapStopOnError(true);
                    configCreator.setDisableChipsBandgapProblem(false);
                }else{
                    configCreator.setDisableChipsBandgapProblem(true);
                    configCreator.setRestartBandgapStopOnError(false);
                }





                configCreator.savePreferences();


                stage.close();

            }catch (SPException spE){
                Controller.alertShow("Error in configuration parameters", spE);
                spE.printStackTrace();
            }



        }



    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }


    @FXML
    public void cancelButtonAction() {
        configCreator = null;
        stage.close();
    }

    private void loadUIPreferences(){
        try {
            ledBlinkCheckBoxID.setSelected(configCreator.isLedBlink());
            multicastCheckBoxID.setSelected(configCreator.isMulticast());
            detectionCheckBoxID.setSelected(configCreator.isDetection());
            detectionStopOnErrorCheckBoxID.setSelected(configCreator.isDetectionStopOnError());
            restartBandgapCheckBoxID.setSelected(configCreator.isRestartBandgap());
            //restartBandgapStopOnErrorCheckBoxID.setSelected(configCreator.isRestartBandgapStopOnError());

            if(configCreator.isDisableChipsBandgapProblem()) {
                comboBoxBandgapOperationID.getSelectionModel().select(DISABLE_CHIPS_BANDGAP_OPERATION);
            }
            else if(configCreator.isRestartBandgapStopOnError()){
                comboBoxBandgapOperationID.getSelectionModel().select(STOP_ON_ERROR_BANDGAP_OPERATION);
            }

            SPConfiguration currentConfiguration = SPConfigurationManager.getSPConfiguration(configCreator.getDefaultSPConfigurationName());
            driverComboBoxID.getSelectionModel().select(currentConfiguration.getDriverName());
            property1ComboBoxID.getSelectionModel().select(currentConfiguration.getDriverParameters().get(0));
            property2ComboBoxID.getSelectionModel().select(currentConfiguration.getDriverParameters().get(1));
            clusterComboBoxID.getSelectionModel().select(currentConfiguration.getCluster().getClusterID());
            mcuComboBoxID.getSelectionModel().select(currentConfiguration.getMcu());
            protocolComboBoxID.getSelectionModel().select(currentConfiguration.getProtocol());
            addressingComboBoxID.getSelectionModel().select(currentConfiguration.getAddressingType());
            vccTextFieldID.setText(Integer.toString(currentConfiguration.getVCC()));
            hostControllerComboBoxID.getSelectionModel().select(currentConfiguration.getHostController());
            apiOwnerComboBoxID.getSelectionModel().select(currentConfiguration.getApiOwner());
        }catch(SPException e) {
            //If this statements' block is reached the defaultConfigurationName hasn't been found in the available configurations
            //in SPConfigurationManager. In this case, if there are any configuration available the first one in the list is showed.
            //Otherwise, the stored values for each UI element are showed.
            try {
                if (SPConfigurationManager.getListSPConfigurationSize() > 0) {
                    //Initialization of all elements with the first configuration (default)
                    SPConfiguration configuration = SPConfigurationManager.getListSPConfiguration().get(0);
                    configCreator.setDefaultSPConfigurationName(SPConfigurationManager.getListSPConfiguration().get(0).getConfigurationName());

                    driverComboBoxID.getSelectionModel().select(configuration.getDriverName());
                    //property1ComboBoxID.getSelectionModel().select(configuration.getDriverParameters().get(0));
                    property2ComboBoxID.getSelectionModel().select(configuration.getDriverParameters().get(1));
                    clusterComboBoxID.getSelectionModel().select(configuration.getCluster().getClusterID());
                    mcuComboBoxID.getSelectionModel().select(configuration.getMcu());
                    protocolComboBoxID.getSelectionModel().select(configuration.getProtocol());
                    addressingComboBoxID.getSelectionModel().select(configuration.getAddressingType());
                    vccTextFieldID.setText(Integer.toString(configuration.getVCC()));
                    hostControllerComboBoxID.getSelectionModel().select(configuration.getHostController());
                    apiOwnerComboBoxID.getSelectionModel().select(configuration.getApiOwner());
                } else {

                    driverComboBoxID.getSelectionModel().selectFirst();
                    property1ComboBoxID.getSelectionModel().selectFirst();
                    property2ComboBoxID.getSelectionModel().selectFirst();
                    clusterComboBoxID.getSelectionModel().selectFirst();
                    mcuComboBoxID.getSelectionModel().selectFirst();
                    protocolComboBoxID.getSelectionModel().selectFirst();
                    addressingComboBoxID.getSelectionModel().selectFirst();
                    vccTextFieldID.setText("3300");//TODO: handle the VCC value
                    hostControllerComboBoxID.getSelectionModel().selectFirst();
                    apiOwnerComboBoxID.getSelectionModel().selectFirst();
                }
            }catch (SPException e1){
                Controller.alertShow("ERROR in loading configurations",e1);
            }



        }


     /*   try {
            if (configCreator.getSPConfiguration() == null) {
                if(SPConfigurationManager.getListSPConfigurationSize()>0){
                    //Initialization of all elements with the first configuration (default)
                    SPConfiguration configuration = SPConfigurationManager.getListSPConfiguration().get(0);
                    configCreator.setDefaultSPConfigurationName(SPConfigurationManager.getListSPConfiguration().get(0).getConfigurationName());

                    driverComboBoxID.getSelectionModel().select(configuration.getDriverName());
                    //property1ComboBoxID.getSelectionModel().select(configuration.getDriverParameters().get(0));
                    property2ComboBoxID.getSelectionModel().select(configuration.getDriverParameters().get(1));
                    clusterComboBoxID.getSelectionModel().select(configuration.getCluster().getClusterID());
                    mcuComboBoxID.getSelectionModel().select(configuration.getMcu());
                    protocolComboBoxID.getSelectionModel().select(configuration.getProtocol());
                    addressingComboBoxID.getSelectionModel().select(configuration.getAddressingType());
                    vccTextFieldID.setText(Integer.toString(configuration.getVCC()));
                    hostControllerComboBoxID.getSelectionModel().select(configuration.getHostController());
                    apiOwnerComboBoxID.getSelectionModel().select(configuration.getApiOwner());
                }
                else {

                    driverComboBoxID.getSelectionModel().selectFirst();
                    property1ComboBoxID.getSelectionModel().selectFirst();
                    property2ComboBoxID.getSelectionModel().selectFirst();
                    clusterComboBoxID.getSelectionModel().selectFirst();
                    mcuComboBoxID.getSelectionModel().selectFirst();
                    protocolComboBoxID.getSelectionModel().selectFirst();
                    addressingComboBoxID.getSelectionModel().selectFirst();
                    vccTextFieldID.setText("3300");//TODO: handle the VCC value
                    hostControllerComboBoxID.getSelectionModel().selectFirst();
                    apiOwnerComboBoxID.getSelectionModel().selectFirst();
                }
            }
            else{
                driverComboBoxID.getSelectionModel().select(configCreator.getSPConfiguration().getDriverName());
                //property1ComboBoxID.getSelectionModel().select(configCreator.getSPConfiguration().getDriverParameters().get(0));
                property2ComboBoxID.getSelectionModel().select(configCreator.getSPConfiguration().getDriverParameters().get(1));
                clusterComboBoxID.getSelectionModel().select(configCreator.getSPConfiguration().getCluster().getClusterID());
                mcuComboBoxID.getSelectionModel().select(configCreator.getSPConfiguration().getMcu());
                protocolComboBoxID.getSelectionModel().select(configCreator.getSPConfiguration().getProtocol());
                addressingComboBoxID.getSelectionModel().select(configCreator.getSPConfiguration().getAddressingType());
                vccTextFieldID.setText(Integer.toString(configCreator.getSPConfiguration().getVCC()));
                hostControllerComboBoxID.getSelectionModel().select(configCreator.getSPConfiguration().getHostController());
                apiOwnerComboBoxID.getSelectionModel().select(configCreator.getSPConfiguration().getApiOwner());
            }

        }catch (SPException spE){
            //TODO: handle the exception
        }

        ledBlinkCheckBoxID.setSelected(configCreator.isLedBlink());
        multicastCheckBoxID.setSelected(true); //TODO: read from configCreator
        detectionCheckBoxID.setSelected(configCreator.isDetection());
        detectionStopOnErrorCheckBoxID.setSelected(configCreator.isDetectionStopOnError());
        restartBandgapCheckBoxID.setSelected(configCreator.isRestartBandgap());
        restartBandgapStopOnErrorCheckBoxID.setSelected(configCreator.isRestartBandgapStopOnError());
        */

    }




}
