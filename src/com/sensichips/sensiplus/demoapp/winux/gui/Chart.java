package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.datareader.queue.SPChartElement;
import com.sensichips.sensiplus.datareader.queue.SPChartQueue;
import com.sensichips.sensiplus.datareader.queue.SPMainQueue;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.StatisticsMeasurements;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.StatisticsTable;
import com.sensichips.sensiplus.util.SPDelay;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.sensichips.sensiplus.config.SPConfigurationManager.getSPConfigurationDefault;

/**
 * Created by mario on 19/02/17.
 */
public class Chart extends LineChart<Number, Number> {

    private ChartSettings chartSettings;
    private ArrayList<Series<Number,Number>> grandezza;
    private NumberAxis xAxis;

    public NumberAxis getyAxis() {
        return yAxis;
    }

    private NumberAxis yAxis;

    private int typeOfMeasure = 0;

    private double time = 0;

    private double xStart, yStart;

    private double SCALE_DELTA = 1.1;

    private boolean firstValue = true;

    private double lowerTime;
    private double upperTime;
    private double mean[];
    private double variance[];
    private double std;
    private double meanValue;
    private double varianceValue;
    private double minValue;
    private double maxValue;

    private int numOfChips;
    private SPChartElement r;

    private SPChartQueue source;

    private int indexToSave;

    //private SPBatchExperiment spBatchExperiment;

    private boolean[] chipMask;

    private SPMainQueue mq;


    private StatisticsTable statisticsTable;

    /**
     * Construct a new LineChart with the given axis.
     *
     * @param xAxis  The x axis to use
     * @param yAxis The y axis to use
     */
    private Chart(SPMainQueue mq, Axis<Number> xAxis, Axis<Number> yAxis, int plotDeep, int typeOfMeasure, ChartSettings chartSettings, boolean chipMask[], int indexToSave) {
        super(xAxis, yAxis);
        this.xAxis = (NumberAxis)xAxis;
        this.yAxis = (NumberAxis)yAxis;
        this.typeOfMeasure = typeOfMeasure;
        this.chartSettings = chartSettings;
        this.indexToSave = indexToSave;
        this.chipMask = chipMask;
        this.mq = mq;
        this.statisticsTable = null;

        source = new SPChartQueue(plotDeep, true, chipMask);
        mq.subscribe(source);

        init();
    }

    /**
     * Construct a new LineChart with the given axis.
     *
     * @param xAxis  The x axis to use
     * @param yAxis The y axis to use
     */
    private Chart(SPMainQueue mq, Axis<Number> xAxis, Axis<Number> yAxis, int plotDeep, int typeOfMeasure, ChartSettings chartSettings, boolean chipMask[], int indexToSave, StatisticsTable statisticsTable) {
        super(xAxis, yAxis);
        this.xAxis = (NumberAxis)xAxis;
        this.yAxis = (NumberAxis)yAxis;
        this.typeOfMeasure = typeOfMeasure;
        this.chartSettings = chartSettings;
        this.indexToSave = indexToSave;
        this.chipMask = chipMask;
        this.mq = mq;
        this.statisticsTable = statisticsTable;

        source = new SPChartQueue(plotDeep, true, chipMask);
        mq.subscribe(source);

        init();
    }




    public ChartSettings getChartSettings() {
        return chartSettings;
    }

    public void setChartSettings(ChartSettings chartSettings) {
        this.chartSettings = chartSettings;
    }

    public static Chart createNewChart(SPMainQueue mq, ChartSettings chartSettings, boolean chipMask[], int indexToSave){
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRanging(true);
        return new Chart(mq, xAxis,yAxis, chartSettings.plotDeep, chartSettings.typeOfMeasure, chartSettings, chipMask, indexToSave);
    }
    public static Chart createNewChart(SPMainQueue mq, ChartSettings chartSettings, boolean chipMask[], int indexToSave, StatisticsTable statisticsTable ){
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRanging(true);
        return new Chart(mq, xAxis,yAxis, chartSettings.plotDeep, chartSettings.typeOfMeasure, chartSettings, chipMask, indexToSave, statisticsTable );
    }
    private void updateGUI(){

        if (statisticsTable == null){
            return;
        }
        try {

            statisticsTable.clearTable();

            DecimalFormat f = new DecimalFormat("#.###");

            double localStats [][] = source.getStatistics(indexToSave);

            StatisticsMeasurements rows[] = new StatisticsMeasurements[numOfChips];
            for(int j = 0; j < numOfChips; j++) {
//                System.out.println("" + localStats[j][SPChartQueue.MIN_INDEX]);
//                System.out.println("" + localStats[j][SPChartQueue.MAX_INDEX]);
//                System.out.println("" + localStats[j][SPChartQueue.MEAN_INDEX]);
//                System.out.println("" + localStats[j][SPChartQueue.STD_INDEX]);
//                System.out.println("" + mq.getMax(j, indexToSave));
//                System.out.println("" + mq.getMin(j, indexToSave));


                rows[j] = new StatisticsMeasurements("" + getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().get(j),
                        "" + f.format(mq.getMax(j, indexToSave)),
                        "" + f.format(mq.getMin(j, indexToSave)),
                        "" + f.format(localStats[j][SPChartQueue.MAX_INDEX]),
                        "" + f.format(localStats[j][SPChartQueue.MIN_INDEX]),
                        "" + f.format(localStats[j][SPChartQueue.STD_INDEX]),
                        "" + f.format(localStats[j][SPChartQueue.MEAN_INDEX]));


            }
            statisticsTable.addStatistics(rows);
        } catch (SPException e) {
            e.printStackTrace();
        }



    }

    protected synchronized void plot(SPChartElement element){

        //double measures[][] = element.getSpDataRepresentation().values;
        //double measuresToPlot[] = new double[measures.length];
        //for (int j = 0; j < measures.length; j++) {
        //    measuresToPlot[j] = measures[j][indexToSave];
        //}

//        evaluateStats(element);

        upperTime = element.getxValue();

        if (firstValue){
            firstValue = false;
            lowerTime = element.getxValue();
        }

        //System.out.println("upperTime - lowerTime: " + (upperTime - lowerTime));
        if ((upperTime - lowerTime) >= (chartSettings.plotDeep - 1)) {
            for(int i = 0; i < grandezza.size(); i++){
                if (grandezza.get(i).getData().size() > 1){
                    grandezza.get(i).getData().remove(0,1);
                    lowerTime = grandezza.get(i).getData().get(0).getXValue().doubleValue();
                }
            }
        }

        for(int i = 0; i < chartSettings.numberOfChips; i++){
            grandezza.get(i).getData().add(new Data<Number,Number>(element.getxValue(), element.getSpDataRepresentation().values[i][indexToSave]));
        }

        //xAxis.setAutoRanging(true);
    }

    public void init(){
        setId("lineStockDemo");
        setCreateSymbols(false);
        setAnimated(false);
        setLegendVisible(false);
        setTitle(chartSettings.title);
        xAxis.setLabel("Time");
        xAxis.setForceZeroInRange(false);
        yAxis.setForceZeroInRange(false);

        yAxis.setLabel("[" + chartSettings.unitOfMeasure + "]");
        //yAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(yAxis,"[V]",null));
        // add starting data

        grandezza = new ArrayList<>();

        try {
            numOfChips = getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().size();
        } catch (SPException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < chartSettings.numberOfChips; i++){
            Series<Number,Number> series = new Series<>();
            grandezza.add(series);
            //grandezza.get(i).setName("To specify");

            getData().add(series);

            series.getNode().setOnMouseEntered(onMouseEnteredSeriesListener);
            series.getNode().setOnMouseExited(onMouseExitedSeriesListener);
        }

        new Thread(new Consumer()).start();

        setOnScroll(onMouseScrolled);
        setOnMousePressed(onMousePressed);
        setOnMouseDragged(onMouseDragged);
    }

    public void unsubscribe(){
        mq.unSubscribe(source);
    }

    class Consumer extends Task<Void> {
        private boolean first = true;
        private SPChartElement element;
        boolean toConsume = true;

        @Override
        public Void call(){


            try {
                String rcvd = "";
                toConsume = true;
                System.out.println("Consumer started ...");
                while (true){

                    element = source.get(indexToSave);
                    toConsume = true;



                    Platform.runLater(() -> {
                        if(first) {
                            first = false;
                            getyAxis().setLabel("[" + element.getSpDataRepresentation().prefix + getChartSettings().unitOfMeasure + "]");
                        }
                        plot(element);
                        updateGUI();
                        time++;

                        toConsume = false;
                    });
                    while(toConsume)
                        SPDelay.delay(1);
                }

            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }

            return null;
        }
    };



    EventHandler<MouseEvent> onMouseEnteredSeriesListener =
            (MouseEvent event) -> {
                ((Node)(event.getSource())).setCursor(Cursor.HAND);
            };

    //Lambda expression
    EventHandler<MouseEvent> onMouseExitedSeriesListener =
            (MouseEvent event) -> {
                ((Node)(event.getSource())).setCursor(Cursor.DEFAULT);
            };
    //Lambda expression
    EventHandler<MouseEvent> onMousePressed =
            (MouseEvent event) -> {
                if (event.getClickCount() == 2) {
                    setScaleX(1.0);
                    setScaleY(1.0);
                }
                System.out.println("Pressed ... (" + event.getX() + ", " + event.getY() + ")");
                xStart = event.getX();
                yStart = event.getY();
            };

    //Lambda expression
    EventHandler<ScrollEvent> onMouseScrolled =
            (ScrollEvent event) -> {
                event.consume();
                if (event.getDeltaY() == 0) {
                    return;
                }

                double scaleFactor = (event.getDeltaY() > 0) ? SCALE_DELTA : 1 / SCALE_DELTA;

                setScaleX(getScaleX() * scaleFactor);
                setScaleY(getScaleY() * scaleFactor);
            };

    EventHandler<MouseEvent> onMouseDragged =
            (MouseEvent event) -> {
                event.consume();

                System.out.println("Dragged: " + (xStart - event.getX()) + ", " + (yStart - event.getY()));
            };


    // aggiungere numero di chip selezionato
    // aggiungere valore iniziale max glob e min glob
    // aggiungere troncamento

}
