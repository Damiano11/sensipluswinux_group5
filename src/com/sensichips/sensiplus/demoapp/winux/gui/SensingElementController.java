package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.ConfigurationListWrapper;
import com.sensichips.sensiplus.model.SensingElement;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.DAOMySQLSettings;
import com.sensichips.sensiplus.model.dao.mysql.SensingElementDAOMySQLImpl;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Duration;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.prefs.Preferences;


public class SensingElementController {
    @FXML
    private TableView<SensingElement> sensingelementTableView;
    @FXML
    private TableColumn<SensingElement, String> idColumn;

    private Stage stage;

    @FXML
    private Label rSenseLabel;
    @FXML
    private Label inGainLabel;
    @FXML
    private Label outGainLabel;
    @FXML
    private Label contactsLabel;
    @FXML
    private Label frequencyLabel;
    @FXML
    private Label harmonicLabel;
    @FXML
    private Label dcBiasLabel;
    @FXML
    private Label modeVILabel;
    @FXML
    private Label measureTechniqueLabel, measureTechniqueDLabel;
    @FXML
    private Label measureTypeLabel;
    @FXML
    private Label filterLabel, filterDLabel;
    @FXML
    private Label phaseShiftModeLabel;
    @FXML
    private Label phaseShiftLabel;
    @FXML
    private Label iqLabel;
    @FXML
    private Label conversionRateLabel, conversionRateDLabel;
    @FXML
    private Label inPortADCLabel, inPortADCDLabel;
    @FXML
    private Label nDataLabel, nDataDLabel;
    @FXML
    private Label measureUnitLabel, measureUnitDLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private Label rangeMinLabel, rangeMinDLabel;
    @FXML
    private Label rangeMaxLabel, rangeMaxDLabel;
    @FXML
    private Label defaultAlarmLabel, defaultAlarmDLabel;
    @FXML
    private Label multiplierLabel, multiplierDLabel;
    @FXML
    private Label outPutLabel;

    @FXML
    private ScrollPane directPane;
    @FXML
    private ScrollPane defaultPane;

    @FXML
    private Button modificaButton;
    @FXML
    private Button cancellaButton;

    // Reference to the main application.
    private SensingElementController a;

    private Stage primaryStage;

    private ConfigurationController configurationController;

    private ObservableList<SensingElement> sensingElementData = FXCollections.observableArrayList();

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public ObservableList<SensingElement> getSensingElementData() {
        return sensingElementData;
    }


    private FadeTransition fadeIn = new FadeTransition(
            Duration.millis(1000)
    );

    private void outPutMessage(String request, int row) {
        outPutLabel.setVisible(false);
        if (row != -1) {
            outPutLabel.setText("SQL: " + request + " Row Affected: " + row + ".");
        } else {
            outPutLabel.setText("SQL: " + request);
        }
        outPutLabel.setVisible(true);
        fadeIn.playFromStart();
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        outPutLabel.setVisible(false);
        fadeIn.setNode(outPutLabel);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.setCycleCount(1);
        fadeIn.setAutoReverse(false);

        // Initialize the se table with the two columns.
        idColumn.setCellValueFactory(cellData -> cellData.getValue().idSPSensingElementProperty());
        modificaButton.setDisable(true);
        cancellaButton.setDisable(true);

        // Clear SE details.
        showSensingElementDetails(null);
        sensingelementTableView.setItems(this.getSensingElementData());
        sensingelementTableView.getItems().addListener((ListChangeListener.Change<? extends SensingElement> c) -> {
            if (c.getList().size() == 0) {
                modificaButton.setDisable(true);
                cancellaButton.setDisable(true);
            } else {
                modificaButton.setDisable(false);
                cancellaButton.setDisable(false);
            }
        });
        // Listen for selection changes and show the person details when changed.
        sensingelementTableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showSensingElementDetails(newValue));

        sensingelementTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2 &&
                        sensingelementTableView.getItems().size() != 0) {
                    modificaButtonAction();
                } else if (mouseEvent.getClickCount() == 2 && sensingelementTableView.getItems().size() == 0) {
                    nuovoButtonAction();
                }
            }
        });
    }

    private void showSensingElementDetails(SensingElement se) {
        if (se != null) {

            /*
              Fill the labels with info from the person object.
              (se.getparam() == Integer.MIN_VALUE ? "" : se.getparam().toString()) serve per stampare una stringa vuota al posto di Integer.MIN_VALUE
              per i parametri di tipo int mentre per i double si utilizza se.getParam().isNaN per vericare che il parametro non sia un Double.NaN,
              NB: NaN = Not a Number è definito nello standar IEEE 754, anche Double.NaN == Double.NaN restituisce false.
            */

            if (se.getMeasureTechnique().equals("DIRECT")) {
                defaultPane.setVisible(false);
                directPane.setVisible(true);
                rangeMaxDLabel.setText((se.getRangeMax().isNaN() ? "" : Double.toString(se.getRangeMax())));
                rangeMinDLabel.setText((se.getRangeMin().isNaN() ? "" : Double.toString(se.getRangeMin())));
                defaultAlarmDLabel.setText((se.getDefaultAlarmThreshold().isNaN() ? "" : Double.toString(se.getDefaultAlarmThreshold())));
                multiplierDLabel.setText((se.getMultiplier() == Integer.MIN_VALUE ? "" : se.getMultiplier().toString()));
                conversionRateDLabel.setText((se.getConversionRate() == Integer.MIN_VALUE ? "" : se.getConversionRate().toString()));
                inPortADCDLabel.setText(se.getInportADC());
                nDataDLabel.setText((se.getNData() == Integer.MIN_VALUE ? "" : se.getNData().toString()));
                measureUnitDLabel.setText(se.getMeasure_Unit());
                filterDLabel.setText((se.getFilter() == Integer.MIN_VALUE ? "" : se.getFilter().toString()));
                measureTechniqueDLabel.setText(se.getMeasureTechnique());

            } else {
                defaultPane.setVisible(true);
                directPane.setVisible(false);
                rSenseLabel.setText((se.getRSense() == Integer.MIN_VALUE ? "" : se.getRSense().toString()));
                inGainLabel.setText((se.getInGain() == Integer.MIN_VALUE ? "" : se.getInGain().toString()));
                outGainLabel.setText((se.getOutGain() == Integer.MIN_VALUE ? "" : se.getOutGain().toString()));
                contactsLabel.setText(se.getContacts());
                frequencyLabel.setText((se.getFrequency() == Integer.MIN_VALUE ? "" : se.getFrequency().toString()));
                harmonicLabel.setText(se.getHarmonic());
                dcBiasLabel.setText((se.getDcBias() == Integer.MIN_VALUE ? "" : se.getDcBias().toString()));
                modeVILabel.setText(se.getModeVI());
                measureTechniqueLabel.setText(se.getMeasureTechnique());
                measureTypeLabel.setText(se.getMeasureType());
                filterLabel.setText((se.getFilter() == Integer.MIN_VALUE ? "" : se.getFilter().toString()));
                phaseShiftModeLabel.setText(se.getPhaseShiftMode());
                phaseShiftLabel.setText((se.getPhaseShift() == Integer.MIN_VALUE ? "" : se.getPhaseShift().toString()));
                iqLabel.setText(se.getIq());
                conversionRateLabel.setText((se.getConversionRate() == Integer.MIN_VALUE ? "" : se.getConversionRate().toString()));
                inPortADCLabel.setText(se.getInportADC());
                nDataLabel.setText((se.getNData() == Integer.MIN_VALUE ? "" : se.getNData().toString()));
                measureUnitLabel.setText(se.getMeasure_Unit());
                nameLabel.setText(se.getName());
                rangeMaxLabel.setText((se.getRangeMax().isNaN() ? "" : Double.toString(se.getRangeMax())));
                rangeMinLabel.setText((se.getRangeMin().isNaN() ? "" : Double.toString(se.getRangeMin())));
                defaultAlarmLabel.setText((se.getDefaultAlarmThreshold().isNaN() ? "" : Double.toString(se.getDefaultAlarmThreshold())));
                multiplierLabel.setText((se.getMultiplier() == Integer.MIN_VALUE ? "" : se.getMultiplier().toString()));
            }

        } else {
            defaultPane.setVisible(true);
            directPane.setVisible(false);
            //SensingElement is null, remove all the text.
            rSenseLabel.setText("");
            inGainLabel.setText("");
            outGainLabel.setText("");
            contactsLabel.setText("");
            frequencyLabel.setText("");
            harmonicLabel.setText("");
            dcBiasLabel.setText("");
            modeVILabel.setText("");
            measureTechniqueLabel.setText("");
            measureTypeLabel.setText("");
            filterLabel.setText("");
            phaseShiftModeLabel.setText("");
            phaseShiftLabel.setText("");
            iqLabel.setText("");
            conversionRateLabel.setText("");
            inPortADCLabel.setText("");

            nDataLabel.setText("");
            measureUnitLabel.setText("");

            nameLabel.setText("");
            rangeMaxLabel.setText("");
            rangeMinLabel.setText("");
            defaultAlarmLabel.setText("");
            multiplierLabel.setText("");
        }
    }

    /**
     * Called when the user clicks on the cancella button.
     */
    @FXML
    private void cancellaButtonAction() {
//the function below returns the index of the selected item in the TableView
        int selectedIndex = sensingelementTableView.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            //sensingelementTableView.getItems().remove(selectedIndex);
            // definisco e implemento un elemento della classe sensingelement a cui ci passo l'emento selezionato nella tabella
            SensingElement sensingelement = sensingelementTableView.getItems().get(selectedIndex);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.initOwner(stage);
            alert.setTitle("Are you sure?");
            alert.setHeaderText("Delete " + sensingelement.getIdSPSensingElement() + "?");
            alert.setContentText("Are you sure that you want to delete the selected item?");

            ButtonType buttonTypeOne = new ButtonType("Yes");
            ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne) {
                try {
                    SensingElementDAOMySQLImpl.getInstance().delete(sensingelement);
                    sensingelementTableView.getItems().remove(sensingelement);
                    outPutMessage("Sensing Element correctly deleted.", 1);
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alertError = new Alert(Alert.AlertType.ERROR);
                    alertError.initOwner(this.getPrimaryStage());
                    alertError.setTitle("Error during DB interaction");
                    alertError.setHeaderText("Error during delete ...");
                    alertError.setContentText(e.getMessage());

                    alertError.showAndWait();
                }
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No SensingElement Selected");
            alert.setContentText("Please select a SensingElement in the table.");

            alert.showAndWait();
        }
    }

    /**
     * Called when the user clicks the nuovo button. Opens a dialog to edit
     * details for a new SensingElement.
     */

    @FXML
    private void nuovoButtonAction() {
        SensingElement tempSensingElement = new SensingElement("", Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, "", 78125, "", 0,
                "", "", "", 1, "", 0,
                "", 50, "", Integer.MIN_VALUE, "", "", 0.0, 100.0,
                50.0, Integer.MIN_VALUE);
        boolean okClicked = this.showSeEditDialog(stage, tempSensingElement, true);
        if (okClicked) {
            try {
                SensingElementDAOMySQLImpl.getInstance().insert(tempSensingElement);
                this.getSensingElementData().addAll(tempSensingElement);
                outPutMessage("Sensing Element correctly inserted.", 1);
            } catch (DAOException e) {
                outPutMessage("WARNING! Some Error occured.", -1);
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("Error during DB interaction");
                alert.setHeaderText("Error during insert ...");
                alert.setContentText(e.getMessage());

                alert.showAndWait();
            }
        }
    }

    /**
     * Called when the user clicks the cerca button. Opens a dialog to search
     * details into a SensingElement.
     */

    @FXML
    public void cercaButtonAction() {

        SensingElement tempSensingElement = new SensingElement();
        boolean okClicked = this.showSeSearchDialog(stage, tempSensingElement, false);
        if (okClicked) {
            try {
                List<SensingElement> list = SensingElementDAOMySQLImpl.getInstance().select(tempSensingElement);
                this.getSensingElementData().clear();
                this.getSensingElementData().addAll(list);
            } catch (DAOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                alert.setTitle("Error during DB interaction");
                alert.setHeaderText("Error during search ...");
                alert.setContentText(e.getMessage());

                alert.showAndWait();
            }
        }
    }

    /**
     * Called when the user clicks the modifica button. Opens a dialog to modify
     * details about SensingElement.
     */

    @FXML
    private void modificaButtonAction() {
        SensingElement selectedSensingElement = sensingelementTableView.getSelectionModel().getSelectedItem();
        if (selectedSensingElement != null) {
            selectedSensingElement.setoldidSPSensingElement();
            boolean okClicked = this.showSeEditDialog(stage, selectedSensingElement, true);
            if (okClicked) {
                try {
                    SensingElementDAOMySQLImpl.getInstance().update(selectedSensingElement);
                    showSensingElementDetails(selectedSensingElement);
                    outPutMessage("Sensing Element correctly updated.", 1);
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during update ...");
                    alert.setContentText(e.getMessage());

                    alert.showAndWait();
                }
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No SensingElement Selected");
            alert.setContentText("Please select a SensingElement in the table.");

            alert.showAndWait();
        }
    }

    private boolean showSeEditDialog(Stage owner, SensingElement se, boolean check) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("SensingElementEditDialog.fxml"));
            AnchorPane dialog = loader.load();

            Scene scene = new Scene(dialog);
            Stage seDialog = new Stage();
            seDialog.setTitle("Edit Sensing Element");
            seDialog.initModality(Modality.WINDOW_MODAL);
//            seDialog.initOwner(this);
            seDialog.setScene(scene);
            seDialog.getIcons().add(new Image(getClass().getResourceAsStream("sensichips_logo.png")));

            SensingElementEditController controller = loader.getController();
//            controller.setMain(this, check);
            controller.setStage(seDialog);
            controller.setSensingElement(se);

            seDialog.showAndWait();

            return controller.okClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean showSeSearchDialog(Stage owner, SensingElement se, boolean check) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("SensingElementSearchDialog.fxml"));
            AnchorPane dialog = loader.load();

            Scene scene = new Scene(dialog);
            Stage seDialog = new Stage();
            seDialog.setTitle("Search Sensing Element");
            seDialog.initModality(Modality.WINDOW_MODAL);
            seDialog.initOwner(owner);
            seDialog.setScene(scene);
            seDialog.getIcons().add(new Image(getClass().getResourceAsStream("sensichips_logo.png")));

            SensingElementSearchController controller = loader.getController();
//            controller.setMain(this, check);
            controller.setStage(seDialog);
            controller.setSensingElement(se);

            seDialog.showAndWait();

            return controller.okClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void cercaSensingElement(String seId) {
        try {
            List<SensingElement> list = SensingElementDAOMySQLImpl.getInstance().select(new SensingElement(seId));
            this.getSensingElementData().clear();
            this.getSensingElementData().addAll(list);
            sensingelementTableView.getSelectionModel().select(0);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

//    @FXML
//    private void settingsButtonAction() {
//        DAOMySQLSettings daoMySQLSettings = DAOMySQLSettings.getCurrentDAOMySQLSettings();
//        if (this.showSettingsEditDialog(daoMySQLSettings)){
//            DAOMySQLSettings.setCurrentDAOMySQLSettings(daoMySQLSettings);
//        }
//
//    }

    public boolean showSettingsEditDialog(DAOMySQLSettings daoMySQLSettings) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("SettingsEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("DAO settings");
            dialogStage.initModality((Modality.WINDOW_MODAL));
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the colleghi into the controller.
            SettingsEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setSettings(daoMySQLSettings);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Saves the file to the person file that is currently open. If there is no
     * open file, the "save as" dialog is shown.
     */
    @FXML
    private void handleSave() {
        File xmlFilePath = this.getXmlFilePath();
        if (xmlFilePath != null) {
            this.saveXmlDataToFile(xmlFilePath);
        } else {
            handleSaveAs();
        }
    }

    /**
     * Opens a FileChooser to let the user select a file to save to.
     */
    @FXML
    private void handleSaveAs() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File file = fileChooser.showSaveDialog(this.getPrimaryStage());

        if (file != null) {
            // Make sure it has the correct extension
            if (!file.getPath().endsWith(".xml")) {
                file = new File(file.getPath() + ".xml");
            }
            this.saveXmlDataToFile(file);
        }
    }

    public File getXmlFilePath() {
        Preferences prefs = Preferences.userNodeForPackage(SensingElementController.class);
        String filePath = prefs.get("filePath", null);
        if (filePath != null) {
            return new File(filePath);
        } else {
            return null;
        }
    }

    public void setXmlFilePath(File file) {
        Preferences prefs = Preferences.userNodeForPackage(SensingElementController.class);
        if (file != null) {
            prefs.put("filePath", file.getPath());

        } else {
            prefs.remove("filePath");
        }
    }

    /**
     * Saves the current person data to the specified file.
     *
     * @param file
     */
    public void saveXmlDataToFile(File file) {
        if(configurationController.getSelectedConfig() != null) {
            try {
                JAXBContext context = JAXBContext
                        .newInstance(ConfigurationListWrapper.class);
                Marshaller m = context.createMarshaller();
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


                ConfigurationListWrapper configWrapper = new ConfigurationListWrapper();
                configWrapper.setConfigurationElement(configurationController.getSelectedConfig());
                configWrapper.setClusterElement();
                configWrapper.setFamilyElement();
                configWrapper.setSensingElementList();

                // Marshalling and saving XML to the file.
                m.marshal(configWrapper, file);

                // Save the file path to the registry.
                setXmlFilePath(file);
            } catch (Exception e) {
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Could not save data");
                alert.setContentText("Could not save data to file:\n" + file.getPath());

                alert.showAndWait();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("There is no Configuration selected.");
            alert.setContentText("Please select a Configuration in order to save Xml.");

            alert.showAndWait();
        }
    }

}