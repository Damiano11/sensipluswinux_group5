package com.sensichips.sensiplus.demoapp.winux.gui;


import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.demoapp.winux.SPWinuxTools;
import com.sensichips.sensiplus.demoapp.winux.SPWinuxToolsSplash;
import com.sensichips.sensiplus.demoapp.winux.UDPServer;
import com.sensichips.sensiplus.demoapp.winux.XMLServer;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.ConfigConnectionListener;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMBLE;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMUSB;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_IP;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.protocols.esp8266.SPProtocolESP8266_SENSIBUS;
import com.sensichips.sensiplus.level1.protocols.stm32l0.SPProtocolSTM32_SENSIBUS;
import com.sensichips.sensiplus.level1.protocols.util.SPProtocolOut;
import com.sensichips.sensiplus.level1.chip.*;
import com.sensichips.sensiplus.level2.SPMeasurementRUN5_MCU;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterADC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterActuator;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.util.SPDataRepresentation;
import com.sensichips.sensiplus.util.batch.SPBatchExecutor;
import com.sensichips.sensiplus.util.batch.SPBatchExecutorObservator;
import com.sensichips.sensiplus.util.batch.SPBatchExperiment;
import com.sensichips.sensiplus.util.batch.SPBatchParameters;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import com.sensichips.sensiplus.util.winux.SPTempHumSHT30;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import jssc.SerialPortList;

import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import static com.sensichips.sensiplus.config.SPConfigurationManager.*;

/**
 * Created by mario on 25/03/16.
 */
public class Controller implements Initializable, SPDriverGenerator, SPBatchExecutorObservator, ConfigConnectionListener {


    private static String TAB_DEBUG = "Debug";
    private static String TAB_LOG = "Log";
    private static String TAB_CONFIG = "Config";
    private int log_text_area_buffer = 1000;
    private int measure_text_area_buffer = 1000;

    private static String EIS = "Impedance Spectroscopy";
    private static String POTENTIOSAT = "Potentiostat";

    private static String SENSING_CONFIG = "Sensing Element";
    private static String FAMILY_CONFIG = "Family";
    private static String CHIP_CONFIG = "Chip";
    private static String CLUSTER_CONFIG = "Cluster";
    private static String CONFI = "Configuration";


    @FXML
    private ImageView leftLogo;

    private static String LOG_MESSAGE = "SensichipsWinuxGUI";

    public static int DEBUG_LEVEL = 0;

    @FXML
    private Label currentConfigurationNameID;

    @FXML
    private CheckBox stopOnError;

    @FXML
    private Button modifyConfigurationButtonID;

    @FXML
    private ImageView imageViewExample;

    @FXML
    private Label availableChipsID;


    @FXML
    private CheckBox debugLevel0;

    @FXML
    private CheckBox debugLevel1;

    @FXML
    private CheckBox debugLevel2;

    @FXML
    private CheckBox cacheActivatedID;

    @FXML
    private CheckBox debugReadWrite;


    @FXML
    private ToggleButton identifyChipsID;

    private String tab;

    @FXML
    private Button clearReadWrite;
    @FXML
    private Button updateLogTextAreaID;
    @FXML
    private Button clearInit;
    @FXML
    private Button saveConfig;

    @FXML
    private TextArea readWriteTextArea;
    @FXML
    private TextArea configTextArea;
    @FXML
    private TextArea logTextArea;

    @FXML
    private VBox vboxLogTextArea;

    @FXML
    private TextArea chipListTextArea;


    //@FXML
    //private Button resetChipsID;


    @FXML
    private TextField delayBetweenInstructionsID;

    @FXML
    private ComboBox commandCombo;

    @FXML
    private Button sendButton;

    @FXML
    private Button testButton;

    @FXML
    private Button changeConfiguration;

    @FXML
    private Button installChipsButton;


    @FXML
    private CheckBox mnenomincOnOff;
    private boolean mnemonic = true;

    @FXML
    private Button debugLoadButton;

    @FXML
    private Button testDumpButton;

    @FXML
    private Button debugPlayButton;
    @FXML
    private Button debugStepButton;
    @FXML
    private Button debugRestartButton;
    @FXML
    private Button debugSaveButton;
    @FXML
    private Button debugInsertButton;
    @FXML
    private Button refreshDumpButton;

    @FXML
    private Button stopXMLBatchID;

    private int nextInstructionToExcecute;

    @FXML
    private TabPane tabPane;


    @FXML
    private TextField testFieldRepetition;

    @FXML
    private ComboBox idAddressingComboID;


    @FXML
    private ComboBox idSpeedComboID;

    //@FXML
    //private ComboBox protocolCombo;

    @FXML
    private ComboBox idComboSend;
    @FXML
    private ComboBox idComboRegDump;

    @FXML
    private ComboBox idComboBatch;

    @FXML
    private ComboBox idComboBatchXML;


    @FXML
    private Button saveChipIDList;

    @FXML
    private TableView<InstructionLine> batchTable;

    @FXML
    private TableView<SPRegisterDumpItem> registerTable;


    private ArrayList<String> listaMnemonic = new ArrayList<String>();
    private ArrayList<String> listaNotMnemonic = new ArrayList<String>();
    private ArrayList<String> configurationList = new ArrayList<String>();
    private ArrayList<String> currentList;

    private ArrayList<String> idComboSendList = new ArrayList<String>();
    private ArrayList<String> idComboRegDumpList = new ArrayList<String>();


    private Stage stage;

    private Stage stageList[];

    private Chart lineChart[];

    @FXML
    private Label numOfChipsID;

    @FXML
    private Label readForSecondID;

    @FXML
    private Label deltaTID;

    @FXML
    private Button loadXMLBatchID;
    @FXML
    private Button saveXMLBatchID;
    @FXML
    private Button clearBatchDumpID;
    @FXML
    private Button applyBufferBatchDumpID;
    @FXML
    private Button updateBufferLogTextAreaID;
    @FXML
    private TextField applyBufferSizeBatchDumpID;

    @FXML
    private TextField deepSizeGraphBatchID;

    @FXML
    private TextField applyBufferSizeLogID;

    @FXML
    private TextArea textAreaXMLBatchID;

    @FXML
    private String textAreaXMLBatchIDString;

    @FXML
    private Label progressLabelBatchXMLID;

    private TextArea textAreaBatchDumpID;


    @FXML
    private Button startXMLBatchID;
    @FXML
    private Button stepXMLBatchID;
    @FXML
    private Button startGraphXMLBatchID;

    private SPBatchParameters spBatchParameters;

    @FXML
    private VBox vBoxTextAreaID;

    private PrintWriter globalFile = null;

    @FXML
    private Button batchForDumpButtonID;

    private SPBatchExecutor spBatchExecutor;
    private SPBatchExperiment spBatchExperiment;

    @FXML
    private ToggleButton greenButtonID;

    private int numberOfSensors = 0;
    private int numOfEIS = 0;
    private List<Integer> numberOfConfigurationsList = new ArrayList<>();
    private int numberOfChips = 0;
    int numOfTotalConfiguration = 0;

    @FXML
    private TabPane tabInstruments;

    @FXML
    private TabPane tabConfigMan;

    @FXML
    private TabPane tabPaneMainGUI;


    @FXML
    private CheckBox startGasIdentificationID;

    @FXML
    private Label identifiedGasID;

    private ConfigCreator configCreator = null;

    private FXMLLoader fXMLLoader = new FXMLLoader();

    // Choice list fetched from server
    private String[] choiceList = {"Impedance Spectroscopy", "Potentiostat"};

    private String[] choiceList2 = {"Sensing Element", "Family", "Chip", "Cluster", "Configuration"};
//    private String[] choiceList = {"Potentiostat"};

    // OPTIONAL : Map for "choice name - choice fxml controller" pairs
    private Map<String, TabControllerInterface> choiceControllerMap = new HashMap<String, TabControllerInterface>();


    @FXML
    private Label sht30TempID;
    private String sht30Temp;

    @FXML
    private Label sht30HumID;
    private String sht30Hum;

    private SPTempHumSHT30 spTempHumSHT30;
    private InternalSensorReader internalSensorReader;

    @FXML
    private CheckBox sht30Id;

    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        // Buttons
        assert mnenomincOnOff != null : "fx:id=\"mnenomincOnOff\" was not injected: check your FXML file 'maingui.fxml'.";
        assert sendButton != null : "fx:id=\"mnenomincOnOff\" was not injected: check your FXML file 'maingui.fxml'.";
        assert debugLoadButton != null : "fx:id=\"debugLoadButton\" was not injected: check your FXML file 'maingui.fxml'.";
        assert debugPlayButton != null : "fx:id=\"debugPlayButton\" was not injected: check your FXML file 'maingui.fxml'.";
        assert debugStepButton != null : "fx:id=\"debugStepButton\" was not injected: check your FXML file 'maingui.fxml'.";
        assert changeConfiguration != null : "fx:id=\"changeConfiguration\" was not injected: check your FXML file 'maingui.fxml'.";

        assert clearReadWrite != null : "fx:id=\"clearReadWrite\" was not injected: check your FXML file 'maingui.fxml'.";
        assert updateLogTextAreaID != null : "fx:id=\"updateLogTextAreaID\" was not injected: check your FXML file 'maingui.fxml'.";
        assert clearInit != null : "fx:id=\"clearInit\" was not injected: check your FXML file 'maingui.fxml'.";;
        assert saveConfig != null : "fx:id=\"clearConfig\" was not injected: check your FXML file 'maingui.fxml'.";


        // Tabs
        assert tabPane != null : "fx:id=\"tabPane\" was not injected: check your FXML file 'maingui.fxml'.";
        assert readWriteTextArea != null : "fx:id=\"readWriteTextArea\" was not injected: check your FXML file 'maingui.fxml'.";
        assert logTextArea != null : "fx:id=\"logTextArea\" was not injected: check your FXML file 'maingui.fxml'.";
        assert configTextArea != null : "fx:id=\"configTextArea\" was not injected: check your FXML file 'maingui.fxml'.";


        // Combo
        assert commandCombo != null : "fx:id=\"commandCombo\" was not injected: check your FXML file 'maingui.fxml'.";
        //assert protocolCombo != null : "fx:id=\"protocolCombo\" was not injected: check your FXML file 'maingui.fxml'.";
        assert idComboSend != null : "fx:id=\"protocolCombo\" was not injected: check your FXML file 'maingui.fxml'.";
        // Label
        // assert configurationNameLabel != null : "fx:id=\"configurationNameLabel\" was not injected: check your FXML file 'maingui.fxml'.";

        spBatchParameters = SPWinuxToolsSplash.command.getBatchParameters();

        // Start internal XML server
        UDPServer udpServer = new UDPServer();
        udpServer.start();

        XMLServer xmlServer = new XMLServer(spBatchParameters);
        xmlServer.start();


        SPWinuxTools.APPLICATION_ICON = this.getClass().getResource("/com/sensichips/sensiplus/resources/ic_launcher.png").toExternalForm();
        SPWinuxTools.SPLASH_IMAGE =     this.getClass().getResource("/com/sensichips/sensiplus/resources/splash.png").toExternalForm();

        leftLogo.setImage(new Image(SPWinuxTools.APPLICATION_ICON) );

        //imageViewExample.setImage(new Image(this.getClass().getResource("/com/sensichips/sensiplus/resources/ic_launcher.png").toExternalForm()));

        applyBufferSizeLogID.setText("" + log_text_area_buffer);
        applyBufferSizeBatchDumpID.setText("" + measure_text_area_buffer);

        logTextArea = new TextArea() {
            @Override
            public void replaceText(int start, int end, String text) {
                super.replaceText(start, end, text);
                while(getText().split("\n", -1).length >= log_text_area_buffer) {
                    int fle = getText().indexOf("\n");
                    super.replaceText(0, fle+1, "");
                }
                positionCaret(getText().length());
            }
        };
        logTextArea.setPrefHeight(750);
        logTextArea.setEditable(false);
        vboxLogTextArea.getChildren().addAll(logTextArea);


        SPLogger.setLogInterface(new SPLoggerImplWinuxGUI(logTextArea));


        //String args[] = new String[]{"-c", "./", "-o", "./", "-f", "./eis_batch_RUN5_allPort.xml", "-d", "0", "-l", "0", "-L", "0"};



        SPConfiguration[] spConfigurationsList = null;
        try {
            Socket s = new Socket(spBatchParameters.host, spBatchParameters.port);

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(s.getOutputStream());
            objectOutputStream.writeObject(XMLServer.WINUX_REQUEST);
            objectOutputStream.flush();
            //objectOutputStream.close();

            ObjectInputStream objectInput = new ObjectInputStream(s.getInputStream());
            spConfigurationsList = (SPConfiguration[]) objectInput.readObject();
            objectInput.close();
            s.close();

            for(int i = 0; i < spConfigurationsList.length; i++){
                SPConfigurationManager.addSPConfiguration(spConfigurationsList[i]);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        if (spConfigurationsList == null){
            try {
                //loadConfigurationsFromFile(new FileInputStream(new File(spBatchParameters.layoutConfiguration)));
                SPConfigurationManager.loadConfigurationFromFileWithJAXB(new File(spBatchParameters.layoutConfigurationFileXML), new File(spBatchParameters.layoutConfigurationFileXSD));

            } catch (SPException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //FileInputStream fileInput = new FileInputStream(spConfigurationFile);

        //SPBatchCommandLineAnalyzer commandLineAnalyzer = new SPBatchCommandLineAnalyzer(args);
        //loadConfigurationsFromFile(new FileInputStream(new File(spBatchParameters.layoutConfiguration)));


        //spConfigurationsList = new SPConfiguration[SPConfigurationManager.getConfigurationNames().size()];
        //for(int i = 0; i < spConfigurationsList.length; i++){
        //    spConfigurationsList[i] = SPConfigurationManager.getSPConfiguration(SPConfigurationManager.getConfigurationNames().get(i));
        //}
        //FileOutputStream fileOutput = new FileOutputStream(spConfigurationFile);
        //ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
        //objectOutput.writeObject(spConfigurationsList);
        //objectOutput.close();


        ArrayList<String> lista = getConfigurationNames();

        for(int i = 0; i < lista.size(); i++){
            configurationList.add(lista.get(i));
        }

        SPRegisterList spRegisterList = new SPRegisterList();
        try {
            String[] listSuggestion = spRegisterList.getCommandSuggestions(SPFamily.RUN5);
            for(int i = 0; i < listSuggestion.length; i++){
                listaMnemonic.add(listSuggestion[i]);
            }

        } catch (SPException e) {
            e.printStackTrace();
        }

        //protocolCombo.getItems().addAll(configurationList);


        // Set value of addressing types
        idAddressingComboID.getItems().addAll(SPProtocol.ADDRESSING_TYPES);
        idAddressingComboID.getSelectionModel().select(0);




        idSpeedComboID.getItems().addAll(SPProtocol.SPEEDS);


        readWriteTextArea.setText("");
        logTextArea.setText("");
        configTextArea.setText("");

        currentList = listaMnemonic;
        commandCombo.getItems().addAll(currentList);

        new AutoCompleteComboBoxListener(commandCombo);



        tabPane.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Tab>() {
                    @Override
                    public void changed(ObservableValue<? extends Tab> ov, Tab start, Tab end) {
                        //System.out.println("Tab Selection changed: " + start.getText() + ", " + end.getText());
                        tab = end.getText();
                    }
                }
        );

        initialize();

        //initConfigTextArea();


        tab = TAB_DEBUG;

        TableColumn numInstruction = new TableColumn("N.");
        numInstruction.setMinWidth(40);
        numInstruction.setPrefWidth(40);
        numInstruction.setCellValueFactory(
                new PropertyValueFactory<InstructionLine, String>("numInstruction"));
        numInstruction.setSortable(false);

        TableColumn instruction = new TableColumn("Instruction");
        instruction.setMinWidth(200);
        instruction.setCellValueFactory(
                new PropertyValueFactory<InstructionLine, String>("instruction"));

        instruction.setSortable(false);
        instruction.setCellFactory(TextFieldTableCell.forTableColumn());
        instruction.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<InstructionLine, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<InstructionLine, String> t) {

                        int row = t.getTablePosition().getRow();

                        try {
                            String decodedInstruction = getSPConfigurationDefault().getSPProtocol().getSPDecoder().decodeSingleInstruction(t.getNewValue(), -1, getSPConfigurationDefault().getCluster().getMulticastChip()).getDecodedInstruction();
                            ((InstructionLine) t.getTableView().getItems().get(row)).setInstruction(t.getNewValue());
                            ((InstructionLine) t.getTableView().getItems().get(row)).setInstructionToSend(decodedInstruction);
                        } catch (SPException e) {
                            ((InstructionLine) t.getTableView().getItems().get(row)).setInstruction(t.getOldValue());
                            alertShow("Instruction error!! Cell not modified.", e);
                        }
                        t.getTableView().refresh();
                    }
                }
        );



        TableColumn comment = new TableColumn("Comment");
        comment.setMinWidth(300);
        comment.setCellValueFactory(
                new PropertyValueFactory<InstructionLine, String>("comment"));
        comment.setSortable(false);
        comment.setCellFactory(TextFieldTableCell.forTableColumn());
        comment.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<InstructionLine, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<InstructionLine, String> t) {
                        t.getTableView().getItems().get(t.getTablePosition().getRow()).setComment(t.getNewValue());
                    }
                }
        );


        TableColumn instructionToSend = new TableColumn("Decoded instruction");
        instructionToSend.setMinWidth(298);
        instructionToSend.setCellValueFactory(
                new PropertyValueFactory<InstructionLine, String>("instructionToSend"));
        instructionToSend.setSortable(false);

        batchTable.getColumns().addAll(numInstruction, instruction, comment, instructionToSend);
        batchTable.setEditable(true);


        //nextInstructionToExecuteLabel.setText("--");



        TableColumn registerName = new TableColumn("Register");
        registerName.setMinWidth(175);
        registerName.setCellValueFactory(
                new PropertyValueFactory<SPRegisterDumpItem, String>("register"));

        registerName.setSortable(false);
        registerName.setEditable(false);

        TableColumn msb = new TableColumn("MSB");
        msb.setMinWidth(80);
        msb.setCellValueFactory(
                new PropertyValueFactory<SPRegisterDumpItem, String>("msb"));
        msb.setSortable(false);
        msb.setCellFactory(TextFieldTableCell.forTableColumn());
        msb.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<SPRegisterDumpItem, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<SPRegisterDumpItem, String> t) {
                        int row = t.getTablePosition().getRow();

                        String instruction = "WRITE " + t.getTableView().getItems().get(row).getSpRegisterListItem().getName() + "+1 S " + t.getNewValue();
                        System.out.println("Instruction: " + instruction);

                        try {
                            SPProtocolOut out = getSPConfigurationDefault().getSPProtocol().sendInstruction(instruction, null);
                            t.getTableView().getItems().get(row).setMSB(t.getNewValue());
                        } catch (SPException e) {
                            t.getTableView().getItems().get(row).setMSB(t.getOldValue());
                            alertShow("Value error!! Cell not modified.", e);
                        }
                        t.getTableView().refresh();
                    }
                }
        );

        TableColumn lsb = new TableColumn("LSB");
        lsb.setMinWidth(80);
        lsb.setCellValueFactory(
                new PropertyValueFactory<SPRegisterDumpItem, String>("lsb"));
        lsb.setSortable(false);
        lsb.setCellFactory(TextFieldTableCell.forTableColumn());
        lsb.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<SPRegisterDumpItem, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<SPRegisterDumpItem, String> t) {
                        int row = t.getTablePosition().getRow();

                        String instruction = "WRITE " + t.getTableView().getItems().get(row).getSpRegisterListItem().getName() + " S " + t.getNewValue();
                        System.out.println("Instruction: " + instruction);

                        try {
                            SPProtocolOut out = getSPConfigurationDefault().getSPProtocol().sendInstruction(instruction,null);
                            t.getTableView().getItems().get(row).setLSB(t.getNewValue());
                        } catch (SPException e) {
                            t.getTableView().getItems().get(row).setLSB(t.getOldValue());
                            alertShow("Value error!! Cell not modified.", e);
                        }
                        t.getTableView().refresh();
                    }
                }
        );


        //String[] registerNames = SPRegisterDump.getRegisterNames();

        registerTable.getColumns().addAll(registerName, msb, lsb);
        registerTable.setEditable(true);






        textAreaBatchDumpID = new TextArea() {
            @Override
            public void replaceText(int start, int end, String text) {
                super.replaceText(start, end, text);
                while(getText().split("\n", -1).length >= measure_text_area_buffer) {
                    int fle = getText().indexOf("\n");
                    super.replaceText(0, fle+1, "");
                }
                positionCaret(getText().length());
            }
        };
        textAreaBatchDumpID.setPrefHeight(750);
        textAreaBatchDumpID.setEditable(false);


        vBoxTextAreaID.getChildren().addAll(textAreaBatchDumpID);

        debugLevelClick();

        commandCombo.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                sendInstruction();
            }
        });

        configCreator = new ConfigCreator(this);
        try {
            currentConfiguration = SPConfigurationManager.getSPConfiguration(configCreator.getDefaultSPConfigurationName());
            //System.out.println("New configuration: " + currentConfiguration);
            //SPConfigurationManager.addSPConfiguration(currentConfiguration);
            /*if(currentConfiguration==null){
                currentConfigurationNameID.setText("Select a configuration before starting");
            }
            else {*/

            currentConfigurationNameID.setText(currentConfiguration.getConfigurationName());

            //}
        } catch (SPException e) {
            currentConfigurationNameID.setText("Select a configuration before starting");
            //alertShow("Problems in configuration loading", e);
            //currentConfigurationNameID.setText("Configuration stored no more available in XML");
            //e.printStackTrace();
        }

        cacheActivatedID.selectedProperty().addListener(new ChangeListener<Boolean>() {
            public void changed(ObservableValue<? extends Boolean> ov,
                                Boolean old_val, Boolean new_val) {
                try {
                    SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().setCacheActivated(new_val);
                } catch (SPException e) {
                    alertShow("Failed to change cache status!", e);
                }
            }
        });

        updateOnConnectivityState();

        // TO ADD CODE ADD CODE BY CERVI MARCO


// Belows are in init method

// Add only tabs dynamically but not their content
        for (String choice : choiceList) {
            tabInstruments.getTabs().add(new Tab(choice));
        }

        for (String choice : choiceList2) {
            tabConfigMan.getTabs().add(new Tab(choice));
        }

// It is important to call it before adding ChangeListener to the tabPane to avoid NPE and
// to be able fire the manual selection event below. Otherwise the 1st tab will be selected
// with empty content.
        tabInstruments.getSelectionModel().clearSelection();

        tabConfigMan.getSelectionModel().clearSelection();






        // Add Tab ChangeListener
        tabInstruments.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                //System.out.println("Tab selected: " + newValue.getText());
                if (newValue.getText().equals(EIS)) {
                    if (newValue.getContent() == null) {
                        try {
                            // Loading content on demand
                            Parent root = (Parent) fXMLLoader.load(this.getClass().getResource("InstrumentsGUI.fxml").openStream());
                            newValue.setContent(root);

                            // OPTIONAL : Store the controller if needed
                            choiceControllerMap.put(newValue.getText(), fXMLLoader.getController());
                            ((InstrumentsGUIController) fXMLLoader.getController()).setMainController(Controller.this);

                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        // Content is already loaded. Update it if necessary.
                        Parent root = (Parent) newValue.getContent();
                        // Optionally get the controller from Map and manipulate the content
                        // via its controller.
                    } // if else
                }
                 if (newValue.getText().equals(POTENTIOSAT)) {
                     if (newValue.getContent() == null) {
                         try {
                             FXMLLoader loader = new FXMLLoader();
                             loader.setLocation(this.getClass().getResource("InstrumentsPOT.fxml"));
                             AnchorPane panel = loader.load();
                             newValue.setContent(panel);

                             // OPTIONAL : Store the controller if needed
                             choiceControllerMap.put(newValue.getText(), loader.getController());
                             ((InstrumentsPOTController) loader.getController()).setMainController(Controller.this);

                         } catch (IOException ex) {
                             ex.printStackTrace();
                         }
                     } else {
                         Parent root = (Parent) newValue.getContent();
                     }

                 }
            }
        });

        tabConfigMan.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                //System.out.println("Tab selected: " + newValue.getText());
                if (newValue.getText().equals(SENSING_CONFIG)) {
                    if (newValue.getContent() == null) {
                        try {
                            FXMLLoader loader = new FXMLLoader();
                            loader.setLocation(this.getClass().getResource("SensingElementView.fxml"));
                            AnchorPane panel = loader.load();
                            newValue.setContent(panel);

                            // OPTIONAL : Store the controller if needed
                            choiceControllerMap.put(newValue.getText(), loader.getController());
//                            ((InstrumentsPOTController) loader2.getController()).setMainController(Controller.this);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        // Content is already loaded. Update it if necessary.
                        Parent root = (Parent) newValue.getContent();
                        // Optionally get the controller from Map and manipulate the content
                        // via its controller.
                    } // if else
                }
                else if (newValue.getText().equals(FAMILY_CONFIG)) {
                    if (newValue.getContent() == null) {
                        try {
                            FXMLLoader loader = new FXMLLoader();
                            loader.setLocation(this.getClass().getResource("FamilyView.fxml"));
                            AnchorPane panel = loader.load();
                            newValue.setContent(panel);

                            // OPTIONAL : Store the controller if needed
                            choiceControllerMap.put(newValue.getText(), loader.getController());
//                            ((InstrumentsPOTController) loader2.getController()).setMainController(Controller.this);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        // Content is already loaded. Update it if necessary.
                        Parent root = (Parent) newValue.getContent();
                        // Optionally get the controller from Map and manipulate the content
                        // via its controller.
                    } // if else
                }
                else if (newValue.getText().equals(CHIP_CONFIG)) {
                    if (newValue.getContent() == null) {
                        try {
                            FXMLLoader loader = new FXMLLoader();
                            loader.setLocation(this.getClass().getResource("ChipView.fxml"));
                            AnchorPane panel = loader.load();
                            newValue.setContent(panel);

                            // OPTIONAL : Store the controller if needed
                            choiceControllerMap.put(newValue.getText(), loader.getController());
//                            ((InstrumentsPOTController) loader2.getController()).setMainController(Controller.this);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        // Content is already loaded. Update it if necessary.
                        Parent root = (Parent) newValue.getContent();
                        // Optionally get the controller from Map and manipulate the content
                        // via its controller.
                    } // if else
                }
                else if (newValue.getText().equals(CLUSTER_CONFIG)) {
                    if (newValue.getContent() == null) {
                        try {
                            FXMLLoader loader = new FXMLLoader();
                            loader.setLocation(this.getClass().getResource("ClusterView.fxml"));
                            AnchorPane panel = loader.load();
                            newValue.setContent(panel);

                            // OPTIONAL : Store the controller if needed
                            choiceControllerMap.put(newValue.getText(), loader.getController());
//                            ((InstrumentsPOTController) loader2.getController()).setMainController(Controller.this);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        // Content is already loaded. Update it if necessary.
                        Parent root = (Parent) newValue.getContent();
                        // Optionally get the controller from Map and manipulate the content
                        // via its controller.
                    } // if else
                }
                else if (newValue.getText().equals(CONFI)) {
                    if (newValue.getContent() == null) {
                        try {
                            FXMLLoader loader = new FXMLLoader();
                            loader.setLocation(this.getClass().getResource("ConfigurationView.fxml"));
                            AnchorPane panel = loader.load();
                            newValue.setContent(panel);

                            // OPTIONAL : Store the controller if needed
                            choiceControllerMap.put(newValue.getText(), loader.getController());
//                            ((InstrumentsPOTController) loader2.getController()).setMainController(Controller.this);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        // Content is already loaded. Update it if necessary.
                        Parent root = (Parent) newValue.getContent();
                        // Optionally get the controller from Map and manipulate the content
                        // via its controller.
                    } // if else
                }
            }


        });

        // Add Tab ChangeListener
        tabPaneMainGUI.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                //System.out.println("Tab from MainGUI selected: " + newValue.getText());
                if (newValue.getText().equals("Instruments")){
                    //System.out.println(choiceControllerMap);
                    //System.out.println(choiceControllerMap.get(EIS));
                    choiceControllerMap.get(EIS).initTab();

                }
//                if (newValue.getText().equals("Configuration Manager")){
//                    choiceControllerMap.get(CONFIGMAN).initTab();
//                }
            }
        });
        // By default, select 1st tab and load its content.
        //tabInstruments.getSelectionModel().selectFirst();

        addConfigConnectionListener(this);

        textAreaXMLBatchID.textProperty().addListener(new ChangeListener<String>(){
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                GUIStatus.BatchDirty = !newValue.equals(textAreaXMLBatchIDString);

                System.out.println("BatchDirty: " + GUIStatus.BatchDirty);
                updateOnConnectivityState();
            }
        });




        new InternalSensorReader().start();



    }

    @FXML
    private void sht30Action(ActionEvent e){
        if (sht30Id.isSelected()){
            spTempHumSHT30 = new SPTempHumSHT30();
            spTempHumSHT30.startMeasurement();
        } else {
            spTempHumSHT30.stopMeasurement();
        }
    }


    private Socket socket;

    @FXML
    private void startGasIdentificationClick(ActionEvent e){
        identifiedGasID.setText("NONE");
    }


    private SPConfiguration currentConfiguration = null;

    @FXML
    private void modifyConfigurationButtonClick(ActionEvent e){

        if (configCreator == null){
            configCreator = new ConfigCreator(this);
        }

        ConfigCreator newConfigCreator = SPWinuxToolsSplash.getMainGUI().showSPConfigurationCreator(configCreator);
        try {
            if (newConfigCreator != null){
                configCreator = newConfigCreator;
                currentConfiguration = SPConfigurationManager.getSPConfiguration(configCreator.getDefaultSPConfigurationName());
                System.out.println("New configuration: " + currentConfiguration);
                //SPConfigurationManager.addSPConfiguration(currentConfiguration);
                currentConfigurationNameID.setText(currentConfiguration.getConfigurationName());
            }
        } catch (SPException e1) {
            e1.printStackTrace();
        }

    }


    @FXML
    private void idSpeedComboClick(ActionEvent e){
        int speedSelected = (int) idSpeedComboID.getSelectionModel().getSelectedIndex();
        String out = "";
        try {
            SPCluster cluster = generateClusterFromComboSend(idComboSend);
            out = SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().setSpeed(SPProtocol.SPEEDS.get(speedSelected), cluster);
        } catch (SPException e1) {
            alertShow("Failed to change addressing type!", e1);
        }
        readWriteTextArea.appendText("New speed: " + SPProtocol.SPEEDS.get(speedSelected) + "\n");
        readWriteTextArea.appendText(out + "\n");
        readWriteTextArea.appendText("----------------------------\n");
    }


    @FXML
    private void idTestStartCommClick(ActionEvent e){
        try {
            String out = SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().testMCU();
            readWriteTextArea.appendText(out + "\n");
            readWriteTextArea.appendText("----------------------------\n");
        } catch (SPException e1) {
            alertShow("Failed to change addressing type!", e1);
        }
    }

    @FXML
    private void idAddressingComboClick(ActionEvent e){
        String addressingSelected = (String) idAddressingComboID.getSelectionModel().getSelectedItem();
        int addressingSelectedIndex = 0;
        try {
            int currentAddresingTypeIndex = SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().getAddressingMode();
            System.out.println("Current: " + SPProtocol.ADDRESSING_TYPES[currentAddresingTypeIndex]);
            System.out.println("Selected: " + addressingSelected);
            int chipsNum = SPConfigurationManager.getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().size();
            System.out.println("chipsNum: " + chipsNum);
            if (!addressingSelected.equalsIgnoreCase(SPProtocol.ADDRESSING_TYPES[currentAddresingTypeIndex])){
                if (addressingSelected.equals(SPProtocol.ADDRESSING_TYPES[SPProtocol.NO_ADDRESS])){
                    if (chipsNum > 1){
                        idAddressingComboID.getSelectionModel().select(SPProtocol.ADDRESSING_TYPES[currentAddresingTypeIndex]);
                        throw new SPProtocolException(SPProtocol.ADDRESSING_TYPES[currentAddresingTypeIndex] + " mode not allowed with # of chips > 1");
                    }
                    addressingSelectedIndex = SPProtocol.NO_ADDRESS;
                } else if (addressingSelected.equals(SPProtocol.ADDRESSING_TYPES[SPProtocol.SHORT_ADDRESS])){
                    if (chipsNum > 255){
                        idAddressingComboID.getSelectionModel().select(SPProtocol.ADDRESSING_TYPES[currentAddresingTypeIndex]);
                        throw new SPProtocolException(SPProtocol.ADDRESSING_TYPES[currentAddresingTypeIndex] + " mode not allowed with # of chips > 255");
                    }
                    addressingSelectedIndex = SPProtocol.SHORT_ADDRESS;
                } else {
                    addressingSelectedIndex = SPProtocol.FULL_ADDRESS;
                }
                System.out.println("Change to: " + addressingSelected);
                SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().setAddressingType(addressingSelectedIndex );
                readWriteTextArea.appendText("-----------------------------------------\n");
                readWriteTextArea.appendText("Previous addressing mode: " + SPProtocol.ADDRESSING_TYPES[currentAddresingTypeIndex] + "\n");
                readWriteTextArea.appendText("Current addressing mode: " + addressingSelected + "\n");
                readWriteTextArea.appendText("-----------------------------------------\n");
            }
        } catch (SPException e1) {
            alertShow("Failed to change addressing type!", e1);
            e1.printStackTrace();
        }
    }



    @FXML
    public void identifyChipsClick(ActionEvent e){

        if(identifyChipsID.isSelected()){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while(identifyChipsID.isSelected()){
                        System.out.println("Chip detection ... ");
                        try {
                            loadConfigurationsFromFile(new FileInputStream(new File(spBatchParameters.layoutConfigurationFileXML)));
                            if (currentConfiguration != null){
                                SPConfiguration spConfiguration = SPConfigurationManager.getSPConfiguration(currentConfiguration.getConfigurationName());
                                spConfiguration.startConfiguration(Controller.this,
                                        configCreator.isLedBlink(),
                                        configCreator.isMulticast(),
                                        configCreator.isDetection(),
                                        configCreator.isDetectionStopOnError(),
                                        configCreator.isRestartBandgap(),
                                        configCreator.isRestartBandgapStopOnError(),
                                        configCreator.isDisableChipsBandgapProblem());



                                setDefaultSPConfiguration(spConfiguration);
                                currentConfigurationNameID.setText(currentConfiguration.getConfigurationName());




                            }
                            //SPConfigurationManager.setDefaultSPConfiguration((String) protocolCombo.getSelectionModel().getSelectedItem());
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }

    }



    public boolean classifySample(){
        return startGasIdentificationID.isSelected();
    }


    /*
    @FXML
    public void resetChipsClick(ActionEvent e){
        try {
            getSPConfigurationDefault().setINIT(true);
        } catch (SPException e1) {
            e1.printStackTrace();
        }
    }*/


    @FXML
    private void stopXMLBatchClick(ActionEvent e){
        startGasIdentificationID.setSelected(false);
        spBatchExecutor.stop();
        spBatchExecutor.play();

        stopPlotThread = true;
        /*for(int i=0; i<chartQueues.length; i++){
            if(spBatchExperiment!=null){
                spBatchExperiment.getMainQueue()[i].unSubscribe(chartQueues[i]);
            }
        }*/

    }


    @FXML
    private void greenButtonClick(ActionEvent e){
        SPCluster cluster = null;
        try {
            cluster = generateClusterFromComboSend(idComboSend);
            if (greenButtonID.isSelected()){
                SPProtocolOut out = getSPConfigurationDefault().getSPProtocol().sendInstruction("WRITE INT_CONFIG+1 S 0x02", cluster);
            } else {
                SPProtocolOut out = getSPConfigurationDefault().getSPProtocol().sendInstruction("WRITE INT_CONFIG+1 S 0x00", cluster);
            }
        } catch (SPException e1) {
            greenButtonID.setSelected(false);
            alertShow("During green command", e1);
        }
    }


    @FXML
    private void startXMLBatchIDClick(ActionEvent e){
        try {
            if(startXMLBatchID.getText().equalsIgnoreCase("Start")) {
                textAreaBatchDumpID.appendText("Measurement is starting, please wait!");
                spBatchParameters.batchFile = batchFilePath;
                spBatchExecutor = new SPBatchExecutor(spBatchParameters, this, getSPConfigurationDefault());

                stopPlotThread = false;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            GUIStatus.BatchRunning = true;
                            GUIStatus.BatchPaused = false;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    updateOnConnectivityState();
                                }
                            });
                            spBatchExecutor.executeExperiments();
                        } catch (SPException e1) {
                            e1.printStackTrace();
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    alertShow("Problem during the Experiment",e1);
                                    GUIStatus.BatchRunning = false;
                                    stopPlotThread = true;
                                    GUIStatus.BatchPaused = false;
                                    updateOnConnectivityState();
                                }
                            });

                        }
                    }
                }).start();



            } else if (startXMLBatchID.getText().equalsIgnoreCase("Restart")) {
                // Riavvio da pausa
                GUIStatus.BatchRunning = true;
                GUIStatus.BatchPaused = false;
                updateOnConnectivityState();
                spBatchExecutor.play();
                startXMLBatchID.setText("Pause");
                //stopXMLBatchID.setDisable(false);
            } else {
                // Pausa da running
                GUIStatus.BatchRunning = false;
                GUIStatus.BatchPaused = true;
                updateOnConnectivityState();

                spBatchExecutor.pause();
                startXMLBatchID.setText("Restart");
            }

        } catch (SPException ex) {
            GUIStatus.BatchRunning = false;
            GUIStatus.BatchPaused = false;
            updateOnConnectivityState();
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during batch launching... " + ex.getMessage(), DEBUG_LEVEL);
            alertShow("Exception during batch launching", ex);

        }
    }

    //private NumberFormat formatter = new DecimalFormat("##0.000");

    private NumberFormat formatter = new DecimalFormat("#0.00000");
    private String[] unitMeasure;
    @Override
    public void measureStarted(SPBatchExperiment spBatchExperiment, String[] unitMeasure) {
        this.spBatchExperiment = spBatchExperiment;
        this.unitMeasure = unitMeasure;
        try {
            globalFile = new PrintWriter(spBatchParameters.outputDir + "Experiment_" + spBatchExperiment.getDate() + "_GLOBAL_" + spBatchExperiment.getId() + ".csv");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        numberOfSensors = spBatchExperiment.getSPSensingElementOnChipSize();
        numOfEIS = spBatchExperiment.getNumOfEISElement();
        numOfTotalConfiguration = spBatchExperiment.getTotalNumEISConfigurations();
        numberOfConfigurationsList = spBatchExperiment.getNumOfConfigurationsList();

        numberOfChips = spBatchExperiment.getSPConfiguration().getCluster().getActiveSPChipList().size();


        Platform.runLater(new Runnable() {
            public void run(){

                startXMLBatchID.setText("Pause");
                GUIStatus.BatchRunning = true;
                updateOnConnectivityState();

            }});



    }






    private boolean stopPlotThread = false;


    @Override
    public void newMeasure(String row, List<SPDataRepresentation> spDataRepresentationList,String className) {

        row = row.replace(",",".");

        int tokenCounter = 0;
        String[] tokens = row.split(";");
        float timeStamp = Float.parseFloat(tokens[tokenCounter++]);

        String output = "";
        output = "Timestamp: " + timeStamp + "\n";


        for(int h = 0; h<numOfEIS; h++) {
            for (int i = 0; i < numberOfConfigurationsList.get(h); i++) {
                for(int l=0; l<spBatchExperiment.eisIndexToPlotList.get(h).size();l++){

                    //output += "EIS: ";
                    int valueToPlotIndex = 0;
                    int configIndex = 0;
                    if(h==0) {
                        valueToPlotIndex = i*spBatchExperiment.eisIndexToPlotList.get(h).size()+l;
                        configIndex = i;
                        output += spBatchExecutor.eis_titles[i]+"_"
                                + SPMeasurementParameterEIS.measureLabels[spBatchExperiment.eisIndexToPlotList.get(h).get(l)]+ " ";
                    }
                    else {
                        for(int k=h-1; k>=0; k--){
                            valueToPlotIndex += numberOfConfigurationsList.get(k)*spBatchExperiment.eisIndexToPlotList.get(k).size();
                            configIndex +=numberOfConfigurationsList.get(k);
                        }
                        valueToPlotIndex += i*spBatchExperiment.eisIndexToPlotList.get(h).size()+l;
                        configIndex += i;
                        output += spBatchExecutor.eis_titles[configIndex] + " "
                                + SPMeasurementParameterEIS.measureLabels[spBatchExperiment.eisIndexToPlotList.get(h).get(l)]+ " ";
                    }

                    for (int j = 0; j < numberOfChips; j++) {
                        //output += formatter.format(values[i][j]) + " ";
                        if(valueToPlotIndex>=spDataRepresentationList.size()){
                            output+=" - ";
                        }
                        else {
                            output += spDataRepresentationList.get(valueToPlotIndex).getFormatter().format(
                                    spDataRepresentationList.get(valueToPlotIndex).values[j][spBatchExperiment.eisIndexToPlotList.get(h).get(l)]) + " "
                                    + spDataRepresentationList.get(valueToPlotIndex).prefix + spDataRepresentationList.get(valueToPlotIndex).measureUnit;
                        }
                /*output += SPDataRepresentation.formatWithFixedNumberOfSignificantDigits(
                        spDataRepresentationManagerList.get(i).getMultiplier() * values[i][j],
                        5).format(spDataRepresentationManagerList.get(i).getMultiplier() * values[i][j]) +
                        " "
                        +spDataRepresentationManagerList.get(i).getMultiplierPrefix()
                        +spDataRepresentationManagerList.get(i).getMeasureUnit();*/
                    }
                    output += "\n";

                }


            }
        }


        for(int i = 0; i < numberOfSensors; i++) {
            output += spBatchExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getID() + ": ";
            for(int j = 0; j < numberOfChips; j++){
                //output += formatter.format(values[i + numOfTotalConfiguration][j]) + " ";

                if((i+spBatchExperiment.getTotalNumberOfEISPlot())>=spDataRepresentationList.size()){
                    output+=" - ";
                }
                else {
                    output += spDataRepresentationList.get(i + spBatchExperiment.getTotalNumberOfEISPlot()).getFormatter().format(
                            spDataRepresentationList.get(i + spBatchExperiment.getTotalNumberOfEISPlot()).values[j][spBatchExperiment.getSensorIndexToPlotList().get(i).get(0)]) + " "
                            + spDataRepresentationList.get(i + spBatchExperiment.getTotalNumberOfEISPlot()).prefix + spDataRepresentationList.get(i + spBatchExperiment.getTotalNumberOfEISPlot()).measureUnit;
                }
               /*output += SPDataRepresentation.formatWithFixedNumberOfSignificantDigits(
                        spDataRepresentationManagerList.get(i+ numOfTotalConfiguration).getMultiplier() * values[i + numOfTotalConfiguration][j],
                        5).format(spDataRepresentationManagerList.get(i+ numOfTotalConfiguration).getMultiplier() * values[i + numOfTotalConfiguration][j]) +
                        " "
                        +spDataRepresentationManagerList.get(i+ numOfTotalConfiguration).getMultiplierPrefix()
                        +spDataRepresentationManagerList.get(i+ numOfTotalConfiguration).getMeasureUnit();*/

            }
            output += "\n";
        }
        appendToMeasureDumpTextArea(output);
        if (className != null && className.length() > 0){
            appendToMeasureDumpTextArea("Class belonging to: " + className);
            Platform.runLater(new Runnable() {
                                  public void run() {
                                      identifiedGasID.setText(className);
                                  }});
        }


        row = tokens[0] + ";";

        row += sht30Temp + ";" + sht30Hum + ";";

        for(int i = 1; i < tokens.length; i++){
            row += tokens[i] + ";";
        }

        if(globalFile != null){
            globalFile.println(row);
        }

    }

    @FXML
    private void resetAddrSpeedClick(ActionEvent e){
        try{
            SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().setAddressingType(SPProtocol.FULL_ADDRESS);
        } catch(SPException exc){
            exc.printStackTrace();
        }
    }



    private void appendToMeasureDumpTextArea(String output){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                textAreaBatchDumpID.appendText(output + "\n");
            }
        });
    }


    @Override
    public void measureFinished(boolean stopped) {
        stopPlotThread = true;

        //for(int i=0; i<chartQueues.length; i++){
        ///    if(spBatchExperiment!=null){
        //        spBatchExperiment.getMainQueue()[i].unSubscribe(chartQueues[i]);
        //    }
        //}

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                GUIStatus.BatchRunning = false;
                updateOnConnectivityState();
                startXMLBatchID.setText("Start");
            }
        });
        if (globalFile != null){
            globalFile.close();
        }

    }

    @Override
    public void updateInfo(float deltaT, float measureForSecond, float progress){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                deltaTID.setText(formatter.format(deltaT));
                readForSecondID.setText(formatter.format(measureForSecond));
                progressLabelBatchXMLID.setText(formatter.format(progress) + "%");

            }
        });

    }

    @Override
    public void newSetting(String row) {

        appendToMeasureDumpTextArea("\n"+row+"\n");

    }

    @FXML
    private void debugLevelClick(){
        SPLogger.DEBUG_TH = 0; //SPLogger.DEBUG_TH | SPLoggerInterface.DEBUG_VERBOSITY_L0
        SPLogger.DEBUG_TH |= debugLevel0.isSelected() ? SPLoggerInterface.DEBUG_VERBOSITY_L0:0;
        SPLogger.DEBUG_TH |= debugLevel1.isSelected() ? SPLoggerInterface.DEBUG_VERBOSITY_L1:0;
        SPLogger.DEBUG_TH |= debugLevel2.isSelected() ? SPLoggerInterface.DEBUG_VERBOSITY_L2:0;
        SPLogger.DEBUG_TH |= debugReadWrite.isSelected() ? SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE:0;
        return;
    }


    @FXML
    private void clearLogTextAreaClick(ActionEvent e){
        logTextArea.setText("");
    }



    @FXML
    private void stepXMLBatchIDClick(ActionEvent e){
        spBatchExecutor.step();
    }


    @FXML
    private void startGraphBatchIDClick(ActionEvent e){
        for(int i = 0; i < spBatchExperiment.getSPSensingElementOnChipSize(); i++){
            System.out.println(spBatchExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getID());
        }

        //globalRepetition*configRepetition
        int sensingPlotDeep = 10;
        int eisPlotDeep = 10;

        if (deepSizeGraphBatchID.getText().equalsIgnoreCase("All")){
            //eisPlotDeep = spBatchExperiment.getGlobalRepetition() * spBatchExperiment.getNumOfConfigurations();
            eisPlotDeep = spBatchExperiment.getGlobalRepetition()*spBatchExperiment.getConfigRepetition();
            sensingPlotDeep = spBatchExperiment.getGlobalRepetition();
        } else {
            try{
                //eisPlotDeep = Integer.parseInt(deepSizeGraphBatchID.getText()) * spBatchExperiment.getNumOfConfigurations();
               // eisPlotDeep = Integer.parseInt(deepSizeGraphBatchID.getText()) * spBatchExperiment.getConfigRepetition();
                eisPlotDeep = Integer.parseInt(deepSizeGraphBatchID.getText());
                sensingPlotDeep = Integer.parseInt(deepSizeGraphBatchID.getText());
            } catch (NumberFormatException nfe){
                nfe.printStackTrace();
            }
        }

        if (startGraphXMLBatchID.getText().equalsIgnoreCase("Close")){
            startGraphXMLBatchID.setText("Open");
            if(stageList != null){
                for(int i = 0; i < stageList.length; i++){
                    if (stageList[i] != null){
                        stageList[i].close();
                        stageList[i] = null;
                    }
                }
                stageList = null;
            }
            System.out.println("Stages closed ...");
        } else {
            startGraphXMLBatchID.setText("Close");
            numOfChipsID.setText("" + spBatchExperiment.getSPConfiguration().getCluster().getActiveSPChipList().size());

            Stage st = (Stage) applyBufferSizeBatchDumpID.getScene().getWindow();


            //stageList = new Stage[spBatchExperiment.getTotalNumEISConfigurations() + spBatchExperiment.getSPSensingElementOnChipSize()];
            stageList = new Stage[spBatchExperiment.getTotalNumberOfEISPlot()+spBatchExperiment.getTotalNumberOfSENSORPlot()];

            lineChart = new Chart[stageList.length];

            int startinOffset = 10;
            int incrementalOffset = 40;
            int graphWinWidth = 800;
            int graphWinHeight = 600;

            boolean chipMask[] = new boolean[numberOfChips];
            for(int i = 0; i < chipMask.length; i++){
                chipMask[i] = true;
            }



            for (int i = 0; i < spBatchExperiment.getNumOfEISElement(); i++) { //i - > eis
                for(int j=0; j<spBatchExperiment.getNumOfConfigurationsList().get(i); j++) {//j - > eis_configuration
                    //chartQueues[i] = new SPChartQueue(measure_text_area_buffer);
                    //spBatchExperiment.getMainQueue()[i].subscribe(chartQueues[i]);


                    //final int currentIndex = i+j;
                    /*int appo;
                    if (i == 0)
                        appo = i + j;
                    else {
                        appo = 0;
                        for (int k = i - 1; k >= 0; k--) {
                            appo += numberOfConfigurationsList.get(k);
                        }
                        appo += j;

                    }
                    final int currentIndex = appo;*/

                    //final int currentIndex = i==0 ? (i+j):(spBatchExperiment.getNumOfConfigurationsList().get(i-1)+j);

                    for (int k = 0; k < spBatchExperiment.eisIndexToPlotList.get(i).size(); k++) {//k - > eis_plot



                        /*int updatedCurrentIndex = 0;
                        if(currentIndex==0)
                            updatedCurrentIndex = currentIndex+k;
                        else{
                            int cont = 0;
                            for(int h=currentIndex-1; h>=0; h--){
                                cont += spBatchExperiment.eisIndexToPlotList.get(h).size();
                            }

                            updatedCurrentIndex = (k+cont);
                        }*/

                        int updatedCurrentIndex =0;

                        int configIndex = 0;
                        for(int h=i-1; h>=0; h--){
                            updatedCurrentIndex+=spBatchExperiment.getNumOfConfigurationsList().get(h)*spBatchExperiment.eisIndexToPlotList.get(h).size();
                            configIndex+=spBatchExperiment.getNumOfConfigurationsList().get(h);
                        }
                        configIndex+=j;
                        updatedCurrentIndex+=j*spBatchExperiment.eisIndexToPlotList.get(i).size()+k;

                        ChartSettings chartSettings = new ChartSettings();
                        chartSettings.plotDeep = eisPlotDeep;
                        chartSettings.lowerY = 0;
                        chartSettings.upperY = 1;
                        chartSettings.typeOfMeasure = ChartSettings.YT_CHART;
                        chartSettings.title = SPMeasurementParameterEIS.measureLabels[spBatchExperiment.eisIndexToPlotList.get(i).get(k)];
                        chartSettings.unitOfMeasure = SPMeasurementParameterEIS.unitMeasuresLabels[spBatchExperiment.eisIndexToPlotList.get(i).get(k)];

                        chartSettings.numberOfChips = numberOfChips;

                        lineChart[updatedCurrentIndex] = Chart.createNewChart(spBatchExperiment.getMainQueue()[updatedCurrentIndex], chartSettings,
                                chipMask,
                                spBatchExperiment.eisIndexToPlotList.get(i).get(k));

                        HBox hb = new HBox();
                        hb.getChildren().add(lineChart[updatedCurrentIndex]);
                        hb.setFillHeight(true);

                        VBox vb = new VBox();
                        vb.setFillWidth(true);
                        vb.getChildren().add(hb);

                        HBox.setHgrow(lineChart[updatedCurrentIndex], Priority.ALWAYS);
                        VBox.setVgrow(hb, Priority.ALWAYS);

                        //new ZoomManager(vb, lineChart, lineChart.getGrandezza());

                        Scene newScene = new Scene(vb, graphWinWidth, graphWinHeight);

                        stageList[updatedCurrentIndex] = new Stage();
                        //stageList[currentIndex].setTitle(SPMeasurementParameterEIS.measureLabels[spBatchExperiment.eisIndexToPlotList.get(i)]);
                        //stageList[currentIndex].setTitle("EIS "+currentIndex);
                        stageList[updatedCurrentIndex].setTitle(spBatchExecutor.eis_titles[configIndex]);
                        stageList[updatedCurrentIndex].setScene(newScene);

                        //Set position of second window, related to primary window.
                        stageList[updatedCurrentIndex].setX(st.getX() + incrementalOffset * configIndex - startinOffset);
                        stageList[updatedCurrentIndex].setY(st.getY() + incrementalOffset * configIndex - startinOffset);

                        final int updatedCurrentIndexFinal =updatedCurrentIndex;
                        stageList[updatedCurrentIndex].setOnCloseRequest(new EventHandler<WindowEvent>() {
                            public void handle(WindowEvent we) {
                                lineChart[updatedCurrentIndexFinal].unsubscribe();
                                closeStage(updatedCurrentIndexFinal);
                                System.out.println("lineChart unsubscribed...");
                            }
                        });
                        stageList[updatedCurrentIndex].show();
                    }
                }

            }


            int indexOffset = spBatchExperiment.getTotalNumberOfEISPlot();
            for (int i = 0; i < spBatchExperiment.getSPSensingElementOnChipSize(); i++) {
                final int currentIndex = i;

                //chartQueues[spBatchExperiment.getNumOfConfigurations()+i] = new SPChartQueue(measure_text_area_buffer);
                //spBatchExperiment.getMainQueue()[spBatchExperiment.getNumOfConfigurations()+i].subscribe(chartQueues[spBatchExperiment.getNumOfConfigurations()+i]);

                ChartSettings chartSettings = new ChartSettings();

                chartSettings.plotDeep = sensingPlotDeep;
                //chartSettings.lowerY = spBatchExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager().getRangeMin();
                //chartSettings.upperY = spBatchExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager().getRangeMax();
                chartSettings.lowerY = spBatchExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager().getRangeMin();
                chartSettings.upperY = spBatchExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager().getRangeMax();


                chartSettings.typeOfMeasure = ChartSettings.YT_CHART;
                //chartSettings.title = spBatchExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getID();
                chartSettings.title = spBatchExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getMeasureType();
                chartSettings.unitOfMeasure = unitMeasure[i];
                chartSettings.numberOfChips = numberOfChips;

                //SPMainQueue mq, ChartSettings chartSettings, int xStep, boolean chipMask[], int indexToSave
                //spBatchExperiment.getSensorIndexToSaveList(i)
                //reateNewChart(SPMainQueue mq, ChartSettings chartSettings, int xStep, SPBatchExperiment spBatchExperiment, boolean chipMask[], int indexToSave){
                lineChart[i + indexOffset] = Chart.createNewChart(spBatchExperiment.getMainQueue()[i + indexOffset],
                        chartSettings,
                        chipMask,
                        spBatchExperiment.getSensorIndexToPlotList().get(i).get(0));

                HBox hb = new HBox();
                hb.getChildren().add(lineChart[i + indexOffset]);
                hb.setFillHeight(true);


                VBox vb = new VBox();
                vb.setFillWidth(true);
                vb.getChildren().add(hb);

                HBox.setHgrow(lineChart[i + indexOffset], Priority.ALWAYS);
                VBox.setVgrow(hb, Priority.ALWAYS);

                //new ZoomManager(vb, lineChart, lineChart.getGrandezza());

                Scene newScene = new Scene(vb, graphWinWidth, graphWinHeight);

                stageList[i + indexOffset] = new Stage();
                stageList[i + indexOffset].setTitle(spBatchExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getID());
                stageList[i + indexOffset].setScene(newScene);

                //Set position of second window, related to primary window.
                stageList[i + indexOffset].setX(st.getX() + incrementalOffset * (i + indexOffset) - startinOffset);
                stageList[i + indexOffset].setY(st.getY() + incrementalOffset * (i + indexOffset) - startinOffset);

                stageList[i + indexOffset].setOnCloseRequest(new EventHandler<WindowEvent>() {
                    public void handle(WindowEvent we) {
                        lineChart[currentIndex + indexOffset].unsubscribe();
                        //stageList[currentIndex + indexOffset] = null;
                        closeStage(currentIndex);
                        System.out.println("lineChart unsubscribed...");
                    }
                });
                stageList[i + indexOffset].show();

            }

            System.out.println("Stages created...");
        }
    }
    private void closeStage(int currentIndex){
        if (stageList != null && stageList[currentIndex] != null){
            stageList[currentIndex] = null;
            boolean stageAllNull = true;
            int i = 0;
            while(i < stageList.length && stageAllNull){
                stageAllNull = stageAllNull && (stageList[i] == null);
                i++;
            }
            if (stageAllNull){
                startGraphXMLBatchID.setText("Open");
            }
        }
    }

    private String batchFilePath;

    @FXML
    private void loadXMLBatchIDClick(ActionEvent e){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Batch File");
        fileChooser.setInitialDirectory(new File("./batch_xml/"));
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            try{
                batchFilePath = file.getCanonicalPath();
                BufferedReader in = new BufferedReader(new FileReader(file.getCanonicalPath()));
                textAreaXMLBatchID.setText("");
                textAreaXMLBatchIDString = "";
                String line = in.readLine();
                while(line != null){
                    textAreaXMLBatchIDString = textAreaXMLBatchIDString + line + "\n";
                    line = in.readLine();
                }
                textAreaXMLBatchID.setText(textAreaXMLBatchIDString);
                in.close();
                GUIStatus.BatchLoaded = true;
                updateOnConnectivityState();
            } catch (Exception ex){
                SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during batch loading... " + ex.getMessage(), DEBUG_LEVEL);
                alertShow("Exception during batch loading", ex);
            }
        }
    }

    @FXML
    private void saveXMLBatchIDClick(ActionEvent e){
        String xml = textAreaXMLBatchID.getText();

        try{
            PrintWriter xmlFile = new PrintWriter(new File(batchFilePath));
            xmlFile.print(xml);
            xmlFile.close();
            textAreaXMLBatchIDString =  textAreaXMLBatchID.getText();
            GUIStatus.BatchDirty = false;
            updateOnConnectivityState();
        } catch (Exception ex){
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during batch saving... " + ex.getMessage(), DEBUG_LEVEL);

            alertShow("Exception during batch saving", ex);

        }
    }
    @FXML
    private void clearBatchDumpIDClick(ActionEvent e){
        //textAreaBatchDumpID.getChildren().removeAll();
        textAreaBatchDumpID.setText("");
    }

    @FXML
    private void applyBufferBatchDumpIDClick(ActionEvent e){
        try{
            measure_text_area_buffer = Integer.parseInt(applyBufferSizeBatchDumpID.getText());
        } catch (Exception ex){
            applyBufferSizeBatchDumpID.setText("500");
        }
    }

    public void setStageAndSetupListeners(Stage stage){
        this.stage = stage;
        stage.setOnCloseRequest(event -> {

            event.consume();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Close Confirmation");
            alert.setHeaderText("Close application");
            alert.setContentText("Are you sure you want to exit application?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                try {
                    stopConnection();
                    spTempHumSHT30.stopMeasurement();
                    SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().closeAll();
                } catch (Exception e) {
                    System.out.println("Close all failed: " + e.getMessage());
                }
                stage.close();
                System.out.println("Bye bye!");
                System.exit(-1);
            }

            if(result.get()==ButtonType.CANCEL){
                alert.close();
            }
        });
    }

    private SPCluster createCluster() throws SPException {
        return getSPConfigurationDefault().getCluster();
    }


    private SPCluster clusterInstance;
    /*
    private SPCluster createClusterOneTime() throws SPException {
        int type = 0;
        if (GlobalParameters.PROTOCOL.equals("I2C")){
            type = SPCluster.TYPE_I2C;
        } else if ((GlobalParameters.decoder.getProtocolName().equals("SENSIBUS"))){
            type = SPChipCluster.TYPE_SENSIBUS;
        } else {
            type = SPChipCluster.TYPE_SPI;
        }
        SPChipCluster cluster = new SPChipCluster(type);
        String[] dirtyTokens = chipListTextArea.getText().split("\n");
        ArrayList<String> tokens = new ArrayList<String>();
        for(int i = 0; i < dirtyTokens.length; i++){
            if (!dirtyTokens[i].startsWith("#")){
                tokens.add(dirtyTokens[i]);
            }
        }
        if (tokens.size() == 0){
            throw new SPException("Any chip defined in tab ListaChip. The correct formt is 0xSENSIBUS_ID 0xI2C_ID for each row. # for comment.");
        } else {
            try{

                SPChipInfo spChipInfo;
                for(int i = 0; i < tokens.size(); i++){
                    String[] ids = tokens.get(i).split(";");
                    spChipInfo = new SPChipInfo();
                    if (type == SPChipCluster.TYPE_SENSIBUS){
                        spChipInfo.setSpChipID(new SPChipID_SENSIBUS(ids[0].trim()));
                    } else if (type == SPChipCluster.TYPE_I2C){
                        spChipInfo.setSpChipID(new SPChipID_I2C(ids[1].trim()));
                    } else {
                        throw new SPException("In charge cluster");
                    }
                    cluster.addSPChipInfo(spChipInfo);
                }

            } catch (Exception e){
                throw new SPException("Error in tab ListaChip. The correct formt is 0xSENSIBUS_ID 0xI2C_ID for each row. # for comment.");
            }
        }
        return cluster;
    }*/


    @FXML
    private void idComboBatch(ActionEvent e){

    }


    // TODO: Cosa fare qui?
    public void activateConfiguration(String configurationName) {
        try{

            //SPCluster cluster = createCluster();
            SPConfiguration spConfiguration = SPConfigurationManager.getSPConfiguration(configurationName);
            spConfiguration.startConfiguration(this,
                    configCreator.isLedBlink(),
                    configCreator.isMulticast(),
                    configCreator.isDetection(),
                    configCreator.isDetectionStopOnError(),
                    configCreator.isRestartBandgap(),
                    configCreator.isRestartBandgapStopOnError(),
                    configCreator.isDisableChipsBandgapProblem());

            setDefaultSPConfiguration(spConfiguration);
            //SPConfiguration conf = getSPConfigurationDefault();

            //GlobalParameters.measurement    = conf.getSPMeasurement()

        } catch (SPException e){
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during connectionUp(): " + e, DEBUG_LEVEL);
            alertShow("Initialization error: ", e);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Connection up, ready to start measurement...", DEBUG_LEVEL);
    }



    @FXML
    private void updateBufferLogTextAreaClick(ActionEvent event){
        try{
            log_text_area_buffer = Integer.parseInt(applyBufferSizeLogID.getText());
        } catch (Exception ex){
            applyBufferSizeBatchDumpID.setText("500");
        }

    }



    private boolean stopInstallation = false;
    private boolean nextStep = false;

    @FXML
    private void installChipsButtonClick(ActionEvent event) {


            if(installChipsButton.getText().equals("Install")) {
            try {
                if (configCreator == null) {
                    configCreator = new ConfigCreator(this);
                    currentConfiguration = SPConfigurationManager.getSPConfiguration(configCreator.getDefaultSPConfigurationName());
                }

                installChipsButton.setText("STOP");

                System.out.println("Change configuration: " + currentConfiguration.getConfigurationName());
                currentConfigurationNameID.setText(currentConfiguration.getConfigurationName());

                //prefs.put("filePath", file.getPath());

                SPConfiguration spConfiguration = SPConfigurationManager.getSPConfiguration(currentConfiguration.getConfigurationName());

                boolean makeInstallation = true;
                if(spConfiguration.getSpInstalledChips().size()>0){
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Do you want to continue the previous installation?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
                    alert.showAndWait();

                    if (alert.getResult() == ButtonType.YES) {

                    }

                    else if (alert.getResult() == ButtonType.NO) {
                        spConfiguration.setSpInstalledChips(new ArrayList<SPChip>());
                    }

                    else if (alert.getResult() == ButtonType.CANCEL) {
                        makeInstallation = false;
                    }
                }


                //spConfiguration.generateAndOpenDriver(this);

                if(makeInstallation) {
                    stopInstallation = false;

                    nextStep = false;

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while (!stopInstallation) {
                                //System.out.println("Don't insert CHIP");

                                nextStep = false;
                                //boolean protocolOpened = false;
                                try {
                                    //try {
                                        spConfiguration.generateAndOpenDriver(Controller.this);
                                        spConfiguration.generateAndOpenProtocol(spConfiguration.getAddressingType());
                                        spConfiguration.getSPProtocol().sendInstruction("WRITE COMMAND S 0x14");
                                        spConfiguration.getSPProtocol().activateMulticast();

                                        //spConfiguration.generateAndOpenProtocol(spConfiguration.getAddressingType());
                                     /* //  protocolOpened = true;
                                    } catch(SPProtocolException e){
                                        System.out.println("No chips on the cable");
                                        protocolOpened = false;

                                    }*/
                                    //if (protocolOpened) {
                                        SPChip foundChip = spConfiguration.getSPProtocol().findChipOnTheCable(spConfiguration.getSpInstalledChips());
                                        if (foundChip != null) {
                                            spConfiguration.getSPProtocol().blinkLEDOnChip(foundChip, 3000);
                                            spConfiguration.getSpInstalledChips().add(foundChip);
                                            System.out.println("CHIP FOUND: " + foundChip.getSerialNumber());

                                            Platform.runLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Chip found: " + foundChip.getSerialNumber() + "\nDo you want to continue the installation?", ButtonType.YES, ButtonType.NO);

                                                        alert.showAndWait();

                                                        if (alert.getResult() == ButtonType.YES) {
                                                            nextStep = true;
                                                        } else if (alert.getResult() == ButtonType.NO) {
                                                            nextStep = true;
                                                            stopInstallation = true;
                                                            try {
                                                                SPConfigurationManager.updateConfigurationFileWithJAXV(currentConfiguration,new File(SPBatchParameters.layoutConfigurationFileXML),new File(SPBatchParameters.layoutConfigurationFileXSD));
                                                            }catch (SPException e1){
                                                                Platform.runLater(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        alertShow("Problems in updating the XML with installed chips", e1);
                                                                    }
                                                                });


                                                            }
                                                            Platform.runLater(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    installChipsButton.setText("Install");
                                                                }
                                                            });
                                                        }
                                                    }catch (SPException e){
                                                        nextStep = true;
                                                        stopInstallation=true;
                                                        try {
                                                            SPConfigurationManager.updateConfigurationFileWithJAXV(currentConfiguration,new File(SPBatchParameters.layoutConfigurationFileXML),new File(SPBatchParameters.layoutConfigurationFileXSD));
                                                        }catch (SPException e1){
                                                            Platform.runLater(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    alertShow("Problems in updating the XML with installed chips", e1);
                                                                }
                                                            });
                                                        }
                                                        Platform.runLater(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                installChipsButton.setText("Install");
                                                            }
                                                        });
                                                    }

                                                }
                                            });

                                            while(!nextStep){
                                                try {
                                                    Thread.sleep(100);
                                                }catch (InterruptedException ioe){
                                                    ioe.printStackTrace();
                                                }
                                            }

                                            //SPLogger.getLogInterface().d(LOG_MESSAGE, "CHIP FOUND: "+foundChip.getSerialNumber(), DEBUG_LEVEL);
                                        } else {
                                            try {
                                                Thread.sleep(1000);
                                            } catch (InterruptedException e) {
                                                e.printStackTrace();
                                            }
                                            System.out.println("No new chip found");
                                        }
                                        //spConfiguration.getSPProtocol().sendInstruction("WRITE COMMAND S 0x00");
                                        spConfiguration.stopConfiguration(false);
                                    //}
                                } catch (SPException e) {
                                    stopInstallation = true;


                                    try {
                                        SPConfigurationManager.updateConfigurationFileWithJAXV(currentConfiguration,
                                                new File(SPBatchParameters.layoutConfigurationFileXML),new File(SPBatchParameters.layoutConfigurationFileXSD));
                                    }catch (SPException e1){
                                        Platform.runLater(new Runnable() {
                                            @Override
                                            public void run() {
                                                alertShow("Problems in updating the XML with installed chips", e1);
                                            }
                                        });


                                    }
                                    //SPDelay.delay(10000);
                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            installChipsButton.setText("Install");
                                            alertShow("Installation Problems", e);
                                        }
                                    });
                                }
                            }


                        }
                    }).start();
                }

            } catch (SPException e) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        alertShow("Installation Problems", e);
                    }
                });


                SPLogger.getLogInterface().d(LOG_MESSAGE, "Installation problems: " + e.getMessage(), DEBUG_LEVEL);
                e.printStackTrace();
            }
        }
        else{
            installChipsButton.setText("Install");
            stopInstallation = true;
            try {
                SPConfigurationManager.updateConfigurationFileWithJAXV(currentConfiguration,new File(SPBatchParameters.layoutConfigurationFileXML),new File(SPBatchParameters.layoutConfigurationFileXSD));
            }catch (SPException e){
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        alertShow("Problems in updating the XML with installed chips", e);
                    }
                });
            }
        }
    }

    @FXML
    private void changeConfigurationButtonClick(ActionEvent event) {


        if (changeConfiguration.getText().equalsIgnoreCase("Close")){
            changeConfiguration.setText("Open");
            modifyConfigurationButtonID.setDisable(false);
            configCreator = null;
            availableChipsID.setText("0");


            try{
                stopConnection();

            } catch (SPException e){
                alertShow("Problem during close operation", e);
            }

            try{
                notifyConfigConnectionListener("Closing connection", ConfigConnectionListener.CLOSE);
            } catch (SPException e){
                e.printStackTrace();
            }



        } else {
            try {
                if (configCreator == null){
                    configCreator = new ConfigCreator(this);
                    currentConfiguration = SPConfigurationManager.getSPConfiguration(configCreator.getDefaultSPConfigurationName());
                }
                System.out.println("Change configuration: " + currentConfiguration.getConfigurationName());
                currentConfigurationNameID.setText(currentConfiguration.getConfigurationName());

                //prefs.put("filePath", file.getPath());

                SPConfiguration spConfiguration = SPConfigurationManager.getSPConfiguration(currentConfiguration.getConfigurationName());

                //try {
                    spConfiguration.startConfiguration(this,
                            configCreator.isLedBlink(),
                            configCreator.isMulticast(),
                            configCreator.isDetection(),
                            configCreator.isDetectionStopOnError(),
                            configCreator.isRestartBandgap(),
                            configCreator.isRestartBandgapStopOnError(),
                            configCreator.isDisableChipsBandgapProblem());



                spConfiguration.changeRedLEDStatusChipsWithBandgap(SPMeasurementParameterActuator.ON);

                setDefaultSPConfiguration(spConfiguration);

                try{
                    getSPConfigurationDefault().getSPProtocol().setSpeed("1x", null);
                } catch (Exception e){
                    e.printStackTrace();
                }

                idSpeedComboID.getSelectionModel().select(0);


                if (configCreator.isLedBlink()){
                    SPConfigurationManager.getSPConfigurationDefault().turnOffGreenLed();
                }
                stage.setTitle(SPWinuxToolsSplash.MAIN_TITLE + " - " + getSPConfigurationDefault().getConfigurationName());

                readWriteTextArea.appendText("--------Chips restarted ------------\n");
                readWriteTextArea.appendText("Available chips [" + getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().size() + "] (VBandGap):"  + "\n");

                for(int i = 0; i < getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().size(); i++){
                    Float vBandGap = getSPConfigurationDefault().getCluster().getActiveSPChipList().get(i).getLastVBandGap();
                    readWriteTextArea.appendText((i + 1) + ") " + getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().get(i) +
                            " (" + (vBandGap == null ? "NA" : vBandGap) + ")" + "\n");
                }

                availableChipsID.setText("" + getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().size());

                readWriteTextArea.appendText("Disabled chips [" + getSPConfigurationDefault().getCluster().getChipWithBandgapProblem().size() + "] (VBandGap):"  + "\n");
                for(int i = 0; i < getSPConfigurationDefault().getCluster().getChipWithBandgapProblem().size(); i++){
                    Float vBandGap = getSPConfigurationDefault().getCluster().getChipWithBandgapProblem().get(i).getLastVBandGap();
                    readWriteTextArea.appendText((i + 1) + ") " + getSPConfigurationDefault().getCluster().getChipWithBandgapProblem().get(i).getSerialNumber() +
                            " (" + (vBandGap == null ? "NA" : vBandGap) + ")" + "\n");
                }

                readWriteTextArea.appendText("------------------------------------\n");

                startConnection();
                updateGUI();

                //protocolCombo.getSelectionModel().select(configurationName);

                //changeConfiguration.setDisable(true);
                //protocolCombo.setDisable(true);

                idAddressingComboID.getSelectionModel().select(SPProtocol.ADDRESSING_TYPES[getSPConfigurationDefault().getSPProtocol().getAddressingMode()]);

                changeConfiguration.setText("Close");
                modifyConfigurationButtonID.setDisable(true);


                try{
                    notifyConfigConnectionListener("Opening connection", ConfigConnectionListener.OPEN);
                } catch (SPException e){
                    e.printStackTrace();
                }

            } catch (SPException e) {

                alertShow("Change configuration error.", e);
                SPLogger.getLogInterface().d(LOG_MESSAGE, "Change configuration error: " + e.getMessage(), DEBUG_LEVEL);
                e.printStackTrace();
            }
        }
    }


    public static void alertShow(String title, Exception ex){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText("Info:");

        alert.setContentText(ex.getMessage());

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);
        alert.getDialogPane().setPrefSize(600, 320);
        alert.showAndWait();
    }


    public void initConfigTextArea(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(spBatchParameters.layoutConfigurationFileXML)));
            String line = reader.readLine();
            while(line != null){
                configTextArea.appendText(line + "\n");
                line = reader.readLine();
            }
            reader.close();
            configTextArea.setScrollTop(Double.MIN_VALUE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void updateGUI(){
        boolean CONNECTED = true;
        if (!CONNECTED){
            commandCombo.setDisable(true);
            mnenomincOnOff.setDisable(true);
            sendButton.setDisable(true);
            sendButton.setDisable(true);
            changeConfiguration.setDisable(false);
            changeConfiguration.setText("Change");
            //protocolCombo.setDisable(false);

            //configurationNameLabel.setText("Configuration name");
            //protocolCombo.getSelectionModel().select(0);
            //protocolCombo.setDisable(false);

            debugLoadButton.setDisable(true);
            debugSaveButton.setDisable(true);
            debugPlayButton.setDisable(true);
            debugStepButton.setDisable(true);
            debugRestartButton.setDisable(true);
            debugInsertButton.setDisable(true);
            //ObservableList<SPDecoderGenericInstruction> items = ;
            // Reset combo
            batchTable.setItems(FXCollections.observableArrayList());

        } else {
            commandCombo.setDisable(false);
            mnenomincOnOff.setDisable(false);
            sendButton.setDisable(false);
            debugLoadButton.setDisable(false);
            debugRestartButton.setDisable(true);
            debugInsertButton.setDisable(true);

            idComboRegDump.getItems().clear();
            idComboSend.getItems().clear();
            idComboBatch.getItems().clear();
            idComboBatchXML.getItems().clear();

            try {
                //protocolCombo.getSelectionModel().select(getListSPConfiguration().get(0).getConfigurationName());

                List<String> sn = getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers();
                String[] snString = new String[sn.size()];
                for(int i = 0; i < sn.size(); i++){
                    snString[i] = sn.get(i);
                }
                idComboRegDump.getItems().addAll(snString);
                idComboRegDump.getSelectionModel().select(snString[0]);

                snString = new String[sn.size() + 1];
                snString[0] = "All";
                for(int i = 0; i < sn.size(); i++){
                    snString[i + 1] = sn.get(i);
                }

                idComboSend.getItems().addAll(snString);
                idComboBatch.getItems().addAll(snString);
                idComboBatchXML.getItems().addAll(snString);

                idComboSend.getSelectionModel().select(snString[0]);
                idComboBatch.getSelectionModel().select(snString[0]);
                idComboBatchXML.getSelectionModel().select(snString[0]);

            } catch (SPException e) {
                //e.printStackTrace();
            }
        }
    }


    @FXML
    private void saveConfig(ActionEvent event) {
        try {
            PrintWriter configFile = new PrintWriter(spBatchParameters.layoutConfigurationFileXML);
            configFile.print(configTextArea.getText());
            configFile.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Config file");
            alert.setHeaderText("Config file saved.");
            alert.showAndWait();
        } catch (FileNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Config file");
            alert.setHeaderText("Config file not saved.");
            alert.setContentText("Path: " + spBatchParameters.layoutConfigurationFileXML);
            alert.showAndWait();
        }
    }

    @FXML
    private void clearReadWriteTextAreaClick(ActionEvent e){
        readWriteTextArea.setText("");
    }


    private void appendMessage(String message){
        Platform.runLater(new Runnable() {
                          public void run(){
                              readWriteTextArea.appendText(message);
                          }});

    }



    private boolean stopTestThread = false;

    class TestThread extends Thread{
        public void run(){

            Hashtable<String, Integer> errorsPerChip = new Hashtable<>();
            try {

                int TIMEOUT_DISTRIBUTION[] = null;
                if(getSPConfigurationDefault().getConfigurationName().contains("ESP8266")){
                    TIMEOUT_DISTRIBUTION = SPProtocolESP8266_SENSIBUS.TIMEOUT_DISTRIBUTION;
                }else if(getSPConfigurationDefault().getConfigurationName().contains("STM32")){
                    TIMEOUT_DISTRIBUTION = SPProtocolSTM32_SENSIBUS.TIMEOUT_DISTRIBUTION;
                }

                boolean stopOnErrorSelected = stopOnError.isSelected();

                SPCluster cluster = generateClusterFromComboSend(idComboSend);
                if (cluster == null){
                    cluster = getSPConfigurationDefault().getCluster();
                }


                for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){
                    errorsPerChip.put(cluster.getActiveSPChipList().get(k).getAddress(getSPConfigurationDefault().getSPProtocol().getAddressingMode()
                            , getSPConfigurationDefault().getSPProtocol().getProtocolName()), 0);
                }


                int numTest = Integer.parseInt(testFieldRepetition.getText());
                int numChips = cluster.getActiveSPChipList().size();
                boolean failed = false;

                String patterns[] = new String[]{"AA55", "55AA", "0005", "0006", "0007", "0008", "00FF", "FF00"};

                String commandWrite = "WRITE CHA_LIMIT M 0x";
                String commandRead = "READ CHA_LIMIT M 2";

                long elapsedTime = System.currentTimeMillis();
                SPProtocol spProtocol = getSPConfigurationDefault().getSPProtocol();

                String message = "";
                int errorCounter = 0;
                int i = 0;
                SPProtocolOut out = null;
                appendMessage("--------------------------------\n");
                for(i = 0; i < numTest && !stopTestThread; i++){
                    //readWriteTextArea.appendText("Step: " + (i + 1) + " of " + numTest + "\n");
                    for(int k = 0; k < patterns.length && (!stopOnErrorSelected || !failed) && !stopTestThread; k++){

                        try{
                            // Write operation
                            out = spProtocol.sendInstruction(commandWrite + patterns[k], null);
                            // Read operation
                            out = spProtocol.sendInstruction(commandRead, null);
                            for(int j = 0; j < numChips && (!stopOnErrorSelected || !failed) && !stopTestThread; j++){
                                String val = out.recValues[j].replace("[", "").replace("]","").replace("0x", "").replace("0X", "").trim();
                                String tokens[] = val.split(" ");
                                val = "";
                                for(int m = tokens.length - 1; m >= 0; m--){
                                    val += tokens[m];
                                }

                                out.recValues[j].replace("[", "").replace("]","").replace("0x", "").replace("0X", "").replace(" ", "").trim();
                                if (!val.equals(patterns[k])){
                                    errorCounter++;
                                    failed = true;

                                    String address = cluster.getActiveSPChipList().get(j).getAddress(getSPConfigurationDefault().getSPProtocol().getAddressingMode()
                                            , getSPConfigurationDefault().getSPProtocol().getProtocolName());

                                    errorsPerChip.put(address, errorsPerChip.get(address) + 1);

                                    message = "Test failed on chip n. " + (j + 1) + ", address: " + address + "\n";
                                    message += "After " + (i + 1) + " read\n";
                                    message += "Last instruction sent:" + commandWrite + patterns[k] + "\n";
                                    message += "Last instruction read:" + val + "\n";

                                    appendMessage(message);
                                }
                            }
                        } catch (SPException e){
                            failed = true;
                            message += "SPException: " + e.getMessage() + "\n";
                            appendMessage(message);
                        }

                }

                    if ((i + 1) % 100 == 0){
                        message = "Progress: " + (i + 1) + " of " + numTest + " (Errors: " + errorCounter + ")\n";
                        appendMessage(message);
                    }

                }



                System.out.println("Timeout distribution histogram (bin dimension " + SPProtocolESP8266_SENSIBUS.STEP_HIST + " [ms]:");
                for(int k = 0; k < TIMEOUT_DISTRIBUTION.length; k++){
                    if (TIMEOUT_DISTRIBUTION[k] > 0)
                        System.out.println("elapsed time " + (k + 1) * SPProtocolESP8266_SENSIBUS.STEP_HIST + " [ms] :\t\t" + TIMEOUT_DISTRIBUTION[k]);
                }

                message = "--------------------------------\n";
                message += "Test " + (failed?" failed!\n" : "ok!\n");
                message += "Number of repetition: " + i + "\n";
                message += "Number of errors: " + errorCounter + "\n";
                message += "Patterns:\n";
                for(int k = 0; k < patterns.length; k++){
                    message += commandWrite + patterns[k] + "\n";
                }
                message += "Chips (" + cluster.getActiveSPChipList().size() + "):\n";
                for(int k = 0; k < cluster.getActiveSPChipList().size(); k++){
                    String address = cluster.getActiveSPChipList().get(k).getAddress(getSPConfigurationDefault().getSPProtocol().getAddressingMode()
                            , getSPConfigurationDefault().getSPProtocol().getProtocolName());
                    message += "errors[" + address + "]: " + errorsPerChip.get(address) + ")";
                    message += "\n";
                }

                message += "Elapsed time: " + (System.currentTimeMillis() - elapsedTime)/1000 + " seconds\n";
                message += "Lines sent: " + spProtocol.GLOBAL_SENT_COUNTER + "\n";
                message += "--------------------------------\n";

                appendMessage(message);

            } catch (SPException e) {
                Platform.runLater(new Runnable() {
                    public void run(){
                        stopTestThread = true;
                        testButton.setText("Start");
                        testFieldRepetition.setDisable(false);
                        stopOnError.setDisable(false);

                        alertShow("Error during test", e);
                    }});
            }

            Platform.runLater(new Runnable() {
                public void run(){
                    testButton.setText("Test");
                    testFieldRepetition.setDisable(false);
                    stopOnError.setDisable(false);
                }});

        }
    }



    @FXML
    private void testButtonClick(ActionEvent ev){

        if (testButton.getText().equals("Test")){
            stopTestThread = false;
            testButton.setText("Stop");
            testFieldRepetition.setDisable(true);
            stopOnError.setDisable(true);

            TestThread t = new TestThread();
            t.start();

        } else {
            stopTestThread = true;
        }

    }


    @FXML
    private void idTestStartCommElapsedClick(ActionEvent e){
        try {
            float elapsed = SPConfigurationManager.getSPConfigurationDefault().getSPProtocol().elapsedStartComResponse();
            readWriteTextArea.appendText("---------------------\n");
            readWriteTextArea.appendText("elapsed: " + elapsed + "\n");
            readWriteTextArea.appendText("---------------------\n");
        } catch (SPException e1) {
            alertShow("Error in idTestStartCommElapsedClick command!", e1);
        }
    }

    @FXML
    private void sendAction(ActionEvent event) {
        //String command = (String) commandCombo.getSelectionModel().getSelectedItem();
        sendInstruction();
    }

    private SPCluster generateClusterFromComboSend(ComboBox combo) throws SPException {
        String idChip = (String) combo.getSelectionModel().getSelectedItem();
        SPCluster cluster = null;
        if (idChip != null && !idChip.equalsIgnoreCase("All")){
            List<String> list = new ArrayList<>();
            list.add(idChip);
            cluster = getSPConfigurationDefault().getCluster().generateTempCluster(list);
        }
        return cluster;
    }

    private SPCluster generateClusterFromComboBatch() throws SPException {
        String idChip = (String) idComboBatch.getSelectionModel().getSelectedItem();
        SPCluster cluster = null;
        if (idChip != null && !idChip.equalsIgnoreCase("All")){
            List<String> list = new ArrayList<>();
            list.add(idChip);
            cluster = getSPConfigurationDefault().getCluster().generateTempCluster(list);
        }
        return cluster;
    }

    private void sendInstruction(){
        String command = commandCombo.getEditor().getText();

        if (command != null && command.length() > 0){
            String recvd[] = null;
            String sent = null;
            try {
                SPCluster cluster = generateClusterFromComboSend(idComboSend);
                if (mnemonic){
                    SPProtocolOut out = getSPConfigurationDefault().getSPProtocol().sendInstruction(command.toUpperCase(), cluster);

                    showSPProtocolOut(out);

                } else {

                    appendNewReadWriteMessageSend(command, command);

                }

                if (!currentList.contains(command)){
                    currentList.add(0, command);
                    commandCombo.getItems().clear();
                    commandCombo.getItems().addAll(currentList);
                }

            } catch (SPException e) {
                alertShow("Error in sendinc command!", e);
            }
        }
    }

    @FXML
    private void mnemonicAction(ActionEvent event) {
        mnemonic = !mnemonic;
        //mnenomincOnOff.setText("Mnemonic " + (mnemonic?"On":"Off"));

        if (mnemonic){
            currentList = listaMnemonic;
        } else {
            currentList = listaNotMnemonic;
        }

        if (mnemonic){
            commandCombo.getItems().clear();
            commandCombo.getItems().addAll(currentList);
        } else {
            commandCombo.getItems().clear();
            commandCombo.getItems().addAll(currentList);
        }

    }
    private void appendNewReadWriteMessageRecv(String recv){
        recv = recv.replace('\n', ' ');
        readWriteTextArea.appendText("recv:\t\t\t" + recv + "\n");
        SPLogger.getLogInterface().d(LOG_MESSAGE, "recv:\t\t\t" + recv + "\n", DEBUG_LEVEL);
    }

    private void appendNewReadWriteMessageSend(String command, String send){
        command = command.replace('\n', ' ');
        send = send.replace('\n', ' ');
        readWriteTextArea.appendText("_____________________________________________________\n");
        readWriteTextArea.appendText("User input:\t" + command + "\n");
        readWriteTextArea.appendText("sent:\t\t\t" + send + "\n");
        SPLogger.getLogInterface().d(LOG_MESSAGE, "_____________________________________________________\n", DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE, "User input:\t" + command + "\n", DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE, "sent:\t\t\t" + send + "\n", DEBUG_LEVEL);
    }



    private void initialize() {
        String[] portNames = SerialPortList.getPortNames();
        if (portNames.length > 0){
            SPLogger.getLogInterface().d(LOG_MESSAGE, "SERIAL PORT AVAILABLE:", DEBUG_LEVEL);
            for(int i = 0; i < portNames.length; i++){
                SPLogger.getLogInterface().d(LOG_MESSAGE, "\t" + (i + 1) + ". " + portNames[i], DEBUG_LEVEL);
            }
        } else {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "ANY DEVICE FOUND:", DEBUG_LEVEL);
            SPLogger.getLogInterface().d(LOG_MESSAGE, "On Linux please verify if /dev/ttyUSBX is available", DEBUG_LEVEL);
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Try also with these settings: sudo chown 777 /dev/ttyUSBX", DEBUG_LEVEL);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "------------------------------------", DEBUG_LEVEL);

        //comPortTextField.setText(COMPort);
        updateGUI();
        // TODO: Aggiornare l'elemento selezionato
        //protocolCombo.getSelectionModel().select(GlobalParameters.decoder.getProtocolName());
    }

    private void stopConnection() throws SPException {
        try{
            setDefaultSPConfiguration(null);
        } catch (SPException e){
            alertShow("Problem during close operation", e);
        }
    }

    @Override
    public String generateDriver(String driverName, ArrayList<String> driverParameters) throws SPDriverException {
        SPDriver sp;
        if (driverName.equals(SPDriverWINUX_COMUSB.DRIVERNAME)){
            sp = new SPDriverWINUX_COMUSB(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_COMBLE.DRIVERNAME)){
            sp = new SPDriverWINUX_COMBLE(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_IP.DRIVERNAME)){
            sp = new SPDriverWINUX_IP(driverParameters);
        } else {
            throw new SPDriverException("Wrong driverName in generateDriver in SPWinuxTools");
        }

        SPDriverManager.add(sp);
        return sp.getDriverKey();
    }



    private void startConnection() throws SPException {



    }




    private static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }


    private int toDeleteCounter = 1;
    @FXML
    private void insertInstruction(){
        batchTable.getSelectionModel().getSelectedIndex();
        batchTable.getItems().add(nextInstructionToExcecute, new InstructionLine("", "new instr" + toDeleteCounter, "comment" +  + toDeleteCounter, "decoded" +  + toDeleteCounter++));
        //nextInstructionToExcecute--;
        batchTable.getSelectionModel().select(nextInstructionToExcecute);
        //nextInstructionToExecuteLabel.setText(batchTable.getItems().get(nextInstructionToExcecute).getInstruction());
    }


    @FXML
    private void loadBatchMnemonicClick(ActionEvent event){

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Mnemonic Batch File");
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            int lineNumber = 1;
            int numInstructionCounter = 1;
            try {
                ObservableList<InstructionLine> batchTableItems     = FXCollections.observableArrayList();
                batchTable.setItems(batchTableItems);


                String instruction, comment, decodedInstruction, numInstruction;

                // Loaded only to verify if the instructions contain errors

                BufferedReader in = new BufferedReader(new FileReader(file.getCanonicalPath()));
                String line = in.readLine();
                lineNumber = 1;
                while(line != null){
                    line = line.trim();
                    line = line.toUpperCase();
                    instruction = "";
                    decodedInstruction = "------------------------------------";
                    comment = "------------------------------------";
                    numInstruction = "";
                    if (line.length() != 0){
                        String[] tokens = line.split("--");
                        if (tokens.length > 1){
                            comment = tokens[1].trim();
                        }
                        instruction = tokens[0].trim();
                        if (instruction.length() > 0){
                            decodedInstruction = getSPConfigurationDefault().getSPProtocol().getSPDecoder().decodeSingleInstruction(instruction,
                                getSPConfigurationDefault().getCluster().getMulticastChip()).getDecodedInstruction();

                            numInstruction = "" + numInstructionCounter++;
                        }
                        //if(cluster.getCurrentType() == SPChipCluster.TYPE_I2C){
                        //    decodedInstruction = GlobalParameters.decoder.decodeSingleInstruction(instruction, -1, new SPChipID_I2C(SPChipID_I2C.ID_I2C_BROADCAST)).getDecodedInstruction();
                        //} else if(cluster.getCurrentType() == SPChipCluster.TYPE_SENSIBUS){
                        //    decodedInstruction = GlobalParameters.decoder.decodeSingleInstruction(instruction, -1, new SPChipID_SENSIBUS(SPChipID_SENSIBUS.ID_SENSIBUS_BROADCAST)).getDecodedInstruction();
                        //}
                        batchTableItems.add(new InstructionLine(numInstruction, instruction, comment, decodedInstruction));
                    }
                    line = in.readLine();
                    lineNumber++;
                }

                debugPlayButton.setDisable(false);
                debugStepButton.setDisable(false);
                debugRestartButton.setDisable(true);
                debugInsertButton.setDisable(true);

                nextInstructionToExcecute = -1;
                searchNextInstructionToExecuteInTable();
                batchTable.getSelectionModel().select(nextInstructionToExcecute);

            } catch (Exception e) {
                SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during batch loading... " + e.getMessage(), DEBUG_LEVEL);
                alertShow("Exception during batch loading", e);
            }
        }

    }

    private boolean searchNextInstructionToExecuteInTable(){
        String instruction;
        nextInstructionToExcecute++;
        if (nextInstructionToExcecute < batchTable.getItems().size()) {

            instruction = batchTable.getItems().get(nextInstructionToExcecute).getInstruction();

            while (nextInstructionToExcecute < batchTable.getItems().size() && instruction.length() == 0) {
                nextInstructionToExcecute++;
                instruction = batchTable.getItems().get(nextInstructionToExcecute).getInstruction();
            }

        } else   {
            instruction = "";
        }

        //nextInstructionToExecuteLabel.setText(instruction);

        return nextInstructionToExcecute < batchTable.getItems().size();
    }

    @FXML
    private void batchForDumpButtonClick(ActionEvent e){
        SPRegisterDump dump = null;
        SPProtocol spProtocol = null;
        try {
            SPCluster spCluster = generateClusterFromComboSend(idComboSend);
            dump = new SPRegisterDump(SPFamily.RUN5);
            spProtocol = getSPConfigurationDefault().getSPProtocol();
            String[] instruction = dump.generateInstructionToRefresh(SPFamily.RUN5);
            for(int i = 0; i < instruction.length; i++)  {
                SPProtocolOut out = spProtocol.sendInstruction(instruction[i], spCluster);
                showSPProtocolOut(out);
            }


        } catch (SPException e1) {
            alertShow("Error during register dump", e1);
        }

    }

    @FXML
    private void playButton(ActionEvent event){


        String command, sent, recvd[];

        long delay = 0;

        try {

            delay = Integer.parseInt(delayBetweenInstructionsID.getText());

            SPCluster spCluster = generateClusterFromComboBatch();
            SPProtocol spProtocol = getSPConfigurationDefault().getSPProtocol();

            do{

                String instruction = batchTable.getItems().get(nextInstructionToExcecute).getInstruction();
                String comment = batchTable.getItems().get(nextInstructionToExcecute).getComment();
                String instructionToSend = batchTable.getItems().get(nextInstructionToExcecute).getInstructionToSend();

                System.out.println("Sending instruction: " + instruction + ", " + comment + ", " + instructionToSend);
                // Execute instruction
                SPCluster cluster = createCluster();
                SPProtocolOut out = spProtocol.sendInstruction(instruction, spCluster);

                showSPProtocolOut(out);

                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            } while(searchNextInstructionToExecuteInTable());


            debugRestartButton.setDisable(false);
            debugInsertButton.setDisable(false);

            debugPlayButton.setDisable(true);
            debugStepButton.setDisable(true);
            batchTable.getSelectionModel().clearSelection();


        } catch (Exception e) {
            String message = "Exception during batch execution on line: " + nextInstructionToExcecute + " (" + e.getMessage() + ")";
            alertShow(message, e);
            SPLogger.getLogInterface().d(LOG_MESSAGE, message, DEBUG_LEVEL);
        }

    }

    private void refreshRegisterDump(){
        try {
            String tokens[];
            int byteToRead = 77;
            try{
                SPProtocol spProtocol = getSPConfigurationDefault().getSPProtocol();



                SPProtocolOut out = spProtocol.sendInstruction("READ STATUS M " + byteToRead);

                showSPProtocolOut(out);

                String response = out.recValues[0];
                response = response.replace("[", "").replace("]", "");
                response = response.trim();
                tokens = response.split(" ");
            } catch (SPException e){
                alertShow("No connection available", e);
                tokens = new String[byteToRead];
                for(int i = 0; i < tokens.length; i++){
                    tokens[i] = "--";
                }
            }


            SPRegisterDump spRegisterDump = new SPRegisterDump(SPFamily.RUN5);
            ObservableList<SPRegisterDumpItem> registerTableItems     = FXCollections.observableArrayList();
            registerTable.setItems(registerTableItems);

            int counter = 0;
            String msb = "--", lsb = "--";
            String[] registerNames = spRegisterDump.getRegisterNames();
            for(int i = 0; i < registerNames.length; i++){
                if (registerNames[i].equalsIgnoreCase("STATUS")){
                    lsb = tokens[counter++];
                    msb = "--";
                } else if (registerNames[i].equalsIgnoreCase("COMMAND")){
                    counter++;
                } else {
                    lsb = tokens[counter++];
                    msb = tokens[counter++];
                }
                registerTableItems.add(new SPRegisterDumpItem(SPRegisterList.getSPRegister(SPFamily.RUN5, registerNames[i]), msb, lsb));
            }

        } catch (SPException e) {
            alertShow("Problem during dump refresh: ", e);
        }

    }


    @FXML
    private void refreshRegisterDump(ActionEvent event){
        refreshRegisterDump();
    }


    private void showSPProtocolOut(SPProtocolOut out){
        String sent = "";

        for(int j = 0; j < out.sentInstructions.length; j++){
            sent += out.sentInstructions[j].toString();
        }
        appendNewReadWriteMessageSend(out.mnemonicInstruction, sent);


        for(int j = 0; j < out.recValues.length; j++){
            appendNewReadWriteMessageRecv(out.recValues[j]);
        }

    }

    @FXML
    private void testDumpButtonAction(ActionEvent event){

        new Thread(new Runnable() {
            @Override
            public void run() {
                SPRegisterDump dump = null;
                SPProtocol spProtocol = null;
                int numOfRepetition= 10;
                try {
                    SPCluster spCluster = generateClusterFromComboSend(idComboSend);
                    dump = new SPRegisterDump(SPFamily.RUN5);
                    spProtocol = getSPConfigurationDefault().getSPProtocol();

                    //spProtocol.sendInstruction("WRITE COMMAND S 0x02");

                    String[] instruction = dump.generateInstructionToRefresh(SPFamily.RUN5);

                    SPProtocolOut currentDump[][] = new SPProtocolOut[numOfRepetition][instruction.length];

                    boolean errorInDump = false;

                    for(int cont =0; cont<numOfRepetition; cont++) {
                        for (int i = 0; i < instruction.length; i++) {
                            currentDump[cont][i] = spProtocol.sendInstruction(instruction[i], spCluster);

                            //SPProtocolOut out = spProtocol.sendInstruction(instruction[i], spCluster);
                            //showSPProtocolOut(out);
                        }


                        String chipError = "";
                        String instructionError = "";
                        if(cont >0){
                            boolean[][] results = compareDump(currentDump[cont],currentDump[0]);

                            for(int i=0; i<results.length; i++){
                                for(int j=0; j<results[i].length; j++){
                                    if(results[i][j] == true){
                                        errorInDump = true;
                                        instructionError = instruction[i];
                                        chipError = SPConfigurationManager.getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().get(j);
                                        break;
                                    }
                                }
                            }

                            if(errorInDump)
                            {
                                appendNewReadWriteMessageRecv("Error occurred: \nChip: "+chipError+"\nInstruction: "+instructionError);
                                break;
                            }
                            else{
                                appendNewReadWriteMessageRecv("No error in Dump "+cont);
                            }

                        }

                    }

                } catch (SPException e1) {
                    alertShow("Error during register dump", e1);
                }
            }
        }).start();

    }

    private boolean[][] compareDump(SPProtocolOut[] currentDump, SPProtocolOut[] previousDump){
        boolean[][] result = new boolean[currentDump.length][currentDump[0].recValues.length];
        for(int i = 0; i< currentDump.length; i++){
            for(int j = 0; j< currentDump[i].recValues.length; j++){
                if(!currentDump[i].mnemonicInstruction.contains("CHA_FIFOH")) {
                    if (!currentDump[i].recValues[j].equals(previousDump[i].recValues[j]))
                        result[i][j] = true;
                    else
                        result[i][j] = false;
                }
            }
        }
        return result;
    }



    @FXML
    private void stepButton(ActionEvent event){
        String instruction = batchTable.getItems().get(nextInstructionToExcecute).getInstruction();

        try {
            SPCluster spCluster = generateClusterFromComboBatch();
            SPProtocol spProtocol = getSPConfigurationDefault().getSPProtocol();
            SPProtocolOut out = spProtocol.sendInstruction(instruction, spCluster);

            showSPProtocolOut(out);

            if (!searchNextInstructionToExecuteInTable()){
                debugRestartButton.setDisable(false);
                debugPlayButton.setDisable(true);
                debugStepButton.setDisable(true);
                batchTable.getSelectionModel().clearSelection();
            } else {
                batchTable.getSelectionModel().select(nextInstructionToExcecute);
            }

        } catch (SPException e) {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during instruction send " + e.getMessage(), DEBUG_LEVEL);
            alertShow("Exception during instruction send", e);
            updateGUI();
        }
    }

    @FXML
    private void restartButton(ActionEvent event){
        nextInstructionToExcecute = -1;
        nextInstructionToExcecute = -1;
        debugPlayButton.setDisable(false);
        debugStepButton.setDisable(false);
        debugRestartButton.setDisable(true);
        searchNextInstructionToExecuteInTable();
        batchTable.getSelectionModel().select(nextInstructionToExcecute);
    }


    @FXML
    private void setEIS_ADC_Action(ActionEvent event){
        try {
            SPMeasurementRUN5_MCU extended = new SPMeasurementRUN5_MCU(SPConfigurationManager.getSPConfigurationDefault(), null, "EIS");
            SPMeasurementParameterEIS eisParam = new SPMeasurementParameterEIS(SPConfigurationManager.getSPConfigurationDefault());
            SPMeasurementParameterADC adcParam = new SPMeasurementParameterADC(SPConfigurationManager.getSPConfigurationDefault());

            //Set EIS

            //Byte 1
            eisParam.setRsense("50000"); // pos: 00
            eisParam.setInGain("40"); // pos: 11
            eisParam.setHarmonic("FIRST_HARMONIC"); // pos: 00
            eisParam.setModeVI("VOUT_IIN"); // pos: 01

            //Byte 2
            eisParam.setOutGain("7"); // pos: 111
            eisParam.setInPort("PORT_HP"); // pos: 001100

            //Byte 3
            eisParam.setIQ("IN_PHASE"); // pos: 00
            eisParam.setContacts("TWO"); // pos: 0
            eisParam.setOutPort("PORT_HP"); // pos: 001100

            //Byte 4
            eisParam.setDCBiasN(0); // 32

            //Byte 5
            eisParam.setMeasure("CAPACITANCE"); // pos: 0111
            eisParam.setFilter("1"); // pos: 000

            //Byte 6
            eisParam.setPhaseShiftMode("Quadrants"); // pos: 00
            eisParam.setPhaseShiftLabel("0"); // pos: 00000

            //Byte 7-8
            eisParam.setDCBiasP(0); // 2048

            //Byte 9-10-11-12
            eisParam.setFrequency(78125.0f);

            //Set ADC
            adcParam.setInPortADC("IA");
            adcParam.setNData(1);

            extended.setEIS(eisParam, null);
            extended.setADC(adcParam, null);
        } catch (SPException e) {
            e.printStackTrace();
        }
    }


    @FXML
    private void getEIS_Action(ActionEvent event){
        try {
            SPMeasurementRUN5_MCU extended = new SPMeasurementRUN5_MCU(SPConfigurationManager.getSPConfigurationDefault(), null, "EIS");
            SPMeasurementParameterEIS eisParam = new SPMeasurementParameterEIS(SPConfigurationManager.getSPConfigurationDefault());

            extended.getEIS(eisParam,null);
        } catch (SPException e) {
            e.printStackTrace();
        }
    }


    private NumberFormat formatterSHT30 = new DecimalFormat("#0.0");
    public void updateSHT30Info(float temp, float hum){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (temp == 0 || hum == 0){
                    sht30Temp = "----";
                    sht30Hum = "----";
                } else {
                    sht30Temp = formatterSHT30.format(temp);
                    sht30Hum = formatterSHT30.format(hum);
                }

                sht30TempID.setText(sht30Temp);
                sht30HumID.setText(sht30Hum);
            }
        });

    }


    class InternalSensorReader extends Thread {

        public boolean stop = false;

        @Override
        public void run() {
            while(true) if (spTempHumSHT30 != null && spTempHumSHT30.isAvailable()) {
                try {
                    updateSHT30Info(spTempHumSHT30.getTemp(), spTempHumSHT30.getHum());
                } catch (SPException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                updateSHT30Info((float) 0, (float) 0);

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private ArrayList<ConfigConnectionListener> configConnectionListenersList = new ArrayList<>();
    /**
     * ConfigConnectionListener manaager
     */
    public void addConfigConnectionListener(ConfigConnectionListener conf){
        configConnectionListenersList.add(conf);
    }

    private void notifyConfigConnectionListener(String info, int op) throws SPException {
        if (op != ConfigConnectionListener.OPEN && op != ConfigConnectionListener.CLOSE){
            throw new SPException("Operation not allowed in ConfigConnectionListenerNotifier");
        }
        for(ConfigConnectionListener item : configConnectionListenersList){
            if (op == ConfigConnectionListener.OPEN) {
                item.open(info);
            } else {
                item.close(info);
            }
        }
    }

    /**
     * ConfigConnectionListener manaager
     */
    public void removeConfigConnectionListener(ConfigConnectionListener conf){
        configConnectionListenersList.remove(conf);

    }


    @FXML
    private Button idTestStartCommID;
    @FXML
    private Button idTestStartCommElapsedID;
    @Override
    public void open(String info){
        System.out.println("Controller received opening connection info");
        GUIStatus.connected = true;
        updateOnConnectivityState();
        try {
            System.out.println("ciao tano " + getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().get(0));
        } catch (SPException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close(String info){
        System.out.println("Controller received closing connection info");
        GUIStatus.connected = false;
        updateOnConnectivityState();
    }

    private void updateOnConnectivityState(){
        idTestStartCommID.setDisable(!GUIStatus.connected);
        idTestStartCommElapsedID.setDisable(!GUIStatus.connected);
        greenButtonID.setDisable(!GUIStatus.connected);
        batchForDumpButtonID.setDisable(!GUIStatus.connected);
        testButton.setDisable(!GUIStatus.connected);
        sendButton.setDisable(!GUIStatus.connected);
        idSpeedComboID.setDisable(!GUIStatus.connected);
        idAddressingComboID.setDisable(!GUIStatus.connected);

        startXMLBatchID.setDisable(!GUIStatus.connected || !GUIStatus.BatchLoaded || GUIStatus.BatchDirty && !GUIStatus.BatchRunning && !GUIStatus.BatchPaused);
        stepXMLBatchID.setDisable(!GUIStatus.connected || !GUIStatus.BatchPaused);
        stopXMLBatchID.setDisable(!GUIStatus.connected || !GUIStatus.BatchRunning);
        startGraphXMLBatchID.setDisable(!GUIStatus.connected || !GUIStatus.BatchRunning);

        saveXMLBatchID.setDisable(!GUIStatus.BatchDirty);
        //loadXMLBatchID
    }
}


class GUIStatus{

    public static boolean connected = false;
    public static boolean DebugBatchLoaded = false;
    public static boolean DebugBatchRunning = false;
    public static boolean BatchLoaded = false;
    public static boolean BatchDirty = false;
    public static boolean BatchRunning = false;
    public static boolean BatchPaused = false;
    public static boolean BatchGraphOpened = false;
    public static boolean InstrumentsEISRunning = false;


}
