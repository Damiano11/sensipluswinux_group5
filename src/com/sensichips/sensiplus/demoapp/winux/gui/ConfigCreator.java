package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMUSB;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import jssc.SerialPortList;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.prefs.Preferences;

public class ConfigCreator {

    private String defaultSPConfigurationName =null;

    private boolean ledBlink = true;
    private boolean multicast = true;
    private boolean detection = true, detectionStopOnError = true;
    boolean restartBandgap = true;
    boolean restartBandgapStopOnError = false;
    boolean disableChipsBandgapProblem = false;




    private SPConfigurationManager.SPDriverGenerator spDriverGenerator;

    public ConfigCreator(SPConfigurationManager.SPDriverGenerator spDriverGenerator){
        this.spDriverGenerator = spDriverGenerator;
        loadPreferences();
    }


    public String getDefaultSPConfigurationName() {
        return defaultSPConfigurationName;
    }

    public void setDefaultSPConfigurationName(String defaultSPConfigurationName) {
        this.defaultSPConfigurationName = defaultSPConfigurationName;
    }


    public void setLedBlink(boolean ledBlink) {
        this.ledBlink = ledBlink;
    }

    public void setMulticast(boolean multicast) {
        this.multicast = multicast;
    }

    public void setDetection(boolean detection) {
        this.detection = detection;
    }

    public void setDetectionStopOnError(boolean detectionStopOnError) {
        this.detectionStopOnError = detectionStopOnError;
    }

    public void setRestartBandgap(boolean restartBandgap) {
        this.restartBandgap = restartBandgap;
    }

    public void setRestartBandgapStopOnError(boolean restartBandgapStopOnError) {
        this.restartBandgapStopOnError = restartBandgapStopOnError;
    }

    public boolean isLedBlink() {
        return ledBlink;
    }

    public boolean isMulticast() {
        return multicast;
    }

    public boolean isDetection() {
        return detection;
    }

    public boolean isDetectionStopOnError() {
        return detectionStopOnError;
    }

    public boolean isRestartBandgap() {
        return restartBandgap;
    }

    public boolean isRestartBandgapStopOnError() {
        return restartBandgapStopOnError;
    }


    public boolean isDisableChipsBandgapProblem() {
        return disableChipsBandgapProblem;
    }

    public void setDisableChipsBandgapProblem(boolean disableChipsBandgapProblem) {
        this.disableChipsBandgapProblem = disableChipsBandgapProblem;
    }
    public SPConfigurationManager.SPDriverGenerator getSPDriverGenerator() {
        return spDriverGenerator;
    }


    public void setSPDriverGenerator(SPConfigurationManager.SPDriverGenerator spDriverGenerator){
        this.spDriverGenerator = spDriverGenerator;
    }


    public void reloadFromPreferences(){
        loadPreferences();
    }

    /*public SPConfiguration getSPConfiguration() throws SPException {

        SPConfiguration currentConfiguration = null;

        try {

            if (defaultSPConfigurationName != null)
                currentConfiguration = SPConfigurationManager.getSPConfiguration(defaultSPConfigurationName);

        }catch (SPException spe){
            return null;
        }
        return currentConfiguration;
    }*/



    private void loadPreferences(){
        // Preferences recovery

        Preferences prefs = Preferences.userNodeForPackage(ConfigCreator.class);

        defaultSPConfigurationName = prefs.get("ConfigurationSelectionComboBoxID", null);

        ledBlink =                  Boolean.parseBoolean(prefs.get("ledBlink", null));
        multicast =                 Boolean.parseBoolean(prefs.get("multicast", null));
        detection =                 Boolean.parseBoolean(prefs.get("detection", null));
        detectionStopOnError =      Boolean.parseBoolean(prefs.get("detectionStopOnError", null));
        restartBandgap =            Boolean.parseBoolean(prefs.get("restartBandgap", null));
        restartBandgapStopOnError = Boolean.parseBoolean(prefs.get("restartBandgapStopOnError", null));
        disableChipsBandgapProblem = Boolean.parseBoolean(prefs.get("disableChipsBandgapProblems", null));


    }



    public void savePreferences(){

        // Preferences recovery
        Preferences prefs = Preferences.userNodeForPackage(ConfigCreator.class);

        prefs.put("ConfigurationSelectionComboBoxID",defaultSPConfigurationName);

        prefs.put("ledBlink", "" + ledBlink);
        prefs.put("multicast", "" + multicast);
        prefs.put("detection", "" + detection);
        prefs.put("detectionStopOnError", "" + detectionStopOnError);
        prefs.put("restartBandgap", "" + restartBandgap);
        prefs.put("restartBandgapStopOnError", "" + restartBandgapStopOnError);
        prefs.put("disableChipsBandgapProblems",""+disableChipsBandgapProblem);

    }



   /*
    public static void main(String args[]){
        //System.out.println("Ciao sono marco");

        Preferences prefs = Preferences.userNodeForPackage(ConfigCreator.class);

        prefs.put("descriptionTextFieldID",     "WINUX_COMUSB-CLUSTERID_0X02-FT232RL_SENSIBUS-ShortAddress-ALL_CHIP(RUN5)");
        //System.out.println("arg inserted: "+args[0]);
        //prefs.put("descriptionTextFieldID",     args[0]);
        prefs.put("driverComboBoxID",           "WINUX_COMUSB");
        prefs.put("property1ComboBoxID",        "COM5");
        prefs.put("property2ComboBoxID",        "115200");
        prefs.put("clusterComboBoxID",          "0x02");
        prefs.put("mcuComboBoxID",              "FT232RL");
        prefs.put("protocolComboBoxID",         "SENSIBUS");
        prefs.put("addressingComboBoxID",       "ShortAddress");
        prefs.put("vccTextFieldID",             "3300");
        prefs.put("hostControllerComboBoxID",   "PC");
        prefs.put("apiOwnerComboBoxID",         "PC");

        //prefs.put("ConfigurationSelectionComboBoxID",defaultSPConfigurationName);

        prefs.put("ledBlink", "" + true);
        prefs.put("multicast", "" + true);
        prefs.put("detection", "" + true);
        prefs.put("detectionStopOnError", "" + true);
        prefs.put("restartBandgap", "" + true);
        prefs.put("restartBandgapStopOnError", "" + true);

    }
    */


}
