package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.*;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.ChipDAOMYSQLImpl;
import com.sensichips.sensiplus.model.dao.mysql.FamilyDAOMYSQLImpl;
import com.sensichips.sensiplus.model.dao.mysql.SensingElementOnChipDAOMYSQLImpl;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class ChipController {
    @FXML
    private TableView<Chip> chipTableView;
    @FXML
    private TableColumn<Chip, String> idSPChipColumn;
    @FXML
    private TableColumn<Chip, String> nameSPFamilyColumn;
    @FXML
    private TableView<Port> portInsTableView;
    @FXML
    private TableColumn<Port, String> nameSPPortInsColumn;
    @FXML
    private TableColumn<Port, String> seSPPortInsColumn;
    @FXML
    private Button modificaButton;
    @FXML
    private Button cancellaButton;
    @FXML
    private Label outPutLabel;

    // Reference to the main application.
    private Stage stage;
    private SensingElementOnChip seOnChipSelected = null;

    private Stage primaryStage;

    private ObservableList<Chip> chipData = FXCollections.observableArrayList();
    private ObservableList<Port> portInsConSeData = FXCollections.observableArrayList();
    private ObservableList<SensingElementOnChip> sensingElementOnChipData = FXCollections.observableArrayList();

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public ObservableList<Chip> getChipData() {
        return chipData;
    }

    public ObservableList<Port> getPortInsConSeData() {
        return portInsConSeData;
    }

    public ObservableList<SensingElementOnChip> getSensingElementOnChipData() {
        return sensingElementOnChipData;
    }




    //tempo necessario per attivarsi
    private FadeTransition fadeIn = new FadeTransition(
            Duration.millis(1000)
    );

    private void outPutMessage(String request, int row) {
        outPutLabel.setVisible(false);
        if ( row != -1) {
            outPutLabel.setText("SQL: " + request + " Row Affected: " + row + ".");
        } else {
            outPutLabel.setText("SQL: " + request);
        }
        outPutLabel.setVisible(true);
        fadeIn.playFromStart();
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        outPutLabel.setVisible(false);
        fadeIn.setNode(outPutLabel);//associo la label alla tranzione
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.setCycleCount(1);
        fadeIn.setAutoReverse(false);

        stage = this.getPrimaryStage();
        modificaButton.setDisable(true);
        cancellaButton.setDisable(true);

        /*Callback<TableColumn<SensingElementOnChip, String>, TableCell<SensingElementOnChip, String>> textFieldFactory
                = (TableColumn<SensingElementOnChip, String> param) -> new TextFieldCell();*/

        // Setting up all tableView  and their Listner
        chipTableView.setItems(this.getChipData());
        portInsTableView.setItems(this.getPortInsConSeData());
        //calibrationOnChip.setItems(main.getSensingElementOnChipData());
        // Initialize Chip table with the two columns.
        idSPChipColumn.setCellValueFactory(cellData -> cellData.getValue().idSPChipProperty());
        nameSPFamilyColumn.setCellValueFactory(cellData -> cellData.getValue().spFamily_idSPFamilyProperty());
        //Initialize Port table
        nameSPPortInsColumn.setCellValueFactory((cellData -> cellData.getValue().nameProperty()));
        seSPPortInsColumn.setCellValueFactory(cellData -> cellData.getValue().seAssociatedProperty());

        //questo metodo controlla se è presente qualche elemento nella tabella. Se non è presente nulla
        //non rende visibili i tasti modifica e cancella. Viceversa gli rende visibili.
        chipTableView.getItems().addListener((ListChangeListener.Change<? extends Chip> c) -> {
            if (c.getList().size() == 0) {
                modificaButton.setDisable(true);
                cancellaButton.setDisable(true);
            } else {
                modificaButton.setDisable(false);
                cancellaButton.setDisable(false);
            }
        });

        //Acolta il cambiamento di selezione nella tabella chipTableView. Quando avverte un cambiamento avvia il metodo
        //showPortAvailable, il quale mostra le porte inserite di quella famiglia e i sensori su quelle porte
        chipTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            showPortAvailable(newValue);
        });


        chipTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2 && chipTableView.getItems().size() != 0) {
                        modificaButtonAction();
                    } else if (mouseEvent.getClickCount() == 2 && chipTableView.getItems().size() == 0) {
                        nuovoButtonAction();
                    }
                }
            }
        });

    }

    private void showPortAvailable(Chip c) {
        if (c != null) {

            List<Port> listPort;
            Family f = new Family(c.getSpFamily_idSPFamily());
            SensingElement se;

            try {
                //tramite query vedo per una determinata famiglia quali porte sono state inserite, e le salvo nella lista
                listPort = FamilyDAOMYSQLImpl.getInstance().selectInPort(f);
                this.getPortInsConSeData().clear();
                this.getSensingElementOnChipData().clear();
                //per porta inserita va a associare un sensing element. Nel caso il se è presente associa il se alla porta
                //altrimenti elimina la porta a cui non è associato il se
                for (int i = 0; i < listPort.size(); i++) {
                    se = (SensingElement) FamilyDAOMYSQLImpl.getInstance().selectSensingElementIn(f, listPort.get(i));
                    if (se != null) {
                        listPort.get(i).setSeAssociated(se.getIdSPSensingElement());
                        this.getPortInsConSeData().add(listPort.get(i));
                    }
                }

                //main.getPortInsData().get(1).setSeAssociated("kasjd");
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
    }


    @FXML
    private void cancellaButtonAction() {
        //the function below returns the index of the selected item in the TableView
        int selectedIndex = chipTableView.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            //definisco e implemento un elemento della classe Family a cui ci passo l'emento selezionato nella tabella
            Chip c = chipTableView.getItems().get(selectedIndex);

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.initOwner(stage);
            alert.setTitle("Are you sure?");
            alert.setHeaderText("Delete " + c.getIdSPChip() + "?");
            alert.setContentText("Are you sure that you want to delete the selected item?");

            ButtonType buttonTypeOne = new ButtonType("Yes");
            ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne) {
                try {
                    ChipDAOMYSQLImpl.getInstance().delete(c);
                    chipTableView.getItems().remove(selectedIndex);
                    outPutMessage("Chip correctly deleted.", 1);

                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alertError = new Alert(Alert.AlertType.ERROR);
                    alertError.initOwner(this.getPrimaryStage());
                    alertError.setTitle("Error during DB interaction");
                    alertError.setHeaderText("Error during delete ...");
                    alertError.setContentText(e.getMessage());
                    alertError.showAndWait();
                }
            }
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Chip Selected");
            alert.setContentText("Please select a Chip in the table.");
            alert.showAndWait();
        }
    }

    @FXML
    private void nuovoButtonAction() {
        Chip tempChip = new Chip();
        boolean okClicked = this.showChipEditDialog(stage, tempChip, true);
        if (okClicked) {
            try {
                ChipDAOMYSQLImpl.getInstance().insert(tempChip);
                this.getChipData().add(tempChip);
                outPutMessage("Chip correctly inserted.", 1);
            } catch (DAOException e) {
                outPutMessage("WARNING! Some Error occured.", -1);
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("Error during DB interaction");
                alert.setHeaderText("Error during insert ...");
                alert.setContentText(e.getMessage());
                alert.showAndWait();
            }
        }
    }

    @FXML
    public void cercaButtonAction() {
        Chip tempChip = new Chip();
        boolean okClicked = this.showChipEditDialog(stage, tempChip, false);
        if (okClicked) {
            try {
                List<Chip> list = ChipDAOMYSQLImpl.getInstance().select(tempChip);
                this.getSensingElementOnChipData().clear();
                this.getPortInsConSeData().clear();
                this.getChipData().clear();
                this.getChipData().addAll(list);//inserisce nella lista osservabile tutti gli elementi
                // restituiti dalla query di sorpa
            } catch (DAOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(this.getPrimaryStage());
                alert.setTitle("Error during DB interaction");
                alert.setHeaderText("Error during search ...");
                alert.setContentText(e.getMessage());
                alert.showAndWait();
            }
        }
    }

    @FXML
    private void modificaButtonAction() {
        Chip selectedChip = chipTableView.getSelectionModel().getSelectedItem();
        if (selectedChip != null) {
            selectedChip.setOldidSPChip();
            selectedChip.setOldspFamily_idSPFamily();
            boolean okClicked = this.showChipEditDialog(stage, selectedChip, false);
            if (okClicked) {
                try {
                    if (!selectedChip.getOldspFamily_idSPFamily().equals(selectedChip.getSpFamily_idSPFamily())) {
                        SensingElementOnChip seToRemove = new SensingElementOnChip(selectedChip.getOldidSPChip());
                        SensingElementOnChipDAOMYSQLImpl.getInstance().delete(seToRemove);
                    }
                    ChipDAOMYSQLImpl.getInstance().update(selectedChip);
                    showPortAvailable(selectedChip);
                    outPutMessage("Chip correctly updated.", 1);
                } catch (DAOException e) {
                    outPutMessage("WARNING! Some Error occured.", -1);
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initOwner(this.getPrimaryStage());
                    alert.setTitle("Error during DB interaction");
                    alert.setHeaderText("Error during update ...");
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(this.getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No SensingElement Chip");
            alert.setContentText("Please select a Chip in the table.");
            alert.showAndWait();
        }
    }

    public boolean showChipEditDialog(Stage owner, Chip c, boolean check) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("ChipEditDialog.fxml"));
            AnchorPane dialog = loader.load();

            Scene scene = new Scene(dialog);
            Stage fDialog = new Stage();
            fDialog.setTitle("Edit Chip");
            fDialog.initModality(Modality.WINDOW_MODAL);
            fDialog.initOwner(owner);
            fDialog.setScene(scene);
            fDialog.getIcons().add(new Image(getClass().getResourceAsStream("sensichips_logo.png")));

            ChipEditDialogController controller = loader.getController();
//            controller.setMain(this, check);
            controller.setStage(fDialog);
            controller.setChip(c);

            fDialog.showAndWait();

            return controller.okClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}



