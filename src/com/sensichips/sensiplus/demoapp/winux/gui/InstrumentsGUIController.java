package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.datareader.SPDataReader;
import com.sensichips.sensiplus.datareader.SPDataReaderConf;
import com.sensichips.sensiplus.datareader.queue.SPChartQueue;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.*;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level1.chip.SPPort;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementMetaParameter;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterADC;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.util.SPDataRepresentationManager;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.demoapp.winux.gui.utility.StatisticsTable;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.StringConverter;

import javax.swing.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.function.UnaryOperator;
import java.util.prefs.Preferences;


public class InstrumentsGUIController implements TabControllerInterface, ConfigConnectionListener {

    public static int DEBUG_LEVEL = 0;
    private static String LOG_MESSAGE = "InstrumentsGUIController";
    ObservableList<String> contactsList = FXCollections.observableArrayList("TWO", "FOUR");
    ObservableList<String> measureList = FXCollections.observableArrayList("IN-PHASE", "QUADRATURE", "CONDUCTANCE", "SUSCEPTANCE", "MODULE", "PHASE", "RESISTANCE", "CAPACITANCE", "INDUCTANCE");
    ObservableList<String> portStimulusList = FXCollections.observableArrayList("PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3");
    ObservableList<String> gainStimulusList = FXCollections.observableArrayList("0", "1", "2", "3", "4", "5", "6", "7");
    ObservableList<String> portMeasurementList = FXCollections.observableArrayList("PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3");
    ObservableList<String> gainMeasurementList = FXCollections.observableArrayList("1", "12", "20", "40");
    ObservableList<String> filterList = FXCollections.observableArrayList("1", "4", "8", "16", "32", "64");
    ObservableList<String> rSenseList = FXCollections.observableArrayList("50000", "5000", "500", "50");

    //phaseShift
    ObservableList<String> harmonicList = FXCollections.observableArrayList("FIRST_HARMONIC", "SECOND_HARMONIC", "THIRD_HARMONIC");
    ObservableList<String> phaseShiftModeList = FXCollections.observableArrayList("Quadrants", "Coarse", "Fine");
    float stepCurrent = SPMeasurement.AvailablePhaseShift.stepQ;
    int phaseShiftModPrev = SPMeasurementParameterEIS.QUADRANT;
    int phaseShiftMod = SPMeasurementParameterEIS.QUADRANT;
    UnaryOperator<TextFormatter.Change> filter = new UnaryOperator<TextFormatter.Change>() {
        @Override
        public TextFormatter.Change apply(TextFormatter.Change t) {
            if (t.isReplaced()) {
                if (t.getText().matches("[^0-9]")) {
                    t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));
                }
            }

            if (t.isAdded()) {
                if (t.getControlText().contains(".") && t.getText().equals(".")) {
                    t.setText("");
                } else if (!t.getText().equals(".") && t.getText().matches("[^0-9]")) {
                    t.setText("");
                }
            }
            return t;
        }
    };

    //@FXML
    //private Label imp_spec_meas_PhaseShiftlabelVal;
    private Controller mainController;
    @FXML
    private Label imp_spec_meas_PhaseShiftlabelMin;
    @FXML
    private Label imp_spec_meas_PhaseShiftlabelMax;
    @FXML
    private Slider phaseSlider;
    @FXML
    private TextField phaseField;
    @FXML
    private ChoiceBox phaseShiftMode;
    @FXML
    private AnchorPane tabEIS;
    private float phaseShiftMin = 0;
    private float phaseShiftMax = 270;
    private float phaseShiftVal = 0;
    private int phaseShiftPos = 0;
    private int Quadrant = 0;

    @FXML
    private TextField plotDeep;

    @FXML
    private Label totMeasure;

    @FXML
    private Label measForSec;

    //private float stepCurrentCoarse = SPMeasurement.AvailablePhaseShift.stepQ;
    //private float stepCurrentFine = SPMeasurement.AvailablePhaseShift.stepQ;
    //private int phaseShiftModPrev = SPMeasurementParameterEIS.QUADRANT;
    //private int phaseShiftMod = SPMeasurementParameterEIS.QUADRANT;
    private int imp_spec_meas_labelMax_Phaseshift = 31;
    private int currentMinValue = 0;
    private float FRDfr;
    private NumberFormat formatter = new DecimalFormat("#0.00");
    private double k = 10;
    private float progress;
    private SPMeasurement.AvailableFrequencies fc = new SPMeasurement.AvailableFrequencies();
    private float selectedFrequency;
    private boolean programmaticallyChanged = true;


    //end phaseShift declaration
    @FXML
    private Slider frequencySlider;

    @FXML
    private Slider dcBiasPSlider;

    @FXML
    private Slider dcBiasNSlider;

    @FXML
    private TextField frequencyField;

    @FXML
    private TextField dcBiasPField;

    @FXML
    private TextField dcBiasNField;


    @FXML
    private ChoiceBox Contacts;

    @FXML
    private ChoiceBox Measure;

    @FXML
    private ChoiceBox portStimulus;

    @FXML
    private ChoiceBox gainStimulus;

    @FXML
    private ChoiceBox portMeasurement;

    @FXML
    private ChoiceBox gainMeasurement;

    @FXML
    private ChoiceBox Filter;

    @FXML
    private ChoiceBox rSense;

    @FXML
    private ChoiceBox Harmonic;

    @FXML
    private ToggleButton viStimulus;

    @FXML
    private ToggleButton viMeasurement;


    @FXML
    private Button setDefault;

    @FXML
    private Button startMeasure;

    @FXML
    private Button plotButton;

    @FXML
    private Button startMeasureReset;

    @FXML
    private Button autoButton;


    @FXML
    private Button setParametersDefault;


    private InstrumentsGUIController setParam;

    private boolean alreadyInitialized = false;
    private SPMeasurementParameterEIS defaultParam;

    public InstrumentsGUIController() {
    }

    public void setMainController(Controller mainController) {
        System.out.println("Main controller passed");
        this.mainController = mainController;
        this.mainController.addConfigConnectionListener(this);
    }

    @Override
    public void initTab() {

        if (!alreadyInitialized) {

            //System.out.println("Try to initialize EIS");
            try {
                SPConfigurationManager.getSPConfigurationDefault();
                setParameterToUI(createDefaultParam());
                alreadyInitialized = true;
            } catch (SPException e) {
                SPLogger.getLogInterface().d("InstrumentsGUIController", "Default configuration not available. Please open a connection", DEBUG_LEVEL);
            }
        } else {
            System.out.println("EIS already initialized.");

        }
        new Thread(updateStatusBar).start();
    }

    private SPDataReader spDataReader;



    private Chart lineChart;
    private int indexToSave;

    @FXML
    private void startMeasureClick(ActionEvent e){

        try {

            if (startMeasure.getText().equals("Start measure")){


                SPMeasurement spMeasurement = SPConfigurationManager.getSPConfigurationDefault().getSPMeasurement("EIS");
                SPMeasurementOutput message = new SPMeasurementOutput();


                SPMeasurementParameterEIS param = getParameterFromUI().param;
                spMeasurement.setEIS(param, message);

                int NDATA = 1;
                spMeasurement.setADC(NDATA, null, SPMeasurementParameterADC.INPORT_IA, true, message);


                boolean timePlot = true;
                int delay = 1;
                int numOfMeasure = 9;
                float spaceMin = Float.MAX_VALUE;
                float spaceMax = Float.MIN_VALUE;
                float threshold = spaceMin + (spaceMax - spaceMin)/2;
                int dimSpaceQueue = 1;
                int dimTimeQueue = 1000;
                boolean plotAutoRange = true;
                int valueToShow = param.getMeasure();
                int numTempMeasure = SPDataReader.TEMPMEASURE_STOP;
                String measure_unit = "";
                indexToSave = valueToShow;

                SPDataRepresentationManager spDataRepresentationManager = new SPDataRepresentationManager(valueToShow);

                if (valueToShow == SPMeasurementParameterEIS.CAPACITANCE){
                    spDataRepresentationManager.setMeasureUnit("F");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax("1.0");
                    spDataRepresentationManager.setInitial_multiplier("-12");
                    spDataRepresentationManager.setDefault_alarm_threshold("0.5");
                } else if (valueToShow == SPMeasurementParameterEIS.RESISTANCE){
                    spDataRepresentationManager.setMeasureUnit("R");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax("1000.0");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold("1000");
                } else if (valueToShow == SPMeasurementParameterEIS.INDUCTANCE){
                    spDataRepresentationManager.setMeasureUnit("H");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax("1.0");
                    spDataRepresentationManager.setInitial_multiplier("-3");
                    spDataRepresentationManager.setDefault_alarm_threshold("1.0");
                } else if (valueToShow == SPMeasurementParameterEIS.IN_PHASE){
                    spDataRepresentationManager.setMeasureUnit("O");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax("33000");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold("15000");
                } else if (valueToShow == SPMeasurementParameterEIS.QUADRATURE){
                    spDataRepresentationManager.setMeasureUnit("rad");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax(Math.PI/2+"");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold(Math.PI/4+"");
                } else if (valueToShow == SPMeasurementParameterEIS.MODULE){
                    spDataRepresentationManager.setMeasureUnit("O");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax("1000.0");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold("500");

                } else if (valueToShow == SPMeasurementParameterEIS.PHASE){
                    spDataRepresentationManager.setMeasureUnit("rad");
                    spDataRepresentationManager.setRangeMin("0.0");
                    spDataRepresentationManager.setRangeMax(""+(Math.PI/2));
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold(Math.PI/4+"");

                }else if (valueToShow == SPMeasurementParameterEIS.CONDUCTANCE){
                    spDataRepresentationManager.setMeasureUnit("S");
                    spDataRepresentationManager.setRangeMin("1E-8");
                    spDataRepresentationManager.setRangeMax("1E-7");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold("0.6E-7");

                } else if (valueToShow == SPMeasurementParameterEIS.SUSCEPTANCE){
                    spDataRepresentationManager.setMeasureUnit("S");
                    spDataRepresentationManager.setRangeMin("10E-9");
                    spDataRepresentationManager.setRangeMax("60E-9");
                    spDataRepresentationManager.setInitial_multiplier("0");
                    spDataRepresentationManager.setDefault_alarm_threshold("50E-9");
                }


                boolean reset = true;
                boolean restartDataReader = reset;


                SPMeasurementMetaParameter meta = new SPMeasurementMetaParameter(SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size());
                meta.setFillBufferBeforeStart(false);
                meta.setBurst(1);
                meta.setRenewBuffer(true);

                String measureName =  SPFamily.MEASURE_TYPES_AVAILABLE[SPFamily.MEASURE_TYPE_EIS];

                SPDataReaderConf spDataReaderConf = new SPDataReaderConf(param, SPConfigurationManager.getSPConfigurationDefault(), meta,
                        timePlot, delay, dimSpaceQueue, dimTimeQueue, restartDataReader, numOfMeasure, plotAutoRange,
                        "t", "", Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, valueToShow, numTempMeasure, measure_unit,
                        measureName,spDataRepresentationManager);

                spDataReaderConf.setTimePlot(true);
                spDataReader = new SPDataReader(spDataReaderConf, spMeasurement);
                spDataReader.setPauseReader(false);

                GUIStatus.InstrumentsEISRunning = true;
                startMeasure.setText("Stop");
                plotButton.setDisable(false);
                autoButton.setDisable(true);


            } else {
                spDataReader.stopReader();
                startMeasure.setText("Start measure");
                plotButton.setDisable(true);
                autoButton.setDisable(false);
            }


        } catch (SPException e1) {
            Controller.alertShow("ERROR", e1);
        }

    }

    Runnable updateStatusBar = new Runnable() {
        @Override
        public void run() {
            while(true){

                if (spDataReader != null){
                    Platform.runLater(() -> {
                        measForSec.setText("" + spDataReader.getMeasForSec());
                        totMeasure.setText("" + spDataReader.getTotMeasure());
                    });
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    };

    @FXML
    private void plotButtonClick(ActionEvent e){

        int dimQueue = 500;

        try{
            dimQueue = Integer.parseInt(plotDeep.getText());
        } catch (Exception ex){
            Controller.alertShow("Plot deep error: " + plotDeep.getText(), ex);
        }


        int numOfChips = 0;
        try {
            numOfChips = SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size();

            int graphWinWidth = 800;
            int graphWinHeight = 600;

            boolean [] chipMask = new boolean[numOfChips];
            for(int i = 0; i < chipMask.length; i++){
                chipMask[i] = true;
            }
            //SPChartQueue spChartQueue = new SPChartQueue(dimQueue,true,chipMask);
            //spDataReader.subscribe(spChartQueue);

            ChartSettings chartSettings = new ChartSettings();
            chartSettings.plotDeep = dimQueue;
            chartSettings.lowerY = 0;
            chartSettings.upperY = 1;
            chartSettings.typeOfMeasure = ChartSettings.YT_CHART;
            chartSettings.title = SPMeasurementParameterEIS.measureLabels[indexToSave];
            chartSettings.unitOfMeasure = SPMeasurementParameterEIS.unitMeasuresLabels[indexToSave];

            chartSettings.numberOfChips = numOfChips;

            StatisticsTable StatTable = new StatisticsTable();

            lineChart = Chart.createNewChart(spDataReader.getSPMainQueue(), chartSettings,
                    chipMask,
                    indexToSave, StatTable);



            // TABLE VIEW
            VBox vb1 = new VBox();
            vb1.getChildren().add(StatTable.getTable());

//            vb1.setPrefSize(1680, 1050);

            vb1.setFillWidth(true);
//            vb1.setAlignment(Pos.CENTER_LEFT);

            HBox hb = new HBox();
            hb.getChildren().add(lineChart);
            hb.setFillHeight(true);

            VBox vb = new VBox();
            vb.setFillWidth(true);
            vb.getChildren().addAll(hb);

            ScrollPane scrollBar = new ScrollPane();
            scrollBar.setContent(vb1);

            HBox.setHgrow(lineChart, Priority.ALWAYS);
            VBox.setVgrow(hb, Priority.ALWAYS);

    //      new ZoomManager(vb, lineChart, lineChart.getGrandezza());

//            SplitPane splitPane = new SplitPane(lineChart, StatTable.getTable());
//            splitPane.setOrientation(Orientation.VERTICAL);

            BorderPane pane = new BorderPane();
            pane.setTop(hb);
            pane.setBottom(vb1);
            pane.setCenter(scrollBar);
//            pane.setCenter(splitPane);

            Scene newScene = new Scene (pane, graphWinWidth, graphWinHeight);

            Stage stage = new Stage();

            vb1.prefWidthProperty().bind(stage.widthProperty().multiply(0.98));

            stage.setTitle("EIS");
            stage.setScene(newScene);

            //Set position of second window, related to primary window.
            //stage.setX(st.getX() + incrementalOffset * (i + indexOffset) - startinOffset);
            //stage.setY(st.getY() + incrementalOffset * (i + indexOffset) - startinOffset);

            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    lineChart.unsubscribe();
                    //stageList[currentIndex + indexOffset] = null;
                    //closeStage(currentIndex);
                    stage.close();
                    System.out.println("lineChart unsubscribed...");
                }
            });

            stage.show();


            //DataGenerator dg = new DataGenerator(StatTable, lineChart);
            //dg.start();
        } catch (SPException e1) {
            Controller.alertShow("ERROR", e1);
        }

    }




    @FXML
    private void initialize() {
        Measure.setItems(measureList);
        Contacts.setItems(contactsList);
        portStimulus.setItems(portStimulusList);
        gainStimulus.setItems(gainStimulusList);
        portMeasurement.setItems(portMeasurementList);
        gainMeasurement.setItems(gainMeasurementList);
        Filter.setItems(filterList);
        rSense.setItems(rSenseList);
        Harmonic.setItems(harmonicList);
        phaseShiftMode.setItems(phaseShiftModeList);

        viStimulus.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (viStimulus.isSelected() == true)
                    viStimulus.setText("I");
                else viStimulus.setText("V");
            }
        });
        viMeasurement.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (viMeasurement.isSelected() == true)
                    viMeasurement.setText("I");
                else viMeasurement.setText("V");
            }
        });


        dcBiasPField.textProperty().bindBidirectional(dcBiasPSlider.valueProperty(), NumberFormat.getIntegerInstance());
        dcBiasNField.textProperty().bindBidirectional(dcBiasNSlider.valueProperty(), NumberFormat.getIntegerInstance());

        phaseSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {


                phaseShiftPos = newValue.intValue();
                if (phaseShiftMod == SPMeasurementParameterEIS.QUADRANT) {
                    Quadrant = phaseShiftPos / 8;
                    phaseShiftVal = Quadrant * stepCurrent;
                } else if (phaseShiftMod == SPMeasurementParameterEIS.COARSE) {
                    phaseShiftVal = phaseShiftPos * stepCurrent + phaseShiftMin;
                } else if (phaseShiftMod == SPMeasurementParameterEIS.FINE) {
                    phaseShiftVal = phaseShiftPos * stepCurrent + phaseShiftMin;
                }
                phaseShiftSectionUpdate();


                /*
                float coarseValue = (float) Math.pow(2, fc.OSM);
                stepCurrentCoarse = 360 / coarseValue;
                if (fc.FRD == 1)
                    FRDfr =(float) 2/3;
                else FRDfr = 1;
                float fineValue = (float) Math.pow(2, fc.OSM);
                stepCurrentFine = 360 / (fineValue * fc.DSS_DIVIDER * FRDfr);


                if (phaseShiftMod == SPMeasurementParameterEIS.QUADRANT) {

                    imp_spec_meas_PhaseShiftlabelMin.setText("0");
                    imp_spec_meas_PhaseShiftlabelMax.setText("270");
                    double progressphase = phaseSlider.getValue();

                    if (progressphase >= 0 && progressphase < 7) {
                        phaseField.setText("0");
                        currentMinValue = 0;



                    } else if (progressphase > 7 && progressphase < 15) {
                        phaseField.setText("90");
                        currentMinValue = 90;



                    } else if (progressphase > 15 && progressphase <= 23) {
                        phaseField.setText("180");
                        currentMinValue = 180;


                    } else {
                        phaseField.setText("270");
                        currentMinValue = 270;


                    }

                } else if (phaseShiftMod == SPMeasurementParameterEIS.COARSE ) {

                    imp_spec_meas_PhaseShiftlabelMin.setText(String.valueOf(currentMinValue));
                    imp_spec_meas_PhaseShiftlabelMax.setText(String.valueOf(31*stepCurrentCoarse + currentMinValue));
                    phaseField.setText(String.valueOf(phaseSlider.getValue()*stepCurrentCoarse + currentMinValue));


                }



                if (phaseShiftMod == SPMeasurementParameterEIS.FINE) {

                    imp_spec_meas_PhaseShiftlabelMin.setText(String.valueOf(currentMinValue));
                    imp_spec_meas_PhaseShiftlabelMax.setText(String.valueOf(31*stepCurrentFine + currentMinValue));
                    phaseField.setText(String.valueOf(phaseSlider.getValue() * stepCurrentFine + currentMinValue));
                }*/
            }

        });


        phaseShiftMode.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                //System.out.println(phaseShiftMode.getItems().get((Integer) newValue));
                phaseShiftMod = (int) newValue;

                phaseShiftSectionValueUpdate();
                phaseShiftSectionUpdate();

                if (!programmaticallyChanged) {
                    phaseSlider.setValue(phaseShiftPos);
                }
                phaseShiftModPrev = phaseShiftMod;
                programmaticallyChanged = false;


                /*
                if (phaseShiftMod == SPMeasurementParameterEIS.QUADRANT)
                {
                    imp_spec_meas_PhaseShiftlabelMin.setText("0");
                    imp_spec_meas_PhaseShiftlabelMax.setText("270");
                    phaseField.setText("0");
                    phaseSlider.setValue(0);
                }
                else if(phaseShiftMod == SPMeasurementParameterEIS.COARSE)
                {
                    imp_spec_meas_PhaseShiftlabelMin.setText(String.valueOf(currentMinValue));
                    imp_spec_meas_PhaseShiftlabelMax.setText(String.valueOf(31*stepCurrentCoarse + currentMinValue));
                    phaseField.setText(String.valueOf(phaseSlider.getValue()*stepCurrentCoarse+currentMinValue));
                }
                else
                {
                    imp_spec_meas_PhaseShiftlabelMin.setText(String.valueOf(currentMinValue));
                    imp_spec_meas_PhaseShiftlabelMax.setText(String.valueOf((31*stepCurrentFine + currentMinValue)));
                    phaseField.setText(String.valueOf((phaseSlider.getValue()*stepCurrentFine + currentMinValue)));
                }*/
            }
        });

        frequencyField.setTextFormatter(new TextFormatter<Object>(filter));
        frequencyField.textProperty().bindBidirectional(frequencySlider.valueProperty(), new MyFrequencyConverter());


        frequencyField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    System.out.println("Textfield out focus");
                    float frequencyRequested = (float) Float.parseFloat(frequencyField.getText());
                    try {
                        SPMeasurement.AvailableFrequencies.availableFrequencies(SPConfigurationManager.getSPConfigurationDefault(), (float) frequencyRequested, fc);
                    } catch (SPException e) {
                        e.printStackTrace();
                    }
                    NumberFormat formatter = new DecimalFormat("#0.00");
                    frequencyField.setText(formatter.format(fc.availableFrequency).replace(",", "."));
                    evaluateStepPhaseShift();

                }
            }
        });


    }

    private void evaluateStepPhaseShift() {
        SPMeasurement.AvailableFrequencies fc = new SPMeasurement.AvailableFrequencies();
        try {
            SPMeasurement.AvailableFrequencies.availableFrequencies(SPConfigurationManager.getSPConfigurationDefault(), (float) selectedFrequency, fc);
        } catch (SPException e) {
            e.printStackTrace();
        }
        SPMeasurement.AvailablePhaseShift.updateSteps(fc);
        //SPMeasurement.AvailablePhaseShift.stepQ = 90;
        //stepC = (float)(360/Math.pow(2, fc.OSM));
        //stepF = (float)(360/(Math.pow(2, fc.OSM) * fc.FRD_fraz * fc.DSS_DIVIDER));

        phaseShiftSectionValueUpdate();
        phaseSlider.setValue(phaseShiftPos);
        phaseShiftSectionUpdate();

    }

    private void phaseShiftSectionValueUpdate() {
        if (phaseShiftMod == SPMeasurementParameterEIS.QUADRANT) {
            stepCurrent = SPMeasurement.AvailablePhaseShift.stepQ;
            phaseShiftMin = 0;
            phaseShiftMax = 270;
            phaseShiftPos = 8 * Quadrant;
            phaseShiftVal = Quadrant * stepCurrent;

        } else if (phaseShiftMod == SPMeasurementParameterEIS.COARSE) {
            stepCurrent = SPMeasurement.AvailablePhaseShift.stepC;
            if (phaseShiftModPrev == SPMeasurementParameterEIS.QUADRANT) {
                phaseShiftPos = 0;
                //Qval = phaseShiftVal;
            } else {
                //phaseShiftPos = (int) ((phaseShiftVal - Qval)/stepC);
            }

            phaseShiftMin = Quadrant * SPMeasurement.AvailablePhaseShift.stepQ;
            phaseShiftMax = phaseShiftMin + stepCurrent * imp_spec_meas_labelMax_Phaseshift;
            phaseShiftVal = phaseShiftMin + phaseShiftPos * stepCurrent;

        } else {
            stepCurrent = SPMeasurement.AvailablePhaseShift.stepF;
            if (phaseShiftModPrev == SPMeasurementParameterEIS.QUADRANT) {
                phaseShiftPos = 0;
                //Qval = phaseShiftVal;
            } else {
                //phaseShiftPos = (int) ((phaseShiftVal - Qval)/stepF);
            }

            phaseShiftMin = Quadrant * SPMeasurement.AvailablePhaseShift.stepQ;
            phaseShiftMax = phaseShiftMin + stepCurrent * imp_spec_meas_labelMax_Phaseshift;
            phaseShiftVal = phaseShiftMin + phaseShiftPos * stepCurrent;

        }

    }

    private void phaseShiftSectionUpdate() {
        NumberFormat formatter = new DecimalFormat("#0.0");
        imp_spec_meas_PhaseShiftlabelMin.setText(formatter.format(phaseShiftMin) + "°");
        imp_spec_meas_PhaseShiftlabelMax.setText(formatter.format(phaseShiftMax) + "°");
        phaseField.setText(formatter.format(phaseShiftVal) + "°");

        //imp_spec_meas_PhaseShiftlabelMin.setText(String.valueOf(currentMinValue));
        //imp_spec_meas_PhaseShiftlabelMax.setText(String.valueOf(31*stepCurrentCoarse + currentMinValue));
        //phaseField.setText(String.valueOf(phaseSlider.getValue()*stepCurrentCoarse + currentMinValue));

        //imp_spec_meas_seek_PhaseShiftPos.setProgress(phaseShiftPos);
    }

    @Override
    public void open(String info) {
        System.out.println("InstrumentsGUIController received opening connection info");
        tabEIS.setDisable(false);
        startMeasure.setDisable(false);
    }

    @Override
    public void close(String info) {
        System.out.println("InstrumentsGUIController received closing connection info");
        tabEIS.setDisable(true);
    }

    @FXML
    public void setParametersChoice() {
        int dialogButton = JOptionPane.YES_NO_OPTION;
        JOptionPane.showConfirmDialog(null, "Are you sure to set this parameters?", "WARNING", dialogButton);
        if (dialogButton == JOptionPane.YES_OPTION) {
            String defaultParam = null;
            defaultParam =
                    Contacts.getValue().toString() + "/" +
                            Measure.getValue().toString() + "/" +
                            portStimulus.getValue().toString() + "/" +
                            gainStimulus.getValue().toString() + "/" +
                            frequencyField.getText() + "/" +
                            dcBiasPField.getText() + "/";

            String modeVI = "";
            if (viStimulus.getText().equals("V") && viMeasurement.getText().equals("V")) {
                modeVI = "VOUT_VIN";
            } else if (viStimulus.getText().equals("V") && viMeasurement.getText().equals("I")) {
                modeVI = "VOUT_IIN";
            } else if (viStimulus.getText().equals("I") && viMeasurement.getText().equals("I")) {
                modeVI = "IOUT_IIN";
            } else {
                modeVI = "IOUT_VIN";
            }

            defaultParam += modeVI + "/" +
                    dcBiasNField.getText() + "/" +
                    portMeasurement.getValue().toString() + "/" +
                    gainMeasurement.getValue().toString() + "/" +
                    Filter.getValue().toString() + "/" +
                    rSense.getValue().toString() + "/" +
                    Harmonic.getValue().toString() + "/" +
                    phaseShiftMode.getValue().toString() + "/" +
                    phaseField.getText();
            Preferences prefs2 = Preferences.userNodeForPackage(InstrumentsGUIController.class);
            prefs2.put("default values", defaultParam);

        } else if (dialogButton == JOptionPane.NO_OPTION) {

        }
    }

    private SPMeasurementParameterEIS createDefaultParam() throws SPException {

        try {
            defaultParam = new SPMeasurementParameterEIS();

            Preferences prefs = Preferences.userNodeForPackage(InstrumentsGUIController.class);
            String defaultValues = prefs.get("default values", "");
            if (defaultValues == "") {

                //defaultParam = new SPMeasurementParameterEIS(SPConfigurationManager.getListSPConfiguration().get(0));
                //defaultParam.DCBiasPProgress = getResources().getInteger(R.integer.imp_spec_stim_progressMax_DCBiasP)/2;
                //defaultParam.DCBiasNProgress = getResources().getInteger(R.integer.imp_spec_stim_progressMax_DCBiasN)/2;

                defaultParam.setRsense(SPMeasurementParameterEIS.rsenseLabels[1]);
                defaultParam.setContacts(SPMeasurementParameterEIS.ContactsLabels[0]);
                defaultParam.setMeasure(SPMeasurementParameterEIS.measureLabels[6]);
                // Stimulus
                defaultParam.setOutPort(SPPort.portLabels[5]);
                defaultParam.setOutGain(SPMeasurementParameterEIS.outgainLabels[7]);
                //defaultParam.setModeVI(SPMeasurementParameterEIS.ModeVILabels[0]);
                defaultParam.setModeVI(SPMeasurementParameterEIS.ModeVILabels[0]);
                //defaultParamPPR.setFrequency(SPMeasurementParameterEIS.frequencyLimits[1]);
                defaultParam.setFrequency((float) 78.99);

                defaultParam.setDCBiasP(2047);
                defaultParam.setDCBiasN(1);
                // Measurement
                defaultParam.setInPort(SPPort.portLabels[12]);
                defaultParam.setInGain(SPMeasurementParameterEIS.ingainLabels[1]);
                defaultParam.setModeVI(SPMeasurementParameterEIS.ModeVILabels[1]);
                //defaultParamPPR.setIQ(SPMeasurementParameterEIS.I_QLabels[0]);
                defaultParam.setHarmonic(SPMeasurementParameterEIS.harmonicLabels[0]);
                defaultParam.setFilter(SPMeasurementParameterEIS.filterLabels[0]);
                defaultParam.setPhaseShiftQuadrants(SPMeasurementParameterEIS.PhaseShiftModeLabels[0], 0, SPMeasurementParameterEIS.I_QLabels[0]);
                //defaultParamPPR.setPhaseShiftMode(SPMeasurementParameterEIS.PhaseShiftModeLabels[0]);
            } else {
                String parameter[] = defaultValues.split("/");

                defaultParam.setContacts(parameter[0]);
                defaultParam.setMeasure(parameter[1]);
                // Stimulus
                defaultParam.setOutPort(parameter[2]);
                defaultParam.setOutGain(parameter[3]);
                //defaultParam.setModeVI(SPMeasurementParameterEIS.ModeVILabels[0]);

                //defaultParamPPR.setFrequency(SPMeasurementParameterEIS.frequencyLimits[1]);
                defaultParam.setFrequency(Float.parseFloat(parameter[4].replace(",", ".")));
                //viStimulus.setText("V");
                //viMeasurement.setText("I");
                defaultParam.setDCBiasP(Integer.parseInt(parameter[5].replace(".", "")));
                defaultParam.setModeVI(parameter[6]);
                if (parameter[6].equals("VOUT_VIN")) {
                    viStimulus.setSelected(false);
                    viMeasurement.setSelected(false);
                } else if (parameter[6].equals("VOUT_IIN")) {
                    viStimulus.setSelected(false);
                    viMeasurement.setSelected(true);
                } else if (parameter[6].equals("IOUT_IIN")) {
                    viStimulus.setSelected(true);
                    viMeasurement.setSelected(true);
                } else {
                    viStimulus.setSelected(true);
                    viMeasurement.setSelected(false);
                }


                defaultParam.setDCBiasN(Integer.parseInt(parameter[7].replace(".", "")));
                // Measurement
                defaultParam.setInPort(parameter[8]);
                defaultParam.setInGain(parameter[9]);

                //defaultParamPPR.setIQ(SPMeasurementParameterEIS.I_QLabels[0]);
                defaultParam.setFilter(parameter[10]);
                defaultParam.setRsense(parameter[11]);
                defaultParam.setHarmonic(parameter[12]);

                defaultParam.setPhaseShiftQuadrants(SPMeasurementParameterEIS.PhaseShiftModeLabels[0], 0, SPMeasurementParameterEIS.I_QLabels[0]);
                //defaultParamPPR.setPhaseShiftMode(SPMeasurementParameterEIS.PhaseShiftModeLabels[0]);
            }


        } catch (SPException e) {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Exception during default loading!\n" + e.getMessage() + "\n", DEBUG_LEVEL);
            throw e;
        }


        return defaultParam;
    }

    private void setParameterToUI(SPMeasurementParameterEIS spMeasurementParameterEIS) throws SPException {


        Contacts.setValue(SPMeasurementParameterEIS.ContactsLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.ContactsValues, spMeasurementParameterEIS.getContacts())]);
        Measure.setValue(SPMeasurementParameterEIS.measureLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.measureValues, spMeasurementParameterEIS.getMeasure())]);
        rSense.setValue(SPMeasurementParameterEIS.rsenseLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.rsenseValues, spMeasurementParameterEIS.getRsense())]);
        portStimulus.setValue(SPPort.portLabels[UtilityGUI.searchVector(SPPort.portValues, spMeasurementParameterEIS.getOutPort())]);
        gainStimulus.setValue(SPMeasurementParameterEIS.outgainLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.outgainValues, spMeasurementParameterEIS.getOutGain())]);
        frequencySlider.setValue(Math.log10(spMeasurementParameterEIS.getFrequency()) * 10);
        //frequencySlider.valueProperty().set(Math.log10(spMeasurementParameterEIS.getFrequency())*10);
        frequencySlider.valueProperty().setValue(Math.log10(spMeasurementParameterEIS.getFrequency()) * 10);
        dcBiasPSlider.setValue(spMeasurementParameterEIS.getDCBiasP());
        dcBiasNSlider.setValue(spMeasurementParameterEIS.getDCBiasN());
        portMeasurement.setValue(SPPort.portLabels[UtilityGUI.searchVector(SPPort.portValues, spMeasurementParameterEIS.getInPort())]);
        gainMeasurement.setValue(SPMeasurementParameterEIS.ingainLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.ingainValues, spMeasurementParameterEIS.getInGain())]);
        Filter.setValue(SPMeasurementParameterEIS.filterLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.filterValues, spMeasurementParameterEIS.getFilter())]);
        Harmonic.setValue(SPMeasurementParameterEIS.harmonicLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.harmonicValues, spMeasurementParameterEIS.getHarmonic())]);
        phaseShiftMode.setValue(SPMeasurementParameterEIS.PhaseShiftModeLabels[UtilityGUI.searchVector(SPMeasurementParameterEIS.PhaseShiftModeValues, spMeasurementParameterEIS.getPhaseShiftMode())]);

    }

    private TabApiImpSpecStatusElem getParameterFromUI() throws SPException {

        TabApiImpSpecStatusElem paraGUI = new TabApiImpSpecStatusElem();

        paraGUI.param = new SPMeasurementParameterEIS(SPConfigurationManager.getSPConfigurationDefault());

        //paraGUI = new SPMeasurementParameterEIS(SPConfigurationManager.getSPConfigurationDefault());
        paraGUI.param.setContacts(Contacts.getValue().toString());
        paraGUI.param.setMeasure(Measure.getValue().toString());
        paraGUI.param.setRsense(rSense.getValue().toString());
        // Stimulus
        paraGUI.param.setOutPort(portStimulus.getValue().toString());
        paraGUI.param.setOutGain(gainStimulus.getValue().toString());


        // Seleziono la frequenza mostrata all'utente (non quella riportata dallo slider)
        paraGUI.param.setFrequency(Float.parseFloat(frequencyField.getText().toString().replace(",", ".")));
        //paraGUI.paramPPR.setFrequency(selectedFrequency);

        paraGUI.param.setFrequency((float) Math.pow(10, progress / k)); // Per salvare lo stato
        paraGUI.param.setDCBiasP(Integer.parseInt(dcBiasPField.getText().toString()));
        paraGUI.param.setDCBiasN(Integer.parseInt(dcBiasNField.getText().toString()));
        paraGUI.param.setDCBiasP((int) dcBiasPSlider.getValue());
        paraGUI.param.setDCBiasN((int) dcBiasNSlider.getValue());


        paraGUI.DCBiasPProgress = Integer.parseInt(dcBiasPField.getText().toString());
        paraGUI.DCBiasNProgress = Integer.parseInt(dcBiasNField.getText().toString());
        paraGUI.phaseShiftMin = phaseShiftMin;
        paraGUI.phaseShiftMax = phaseShiftMax;
        paraGUI.phaseShiftPos = phaseShiftPos;
        paraGUI.Quadrant = Quadrant;


        // Measurement
        paraGUI.param.setFilter(Filter.getValue().toString());
        paraGUI.param.setInPort(portMeasurement.getValue().toString());
        paraGUI.param.setInGain(gainMeasurement.getValue().toString());
        String modeVI_IN = viMeasurement.isSelected()? "IIN" : "VIN";
        String modeVI_OUT = viStimulus.isSelected()? "IOUT" : "VOUT";

        paraGUI.param.setModeVI(modeVI_OUT + "_" + modeVI_IN);


        paraGUI.param.setHarmonic(Harmonic.getValue().toString());

        paraGUI.param.setPhaseShiftQuadrants(phaseShiftMode.getValue().toString(), (int)phaseSlider.getValue(), SPMeasurementParameterEIS.I_QLabels[Quadrant]);

        //
        //float valToConvert = Qval;
        //if (phaseShiftMod == SPMeasurementParameterEIS.QUADRANT){
        //    valToConvert = phaseShiftVal;
        //}
        //int I_QIndex = (int) (valToConvert/SPMeasurement.AvailablePhaseShift.stepQ);
        //paraGUI.paramPPR.setIQ(SPMeasurementParameterEIS.I_QLabels[I_QIndex]);

        // PhaseShift mode modify also Q_I
        //paraGUI.paramPPR.setPhaseShiftMode(imp_spec_meas_spinner_PhaseShiftMode.getSelectedItem().toString());
        //paraGUI.paramPPR.setPhaseShift(imp_spec_meas_seek_PhaseShiftPos.getProgress());
        //paraGUI.setPhaseShiftQuadrants(phaseShiftMode.getValue().toString(), phaseSlider.getValue(), SPMeasurementParameterEIS.I_QLabels[Quadrant]);


        //paraGUI.param.setOutputDecimation(0);
        //paraGUI.param.setFillBufferBeforeStart(false);

        return paraGUI;
    }

    @FXML
    private void setParametersDefaultOnAction() throws SPException {

        setParameterToUI(createDefaultParam());


    }

    class MyFrequencyConverter extends StringConverter<Number> {

        /**
         * Converts the object provided into its string form.
         * Format of the returned string is defined by the specific converter.
         *
         * @param object
         * @return a string representation of the object passed in.
         */
        @Override
        public String toString(Number object) {

            String output = "0";
            progress = object.floatValue(); //frequencySlider.getValue();

            if (progress != 0) {
                selectedFrequency = (float) Math.pow(10, progress / k);
            } else {
                selectedFrequency = 0;
            }

            try {
                SPMeasurement.AvailableFrequencies.availableFrequencies(SPConfigurationManager.getSPConfigurationDefault(), (float) selectedFrequency, fc);
                NumberFormat formatter = new DecimalFormat("#0.00");
                output = formatter.format(fc.availableFrequency);
                output = output.replace(",", ".");
                evaluateStepPhaseShift();

            } catch (SPException e) {
                System.out.println("SPConfigurationDefault() not available!");
            }
            //frequencyField.setText(formatter.format(fc.availableFrequency));

            //System.out.println("toString. From: " + object + " to " + output);

            return output;
        }

        /**
         * Converts the string provided into an object defined by the specific converter.
         * Format of the string and type of the resulting object is defined by the specific converter.
         *
         * @param string
         * @return an object representation of the string passed in.
         */
        @Override
        public Number fromString(String string) {
            string = string.replace(",", ".");
            float value = Float.parseFloat(string);

            if (value == 0) {
                progress = 0;
            } else {
                value = (float) Math.log10(value);
                progress = Math.round(value * k);
            }
            //System.out.println("Current position of frequency slider: " + frequencySlider.getValue());
            //System.out.println("To Number from: " + string + " to " + progress);
            evaluateStepPhaseShift();
            return progress;
        }
    }


}


