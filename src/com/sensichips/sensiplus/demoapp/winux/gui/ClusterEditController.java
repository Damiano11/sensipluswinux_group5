package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.Cluster;
import com.sensichips.sensiplus.model.Family;
import com.sensichips.sensiplus.model.SensingElementOnChip;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.mysql.FamilyDAOMYSQLImpl;
import com.sensichips.sensiplus.model.dao.mysql.SensingElementOnChipDAOMYSQLImpl;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.List;
import java.util.Optional;

public class ClusterEditController {
    @FXML
    private TextField idClusterTextField;

    @FXML
    private ComboBox <String> idSpFamilyComboBox;


    private Cluster cluster;
    private Stage stage;
    private Stage primaryStage;
    private boolean check = false;
    private boolean ok = false;

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    @FXML
    public void initialize() {
        //carico gli elementi selezionabili all'interno della combox
        idSpFamilyComboBox.getItems().addAll(getItem());
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }


    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
        idClusterTextField.setText(cluster.getIdCluster());
        idSpFamilyComboBox.setValue(cluster.getIdSPFamily());
    }

    @FXML
    public void okButtonAction() {
        if (checkField(check)) {
            cluster.setIdCluster(idClusterTextField.getText());
            if (!cluster.getIdSPFamily().equals("") &&
                    !cluster.getIdSPFamily().equals(idSpFamilyComboBox.getValue())) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.initOwner(stage);
                alert.setTitle("Warning");
                alert.setHeaderText("Update " + cluster.getIdSPFamily() + "?");
                alert.setContentText("Updating IdSpFamily you will lose all the data related to it!");

                ButtonType buttonTypeOne = new ButtonType("Yes");
                ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
                alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == buttonTypeOne) {
                    try {
                        SensingElementOnChip seToRemove = new SensingElementOnChip();
                        seToRemove.setIdSPCluster(cluster.getIdCluster());
                        SensingElementOnChipDAOMYSQLImpl.getInstance().delete(seToRemove);
                        cluster.setIdSPFamily(idSpFamilyComboBox.getValue());
                    } catch (DAOException e) {
                        e.printStackTrace();
                        Alert alertError = new Alert(Alert.AlertType.ERROR);
                        alertError.initOwner(this.getPrimaryStage());
                        alertError.setTitle("Error during DB interaction");
                        alertError.setHeaderText("Error during delete ...");
                        alertError.setContentText(e.getMessage());

                        alertError.showAndWait();
                    }
                } else {
                    ok = false;
                    return;
                }
            } else {
                cluster.setIdSPFamily(idSpFamilyComboBox.getValue());
            }
            ok = true;
            stage.close();
        } else {
            ok = false;
        }
    }

    private boolean checkField(boolean check) {
        String error = "";

        if (check) {
            error += findError(idClusterTextField, "idClusterTextField");
            if (idSpFamilyComboBox.getValue().equals("")) {
                error +=  "idSpFamilyComboBox can not be empty.\n";
            }


            //se la stringa di errore non è nulla significa che c'è stato qualche errore di inserimento,
            //e dunque parte un allert che ci spiega cosa è andato storto
            if (error.length() == 0) {
                return true;
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(stage);
                alert.setTitle("Invalid fields");
                alert.setHeaderText("Please correct invalid fields");
                alert.setContentText(error);

                alert.showAndWait();

                return false;
            }
        } else {
            return true;
        }
    }

    private String findError(TextField textField, String fieldName) {
        String error = "";

        if (textField.getText().equals("")) {
            error += fieldName + " can not be empty.\n";
        }
        return error;
    }

    @FXML
    public void cancelButtonAction() {
        stage.close();
    }

    public boolean okClicked() {
        return ok;
    }

    private String[] getItem(){
        String items = "";
        List<Family> list;
        try {
            list = FamilyDAOMYSQLImpl.getInstance().select(new Family());//carico nella lista le famiglie ricevuti dall'interrogazione.
            //faccio un ciclo for, e inserisco ogni elemento della lista in una striga e la separo con una virgola
            for ( int i=0; i<list.size(); i++){
                if(i==0){
                    items = list.get(i).getIdSPFamily();}
                else{
                    items = items + ";" + list.get(i).getIdSPFamily();
                }
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }

        //splitto la strnga items precedentemente caricata, in recived
        String recived[] = items.split(";");
        return recived;
    }
}
