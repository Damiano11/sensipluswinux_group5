package com.sensichips.sensiplus.demoapp.winux.gui.utility;

import com.sensichips.sensiplus.SPException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import static com.sensichips.sensiplus.config.SPConfigurationManager.getSPConfigurationDefault;

public class StatisticsTable {
    private TableView<StatisticsMeasurements> table;
    private final ObservableList<StatisticsMeasurements> data = FXCollections.observableArrayList();

    public StatisticsTable(){
        table = new TableView<>();

        TableColumn ID = new TableColumn("N°");
        ID.setCellValueFactory(new PropertyValueFactory<StatisticsMeasurements, String>("ID"));


        ID.setCellFactory(new Callback<TableColumn<StatisticsMeasurements, String>, TableCell<StatisticsMeasurements, String>>() {
            @Override
            public TableCell<StatisticsMeasurements, String> call(TableColumn<StatisticsMeasurements, String> param) {
                return new TableCell<StatisticsMeasurements, String>() {

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        TableRow currentRow = getTableRow();
                        if (item != null) {
                            setText(item);

                            try {
                                if (item.equals(getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().get(0))) {
                                    currentRow.setStyle("-fx-background-color:red; -fx-text-fill: black");
                                }

                                else if (item.equals(getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().get(1))){
                                    currentRow.setStyle("-fx-background-color:orange; -fx-text-fill: black");
                                }

                                // Aggiungere gli altri if con i relativi colori per i nuovi chip collegati in più

                            } catch (SPException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                };
            }

        });


        ID.setStyle("-fx-border-color: #C0C0C0");

        // ID
        TableColumn IdParam = new TableColumn("CHIP");
        IdParam.setStyle("-fx-background-color: #7FFF00; -fx-border-color: #32CD32; -fx-font-size: 14;");
        IdParam.getColumns().addAll(ID);

        TableColumn MainGlobalMax = new TableColumn("Global Max");
        MainGlobalMax.setCellValueFactory(new PropertyValueFactory<StatisticsMeasurements, String>("MainGlobalMax"));
        MainGlobalMax.setStyle("-fx-border-color: #C0C0C0");
        TableColumn MainGlobalMin = new TableColumn("Global Min");
        MainGlobalMin.setCellValueFactory(new PropertyValueFactory<StatisticsMeasurements, String>("MainGlobalMin"));
        MainGlobalMin.setStyle("-fx-border-color: #C0C0C0");

        // Global Param
        TableColumn GlobalParam = new TableColumn("GLOBAL PARAMETERS");
        GlobalParam.setStyle("-fx-background-color: #7FFF00; -fx-border-color: #32CD32; -fx-font-size: 14;");

        // Sottoparametri GLOBAL PARAMETERS
        GlobalParam.getColumns().addAll(MainGlobalMax, MainGlobalMin);

        TableColumn Max = new TableColumn("Max");
        Max.setCellValueFactory(new PropertyValueFactory<StatisticsMeasurements, String>("Max"));
        Max.setStyle("-fx-border-color: #C0C0C0");
        TableColumn Min = new TableColumn("Min");
        Min.setCellValueFactory(new PropertyValueFactory<StatisticsMeasurements, String>("Min"));
        Min.setStyle("-fx-border-color: #C0C0C0");
        TableColumn Std = new TableColumn("Std");
        Std.setCellValueFactory(new PropertyValueFactory<StatisticsMeasurements, String>("Std"));
        Std.setStyle("-fx-border-color: #C0C0C0");
        TableColumn Mean = new TableColumn("Mean");
        Mean.setCellValueFactory(new PropertyValueFactory<StatisticsMeasurements, String>("Mean"));
        Mean.setStyle("-fx-border-color: #C0C0C0");

        // Local Param
        TableColumn LocalParam = new TableColumn("LOCAL PARAMETERS");
        LocalParam.setStyle("-fx-background-color: #7FFF00; -fx-border-color: #32CD32; -fx-font-size: 14;");

        // Sottoparametri LOCAL PARAMETERS
        LocalParam.getColumns().addAll(Max, Min, Std, Mean);

        table.setItems(data);
        table.getColumns().addAll(IdParam ,GlobalParam, LocalParam);
        table.setColumnResizePolicy(table.CONSTRAINED_RESIZE_POLICY);
    }

    public TableView<StatisticsMeasurements> getTable(){
        return table;
    }

    public void clearTable(){
        data.removeAll();
        for ( int i = 0; i<table.getItems().size(); i++) {
            table.getItems().clear();
        }
    }

    public void addStatistics(StatisticsMeasurements[] list){
        for(StatisticsMeasurements item:list){
            data.add(item);
        }
    }

    public void addSM(StatisticsMeasurements sm){
        table.getItems().add(sm);
    }

}



