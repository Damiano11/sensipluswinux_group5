package com.sensichips.sensiplus.demoapp.winux.gui.utility;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.datareader.queue.SPChartQueue;
import com.sensichips.sensiplus.demoapp.winux.gui.Chart;
import javafx.application.Platform;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class DataGenerator  extends Thread {
    private StatisticsTable statisticsTable;
    int chipsNum;

    private Chart pippo;
    private SPChartQueue pluto;

    public DataGenerator(StatisticsTable statisticsTable, Chart achart){
        this.statisticsTable = statisticsTable;
        this.pippo = achart;
//        this.pluto = pluto;
    }

    public void run(){

        StatisticsMeasurements[] list = new StatisticsMeasurements[5];

//        double[][] stats = source.getStatistics();
        DecimalFormat f = new DecimalFormat("#.###");

        while(true){
            statisticsTable.clearTable();

            try {
                chipsNum = SPConfigurationManager.getSPConfigurationDefault().getCluster().getSPActiveSerialNumbers().size();
            } catch (SPException e) {
                e.printStackTrace();
            }

            String nanni[][] = new String[chipsNum][6];

            for(int i=0; i<chipsNum; i++) {

                //nanni[i][0] = f.format(pippo.getMax_Glob(i));
                //nanni[i][1] = f.format(pippo.getMin_Glob(i));
                //nanni[i][2] = f.format(pippo.getMax_Loc(i));
                //nanni[i][3] = f.format(pippo.getMin_Loc(i));
                //nanni[i][4] = f.format(pippo.getStd(i));
                //nanni[i][5] = f.format(pippo.getMean(i));

                //list[i] = new StatisticsMeasurements(String.valueOf(chipsNum),nanni[i][0],
                //        nanni[i][1], nanni[i][2], nanni[i][3], nanni[i][4], nanni[i][5]);

                Platform.runLater(() -> {
                    statisticsTable.addStatistics(list);
                });

                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
