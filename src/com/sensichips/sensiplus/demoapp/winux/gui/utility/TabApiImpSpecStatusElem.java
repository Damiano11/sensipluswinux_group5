package com.sensichips.sensiplus.demoapp.winux.gui.utility;


import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterPOT;


/**
 * Created by mario on 19/04/15.
 */


public class TabApiImpSpecStatusElem {
    public SPMeasurementParameterEIS param;
    public int frequencyProgress;
    public int DCBiasPProgress;
    public int DCBiasNProgress;
    public float phaseShiftMin = 0;
    public float phaseShiftMax = 270;
    public float phaseShiftVal = 0;
    public int   phaseShiftPos = 0;
    public int   Quadrant = 0;

    public TabApiImpSpecStatusElem(){}

}

