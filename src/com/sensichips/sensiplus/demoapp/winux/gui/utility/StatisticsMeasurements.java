package com.sensichips.sensiplus.demoapp.winux.gui.utility;

import javafx.beans.property.SimpleStringProperty;

public class StatisticsMeasurements {

    private final SimpleStringProperty ID;
    private final SimpleStringProperty MainGlobalMax;
    private final SimpleStringProperty MainGlobalMin;
    private final SimpleStringProperty MaxVal;
    private final SimpleStringProperty MinVal;
    private final SimpleStringProperty StdVal;
    private final SimpleStringProperty MeanVal;

    public StatisticsMeasurements(String ID, String GMax, String GMin, String Max, String Min, String Std, String Mean) {
        this.ID = new SimpleStringProperty(ID);
        this.MainGlobalMax = new SimpleStringProperty(GMax);
        this.MainGlobalMin = new SimpleStringProperty(GMin);
        this.MaxVal = new SimpleStringProperty(Max);
        this.MinVal = new SimpleStringProperty(Min);
        this.StdVal= new SimpleStringProperty(Std);
        this.MeanVal = new SimpleStringProperty(Mean);
    }


    public String getID() { return ID.get(); }
    public void setID(String ChipID) { ID.set(ChipID);}

    public String getMainGlobalMax() { return MainGlobalMax.get(); }
    public void setMainGlobalMax(String GMax) { MainGlobalMax.set(GMax); }

    public String getMainGlobalMin() { return MainGlobalMin.get(); }
    public void setMainGlobalMin(String GMin) { MainGlobalMin.set(GMin); }

    public String getMin() { return MinVal.get(); }
    public void setMinVal(String Min) { MinVal.set(Min); }

    public String getMax(){ return MaxVal.get(); }
    public void setMaxVal(String Max) { MaxVal.set(Max); }

    public String getStd(){ return StdVal.get(); }
    public void setStdVal(String Std) { StdVal.set(Std); }

    public String getMean(){ return MeanVal.get(); }
    public void setMeanVal(String Mean) { MeanVal.set(Mean); }

}

