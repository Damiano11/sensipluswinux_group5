package com.sensichips.sensiplus.demoapp.winux.gui;

public class ChartSettings {

    public static final int YT_CHART = 0;
    public static final int YX_CHART= 1;

    public int plotDeep = 500;
    public double lowerY = 0;
    public double upperY = 1;
    public int typeOfMeasure = YT_CHART;
    public String title = "Title here...";
    public String unitOfMeasure = "F";
    public int numberOfChips = 0;
}
