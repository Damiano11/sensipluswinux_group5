package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.SensingElement;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.Optional;


public class SensingElementEditController {

    private final static int dcBiasMIN = -2048;
    private final static int dcBiasMAX = 2048;
    private final static int frequencyMIN = 0;
    private final static int frequencyMAX = 5000000;
    private final static int phaseShiftMIN = 0;
    private final static int phaseShiftMAX = 360;
    private final static int filterMIN = 1;
    private final static int filterMAX = 256;
    private final static int conversionRateMIN = 1;
    private final static int conversionRateMAX = 100000;

//    private Main main;
    private boolean check = false;
    private SensingElement se;
    private boolean ok = false;
    private Stage stage;
    private boolean direct = false;
    private boolean cambiato = true;

    @FXML
    private TextField idField;
    @FXML
    private ComboBox<Integer> rSenseBox;
    @FXML
    private ComboBox<Integer> inGainBox;
    @FXML
    private ComboBox<Integer> outGainBox;
    @FXML
    private ComboBox<String> contactsBox;
    @FXML
    private TextField frequencyField;
    @FXML
    private ComboBox<String> harmonicBox;
    @FXML
    private TextField dcBiasField;
    @FXML
    private ComboBox<String> modeVIBox;
    @FXML
    private ComboBox<String> measureTechniqueBox;
    @FXML
    private ComboBox<String> measureTypeBox;
    @FXML
    private TextField filterField;
    @FXML
    private ComboBox<String> phaseShiftModeBox;
    @FXML
    private TextField phaseShiftField;
    @FXML
    private ComboBox<String> iQBox;
    @FXML
    private TextField conversionRateField;
    @FXML
    private ComboBox<String> inPortADCBox;
    @FXML
    private ComboBox<Integer> nDataBox;
    @FXML
    private ComboBox<String> measureUnitBox;
    @FXML
    private TextField nameField;
    @FXML
    private TextField rangeMinField;
    @FXML
    private TextField rangeMaxField;
    @FXML
    private TextField defaultAlarmThresholdField;
    @FXML
    private ComboBox<Integer> multiplierBox;


    private void setListner(TextField textField, String regex, String replacement) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > oldValue.length()) {
                if ((newValue.endsWith(".") && oldValue.contains(".")) ||
                        (newValue.endsWith(".") && (newValue.length() == 1 || oldValue.endsWith("-")))) {
                    textField.setText(oldValue);
                } else if ((newValue.endsWith("-") && oldValue.contains("-")) ||
                        (newValue.endsWith("-") && newValue.length() != 1)) {
                    textField.setText(oldValue);
                } else {
                    textField.setText(newValue.replaceAll(regex, replacement));
                }
            }
        });
    }

    @FXML
    public void initialize() {
        /*
            [] means one of these..
            [^ ..] means (not one of these)
            \  is escape character (because in Java you need to escape it)
            \d   means digit
            in !newValue.matches("\\d"); stiamo dicendo di prendere tutti i caratteri che non sono numeri, perche \d indica tutti i numeri
            il doppio \ serve perche 1 viene ignorato (escape character)
            quindi replaceAll("[^\\d]", ""); in pratica rimpiazza tutto cioè che non è un numero con ""
            questo perche \\d indica tutti i numeri il simbolo ^\\d significa tutto ciè che non è un numero
            ed infine con le [] indica che dobbiamo prendere tutti i caratteri presenti in newValue che non sono dei numeri
        */
        setListner(frequencyField, "[^\\d]", "");
        setListner(dcBiasField, "[^\\d\\-]", "");
        setListner(filterField, "[^\\d]", "");
        setListner(phaseShiftField, "[^\\d]", "");
        setListner(conversionRateField, "[^\\d]", "");
        //in questo caso essendo il campo di tipo Double allora dobbiamo dare la possibilita di immettere il '.'
        //e quindi diciamo di rimppiazzare tutto cio che non è [^ ] un numero [^\\d] e non è il '.' [^\\d\\.]
        setListner(rangeMinField, "[^\\d\\.\\-]", "");
        setListner(rangeMaxField, "[^\\d\\.\\-]", "");
        setListner(defaultAlarmThresholdField, "[^\\d\\.\\-]", "");

        //Adding all comboboxes items according with defaults values and setted the predefined value.
        rSenseBox.getItems().addAll(50, 500, 5000, 50000);
        rSenseBox.getSelectionModel().select(0);
        inGainBox.getItems().addAll(1, 12, 20, 40);
        inGainBox.getSelectionModel().select(0);
        outGainBox.getItems().addAll(0, 1, 2, 3, 4, 5, 6, 7);
        outGainBox.getSelectionModel().select(0);
        contactsBox.getItems().addAll("TWO", "FOUR");
        contactsBox.getSelectionModel().select("TWO");
        harmonicBox.getItems().addAll("FIRST_HARMONIC", "SECOND_HARMONIC", "THIRD_HARMONIC");
        harmonicBox.getSelectionModel().select("FIRST_HARMONIC");
        modeVIBox.getItems().addAll("VOUT_IIN", "VIN_IIN", "VOUT_VIN", "VOUT_VOUT");
        modeVIBox.getSelectionModel().select("VOUT_IIN");
        measureTechniqueBox.getItems().addAll("DIRECT", "EIS", "POT", "ENERGY_SPECTROSCOPY", "ULTRASOUND");
        measureTechniqueBox.getSelectionModel().select("EIS");
        measureTypeBox.getItems().addAll("QUADRATURE", "MODULE", "PHASE", "RESISTENCE", "CAPACITANCE", "INDUCTANCE");
        measureTypeBox.getSelectionModel().select("IN-PHASE");
        phaseShiftModeBox.getItems().addAll("QUADRANTS", "COARSE", "FINE");
        phaseShiftModeBox.getSelectionModel().select("QUADRANTS");
        iQBox.getItems().addAll("IN_PHASE", "QUADRATURE");
        iQBox.getSelectionModel().select("IN_PHASE");
        inPortADCBox.getItems().addAll("IA", "VA");
        inPortADCBox.getSelectionModel().select("IA");
        nDataBox.getItems().addAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
        nDataBox.getSelectionModel().select(0);
        multiplierBox.getItems().addAll(-21, -18, -15, -12, -9, -6, -3, 0, 3, 6, 9, 12, 15, 18, 21);
        multiplierBox.getSelectionModel().select(7);
        measureUnitBox.getItems().addAll("O", "F", "H", "C", "%", "V",
                "A", "L", "t");
        measureUnitBox.getSelectionModel().select("O");

        //Added Listner to measureTechniqueBox selection change.
        measureTechniqueBox.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue.equals("DIRECT")) {
                        directSetted(true);

                    } else {
                        directSetted(false);
                    }
                });
    }

    public void directSetted(boolean set) {
        rSenseBox.setDisable(set);
        inGainBox.setDisable(set);
        outGainBox.setDisable(set);
        contactsBox.setDisable(set);
        frequencyField.setDisable(set);
        harmonicBox.setDisable(set);
        dcBiasField.setDisable(set);
        modeVIBox.setDisable(set);
        phaseShiftModeBox.setDisable(set);
        phaseShiftField.setDisable(set);
        iQBox.setDisable(set);
        nameField.setDisable(set);
        measureTypeBox.setDisable(set);
        direct = set;
    }

    public boolean isDirectSetted() {
        return direct;
    }

//    public void setMain(Main main, boolean check) {
//        this.main = main;
//        this.check = check;
//    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setSensingElement(SensingElement se) {

        this.se = se;

        if (se.getMeasureTechnique().equals("DIRECT")) {
            directSetted(true);
        }

        idField.setText(se.getIdSPSensingElement());

        if (rSenseBox.getItems().contains(se.getRSense()) && !rSenseBox.isDisable()) {
            rSenseBox.setValue(se.getRSense());
        }

        if (inGainBox.getItems().contains(se.getInGain()) && !inGainBox.isDisable()) {
            inGainBox.setValue(se.getInGain());
        }

        if (outGainBox.getItems().contains(se.getOutGain()) && !outGainBox.isDisable()) {
            outGainBox.setValue(se.getOutGain());
        }

        if (contactsBox.getItems().contains(se.getContacts()) && !contactsBox.isDisable()) {
            contactsBox.setValue(se.getContacts());
        }

        if (!frequencyField.isDisable()) {
            frequencyField.setText(se.getFrequency() == Integer.MIN_VALUE ? "" : se.getFrequency().toString());
        }
        if (harmonicBox.getItems().contains(se.getHarmonic()) && !harmonicBox.isDisable()) {
            harmonicBox.setValue(se.getHarmonic());
        }
        if (!dcBiasField.isDisable()) {
            dcBiasField.setText(se.getDcBias() == Integer.MIN_VALUE ? "" : se.getDcBias().toString());
        }
        if (modeVIBox.getItems().contains(se.getModeVI()) && !modeVIBox.isDisable()) {
            modeVIBox.setValue(se.getModeVI());
        }

        if (measureTechniqueBox.getItems().contains(se.getMeasureTechnique())) {
            measureTechniqueBox.setValue(se.getMeasureTechnique());
        }

        if (measureTypeBox.getItems().contains(se.getMeasureType()) && !measureTypeBox.isDisable()) {
            measureTypeBox.setValue(se.getMeasureType());
        }

        filterField.setText(se.getFilter() == Integer.MIN_VALUE ? "" : se.getFilter().toString());

        if (phaseShiftModeBox.getItems().contains(se.getPhaseShiftMode()) && phaseShiftModeBox.isDisable()) {
            phaseShiftModeBox.setValue(se.getPhaseShiftMode());
        }

        if (!phaseShiftField.isDisable()) {
            phaseShiftField.setText(se.getPhaseShift() == Integer.MIN_VALUE ? "" : se.getPhaseShift().toString());
        }
        if (iQBox.getItems().contains(se.getIq()) && !iQBox.isDisable()) {
            iQBox.setValue(se.getIq());
        }

        conversionRateField.setText(se.getConversionRate() == Integer.MIN_VALUE ? "" : se.getConversionRate().toString());

        if (inPortADCBox.getItems().contains(se.getInportADC())) {
            inPortADCBox.setValue(se.getInportADC());
        }

        if (nDataBox.getItems().contains(se.getNData())) {
            nDataBox.setValue(se.getNData());
        }

        if (measureUnitBox.getItems().contains(se.getMeasure_Unit())) {
            measureUnitBox.setValue(se.getMeasure_Unit());
        }

        if (!nameField.isDisable()) {
            nameField.setText(se.getName());
        }
        rangeMinField.setText(se.getRangeMin().isNaN() ? "" : se.getRangeMin().toString());
        rangeMaxField.setText(se.getRangeMax().isNaN() ? "" : se.getRangeMax().toString());
        defaultAlarmThresholdField.setText(se.getDefaultAlarmThreshold().isNaN() ? "" : se.getDefaultAlarmThreshold().toString());

        if (multiplierBox.getItems().contains(se.getMultiplier())) {
            multiplierBox.setValue(se.getMultiplier());
        }
    }


    @FXML
    public void okButtonAction() {

        if (checkField(check)) {
            if (isDirectSetted()) {
                if (measureTechniqueBox.getValue().equals("DIRECT") && !se.getMeasureTechnique().equals("DIRECT")) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.initOwner(stage);
                    alert.setTitle("Are you sure?");
                    alert.setHeaderText("WARNING!\nChanging MeasureTechnique to \"DIRECT\" could result in lost of data.");
                    alert.setContentText("Are you sure to continue?");

                    ButtonType buttonTypeOne = new ButtonType("Yes");
                    ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
                    alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() != buttonTypeOne) {
                        measureTechniqueBox.getSelectionModel().select(se.getMeasureTechnique());
                        ok = false;
                        return;
                    }
                }
                se.setidSPSensingElement(idField.getText());
                se.setConversionRate(conversionRateField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(conversionRateField.getText()));
                se.setInportADC(inPortADCBox.getSelectionModel().getSelectedItem());
                se.setnData(nDataBox.getSelectionModel().getSelectedItem());
                se.setMeasureTechnique(measureTechniqueBox.getSelectionModel().getSelectedItem());
                se.setFilter(filterField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(filterField.getText()));
                se.setRangeMin(rangeMinField.getText().isEmpty() ? Double.NaN : Double.parseDouble(rangeMinField.getText()));
                se.setRangeMax(rangeMaxField.getText().isEmpty() ? Double.NaN : Double.parseDouble(rangeMaxField.getText()));
                se.setDefaultAlarmThreshold(defaultAlarmThresholdField.getText().isEmpty() ? Double.NaN : Double.parseDouble(defaultAlarmThresholdField.getText()));
                se.setMultiplier(multiplierBox.getSelectionModel().getSelectedItem());
                se.setMeasure_Unit(measureUnitBox.getSelectionModel().getSelectedItem());

                //The fields listed below are necessary to build the UPDATE query in the case someone would change and
                //existing element from a non DIRECT measureTechnique to a DIRECT measureTechnique. In this case is
                //necessary to set the disabled field to their defaults values to allow the UPDATE query to manage it.

                se.setrSense(Integer.MIN_VALUE);
                se.setContacts("");
                se.setDcBias(Integer.MIN_VALUE);
                se.setFrequency(Integer.MIN_VALUE);
                se.setHarmonic("");
                se.setInGain(Integer.MIN_VALUE);
                se.setIq("");
                se.setMeasureType("");
                se.setModeVI("");
                se.setName("");
                se.setOutGain(Integer.MIN_VALUE);
                se.setPhaseShift(Integer.MIN_VALUE);
                se.setPhaseShiftMode("");

            } else if (!isDirectSetted()) {
                se.setrSense(rSenseBox.getSelectionModel().getSelectedItem());
                se.setContacts(contactsBox.getSelectionModel().getSelectedItem());
                se.setDcBias(dcBiasField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(dcBiasField.getText()));
                se.setFrequency(frequencyField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(frequencyField.getText()));
                se.setHarmonic(harmonicBox.getSelectionModel().getSelectedItem());
                se.setInGain(inGainBox.getSelectionModel().getSelectedItem());
                se.setIq(iQBox.getSelectionModel().getSelectedItem());
                se.setMeasureType(measureTypeBox.getSelectionModel().getSelectedItem());
                se.setModeVI(modeVIBox.getSelectionModel().getSelectedItem());
                se.setName(nameField.getText());
                se.setOutGain(outGainBox.getSelectionModel().getSelectedItem());
                se.setPhaseShift(phaseShiftField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(phaseShiftField.getText()));
                se.setPhaseShiftMode(phaseShiftModeBox.getSelectionModel().getSelectedItem());
                se.setidSPSensingElement(idField.getText());
                se.setConversionRate(conversionRateField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(conversionRateField.getText()));
                se.setInportADC(inPortADCBox.getSelectionModel().getSelectedItem());
                se.setnData(nDataBox.getSelectionModel().getSelectedItem());
                se.setMeasureTechnique(measureTechniqueBox.getSelectionModel().getSelectedItem());
                se.setFilter(filterField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(filterField.getText()));
                se.setRangeMin(rangeMinField.getText().isEmpty() ? Double.NaN : Double.parseDouble(rangeMinField.getText()));
                se.setRangeMax(rangeMaxField.getText().isEmpty() ? Double.NaN : Double.parseDouble(rangeMaxField.getText()));
                se.setDefaultAlarmThreshold(defaultAlarmThresholdField.getText().isEmpty() ? Double.NaN : Double.parseDouble(defaultAlarmThresholdField.getText()));
                se.setMultiplier(multiplierBox.getSelectionModel().getSelectedItem());
                se.setMeasure_Unit(measureUnitBox.getSelectionModel().getSelectedItem());
            }

            ok = true;
            stage.close();
        } else {
            ok = false;
        }
    }

    @FXML
    public void cancelButtonAction() {
        stage.close();
    }

    public boolean okClicked() {
        return ok;
    }

    private boolean checkField(boolean check) {
        String error = "";

        if (check) {

            error += findError(idField, null, null, null, null, "IdSPSensingElement");

            if (!isDirectSetted()) {
                //i campi relativi a Rsense, in/outGain, Contacts, Harmonic, ModeVI, MeasureType, PhaseShiftMode, IQ, non sono
                //elencati in quanto i valori vengono scelti da una combo box con valori predefiniti e quindi sempre(?) validi
                error += findError(frequencyField, frequencyMIN, frequencyMAX, null, null, "Frequency");
                error += findError(dcBiasField, dcBiasMIN, dcBiasMAX, null, null, "DCBias");
                error += findError(phaseShiftField, phaseShiftMIN, phaseShiftMAX, null, null, "PhaseShift");
                error += findError(nameField, null, null, null, null, "Name");

            }
            error += findError(filterField, filterMIN, filterMAX, null, null, "Filter");
            error += findError(conversionRateField, conversionRateMIN, conversionRateMAX, null, null, "ConversionRate");
            error += findError(rangeMinField, null, null, Double.NaN, Double.NaN, "RangeMin");
            error += findError(rangeMaxField, null, null, Double.NaN, Double.NaN, "RangeMax");
            error += findError(defaultAlarmThresholdField, null, null, Double.NaN, Double.NaN, "DefaultAlarmThreshold");
            if (error.length() == 0) {
                if (Double.parseDouble(rangeMaxField.getText()) < Double.parseDouble(rangeMinField.getText())) {
                    error += "RangeMax must be greater than RangeMin.";
                }
            }

            if (error.length() == 0) {
                return true;
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(stage);
                alert.setTitle("Invalid fields");
                alert.setHeaderText("Please correct invalid fields");
                alert.setContentText(error);

                alert.showAndWait();

                return false;
            }
        } else {
            return true;
        }
    }


    private String findError(TextField textField, Integer rangeMin, Integer rangeMax, Double rangeDMin, Double rangeDMax, String fieldName) {
        String error = "";
        try {
            if (textField.getText().equals("")) {
                error += fieldName + " can not be empty.\n";
            } else if (rangeMin != null && rangeMax != null) {
                if (Integer.parseInt(textField.getText()) < rangeMin ||
                        Integer.parseInt(textField.getText()) > rangeMax) {
                    error += fieldName + " must be between " + rangeMin + " and " + rangeMax + ".\n";
                }
            } else if (rangeDMin != null && rangeDMax != null) {
                if (Double.parseDouble(textField.getText()) < rangeDMin ||
                        Double.parseDouble(textField.getText()) > rangeDMax) {
                    error += fieldName + " must be between " + rangeDMin + " and " + rangeDMax + ".\n";
                }
            }
        } catch (NumberFormatException e) {
            error += fieldName + " msut be a number.\n";
        }
        return error;
    }

}