package com.sensichips.sensiplus.demoapp.winux.gui;

import com.sensichips.sensiplus.model.SensingElement;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SensingElementSearchController {
    private final static int dcBiasMIN = -2048;
    private final static int dcBiasMAX = 2048;
    private final static int frequencyMIN = 0;
    private final static int frequencyMAX = 5000000;
    private final static int phaseShiftMIN = 0;
    private final static int phaseShiftMAX = 360;
    private final static int filterMIN = 1;
    private final static int filterMAX = 256;
    private final static int conversionRateMIN = 1;
    private final static int conversionRateMAX = 100000;

    private boolean check = false;
    private SensingElement se;
    private boolean ok = false;
    private Stage stage;
    private boolean direct = false;
    private boolean cambiato = true;

    @FXML
    private TextField idField;
    @FXML
    private ComboBox<String> rSenseBox;
    @FXML
    private ComboBox<String> inGainBox;
    @FXML
    private ComboBox<String> outGainBox;
    @FXML
    private ComboBox<String> contactsBox;
    @FXML
    private TextField frequencyField;
    @FXML
    private ComboBox<String> harmonicBox;
    @FXML
    private TextField dcBiasField;
    @FXML
    private ComboBox<String> modeVIBox;
    @FXML
    private ComboBox<String> measureTechniqueBox;
    @FXML
    private ComboBox<String> measureTypeBox;
    @FXML
    private TextField filterField;
    @FXML
    private ComboBox<String> phaseShiftModeBox;
    @FXML
    private TextField phaseShiftField;
    @FXML
    private ComboBox<String> iQBox;
    @FXML
    private TextField conversionRateField;
    @FXML
    private ComboBox<String> inPortADCBox;
    @FXML
    private ComboBox<String> nDataBox;
    @FXML
    private ComboBox<String> measureUnitBox;
    @FXML
    private TextField nameField;
    @FXML
    private TextField rangeMinField;
    @FXML
    private TextField rangeMaxField;
    @FXML
    private TextField defaultAlarmThresholdField;
    @FXML
    private ComboBox<String> multiplierBox;


    private void setListner(TextField textField, String regex, String replacement) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > oldValue.length()) {
                if ((newValue.endsWith(".") && oldValue.contains(".") && cambiato) || (newValue.endsWith(".") && (newValue.length() == 1 || oldValue.endsWith("-")))) {
                    cambiato = false;
                    textField.setText(oldValue);
                } else if ((newValue.endsWith("-") && oldValue.contains("-")) || (newValue.endsWith("-") && newValue.length()!=1)) {
                    cambiato = false;
                    textField.setText(oldValue);
                } else {
                    cambiato = false;
                    textField.setText(newValue.replaceAll(regex, replacement));
                }

            }
            cambiato = true;
        });
    }

    @FXML
    public void initialize() {
        /*
            [] means one of these..
            [^ ..] means (not one of these)
            \  is escape character (because in Java you need to escape it)
            \d   means digit
            in !newValue.matches("\\d"); stiamo dicendo di prendere tutti i caratteri che non sono numeri, perche \d indica tutti i numeri
            il doppio \ serve perche 1 viene ignorato (escape character)
            quindi replaceAll("[^\\d]", ""); in pratica rimpiazza tutto cioè che non è un numero con ""
            questo perche \\d indica tutti i numeri il simbolo ^\\d significa tutto ciè che non è un numero
            ed infine con le [] indica che dobbiamo prendere tutti i caratteri presenti in newValue che non sono dei numeri
        */
        setListner(frequencyField, "[^\\d]", "");
        setListner(dcBiasField, "[^\\d\\-]", "");
        setListner(filterField, "[^\\d]", "");
        setListner(phaseShiftField, "[^\\d]", "");
        setListner(conversionRateField, "[^\\d]", "");
        //in questo caso essendo il campo di tipo Double allora dobbiamo dare la possibilita di immettere il '.'
        //e quindi diciamo di rimppiazzare tutto cio che non è [^ ] un numero [^\\d] e non è il '.' [^\\d\\.]
        setListner(rangeMinField, "[^\\d\\.\\-]", "");
        setListner(rangeMaxField, "[^\\d\\.\\-]", "");
        setListner(defaultAlarmThresholdField, "[^\\d\\.\\-]", "");


        rSenseBox.getItems().addAll("ALL", "50", "500", "5000", "50000");
        rSenseBox.getSelectionModel().select("ALL");
        inGainBox.getItems().addAll("ALL", "1", "12", "20", "40");
        inGainBox.getSelectionModel().select("ALL");
        outGainBox.getItems().addAll("ALL", "0", "1", "2", "3", "4", "5", "6", "7");
        outGainBox.getSelectionModel().select("ALL");
        contactsBox.getItems().addAll("ALL", "TWO", "FOUR");
        contactsBox.getSelectionModel().select("ALL");
        harmonicBox.getItems().addAll("ALL", "FIRST_HARMONIC", "SECOND_HARMONIC", "THIRD_HARMONIC");
        harmonicBox.getSelectionModel().select("ALL");
        modeVIBox.getItems().addAll("ALL", "VOUT_IIN", "VIN_IIN", "VOUT_VIN", "VOUT_VOUT");
        modeVIBox.getSelectionModel().select("ALL");
        measureTechniqueBox.getItems().addAll("ALL", "DIRECT", "EIS", "POT", "ENERGY_SPECTROSCOPY", "ULTRASOUND");
        measureTechniqueBox.getSelectionModel().select("ALL");
        measureTypeBox.getItems().addAll("ALL", "QUADRATURE", "MODULE", "PHASE", "RESISTENCE", "CAPACITANCE", "INDUCTANCE");
        measureTypeBox.getSelectionModel().select("ALL");
        phaseShiftModeBox.getItems().addAll("ALL", "QUADRANTS", "COARSE", "FINE");
        phaseShiftModeBox.getSelectionModel().select("ALL");
        iQBox.getItems().addAll("ALL", "IN_PHASE", "QUADRATURE");
        iQBox.getSelectionModel().select("ALL");
        inPortADCBox.getItems().addAll("ALL", "IA", "VA");
        inPortADCBox.getSelectionModel().select("ALL");
        nDataBox.getItems().addAll("ALL", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16");
        nDataBox.getSelectionModel().select("ALL");
        multiplierBox.getItems().addAll("ALL", "-21", "-18", "-15", "-12", "-9", "-6", "-3", "0", "3", "6", "9", "12", "15", "18", "21");
        multiplierBox.getSelectionModel().select("ALL");
        measureUnitBox.getItems().addAll("ALL", "O=Ohm", "F=Farad", "H=Henry", "C=Celsius", "%=relative", "V=Voltage",
                "A=Current", "L=Lumen", "t=time");
        measureUnitBox.getSelectionModel().select("ALL");

        //Added Listner to measureTechniqueBox selection change.
        measureTechniqueBox.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (measureTechniqueBox.getSelectionModel().getSelectedItem().equals("DIRECT")) {
                        directSetted(true);
                    } else {
                        directSetted(false);
                    }
                });
    }

    public void directSetted(boolean set) {
        rSenseBox.setDisable(set);
        inGainBox.setDisable(set);
        outGainBox.setDisable(set);
        contactsBox.setDisable(set);
        frequencyField.setDisable(set);
        harmonicBox.setDisable(set);
        dcBiasField.setDisable(set);
        modeVIBox.setDisable(set);
        phaseShiftModeBox.setDisable(set);
        phaseShiftField.setDisable(set);
        iQBox.setDisable(set);
        nameField.setDisable(set);
        measureTypeBox.setDisable(set);
        direct = set;
    }

    public boolean isDirectSetted() {
        return direct;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setSensingElement(SensingElement se) {
        this.se = se;
    }

    @FXML
    public void okButtonAction() {
        if (checkField(check)) {
            if (isDirectSetted()) {
                se.setidSPSensingElement(idField.getText());
                se.setConversionRate(conversionRateField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(conversionRateField.getText()));
                se.setInportADC(inPortADCBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : inPortADCBox.getSelectionModel().getSelectedItem());
                se.setnData(nDataBox.getSelectionModel().getSelectedItem().equals("ALL") ? Integer.MIN_VALUE : Integer.parseInt(nDataBox.getSelectionModel().getSelectedItem()));
                se.setMeasureTechnique(measureTechniqueBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : measureTechniqueBox.getSelectionModel().getSelectedItem());
                se.setFilter(filterField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(filterField.getText()));
                se.setRangeMin(rangeMinField.getText().isEmpty() ? Double.NaN : Double.parseDouble(rangeMinField.getText()));
                se.setRangeMax(rangeMaxField.getText().isEmpty() ? Double.NaN : Double.parseDouble(rangeMaxField.getText()));
                se.setDefaultAlarmThreshold(defaultAlarmThresholdField.getText().isEmpty() ? Double.NaN : Double.parseDouble(defaultAlarmThresholdField.getText()));
                se.setMultiplier((multiplierBox.getSelectionModel().getSelectedItem().equals("ALL")) ? Integer.MIN_VALUE : Integer.parseInt(multiplierBox.getSelectionModel().getSelectedItem()));
                se.setMeasure_Unit(measureUnitBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : measureUnitBox.getSelectionModel().getSelectedItem());

                //The fields listed below are necessary to build the UPDATE query in the case someone would change and
                // existing element from a non DIRECT measureTechnique to a DIRECT measureTechnique. In this case is
                //necessary to set the disabled field to their defaults values to allow the UPDATE query to manage it.

                se.setrSense(Integer.MIN_VALUE);
                se.setContacts("");
                se.setDcBias(Integer.MIN_VALUE);
                se.setFrequency(Integer.MIN_VALUE);
                se.setHarmonic("");
                se.setInGain(Integer.MIN_VALUE);
                se.setIq("");
                se.setMeasureType("");
                se.setModeVI("");
                se.setName("");
                se.setOutGain(Integer.MIN_VALUE);
                se.setPhaseShift(Integer.MIN_VALUE);
                se.setPhaseShiftMode("");

            } else if (!isDirectSetted()) {
                se.setidSPSensingElement(idField.getText());
                se.setrSense(((rSenseBox.getSelectionModel().getSelectedItem().equals("ALL")) ? Integer.MIN_VALUE : Integer.parseInt(rSenseBox.getSelectionModel().getSelectedItem())));
                se.setContacts(contactsBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : contactsBox.getSelectionModel().getSelectedItem());
                se.setConversionRate(conversionRateField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(conversionRateField.getText()));
                se.setDcBias(dcBiasField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(dcBiasField.getText()));
                se.setMultiplier((multiplierBox.getSelectionModel().getSelectedItem().equals("ALL")) ? Integer.MIN_VALUE : Integer.parseInt(multiplierBox.getSelectionModel().getSelectedItem()));
                se.setDefaultAlarmThreshold(defaultAlarmThresholdField.getText().isEmpty() ? Double.NaN : Double.parseDouble(defaultAlarmThresholdField.getText()));
                se.setFilter(filterField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(filterField.getText()));
                se.setFrequency(frequencyField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(frequencyField.getText()));
                se.setHarmonic((harmonicBox.getSelectionModel().getSelectedItem().equals("ALL")) ? "" : harmonicBox.getSelectionModel().getSelectedItem());
                se.setInGain(inGainBox.getSelectionModel().getSelectedItem().equals("ALL") ? Integer.MIN_VALUE : Integer.parseInt(inGainBox.getSelectionModel().getSelectedItem()));
                se.setInportADC(inPortADCBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : inPortADCBox.getSelectionModel().getSelectedItem());
                se.setIq(iQBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : iQBox.getSelectionModel().getSelectedItem());
                se.setMeasure_Unit(measureUnitBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : measureUnitBox.getSelectionModel().getSelectedItem());
                se.setMeasureTechnique(measureTechniqueBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : measureTechniqueBox.getSelectionModel().getSelectedItem());
                se.setMeasureType(measureTypeBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : measureTypeBox.getSelectionModel().getSelectedItem());
                se.setModeVI(modeVIBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : modeVIBox.getSelectionModel().getSelectedItem());
                se.setName(nameField.getText());
                se.setnData(nDataBox.getSelectionModel().getSelectedItem().equals("ALL") ? Integer.MIN_VALUE : Integer.parseInt(nDataBox.getSelectionModel().getSelectedItem()));
                se.setOutGain(outGainBox.getSelectionModel().getSelectedItem().equals("ALL") ? Integer.MIN_VALUE : Integer.parseInt(outGainBox.getSelectionModel().getSelectedItem()));
                se.setPhaseShift(phaseShiftField.getText().isEmpty() ? Integer.MIN_VALUE : Integer.parseInt(phaseShiftField.getText()));
                se.setPhaseShiftMode(phaseShiftModeBox.getSelectionModel().getSelectedItem().equals("ALL") ? "" : phaseShiftModeBox.getSelectionModel().getSelectedItem());
                se.setRangeMax(rangeMaxField.getText().isEmpty() ? Double.NaN : Double.parseDouble(rangeMaxField.getText()));
                se.setRangeMin(rangeMinField.getText().isEmpty() ? Double.NaN : Double.parseDouble(rangeMinField.getText()));
            }

            ok = true;
            stage.close();
        } else {
            ok = false;
        }
    }

    @FXML
    public void cancelButtonAction() {
        stage.close();
    }

    public boolean okClicked() {
        return ok;
    }

    private boolean checkField(boolean check) {
        String error = "";

        if (check) {

            error += findError(idField, null, null, null, null, "IdSPSensingElement");
            error += findError(frequencyField, frequencyMIN, frequencyMAX, null, null, "Frequency");
            if (!isDirectSetted()) {
                //i campi relativi a Rsense, in/outGain, Contacts, Harmonic, ModeVI, MeasureType, PhaseShiftMode, IQ, non sono
                //elencati in quanto i valori vengono scelti da una combo box con valori predefiniti e quindi sempre(?) validi
                error += findError(dcBiasField, dcBiasMIN, dcBiasMAX, null, null, "DCBias");
                error += findError(phaseShiftField, phaseShiftMIN, phaseShiftMAX, null, null, "PhaseShift");
                error += findError(nameField, null, null, null, null, "Name");

            }
            error += findError(filterField, filterMIN, filterMAX, null, null, "Filter");
            error += findError(conversionRateField, conversionRateMIN, conversionRateMAX, null, null, "ConversionRate");
            error += findError(rangeMinField, null, null, Double.NaN, Double.NaN, "RangeMin");
            error += findError(rangeMaxField, null, null, Double.NaN, Double.NaN, "RangeMax");
            error += findError(defaultAlarmThresholdField, null, null, Double.NaN, Double.NaN, "DefaultAlarmThreshold");
            if (error.length() == 0) {
                if (Double.parseDouble(rangeMaxField.getText()) < Double.parseDouble(rangeMinField.getText())) {
                    error += "RangeMax must be greater than RangeMin.";
                }
            }

            if (error.length() == 0) {
                return true;
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(stage);
                alert.setTitle("Invalid fields");
                alert.setHeaderText("Please correct invalid fields");
                alert.setContentText(error);

                alert.showAndWait();

                return false;
            }
        } else {
            return true;
        }
    }


    private String findError(TextField textField, Integer rangeMin, Integer rangeMax, Double rangeDMin, Double rangeDMax, String fieldName) {
        String error = "";
        try {
            if (textField.getText().equals("")) {
                error += fieldName + " can not be empty.\n";
            } else if (rangeMin != null && rangeMax != null) {
                if (Integer.parseInt(textField.getText()) < rangeMin ||
                        Integer.parseInt(textField.getText()) > rangeMax) {
                    error += fieldName + " must be between " + rangeMin + " and " + rangeMax + ".\n";
                }
            } else if (rangeDMin != null && rangeDMax != null){
                if (Double.parseDouble(textField.getText()) < rangeDMin ||
                        Double.parseDouble(textField.getText()) > rangeDMax) {
                    error += fieldName + " must be between " + rangeDMin + " and " + rangeDMax + ".\n";
                }
            }
        } catch (
                NumberFormatException e)

        {
            error += fieldName + " msut be a number.\n";
        }
        return error;
    }
}
