package com.sensichips.sensiplus.demoapp.winux;
import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.demoapp.winux.gui.ConfigCreator;
import com.sensichips.sensiplus.demoapp.winux.gui.ConfigCreatorController;
import com.sensichips.sensiplus.demoapp.winux.gui.Controller;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMBLE;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_COMUSB;
import com.sensichips.sensiplus.level0.driver.winux.SPDriverWINUX_IP;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.util.batch.SPBatchExecutor;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import com.sensichips.sensiplus.util.winux.SPBatchCommandLineAnalyzer;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.concurrent.*;
import javafx.fxml.FXMLLoader;
import javafx.geometry.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.*;
import javafx.util.Duration;
import jssc.SerialPortList;
import sun.misc.Signal;
import sun.misc.SignalHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import static com.sensichips.sensiplus.config.SPConfigurationManager.setDefaultSPConfiguration;

/**
 * Example of displaying a splash page for a standalone JavaFX application
 */
public class SPWinuxToolsSplash extends Application implements SPConfigurationManager.SPDriverGenerator {
    //public static final String APPLICATION_ICON =
    //        "http://cdn1.iconfinder.com/data/icons/Copenhagen/PNG/32/people.png";
    //public static final String APPLICATION_ICON = "file:/images/ic_launcher.png";
    //public static String APPLICATION_ICON; // = this.getClass().getResource("/com/sensichips/sensiplus/resources/ic_launcher.png").toExternalForm();
    //public static String SPLASH_IMAGE; // = "file:/images/splash.png";
    //        "http://fxexperience.com/wp-content/uploads/2010/06/logo.png";


    private static SPWinuxToolsSplash mainGUI;




    public static String MAIN_TITLE = "SENSIPLUS WinuxGUI - Unicas for Sensichips, Ver 0.4";
    public static String LOG_MESSAGE = "SPWinuxTools";
    private static int DEBUG_LEVEL_ERROR = SPLoggerInterface.DEBUG_VERBOSITY_ERROR;


    private Pane splashLayout;
    private ProgressBar loadProgress;
    private Label progressText;
    private Stage mainStage;
    private static final int SPLASH_WIDTH = 1200;
    private static final int SPLASH_HEIGHT = 250;

    private Stage primaryStage;

    private Parent root;
    private Controller controller;
    public static SPBatchCommandLineAnalyzer command;

    public static void main(String[] args) {

        try {

            // Analyze command line parameters
            command = new SPBatchCommandLineAnalyzer(args);

            if (!command.getBatchParameters().valid){
                System.exit(-1);
            }

            if(command.getBatchParameters().gui){
                launch(args);
            } else {
                startWithoutGUI(command);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(-1);
    }



    private static void startWithoutGUI(SPBatchCommandLineAnalyzer command) throws Exception {
        SPWinuxTools winuxTools = new SPWinuxTools(command.getBatchParameters());

        SPConfigurationManager.loadConfigurationsFromFile(new FileInputStream(new File(command.getBatchParameters().layoutConfigurationFileXML)));
        //SPConfigurationManager.setDefaultSPConfiguration("WINUX_COMUSB-CLUSTERID_0X02-FT232RL_SENSIBUS-ShortAddress-ALL_CHIP(RUN5)");


        verifySerialPort();


        SPConfiguration spConfiguration = SPConfigurationManager.getSPConfiguration(command.getBatchParameters().currentConfiguration);
        //spConfiguration.startConfiguration(this);
        setDefaultSPConfiguration(spConfiguration);



        //SPConfigurationManager.setDefaultSPConfiguration(command.getBatchParameters().currentConfiguration);

        Thread.sleep(500);
        SPConfigurationManager.getSPConfigurationDefault().turnOffGreenLed();

        // Load configurations
        SPBatchExecutor spBatchExecutor = new SPBatchExecutor(command.getBatchParameters(), winuxTools, SPConfigurationManager.getSPConfigurationDefault());
        if (spBatchExecutor.getExperimentList().size() <= 0){
            throw new SPException("Any experiment defined in " + command.getBatchParameters().batchFile);
        }

        // Generate executor
        winuxTools.setSpBatchExecutor(spBatchExecutor);

        // Execute experiments
        spBatchExecutor.executeExperiments();


        Signal.handle(new Signal("INT"), new SignalHandler() {
            final SPProtocol prot = SPConfigurationManager.getSPConfigurationDefault().getSPProtocol();
            public void handle(Signal signal) {
                try {
                    SPLogger.getLogInterface().d(LOG_MESSAGE,"\nSENSIPLUS powering off...\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                    prot.closeAll();
                } catch (SPException e) {
                    e.printStackTrace();
                }
                SPLogger.getLogInterface().d(LOG_MESSAGE,"Program closed with ctrl+c", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                System.exit(-1);
            }
        });
    }


    private static void verifySerialPort() throws Exception {
        String[] portNames = SerialPortList.getPortNames();
        if (portNames.length > 0){
            System.out.println("SERIAL PORT AVAILABLE:");
            for(int i = 0; i < portNames.length; i++){
                System.out.println("\t" + (i + 1) + ". " + portNames[i]);
            }
        } else {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "ANY DEVICE FOUND:", DEBUG_LEVEL_ERROR);
            SPLogger.getLogInterface().d(LOG_MESSAGE, "On Linux please verify if /dev/ttyUSBX is available", DEBUG_LEVEL_ERROR);
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Try also with these settings: sudo chown 777 /dev/ttyUSBX", DEBUG_LEVEL_ERROR);
            throw new Exception("ANY DEVICE FOUND, THE PROGRAM WILL EXIT!!");

        }
    }

    public static SPWinuxToolsSplash getMainGUI(){
        return mainGUI;
    }

    public ConfigCreator showSPConfigurationCreator(ConfigCreator configCreator) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(SPWinuxToolsSplash.class.getResource("gui/ConfigCreator.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Modify Configuration");
            dialogStage.initModality((Modality.WINDOW_MODAL));
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);


            // Set the colleghi into the controller.
            ConfigCreatorController controller = loader.getController();
            controller.setStage(dialogStage);
            controller.setSPDriverGenerator(this);
            controller.setConfigCreator(configCreator);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();


            return controller.getConfigCreator();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void init() {

        mainGUI = this;

        SPWinuxTools.APPLICATION_ICON = this.getClass().getResource("/com/sensichips/sensiplus/resources/ic_launcher.png").toExternalForm();
        SPWinuxTools.SPLASH_IMAGE = this.getClass().getResource("/com/sensichips/sensiplus/resources/splash.png").toExternalForm();


        ImageView splash = new ImageView(new Image(
                SPWinuxTools.SPLASH_IMAGE
        ));
        loadProgress = new ProgressBar();
        loadProgress.setPrefWidth(SPLASH_WIDTH);
        progressText = new Label("Will find friends for peanuts . . .");
        splashLayout = new VBox();
        splashLayout.getChildren().addAll(splash, loadProgress, progressText);
        progressText.setAlignment(Pos.CENTER);
        splashLayout.setStyle(
                "-fx-padding: 5; " +
                        "-fx-background-color: black; " +
                        "-fx-border-width:5; " +
                        "-fx-border-color: " +
                        "linear-gradient(" +
                        "to bottom, " +
                        "darkolivegreen, " +
                        "derive(darkolivegreen, 50%)" +
                        ");"
        );
        splashLayout.setEffect(new DropShadow());
    }

    @Override
    public void start(final Stage initStage) throws Exception {

        initStage.getIcons().add(new Image(SPWinuxTools.APPLICATION_ICON));
        primaryStage = initStage;

        final Task<Boolean> friendTask = new Task<Boolean>() {
            @Override
            protected Boolean call() throws InterruptedException {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("gui/maingui.fxml"));
                updateMessage("Generating GUI");

                try {
                    root = (Parent)loader.load();
                    controller = loader.getController();
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }

                updateMessage("GUI generated");

                return true;
            }
        };

        showSplash(
                initStage,
                friendTask,
                () -> showMainStage(friendTask.valueProperty())
        );
        new Thread(friendTask).start();
    }

    private void showMainStage(
            ReadOnlyObjectProperty<Boolean> friends
    ) {
        mainStage = new Stage(StageStyle.DECORATED);

        mainStage.setResizable(false);
        mainStage.setTitle(MAIN_TITLE);
        mainStage.setScene(new Scene(root));
        mainStage.getIcons().add(new Image(SPWinuxTools.APPLICATION_ICON));

        controller.setStageAndSetupListeners(mainStage);

        mainStage.show();

    }

    private void showSplash(
            final Stage initStage,
            Task<?> task,
            InitCompletionHandler initCompletionHandler
    ) {
        progressText.textProperty().bind(task.messageProperty());
        loadProgress.progressProperty().bind(task.progressProperty());
        task.stateProperty().addListener((observableValue, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                loadProgress.progressProperty().unbind();
                loadProgress.setProgress(1);
                initStage.toFront();
                FadeTransition fadeSplash = new FadeTransition(Duration.seconds(1.2), splashLayout);
                fadeSplash.setFromValue(1.0);
                fadeSplash.setToValue(0.0);
                fadeSplash.setOnFinished(actionEvent -> initStage.hide());
                fadeSplash.play();

                initCompletionHandler.complete();
            } // todo add code to gracefully handle other task states.
        });

        Scene splashScene = new Scene(splashLayout, Color.TRANSPARENT);
        final Rectangle2D bounds = Screen.getPrimary().getBounds();
        initStage.setScene(splashScene);
        initStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
        initStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);
        initStage.initStyle(StageStyle.TRANSPARENT);
        initStage.setAlwaysOnTop(true);
        initStage.show();
    }

    public interface InitCompletionHandler {
        void complete();
    }

    @Override
    public String generateDriver(String driverName, ArrayList<String> driverParameters) throws SPDriverException {
        SPDriver sp;
        if (driverName.equals(SPDriverWINUX_COMUSB.DRIVERNAME)){
            sp = new SPDriverWINUX_COMUSB(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_COMBLE.DRIVERNAME)){
            sp = new SPDriverWINUX_COMBLE(driverParameters);
        } else if (driverName.equals(SPDriverWINUX_IP.DRIVERNAME)){
            sp = new SPDriverWINUX_IP(driverParameters);
        } else {
            throw new SPDriverException("Wrong driverName in generateDriver in SPWinuxTools");
        }
        SPDriverManager.add(sp);
        return sp.getDriverKey();
    }


}
