package com.sensichips.sensiplus.level0;

import java.util.ArrayList;

/**
 * LEVEL 0 contains two sublevels: SPProtocol and SPDriver. SPProtocol control the interaction with the bridge connected
 * to the SENSIPLUS. Two known bridges are used at moment: the USB based buspirate v.3.6 (Dangerous Prototype)
 * and BLE based TI cc2541 (Texas Instruments).
 * This protocols: initialize the bridge in order to communicate with the chip and
 * adapt the general instruction format (e.g. [0x03 r r r r] or [0x48 0x03]) into a suitable send and read byte
 * commands made available by the SPDriver class.
 *
 * SPProtocol stay on SPDriver level. The main method available from SPDriver are read and send.
 * These two methods work on a bytes level and manage the physical channel adapting the general instruction format
 * (e.g. [0x03 r r r r] or [0x48 0x03]) into one or more send/read instructions.
 *
 * LEVEL 0 includes the interface driver between the host and the SENSIPLUS chip. This level implements a
 * chip abstraction layer that allows to send/receive bytes to/from the chip ignoring the specific hardware
 * interface I2C, SPI or SENSIBUS being accessed either directly or through an intermediate bridge interface.
 * This level also turns read accesses to the chip synchronous with the host access even if the connection
 * channel is asyncrhonous (for example USB on Android). Synchronous host access
 * allows to implement a blocking read with a user specified number of bytes that releases control to the host
 * at access completion. A timeout notification is included if access duration exceeds a user defined value.
 * As anticipated this level includes support for a bridge interface between the host and the SENSIPLUS chip,
 * this layer can be removed if instead the chip interfaces directly to the host or can be replaced
 * if the connection is realized with other standards (BT, BLE, ecc.).
 *
 * Property: Sensichips s.r.l.
 *
 * @author: Mario Molinara, mario.molinara@sensichips.com, m.molinara@unicas.it
 */
public abstract class SPDriver {

    public static String LOG_MESSAGE = "SPDriver";

    protected ArrayList<SPDriverListener> spDriverListenerAdapters = new ArrayList<>();

    public SPDriver() throws SPDriverException {
        super();
    }

    /**
     * Verify if low level connection is active
     * @return true if connection is up.
     * @throws SPDriverException
     */
    public abstract boolean isConnected() throws SPDriverException;


    /**
     * Return the timeout for the protocol
     * @return
     */
    public abstract long getSPDriverTimeOut();


    /**
     * Open low level connection
     *
     * @throws SPDriverException
     */
    public abstract void openConnection() throws SPDriverException;


    public boolean equals(SPDriver spDriver){
        return this.getDriverName().equals(spDriver.getDriverName());
    }



    /**
     * Receive bytes on USB at the low possible level
     *
     * @param buf with this buffer the function return the received bytes to the calling function.
     * @return the number received bytes
     */
    public abstract int read(byte[] buf) throws SPDriverException;

    /**
     * Send bytes on USB at the low possible level
     *
     * @param buf all the bytes into te buf array will be send on the USB channel
     * @return the number of bytes effectively sent
     */
    public abstract int write(byte[] buf) throws SPDriverException;



    public abstract String getDriverKey();

    /**
     * Close low level connection.
     *
     * @throws SPDriverException
     */
    public abstract void closeConnection() throws SPDriverException;


    public abstract void setParameters(ArrayList<String> driverParameters) throws SPDriverException;


    /**
     * Return the driver name
     * @return driver name
     * @throws SPDriverException
     */
    public abstract String getDriverName();


    public void addSPDriverListener(SPDriverListener listener){
        spDriverListenerAdapters.add(listener);
    }

}
