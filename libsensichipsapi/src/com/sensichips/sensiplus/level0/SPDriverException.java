package com.sensichips.sensiplus.level0;

import com.sensichips.sensiplus.SPException;

public class SPDriverException extends SPException {
    public SPDriverException(String message) {
        super(message);
    }
}
