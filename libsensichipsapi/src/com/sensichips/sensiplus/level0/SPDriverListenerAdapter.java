package com.sensichips.sensiplus.level0;


import com.sensichips.sensiplus.level1.SPProtocolListener;

/**
 * Created by mario on 25/09/16.
 */

public abstract class SPDriverListenerAdapter implements SPDriverListener {

    protected SPProtocolListener spProtocolListener;
    //protected SPDriver spDriver;

    public SPDriverListenerAdapter() throws SPDriverException {
        super();
    }

    public SPDriverListenerAdapter(SPProtocolListener spProtocolListener) throws SPDriverException {
        super();
    }

    void setSPProtocolListener(SPProtocolListener spProtocolListener){
        this.spProtocolListener = spProtocolListener;
    }


    @Override
    public void message(String m) {
        spProtocolListener.message(m);
    }

    @Override
    public void connectionDown() {
        spProtocolListener.connectionDown();
    }

    @Override
    public void interrupt(byte interruptValue) {
        spProtocolListener.interrupt(interruptValue);
    }

    @Override
    public void fatalError(String message) {
        spProtocolListener.fatalError(message);
    }



}
