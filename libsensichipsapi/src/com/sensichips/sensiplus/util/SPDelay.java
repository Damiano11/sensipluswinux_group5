package com.sensichips.sensiplus.util;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPDelay {
    public static int DELAY_LOW  = 1;

    public static void delay(long delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException ie) {
            //
        }
    }

    public static void main(String args[]) {
        String bitValueToFillWithZero = Integer.toBinaryString(0); // integer 6 bit
        System.out.println(bitValueToFillWithZero);
    }

}
