package com.sensichips.sensiplus.util.batch;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.datareader.queue.SPChartElement;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.protocols.ft232rl.SPProtocolFT232RL_SENSIBUS;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.SPMeasurementRUN4;
import com.sensichips.sensiplus.level2.parameters.*;
import com.sensichips.sensiplus.util.SPDataRepresentation;
import com.sensichips.sensiplus.util.SPDataRepresentationManager;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.classifier.SPClientOnLineClassifier;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import com.sensichips.sensiplus.util.rabbitmq_demo.MeasureToSend;
import com.sensichips.sensiplus.util.rabbitmq_demo.RabbitMQHandler;

import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class SPBatchExecutor {
    public static String LOG_MESSAGE = "SPBatchExecutor";
    private boolean pause = false;
    private boolean step = false;
    private SPConfiguration conf = null;

    private SPBatchParameters spBatchParameters;
    private List<SPBatchExperiment> experimentList;
    private SPBatchExecutorObservator spBatchExecutorObservator;
    private boolean RUN5_ledTurnedOn = false;
    private static int DEBUG_LEVEL = 0;
    private static int DEBUG_LEVEL_ERROR = SPLoggerInterface.DEBUG_VERBOSITY_ERROR;
    private boolean stop = false;

    public String[] eis_titles = null;

    public SPBatchExecutor(SPBatchParameters spBatchParameters, SPBatchExecutorObservator spBatchExecutorObservator, SPConfiguration conf) throws SPException {
        this.spBatchParameters = spBatchParameters;
        this.spBatchExecutorObservator = spBatchExecutorObservator;
        this.conf = conf;
        // ************************************
        // Generate a deactivation list *******
        //ArrayList<String> deactivationList = new ArrayList<>();
        //deactivationList.add("0X0000004305");
        //deactivationList.add("0X0000004205");
        //deactivationList.add("0X0000004105");
        //deactivationList.add("0X0000004005");
        //String configurationFromWichRemoveChips = "WINUX_COMUSB-CLUSTERID_0X10-FT232RL_SENSIBUS-ShortAddress-ALL_SENSORS(RUN5)";
        //ArrayList<SPConfiguration> configurationsList = SPConfigurationManager.getListSPConfiguration();
        //for(int i = 0; i < configurationsList.size(); i++){
        //    if(configurationsList.get(i).getConfigurationName().equals(configurationFromWichRemoveChips)){
        //        configurationsList.get(i).getCluster().setSPChipActivationList(deactivationList, false);
        //    }
        //}
        // Generate a deactivation list *******
        // ************************************


        experimentList = null;
        try {
            experimentList = SPBatchManager.readConfig(new FileInputStream(new File(spBatchParameters.batchFile)), conf);
        } catch (FileNotFoundException e) {
            throw new SPException("Exception in batchFile access: " + spBatchParameters.batchFile + ". " + e.getMessage());
        }


    }

    public List<SPBatchExperiment> getExperimentList(){
        return experimentList;
    }


    public synchronized void play() {
        pause = false;
        step = false;
        notify();
    }

    public synchronized void stop(){
        stop = true;
    }

    public synchronized void pause() {
        pause = true;
    }

    public synchronized void step() {
        pause = true;
        step = true;
        notify();
    }

    public void executeExperiments() throws SPException {



        for(int i= 0; i < experimentList.size(); i++){

            SPLogger.getLogInterface().d(LOG_MESSAGE,"Current configuration: " + experimentList.get(i).getSPConfiguration().getConfigurationName(), SPLoggerInterface.DEBUG_VERBOSITY_ANY);

            executeExperiment(experimentList.get(i));


        }
    }

    private SPClientOnLineClassifier classifierClient = null;
    private void executeExperiment(SPBatchExperiment currentExperiment) throws SPException {

        currentExperiment.initMeasurementQueues();
        int numOfChip = SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size();
        int numOfMeasures = 13;//TODO change this fixed value
        int globalRepetition = currentExperiment.getGlobalRepetition();

        boolean measurementTimeFLAG = false;//It indicates if there is a measurement configuration (EIS or SENSOR)
                                            //whose measurement time is greater than zero.


        int rep = 0;
        float progress = 0;
        long startTime = System.currentTimeMillis();

        RabbitMQHandler rabbitMQHandler = null;

        if(currentExperiment.getRabbitmq_server_ip()!=null && currentExperiment.getRabbitmq_username()!=null &&
                currentExperiment.getRabbitmq_password()!=null && currentExperiment.getRabbitmq_virtualhost()!=null &&
                currentExperiment.getRabbitmq_exchange()!=null){

            try {

                rabbitMQHandler = new RabbitMQHandler(currentExperiment.getRabbitmq_server_ip(), currentExperiment.getRabbitmq_username(),
                        currentExperiment.getRabbitmq_password(), currentExperiment.getRabbitmq_virtualhost(), currentExperiment.getRabbitmq_exchange());
            }catch (IOException|TimeoutException e){
                SPLogger.getLogInterface().d(LOG_MESSAGE,"problem with rabbitmq connection", DEBUG_LEVEL);
                throw new SPException("Problems with RabbitMQ server connection:" +e.getMessage());
            }


        }




        try {

            NumberFormat formatter4 = new DecimalFormat("#0.000000");
            NumberFormat formatter2 = new DecimalFormat("#0.000000");

            SPMeasurementOutput feedback  = new SPMeasurementOutput();
            SPMeasurementOutput m;


            int numOfEIS = currentExperiment.getNumOfEISElement();
            //int numOfConfigurations = currentExperiment.getNum
            int numOfSensingElementOnChip = currentExperiment.getSPSensingElementOnChipSize();

            // PREPARE MEASURE OBJECTS
            List<SPMeasurement[]> measurementOnEISList = currentExperiment.getEISMeasurement(spBatchParameters.outputDir, spBatchParameters.spProtocol);
            SPMeasurement[] measurementOnSensors = currentExperiment.getSENSORMeasurement(spBatchParameters.outputDir, spBatchParameters.spProtocol);



            //SPMeasurementParameterSENSOR array whose size are the number of sensingelementonchip.
            SPMeasurementParameterSENSORS parameterSENSORS[] = new SPMeasurementParameterSENSORS[numOfSensingElementOnChip];
            for(int i=0; i<numOfSensingElementOnChip; i++){
                parameterSENSORS[i] = new SPMeasurementParameterSENSORS(SPConfigurationManager.getSPConfigurationDefault(),currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getID());
                //parameterSENSORS[i].setSPMeasurementMetaParameter(meta);
            }

            //MARCO FERDINANDI
            //Preparing eis experiment: I clone all SPMeasurementParameterEIS in a local array in order to avoid any overwriting
            List<SPMeasurementParameterEIS[]> localSPMeasurementParameterEISList = new ArrayList<>();


            if(numOfEIS>0) {
                for(int i=0; i<numOfEIS; i++) {
                    int currentNumOfConf = currentExperiment.getNumOfConfigurationsList().get(i);
                    SPMeasurementParameterEIS[] localSPMeasurementParameterEIS = new SPMeasurementParameterEIS[currentNumOfConf];

                    for (int j = 0; j < currentNumOfConf; j++) {
                        localSPMeasurementParameterEIS[j] = new SPMeasurementParameterEIS(SPConfigurationManager.getSPConfigurationDefault());
                        localSPMeasurementParameterEIS[j].clone(currentExperiment.getSPMeasurementParameterEIS(i,j));
                    }
                    localSPMeasurementParameterEISList.add(localSPMeasurementParameterEIS);

                    if(currentExperiment.measurementTimeList.get(i)>0)
                        measurementTimeFLAG = true;
                }
                eis_titles = SPMeasurementParameterEIS.getDifferences(localSPMeasurementParameterEISList);

            }

            RUN5_ledTurnedOn = currentExperiment.getSPConfiguration().getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5);

            // Autoranging if requested
            if (numOfEIS > 0) {
                long startTimeAutoRange = System.currentTimeMillis();
                for (int i = 0; i < numOfEIS; i++) {
                    for(int j=0; j<currentExperiment.getNumOfConfigurationsList().get(i); j++){

                        measurementOnEISList.get(i)[j].setMeasureIndexToSave(currentExperiment.eisIndexToSaveList.get(i));

                        SPMeasurementParameterEIS param =  localSPMeasurementParameterEISList.get(i)[j];
                        if (currentExperiment.eisAutoRangeActiveFlags.get(i)) {
                            turnOnLedOnchips(measurementOnEISList.get(i)[j], currentExperiment.getSPConfiguration());
                            SPLogger.getLogInterface().d(LOG_MESSAGE,"************************* Autorange running *********************\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);

                            measurementOnEISList.get(i)[j].autoRange(param);
                            spBatchExecutorObservator.newSetting("----AUTORANGE OK ----\nFrequency: "+ param.getFrequency()+ "\nRsense: "+param.getRsenseLabel()+"\nInGain: "+param.getInGainLabel()
                                    + "\nOutGain: "+param.getOutGainLabel());
                            SPLogger.getLogInterface().d(LOG_MESSAGE,param.toString() + "\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                            SPLogger.getLogInterface().d(LOG_MESSAGE,"************************* Autorange ok **************************\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                        } else {
                            SPLogger.getLogInterface().d(LOG_MESSAGE,"************************* No Autorange *************************\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                        }
                    }

                }

                long endTimeAutoRange = System.currentTimeMillis();
                SPLogger.getLogInterface().d(LOG_MESSAGE, "Elapsed time for autorange: " + (endTimeAutoRange - startTimeAutoRange), DEBUG_LEVEL);
            }



            SPLogger.getLogInterface().d(LOG_MESSAGE,"************************* Start measure cycle *************************", DEBUG_LEVEL);

            int totalMeasureToFillBuffer = currentExperiment.totalMeasureToFillBuffer();
            if (totalMeasureToFillBuffer > 0 && currentExperiment.getFillBufferBeforeStart()){
                SPLogger.getLogInterface().d(LOG_MESSAGE,"Wait for " + totalMeasureToFillBuffer + " measure before start in order to fill buffer...\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
            } else {
                SPLogger.getLogInterface().d(LOG_MESSAGE,"Measurement started.\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
            }


            int configRepetition = currentExperiment.getConfigRepetition();

            //double[] logDati = new double[1 + currentExperiment.getSPSensingElementOnChipSize()];

            String globalRow;

            String[] unitMeasure = new String[numOfSensingElementOnChip];
            boolean globalRowFLAG = true;

            for(int i=0; i<numOfSensingElementOnChip; i++){
                /*if(currentExperiment.getSensorMeasurementTimeList().get(i)>0)
                    globalRowFLAG= false;*/

                if(currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getMeasureTechnique().equals("DIRECT")) {
                    unitMeasure[i] = currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager().getMeasureUnit();

                }
                else{
                    unitMeasure[i] = SPMeasurementParameterEIS.unitMeasuresLabels[currentExperiment.getSensorIndexToPlotList().get(i).get(0)];
                    currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager().setIndexToReduce(currentExperiment.getSensorIndexToPlotList().get(i).get(0));
                    /*TODO: remove this 7. It's a specialized version for Capacitance*/
                    if(currentExperiment.getSensorIndexToPlotList().get(i).get(0)==7){
                        currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager().setInitial_multiplier("-12");
                    }

                }

                if(currentExperiment.getSensorMeasurementTimeList().get(i)>0)
                    measurementTimeFLAG = true;

            }
            spBatchExecutorObservator.measureStarted(currentExperiment,unitMeasure);


            long startTimeStamp = System.currentTimeMillis();
            long endTimeStamp = startTimeStamp;
            long lastEndTimeStamp;
            spBatchExecutorObservator.updateInfo((float)0, (float)0, (float) 0);

            float deltaTMedio = 0;

            // ID of the first chip
            String idOfTheFirstChip =  currentExperiment.getSPConfiguration().getCluster().getSPActiveSerialNumbers().get(0);

            boolean flagSetEISApplied = false;
            boolean flagSetSENSORApplied = false;



            //int outputIndexToPlot = -1;
            //outputIndexToSave contains the eis measurement array index to save.

            //outputIndexToPlot = currentExperiment.getIndexToPlot();
            List<SPDataRepresentationManager[]> dataRepresentationManagerEISList = new ArrayList<>();





            for(int i=0; i<numOfEIS; i++){

                /*For each EIS configuration there is the possibility to more than one plot. For this reason the array size is the product between
                * the number of configurations of the EIS and the number of plot requested for the EIS.*/
                SPDataRepresentationManager[] currentDataRepresentationManager = new SPDataRepresentationManager[currentExperiment.getNumOfConfigurationsList().get(i)*currentExperiment.eisIndexToPlotList.get(i).size()];

                for(int j=0; j<currentExperiment.getNumOfConfigurationsList().get(i);j++) {
                    for(int h=0; h<currentExperiment.eisIndexToPlotList.get(i).size(); h++) {
                        int ind = j*currentExperiment.eisIndexToPlotList.get(i).size()+h;
                        currentDataRepresentationManager[ind] = new SPDataRepresentationManager(currentExperiment.eisIndexToPlotList.get(i).get(h));
                        currentDataRepresentationManager[ind].initForEISRawMeasurements();
                    }
                }

                /*for(int j=0; j<(currentExperiment.getNumOfConfigurationsList().get(i)*currentExperiment.eisIndexToPlotList.get(i).size());j++) {
                    currentDataRepresentationManager[j] = new SPDataRepresentationManager(currentExperiment.eisIndexToPlotList.get(i).get(j));
                    currentDataRepresentationManager[j].initForEISRawMeasurements();
                }*/
                dataRepresentationManagerEISList.add(currentDataRepresentationManager);
            }


            for(rep = 0; rep < globalRepetition && !stop    ; rep++) {

                GlobalMeasurementRow globalMeasurementRow = new GlobalMeasurementRow(numOfChip,currentExperiment);

                List<SPDataRepresentation> spDataRepresentationList = new ArrayList<>();

                globalRow = "";

                if (rep % 10 == 0 && rep != 0) {
                    SPLogger.getLogInterface().d(LOG_MESSAGE, "Progress: " + formatter4.format((((float) (rep) * 100) / (globalRepetition))) + "% (" + (rep) + " of " + (globalRepetition) + ")\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                }

                //long singleCycleTime = System.currentTimeMillis();
                boolean stoppedRead = false;

                // This cycle explore all the parameters ccombination


                String classBelongingTo = "";
                    for(int i=0; i<numOfEIS; i++) {


                    //measurementTime handling
                    Long startTime_measurementTime = System.currentTimeMillis();
                    Long tot_measurementTime = startTime_measurementTime;

                    double xCurrentTime = 0;

                    int cont = 0;
                    do{

                    if(cont>0){
                        globalMeasurementRow = new GlobalMeasurementRow(numOfChip,currentExperiment);

                    }
                        //cont++;
                    int outputIndexToPlot = currentExperiment.eisIndexToPlotList.get(i).get(0);
                    int currentConfigurationIndex = 0;
                    SPMeasurement measurementOnEIS[] = measurementOnEISList.get(i);
                    SPMeasurementParameterEIS localSPMeasurementParameterEIS[] = localSPMeasurementParameterEISList.get(i);
                    int numOfCurrentConfig = currentExperiment.getNumOfConfigurationsList().get(i);
                    SPDataRepresentationManager[] dataRepresentationManagerEIS = dataRepresentationManagerEISList.get(i);
                    while (currentConfigurationIndex < numOfCurrentConfig) {

                        SPDelay.delay(currentExperiment.timeToWaitList.get(i));

                        classBelongingTo = "";
                        feedback = new SPMeasurementOutput();

                        //System.out.println("************************* start setINIT *************************");

                        measurementOnEIS[currentConfigurationIndex].setAutomaticADCRangeAdjustment(currentExperiment.eisAutoscaleActiveFlags.get(0));

                        measurementOnEIS[currentConfigurationIndex].setMeasureIndexToSave(currentExperiment.eisIndexToSaveList.get(i));

                        turnOnLedOnchips(measurementOnEIS[currentConfigurationIndex], currentExperiment.getSPConfiguration());

                        localSPMeasurementParameterEIS[currentConfigurationIndex].getSPMeasurementMetaParameter().setRenewBuffer(rep == 0);

                        if (false && flagSetEISApplied && ((numOfCurrentConfig + numOfSensingElementOnChip) <= 1)) {
                            SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* setEIS avoided *************************", DEBUG_LEVEL);
                        } else {
                            SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* start setEIS *************************", DEBUG_LEVEL);

                            //ADDED by MARCO FERDINANDI : Inizializzo il parametro "Measure" di SPMeasurementParameter all'indice indicato nell'experiment eis.
                            //localSPMeasurementParameterEIS[currentConfigurationIndex].setMeasure(SPMeasurementParameterEIS.measureLabels[currentExperiment.eisIndexToSaveList.get(i)]);
                            //---

                            measurementOnEIS[currentConfigurationIndex].setEIS(localSPMeasurementParameterEIS[currentConfigurationIndex], feedback);
                            flagSetEISApplied = true;
                        }

                        feedback = new SPMeasurementOutput();
                        int NDATA = 1;
                        //int conversionRate = SPMeasurement.AvailableFrequencies.adcFreq(param.getFrequency(), 10, 200);
                        SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* start setADC *************************", DEBUG_LEVEL);
                        localSPMeasurementParameterEIS[currentConfigurationIndex].setInPortADC(SPMeasurementParameterADC.INPORT_IA);
                        localSPMeasurementParameterEIS[currentConfigurationIndex].setNData(NDATA);
                        measurementOnEIS[currentConfigurationIndex].setADC((SPMeasurementParameterADC) localSPMeasurementParameterEIS[currentConfigurationIndex], feedback);
                        //measurementOnEIS[currentConfigurationIndex].setADC(NDATA, null, SPMeasurementParameterADC.INPORT_IA, true, feedback);
                        double[][] output = null;

                        /*int mainQueueIndex = 0;
                        for(int cont=0; cont<=i; cont++){
                            mainQueueIndex += currentExperiment.getNumOfConfigurationsList().get(cont);
                        }
                        mainQueueIndex -= 1;*/




                        double[][] toStore = null;
                        for (int j = 0; j < configRepetition; j++) {


                            feedback = new SPMeasurementOutput();

                            SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* start getEIS *************************", DEBUG_LEVEL);

                            output = measurementOnEIS[currentConfigurationIndex].getEIS(localSPMeasurementParameterEIS[currentConfigurationIndex], feedback);

                            toStore = new double[SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size()][];

                            for (int r = 0; r < toStore.length; r++) {
                                toStore[r] = new double[output[r].length];
                                for (int c = 0; c < output[r].length; c++) {
                                    toStore[r][c] = output[r][c];
                                }
                            }




                            //System.out.println("j: " + (j + 1) + ", capacitance: " +  output[0][5]);

                            if ((rep % currentExperiment.getOutputDecimation() == 0)) {

                                //Insert the measure in the relative main queue!
                                //QUEUE: Adding a SPChartElement in the relative queue

                                //lastEndTimeStamp = endTimeStamp;
                                endTimeStamp = System.currentTimeMillis();

                                double x = (double) (endTimeStamp - startTimeStamp) / 1000;

                                if(rep>0){
                                    int totalTimeDiff = 0;
                                    for(int k=0; k<currentExperiment.measurementTimeList.size();k++){
                                        if(k!=i){
                                            totalTimeDiff+=currentExperiment.measurementTimeList.get(k);
                                        }
                                    }

                                    for(int k=0; k<currentExperiment.getSensorMeasurementTimeList().size(); k++){
                                        totalTimeDiff+=currentExperiment.getSensorMeasurementTimeList().get(k);
                                    }


                                    x = x-(totalTimeDiff*rep/1000);
                                }
                                xCurrentTime = x;

                                //SPDataRepresentation appo = dataRepresentationManagerEIS[currentConfigurationIndex].adapt(output, rep == 0 && j == 0 && cont==0);
                                //spDataRepresentationList.add(appo);

                                //spDataRepresentationList.add(appo);


                                //List<Integer> eis_indexToPlot =



                                /*
                                if (i == 0)
                                    mainQueueIndex = currentConfigurationIndex;
                                else {
                                    mainQueueIndex = 0;
                                    for (int k = i - 1; k >= 0; k--) {
                                        mainQueueIndex += currentExperiment.getNumOfConfigurationsList().get(k)*currentExperiment.eisIndexToPlotList.get(k).size();
                                    }
                                    mainQueueIndex += currentConfigurationIndex;
                                }*/

                                for(int plot_ind = 0; plot_ind<currentExperiment.eisIndexToPlotList.get(i).size(); plot_ind++){

                                    int mainQueueIndex = 0;

                                    for (int k = i - 1; k >= 0; k--) {
                                        mainQueueIndex += currentExperiment.getNumOfConfigurationsList().get(k)*currentExperiment.eisIndexToPlotList.get(k).size();
                                    }

                                    mainQueueIndex += currentConfigurationIndex*currentExperiment.eisIndexToPlotList.get(i).size()+plot_ind;

                                    int dataRepresentationIndex = currentConfigurationIndex*currentExperiment.eisIndexToPlotList.get(i).size()+plot_ind;

                                    SPDataRepresentation appo = dataRepresentationManagerEIS[dataRepresentationIndex].adapt(output, rep == 0 && j == 0 && cont==0);
                                    if(cont==0)
                                        spDataRepresentationList.add(appo);
                                    else{
                                        spDataRepresentationList.get(mainQueueIndex).values = appo.values;
                                    }
                                    SPChartElement item = new SPChartElement(x, output, null, numOfChip, numOfMeasures, appo);
                                    currentExperiment.getMainQueue()[mainQueueIndex].put(item);


                                    for(int cid = 0; cid<numOfChip; cid++){
                                        globalMeasurementRow.addEISMeasure(toStore[cid][currentExperiment.eisIndexToPlotList.get(i).get(plot_ind)],i,currentConfigurationIndex,plot_ind,cid);
                                    }

                                }


                                //currentExperiment.getMainQueue()[currentConfigurationIndex+i].put(item);

                                currentExperiment.saveEISMeasure(toStore, localSPMeasurementParameterEIS[currentConfigurationIndex], i);
                                // String to send to the server
                                // Classify only the first chip

                                /*if (spBatchExecutorObservator.classifySample()) {
                                    try {
                                        if (classifierClient == null) {
                                            classifierClient = new SPClientOnLineClassifier("hego.unicas.it", 43547, "5A05", 3);
                                        }
                                        classBelongingTo = classifierClient.sendDataToClassifier("" + output[0][SPMeasurementParameterEIS.IN_PHASE]);

                                    } catch (Exception e) {
                                        classifierClient = null;
                                        e.printStackTrace();
                                    }
                                    SPDelay.delay(1);

                                } else if (classifierClient != null) {
                                    try {
                                        classifierClient.close();
                                        classifierClient = null;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }*/
                            }

                            Thread.sleep(currentExperiment.getDelay());

                        }

                        /*if (output != null && outputIndexToPlot >= 0 && cont==0) {
                            for (int k = 0; k < SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size(); k++) {
                                globalRow += "" + toStore[k][outputIndexToPlot] + ";";
                            }
                        }*/

                        currentConfigurationIndex++;

                    }

                    if(measurementTimeFLAG){
                        endTimeStamp = System.currentTimeMillis();
                        globalMeasurementRow.insertTimeStamp((double)(endTimeStamp - startTimeStamp)/1000);
                        spBatchExecutorObservator.newMeasure(globalMeasurementRow.toString(), spDataRepresentationList,classBelongingTo);
                    }


                    cont++;
                    tot_measurementTime = System.currentTimeMillis()-startTime_measurementTime;
                    }while(tot_measurementTime<currentExperiment.measurementTimeList.get(i));

                        SPLogger.getLogInterface().d(LOG_MESSAGE, "EIS '"+i+"'"+" - Total Time = "+tot_measurementTime, SPLoggerInterface.DEBUG_VERBOSITY_ANY);

                }
                //---end eis



                // SENSORS measure *******************************************
                for(int i=0; i<numOfSensingElementOnChip; i++){
                    parameterSENSORS[i].getSPMeasurementMetaParameter().setRenewBuffer(rep==0);
                    parameterSENSORS[i].getSPMeasurementMetaParameter().setFillBufferBeforeStart(currentExperiment.getFillBufferBeforeStart());

                    currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().
                            getSpDataRepresentationManager().setIndexToReduce(currentExperiment.getSensorIndexToPlotList().get(i).get(0));
                }



                for(int i = 0; i < numOfSensingElementOnChip; i++) {

                    Long start_measurementTime = System.currentTimeMillis();
                    Long tot_measurementTime = start_measurementTime;
                    double xCurrentTime = 0;
                    int cont =0;
                    do{

                        if(cont>0){
                            globalMeasurementRow = new GlobalMeasurementRow(numOfChip,currentExperiment);
                        }

                        SPDelay.delay(currentExperiment.getSensorTimeToWait(i));

                        measurementOnSensors[i].setMeasureIndexToSave(currentExperiment.getSensorIndexToSaveList(i));
                        turnOnLedOnchips(measurementOnSensors[i], currentExperiment.getSPConfiguration());

                        m = new SPMeasurementOutput();

                        int outputIndexToPlot = currentExperiment.getSensorIndexToPlotList().get(i).get(0);
                        parameterSENSORS[i].setFilter(currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getFilter());
                        parameterSENSORS[i].setPort(currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getPort().getPortLabel());

                        if (false && flagSetSENSORApplied && ((0 + numOfSensingElementOnChip) <= 1)) {//TODO: control it
                            SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* setSENSOR avoided **********************", DEBUG_LEVEL);
                        } else {
                            SPLogger.getLogInterface().d(LOG_MESSAGE, "************************* start setSENSOR *************************", DEBUG_LEVEL);
                            measurementOnSensors[i].setSENSOR(parameterSENSORS[i], m);
                            flagSetSENSORApplied = true;
                        }

                        //ADDED BY MARCO FERDINANDI
                        double[][] output = measurementOnSensors[i].getSENSOR(parameterSENSORS[i], m);

                        double[][] toStore = new double[SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size()][];

                        for (int r = 0; r < toStore.length; r++) {
                            toStore[r] = new double[output[r].length];
                            for (int c = 0; c < output[r].length; c++) {
                                toStore[r][c] = output[r][c];
                            }
                        }

                        //Adding the measure to the relative queue
                        SPDataRepresentation appo = currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager().adapt(output, rep == 0);
                        //spDataRepresentationManagerList.add(currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager());

                        if(cont==0) {
                            spDataRepresentationList.add(appo);
                        }else{
                            spDataRepresentationList.get(currentExperiment.getTotalNumberOfEISPlot() + i).values = appo.values;
                        }

                        endTimeStamp = System.currentTimeMillis();
                        double x = (double) (endTimeStamp - startTimeStamp) / 1000;

                        if(rep>0){
                            int totalTimeDiff = 0;
                            for(int k=0; k<currentExperiment.measurementTimeList.size();k++){

                                totalTimeDiff+=currentExperiment.measurementTimeList.get(k);
                            }

                            for(int k=0; k<currentExperiment.getSensorMeasurementTimeList().size(); k++){
                                if(k!=i) {
                                    totalTimeDiff += currentExperiment.getSensorMeasurementTimeList().get(k);
                                }
                            }




                            x = x-(totalTimeDiff*rep/1000);

                            xCurrentTime =x ;
                        }

                        int numofMeas = 9;
                        if (parameterSENSORS[i].getParamInternalEIS() == null) {
                            numofMeas = 1;
                        }
                        SPChartElement item = new SPChartElement(x, output, null, numOfChip, numofMeas, appo);
                        currentExperiment.getMainQueue()[currentExperiment.getTotalNumberOfEISPlot() + i].put(item);


                        if (rabbitMQHandler != null) {
                            for (int k = 0; k < SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size(); k++) {
                                rabbitMQHandler.sendMeasure(
                                        new MeasureToSend(SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().get(k).getSerialNumber(),
                                                currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getName(),
                                                SPConfigurationManager.getSPConfigurationDefault().getConfigurationName(),
                                                Double.toString(x),
                                                appo.getFormatter().format(output[k][currentExperiment.getSensorIndexToPlotList().get(i).get(0)]),
                                                currentExperiment.getSPSensingElementOnChip(i).getSensingElementOnFamily().getSensingElement().getSpDataRepresentationManager().getMeasureUnit(),
                                                "40%"));
                            }
                        }


                        if (outputIndexToPlot >= 0) {
                            // TODO: Rimuovere ciclo
                            for (int k = 0; k < SPConfigurationManager.getSPConfigurationDefault().getCluster().getActiveSPChipList().size(); k++) {
                                //globalRow += "" +appo.getFormatter().format(output[k][outputIndexToSave]) + ";";

                                globalRow += "" + toStore[k][outputIndexToPlot] + ";";

                                //ATTENTION: THE PLOT_INDEX IS ALWAYS ZERO BECAUSE IT IS NOT POSSIBLE TO SHOW MULTIPLE PLOT FOR SENSOR
                                globalMeasurementRow.addSENSORMeasure(toStore[k][outputIndexToPlot],i,0,k);
                            }
                        }





                        if ((rep % currentExperiment.getOutputDecimation() == 0)) {
                            currentExperiment.saveSENSORSMeasure(toStore, i, (SPMeasurementRUN4) measurementOnSensors[i], parameterSENSORS[i]);
                        }
                        //timeForOneMeasure = (System.currentTimeMillis() - timeForOneMeasure);

                        if(measurementTimeFLAG){
                            endTimeStamp = System.currentTimeMillis();
                            globalMeasurementRow.insertTimeStamp((double)(endTimeStamp - startTimeStamp)/1000);
                            spBatchExecutorObservator.newMeasure(globalMeasurementRow.toString(), spDataRepresentationList,classBelongingTo);
                        }

                        tot_measurementTime = System.currentTimeMillis()-start_measurementTime;
                        cont++;
                    }while(tot_measurementTime<currentExperiment.getSensorMeasurementTimeList().get(i));

                    SPLogger.getLogInterface().d(LOG_MESSAGE, "Sensor '"+i+"'"+" - Total Time = "+tot_measurementTime, SPLoggerInterface.DEBUG_VERBOSITY_ANY);
                }
                lastEndTimeStamp = endTimeStamp;
                endTimeStamp = System.currentTimeMillis();
                //globalRow = formatter2.format(((double)(endTimeStamp - startTimeStamp))/1000) + ";" + globalRow;


                stoppedRead = true;


                globalMeasurementRow.insertTimeStamp((double)(endTimeStamp - startTimeStamp)/1000);

                /**Here the measures are sent to the server for the classification if the classify sample is active*/
                if (spBatchExecutorObservator.classifySample()) {
                    try {
                        if (classifierClient == null) {
                            //classifierClient = new SPClientOnLineClassifier("hego.unicas.it", 43547, "5A05", 3);
                            classifierClient = new SPClientOnLineClassifier("localhost", 8182, 5);
                        }
                        classBelongingTo = classifierClient.sendDataToClassifier(globalMeasurementRow.toString());

                    } catch (Exception e) {
                        classifierClient = null;
                        e.printStackTrace();
                    }
                    SPDelay.delay(1);

                } else if (classifierClient != null) {
                    try {
                        classifierClient.close();
                        classifierClient = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }



                if ((rep % currentExperiment.getOutputDecimation() == 0 && !measurementTimeFLAG)){
                    //lobalRow = formatter2.format(((double)(endTimeStamp - startTimeStamp))/1000) + ";" + globalRow;

                    //TODO:ATTENTION- TEMPOrary COMMENT
                    //spBatchExecutorObservator.newMeasure(globalRow, spDataRepresentationList,classBelongingTo);

                    //globalMeasurementRow.insertTimeStamp((double)(endTimeStamp - startTimeStamp)/1000);

                    spBatchExecutorObservator.newMeasure(globalMeasurementRow.toString(), spDataRepresentationList,classBelongingTo);
                }







                deltaTMedio += (float)(endTimeStamp - lastEndTimeStamp)/1000;
                progress = ((float) (rep + 1) * 100) / globalRepetition;

                spBatchExecutorObservator.updateInfo(deltaTMedio/(rep + 1),
                        (rep + 1)/((float)(endTimeStamp - startTimeStamp)/1000),
                        progress);




                for(int i=0;i<currentExperiment.getPotMeasurementParameterList().size();i++){
                    SPMeasurementParameterPOT currentParPOT = currentExperiment.getPotMeasurementParameterList().get(i);

                    SPMeasurementOutput out = new SPMeasurementOutput();

                }




                verityPause();
                //SPDelay.delay(1000);

            }

            //spBatchExecutorObservator.updateInfo(deltaTMedio/globalRepetition,
            //        globalRepetition/((float)(endTimeStamp - startTimeStamp)/1000),
            //        (float)100);


            //currentExperiment.getSPConfiguration().stopConfiguration(true);

            //currentExperiment.getSPConfiguration().getSPProtocol().sendInstruction("WRITE COMMAND S 0X14", null);
            currentExperiment.closeFiles();

            spBatchExecutorObservator.measureFinished(stop);

        } catch (Exception ex) {

            spBatchExecutorObservator.measureFinished(stop);
            ex.printStackTrace();

            String messaggio = "Exception during measurement. Repetition: " + rep + ", progress: " + progress + "\n" +
                    "Error message: " + ex.getMessage();
            SPLogger.getLogInterface().d(LOG_MESSAGE, messaggio, DEBUG_LEVEL_ERROR);
            throw new SPException(messaggio);
        }
        long endTime = System.currentTimeMillis();
        System.out.println();
        SPLogger.getLogInterface().d(LOG_MESSAGE,"Elapsed time: " + (endTime-startTime)/1000 + "\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);

        SPLogger.getLogInterface().d(LOG_MESSAGE,"Total byte sent: " + SPProtocolFT232RL_SENSIBUS.TOTAL_BYTE_SENT, DEBUG_LEVEL);

        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPMeasurementRUN4.GET_EIS_ELAPSED: " + SPMeasurementRUN4.GET_EIS_ELAPSED, DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPMeasurementRUN4.GET_I_ELAPSED: " + SPMeasurementRUN4.GET_I_ELAPSED, DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPMeasurementRUN4.GET_Q_ELAPSED: " + SPMeasurementRUN4.GET_Q_ELAPSED, DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPMeasurementRUN4.GET_DATA_ELAPSED: " + SPMeasurementRUN4.GET_DATA_ELAPSED, DEBUG_LEVEL);

        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPProtocol.SPMNEMONIC_ELAPSED: " + SPProtocol.SPMNEMONIC_ELAPSED + ", with: ", DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPDecoder.DECODE_SINGLE_INSTRUCTION_ELAPSED: " + SPDecoder.DECODE_SINGLE_INSTRUCTION_ELAPSED, DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPProtocolFT232RL_SENSIBUS.SENDL_ELAPSED: " + SPProtocolFT232RL_SENSIBUS.SENDL_ELAPSED + ", di cui: ", DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE,"SPProtocolFT232RL_SENSIBUS.WRITE_ELAPSED: " + SPProtocolFT232RL_SENSIBUS.WRITE_ELAPSED, DEBUG_LEVEL);

        for(int i = 0; i < SPProtocolFT232RL_SENSIBUS.COUNTER.length; i++){
            if (SPProtocolFT232RL_SENSIBUS.COUNTER[i] > 0){
                SPLogger.getLogInterface().d(LOG_MESSAGE,"\t" + i + " - " + SPProtocolFT232RL_SENSIBUS.COUNTER[i], DEBUG_LEVEL);
            }
        }

        SPLogger.getLogInterface().d(LOG_MESSAGE,"File saved. Program ended.\n", SPLoggerInterface.DEBUG_VERBOSITY_ANY);

    }


    private synchronized void verityPause() throws InterruptedException {

        if (pause){
            wait();
        }
        pause = step;

    }


    private void turnOnLedOnchips(SPMeasurement spMeasurement, SPConfiguration spConfiguration) throws SPException {

        SPCluster cluster = SPConfigurationManager.getSPConfigurationDefault().getCluster();
        ArrayList<String> turnOnList = new ArrayList<>();
        /*turnOnList.add("0X0000004005");
        turnOnList.add("0X0000004105");
        turnOnList.add("0X0000004205");
        turnOnList.add("0X0000004305");*/
        //turnOnList.add("0X0000004405");

        SPCluster newCluster = cluster.generateTempCluster(turnOnList);

        if (RUN5_ledTurnedOn && turnOnList.size() > 0){
            SPMeasurementParameterActuator actuator = new SPMeasurementParameterActuator(spConfiguration);
            actuator.setTemporaryCluster(newCluster);
            actuator.setPort(SPMeasurementParameterActuator.ACTUATOR_PORT_RDY);
            actuator.setMode(SPMeasurementParameterActuator.ON);
            spMeasurement.setACTUATOR(actuator);

            actuator.setPort(SPMeasurementParameterActuator.ACTUATOR_PORT_INT);
            spMeasurement.setACTUATOR(actuator);
            //setACTUATOR
            RUN5_ledTurnedOn = false;
        }
    }

}
