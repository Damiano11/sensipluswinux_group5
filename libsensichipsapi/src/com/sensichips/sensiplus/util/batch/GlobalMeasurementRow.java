package com.sensichips.sensiplus.util.batch;

import com.sensichips.sensiplus.SPException;

import java.util.Arrays;

public class GlobalMeasurementRow {

    private SPBatchExperiment spBatchExperiment;
    private int numOfChips;

    private double[] measureToSave;


    public GlobalMeasurementRow(int numOfChips, SPBatchExperiment spBatchExperiment){
        this.numOfChips = numOfChips;
        this.spBatchExperiment = spBatchExperiment;

        int numOfColumns = 0;

        for(int i=0; i<spBatchExperiment.getNumOfEISElement(); i++){
            numOfColumns+=spBatchExperiment.getNumOfConfigurationsList().get(i)*spBatchExperiment.eisIndexToPlotList.get(i).size()*numOfChips;
        }

        for(int i=0; i<spBatchExperiment.getSPSensingElementOnChipSize();i++){
            numOfColumns += numOfChips;
        }

        measureToSave = new double[numOfColumns+1];
    }

    public void insertTimeStamp (double time){
        measureToSave[0] = time;
    }


    //ATTENTION:
    /*eisIndex : it indicates the index of the measurment EIS in the batch experiment.
    * */
    public void addEISMeasure(double measure, int eisIndex, int eisConfigIndex, int plotIndex, int chipIndex) throws SPException{

        int offset = 0;
        for (int i = eisIndex - 1; i >= 0; i--) {
            offset += spBatchExperiment.getNumOfConfigurationsList().get(i) * spBatchExperiment.eisIndexToPlotList.get(i).size() * numOfChips;
        }
        int correctIndex = offset + eisConfigIndex * spBatchExperiment.eisIndexToPlotList.get(eisIndex).size() * numOfChips + plotIndex * numOfChips + chipIndex;

        correctIndex += 1; //Timestamp
        if (correctIndex < 0 || correctIndex >= measureToSave.length) {
            throw new SPException("Exception in adding measure in the GlobalMeasurementRow");
        }

        measureToSave[correctIndex] = measure;

    }

    public void addSENSORMeasure(double measure, int sensorIndex, int plotIndex, int chipIndex) throws SPException{


        int offsetForEIS=0;

        for(int i=0; i<spBatchExperiment.getNumOfEISElement(); i++){
            offsetForEIS += spBatchExperiment.getNumOfConfigurationsList().get(i)*spBatchExperiment.eisIndexToPlotList.get(i).size()*numOfChips;
        }


        offsetForEIS += 1; //Consider the Timestamp column

        int sensorOffset = 0;
        int correctIndex=0;
        for(int i=sensorIndex-1; i>=0; i--){
            sensorOffset += spBatchExperiment.getSensorIndexToPlotList().get(i).size()*numOfChips;
        }
        //correctIndex = offset+plotIndex*numOfChips+chipIndex;
        correctIndex = offsetForEIS+sensorOffset+plotIndex*numOfChips+chipIndex;

        //correctIndex+=offsetForEIS;



        if(correctIndex<0 || correctIndex>= measureToSave.length){
            throw new SPException("Exception in adding measure in the GlobalMeasurementRow");
        }

        measureToSave[correctIndex] = measure;
    }


    @Override
    public String toString() {
        String result = "";
        for(int i=0; i<measureToSave.length; i++){
            if(measureToSave[i]==0){
                result+="-;";
            }
            else {
                result += measureToSave[i] + ";";
            }
        }



        return result;
    }
}
