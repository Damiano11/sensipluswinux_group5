package com.sensichips.sensiplus.util.batch;

import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;


/**
 * Created by mario on 26/03/16.
 */
public class SPBatchParameters {

    public static String LOG_MESSAGE = "SPBatchParameters";

    //public String batchFile = "";

    public static final String layoutConfigurationName = "lc";
    public static final String outputDirName = "o";
    public static final String batchFileName = "f";
    public static final String debugLevel0Name = "d1";
    public static final String debugLevel1Name = "d2";
    public static final String debugLevel2Name = "d3";
    public static final String guiName = "gui";
    public static final String currentConfiguratinoName = "cc";
    public static final String hostName = "host";
    public static final String portName = "port";

    public boolean valid = false;
    public String layoutConfiguration = "./layout_configuration.xml";
    //public String layoutConfiguration = "layout_configuration_files/layout_configuration_new.xml";

    public String host = "localhost";
    public int port = 9999;

    //public static String layoutConfigurationFileXMLWinux = "layout_configuration_files/layout_configuration_new.xml";
    //public static String layoutConfigurationFileXMLAndroid = "layout_configuration_files/layout_configuration_new_android.xml";
    public static String layoutConfigurationFileXML = "layout_configuration_files/layout_configuration_new.xml";

    public static String layoutConfigurationFileXSD = "layout_configuration_files/schema_layout_configuration_new.xsd";

    public String outputDir = "./measures";
    public String batchFile = "./batch.xml";
    public boolean gui = false;
    public String currentConfiguration = "WINUX_IP-CLUSTERID_0X02-FT232RL_SENSIBUS-ShortAddress-OVER_IP_RUN5";

    public  static SPDecoder            decoder;
    public  static SPProtocol spProtocol;

    public SPBatchParameters(){}


}
