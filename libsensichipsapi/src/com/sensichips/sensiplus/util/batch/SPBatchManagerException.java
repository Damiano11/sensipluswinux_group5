package com.sensichips.sensiplus.util.batch;

import com.sensichips.sensiplus.SPException;

/**
 * Created by mario on 06/11/2015.
 */
public class SPBatchManagerException extends SPException {
    public SPBatchManagerException(String message){
        super(message);
    }
}
