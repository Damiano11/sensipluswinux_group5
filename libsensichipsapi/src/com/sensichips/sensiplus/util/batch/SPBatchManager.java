package com.sensichips.sensiplus.util.batch;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.chip.SPSensingElementOnChip;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterPOT;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SPBatchManager {
	private static final String EXPERIMENT = "experiment";
	private static final String SENSICHIPS = "sensichips";
	private static final String DATE = "date";
	private static final String CONFIG_REPETITION = "configRepetition";
	private static final String GLOBAL_REPETITION = "globalRepetition";
	private static final String FILL_BUFFER_BEFORE_START = "fillBufferBeforeStart";
	private static final String OUTPUT_DECIMATION = "outputDecimation";
	private static final String DELAY = "delay";
	private static final String WAIT = "wait";
	private static final String ID = "id";
	private static final String ORDER = "order";
	private static final String VAL = "val";
	private static final String BURST = "burst";
	private static final String SAVE = "save";
	private static final String PLOT = "plot";
	private static final String POT = "pot";
	private static final String INITIAL_POTENTIAL = "initial_potential";
	private static final String FINAL_POTENTIAL = "final_potential";
	private static final String STEP = "step";
	private static final String PULSE_PERIOD = "pulse_period";
	private static final String PULSE_AMPLITUDE= "pulse_amplitude";
	private static final String TYPE = "type";

	private static final String RABBITMQ_SERVER_IP = "rabbitmq_server_ip";
	private static final String RABBITMQ_USERNAME = "rabbitmq_username";
	private static final String RABBITMQ_PASSWORD = "rabbitmq_password";
	private static final String RABBITMQ_VIRTUALHOST = "rabbitmq_virtualhost";
	private static final String RABBITMQ_EXCHANGE = "rabbitmq_exchange";


	private static final String ALL_MEASUREMENTS = "ALL";

	private static final String AUTOSCALE = "autoscale";
	private static final String AUTORANGE = "autorange";

	private static final String MEASUREMENT_TIME = "measurementTime";



	// Allowed values ...
	public static final String INPORT = "inport";
	public static final String OUTPORT = "outport";
	public static final String OUTGAIN = "outgain";
	public static final String DCBIASP = "dcbiasP";
	public static final String DCBIASN = "dcbiasN";
	public static final String FREQUENCIES = "frequencies";
	public static final String RSENSE = "rsense";
	public static final String HARMONIC = "harmonic";
	public static final String INGAIN = "ingain";
	public static final String MODEVI = "modevi";
	public static final String CONTACTS = "contacts";
	public static final String FILTER = "filter";
	public static final String PHASESHIFT = "phaseShift";
	public static final String PHASESHIFTMODE = "phaseShiftMode";
	public static final String SENSOR = "sensor";
	public static final String EIS = "eis";
	public static final String PORT = "port";
	public static final String REGISTERS = "registers";

	public static final Map<String, String> allowedValues;



	//private SPConfiguration defaultConfiguration;


	static {
		allowedValues = new HashMap<String, String>();
		allowedValues.put(INPORT, "");
		allowedValues.put(OUTPORT, "");
		allowedValues.put(OUTGAIN, "");
		allowedValues.put(DCBIASP, "");
		allowedValues.put(DCBIASN, "");
		allowedValues.put(FREQUENCIES, "");
		allowedValues.put(RSENSE, "");
		allowedValues.put(HARMONIC, "");
		allowedValues.put(INGAIN, "");
		allowedValues.put(MODEVI, "");
		allowedValues.put(CONTACTS, "");
		allowedValues.put(FILTER, "");
		allowedValues.put(PHASESHIFT, "");
		allowedValues.put(PHASESHIFTMODE, "");
		allowedValues.put(SENSOR, "");
		allowedValues.put(POT, "");
		allowedValues.put(INITIAL_POTENTIAL, "");
		allowedValues.put(FINAL_POTENTIAL, "");
		allowedValues.put(STEP, "");
		allowedValues.put(PULSE_AMPLITUDE, "");
		allowedValues.put(PULSE_PERIOD, "");
		allowedValues.put(TYPE, "");
		allowedValues.put(PORT, "");
		allowedValues.put(REGISTERS, "");

	}



	public static List<SPBatchExperiment> readConfig(FileInputStream file, SPConfiguration conf) throws SPBatchManagerException {
		String currentField = null;
		String order = null;
		SPBatchExperiment item = null;
		ArrayList<SPBatchExperiment> list = new ArrayList<>();
		boolean SENSOR_TAG = false;
		boolean POT_TAG = false;
		int sensorTagIndex = -1;
		List<Integer> toPlotIndex = null;
		long timeToWait = 0;
		long timeToMeasure = 0;
		boolean autoscaleFlag = false;
		boolean autoRangeFlag = false;

		SPMeasurementParameterPOT currentMeasurementParamPOT = null;
		try {
			SPMeasurementParameterEIS verifyParam = null;// = new SPMeasurementParameterEIS(conf.getCluster().getFamilyOfChips().getSYS_CLOCK());
			//FileReader in = new FileReader(file, null);
			XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
			XmlPullParser myParser = xmlFactoryObject.newPullParser();
			myParser.setInput(file, null);

            //SPConfiguration conf = SPConfigurationManager.getSPConfigurationDefault();

			int event = myParser.getEventType();
			while (event != XmlPullParser.END_DOCUMENT)
			{
				String name=myParser.getName();
				String value = myParser.getText();

				if (event == XmlPullParser.START_TAG && !name.equals(SENSICHIPS) && !name.equals(VAL)) {
					SENSOR_TAG = false;


					if (name.equals(EXPERIMENT)) {
						if (item != null) {
							item.prepareInternalState();
						}
                        //String layoutConfiguration = myParser.getAttributeValue(null, LAYOUT_CONFIGURATION);
						//conf = ;

						verifyParam = new SPMeasurementParameterEIS(conf);

						item = new SPBatchExperiment(conf);
						list.add(item);
						item.setDate(myParser.getAttributeValue(null, DATE));
						item.setGlobalRepetition(Integer.parseInt(myParser.getAttributeValue(null, GLOBAL_REPETITION)));
						item.setConfigRepetition(Integer.parseInt(myParser.getAttributeValue(null, CONFIG_REPETITION)));
						item.setDelay(Long.parseLong(myParser.getAttributeValue(null, DELAY)));

						if(myParser.getAttributeValue(null, RABBITMQ_SERVER_IP)!=null){
							item.setRabbitmq_server_ip(myParser.getAttributeValue(null, RABBITMQ_SERVER_IP));
						}
						if(myParser.getAttributeValue(null, RABBITMQ_USERNAME)!=null){
							item.setRabbitmq_username(myParser.getAttributeValue(null, RABBITMQ_USERNAME));
						}
						if(myParser.getAttributeValue(null, RABBITMQ_PASSWORD)!=null){
							item.setRabbitmq_password(myParser.getAttributeValue(null, RABBITMQ_PASSWORD));
						}
						if(myParser.getAttributeValue(null, RABBITMQ_VIRTUALHOST)!=null){
							item.setRabbitmq_virtualhost(myParser.getAttributeValue(null, RABBITMQ_VIRTUALHOST));
						}
						if(myParser.getAttributeValue(null, RABBITMQ_EXCHANGE)!=null){
							item.setRabbitmq_exchange(myParser.getAttributeValue(null, RABBITMQ_EXCHANGE));
						}


						item.setOutputDecimation(Integer.parseInt(myParser.getAttributeValue(null, OUTPUT_DECIMATION)));
						item.setFillBufferBeforeStart(Boolean.parseBoolean(myParser.getAttributeValue(null, FILL_BUFFER_BEFORE_START)));
						//item.setLayout_configuration(layoutConfiguration);
                        item.setBurst(Integer.parseInt(myParser.getAttributeValue(null, BURST)));
						item.setId(myParser.getAttributeValue(null, ID));

					} else if (name.equals(SENSOR)){
						SENSOR_TAG = true;
                        currentField = name;
                        if(myParser.getAttributeValue(null, SAVE).equals(ALL_MEASUREMENTS)){
							sensorTagIndex = SPMeasurementParameterEIS.CAPACITANCE;
						}
						else {
							sensorTagIndex = SPMeasurementParameterEIS.getMeasureTypeIndex(myParser.getAttributeValue(null, SAVE));
						}
						String[] toPlotTokens = myParser.getAttributeValue(null, PLOT).split(",");
                        if(toPlotTokens.length>1){
                        	throw new SPException("Multiple plot not allowed for SENSORS");
						}
                        toPlotIndex = new ArrayList<Integer>();
                        //for(int i=0; i<toPlotTokens.length; i++)
                        //{
                            toPlotIndex.add(SPMeasurementParameterEIS.getMeasureTypeIndex(toPlotTokens[0]));
                        //}

                        if(myParser.getAttributeValue(null,AUTOSCALE)!=null)
							autoscaleFlag = Boolean.parseBoolean(myParser.getAttributeValue(null,AUTOSCALE));
                        else{
                        	autoscaleFlag = false;
						}

						if(myParser.getAttributeValue(null, WAIT)!=null)
							timeToWait = Long.parseLong(myParser.getAttributeValue(null, WAIT));
						else{
							timeToWait = 0;
						}

						if(myParser.getAttributeValue(null, MEASUREMENT_TIME)!=null)
							timeToMeasure = Long.parseLong(myParser.getAttributeValue(null, MEASUREMENT_TIME));
						else{
							timeToMeasure = 0;
						}

                    }
                    else if (name.equals(EIS)){
						//currentField = name;

						item.containersList.add(new HashMap<>());
						item.eisnameList.add(new SPCSVRow());



						if(myParser.getAttributeValue(null, SAVE).equals(ALL_MEASUREMENTS)){
							//item.setIndexToSave(SPMeasurementParameterEIS.CAPACITANCE);
							item.eisIndexToSaveList.add(SPMeasurementParameterEIS.CAPACITANCE);
						}
						else {
							//item.setIndexToSave(SPMeasurementParameterEIS.getMeasureTypeIndex(myParser.getAttributeValue(null, SAVE)));
							item.eisIndexToSaveList.add(SPMeasurementParameterEIS.getMeasureTypeIndex(myParser.getAttributeValue(null, SAVE)));
						}

                        //item.setIndexToPlot(SPMeasurementParameterEIS.getMeasureTypeIndex(myParser.getAttributeValue(null, PLOT)));

						String[] plotTokens = (myParser.getAttributeValue(null, PLOT)).split(",");
						ArrayList<Integer> indexToPlot = new ArrayList<Integer>();
						for(int i=0; i<plotTokens.length; i++){

							indexToPlot.add(SPMeasurementParameterEIS.getMeasureTypeIndex(plotTokens[i].trim()));
						}
						item.eisIndexToPlotList.add(indexToPlot);

						//item.eisIndexToPlotList.add(SPMeasurementParameterEIS.getMeasureTypeIndex(myParser.getAttributeValue(null, PLOT)));

						autoscaleFlag = Boolean.parseBoolean(myParser.getAttributeValue(null,AUTOSCALE));
						item.eisAutoscaleActiveFlags.add(autoscaleFlag);

						autoRangeFlag = Boolean.parseBoolean(myParser.getAttributeValue(null,AUTORANGE));
						item.eisAutoRangeActiveFlags.add(autoRangeFlag);
						item.timeToWaitList.add(Long.parseLong(myParser.getAttributeValue(null, WAIT)));

						if(myParser.getAttributeValue(null, MEASUREMENT_TIME)!=null)
                        	item.measurementTimeList.add(Long.parseLong(myParser.getAttributeValue(null, MEASUREMENT_TIME)));
						else
							item.measurementTimeList.add(new Long(0));

						//item.setTimeToWait(Long.parseLong(myParser.getAttributeValue(null, WAIT)));


						autoscaleFlag=false;
					} else if(name.equals(POT)){
                    	currentField = name;
						POT_TAG = true;
						currentMeasurementParamPOT = new SPMeasurementParameterPOT(conf);


					}else if(POT_TAG){
						currentField = name;
					}else if (currentField == null) {
                        SENSOR_TAG = false;
						currentField = name;
						order = myParser.getAttributeValue(null, ORDER);
						insertInOrder(item.eisnameList.get(item.eisnameList.size()-1), currentField, Integer.parseInt(order));
                        //System.out.println("order: " + order + ", name: " + name);
					}


				} else if (event == XmlPullParser.TEXT && value != null && !value.contains("\n") && currentField != null){

				    if (SENSOR_TAG){
                        SPSensingElementOnChip sensingElementOnChip = conf.searchSPSensingElementOnChip(value);
                        sensingElementOnChip.setAutoScaleActive(autoscaleFlag);

                        autoscaleFlag = false;
                        if(sensingElementOnChip.getSensingElementOnFamily().getSensingElement().getMeasureTechnique().equals("DIRECT")){
                        	sensorTagIndex = 0;
							//toPlotIndex.add(0);
						}

                        item.addSPSensingElementOnChip(sensingElementOnChip, sensorTagIndex,toPlotIndex,timeToWait,timeToMeasure);
                        //SENSOR_TAG = false;
					}else if(POT_TAG){
				    	if(currentField.equals(TYPE)){
							currentMeasurementParamPOT.setType(value);
						}
						else if(currentField.equals(RSENSE)){
							currentMeasurementParamPOT.setRsense(value);
						}
						else if(currentField.equals(INGAIN)){
							currentMeasurementParamPOT.setInGain(value);
						}
						else if(currentField.equals(PORT)){
							currentMeasurementParamPOT.setPort(value);
						}
						else if(currentField.equals(CONTACTS)){
							currentMeasurementParamPOT.setContacts(value);
						}
						else if(currentField.equals(INITIAL_POTENTIAL)){
							currentMeasurementParamPOT.setInitialPotential(Integer.parseInt(value));
						}
						else if(currentField.equals(FINAL_POTENTIAL)){
							currentMeasurementParamPOT.setFinalPotential(Integer.parseInt(value));
						}
						else if(currentField.equals(STEP)){
							currentMeasurementParamPOT.setStep(Integer.parseInt(value));
						}
						else if(currentField.equals(PULSE_AMPLITUDE)){
							currentMeasurementParamPOT.setPulseAmplitude(Integer.parseInt(value));
						}
						else if(currentField.equals(PULSE_PERIOD)){
							currentMeasurementParamPOT.setPulsePeriod(Integer.parseInt(value));
						}
					}
					else {
						if (allowedValues.get(currentField) == null) {
							throw new XmlPullParserException("Unknown field " + currentField + " in batch batch file at line " + myParser.getLineNumber() + ".");
						}
						if (item.containersList.get(item.containersList.size()-1).get(order) == null) {
							item.containersList.get(item.containersList.size()-1).put(order, new ArrayList<String>());
						}

						item.containersList.get(item.containersList.size()-1).get(order).add(value);
						// Verify if the name of the field and the value are allowed

						if(!value.equals("auto"))
							SPBatchExperiment.setParameters(verifyParam, currentField, value);
					}

				} else if (event == XmlPullParser.END_TAG){
					if (name.equals(VAL)){
						if (currentField == null){
							throw new XmlPullParserException("Field not specified in batch batch file at line " + myParser.getLineNumber());
						}
					} else if (name.equals(currentField)){
						currentField = null;
					} else if (name.equals(EXPERIMENT)){
						// How to recover attributes?
						//items.add(item);
					} else if (name.equals(EIS)){


                    }
                    else if(name.equals(POT)){
						POT_TAG = false;
						item.getPotMeasurementParameterList().add(currentMeasurementParamPOT);
					}
				}
				event = myParser.next();
				//System.out.println("SENSOR_TAG: " + SENSOR_TAG);
			}

			for (int i = 0; i < item.eisnameList.size(); i++) {
				for(int j=0; j<item.eisnameList.get(i).size(); j++) {
					if (item.eisnameList.get(i).getItem(j) == null) {
						throw new XmlPullParserException("The order number " + (j + 1) + " is missing in the batch file.");
					}
				}
			}

			if(item != null){
				item.prepareInternalState();
			}

		} catch (SPException e) {
			throw new SPBatchManagerException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new SPBatchManagerException(e.getMessage());
		}
		//item.prepareInternalState();
		return list;
	}

	private static void insertInOrder(SPCSVRow nameList, String currentField, int order) throws XmlPullParserException
	{
		int elementsToAdd = order - nameList.size();

		for (int i = 0; i < elementsToAdd; i++) {
			nameList.addItem("");
		}
		if (!nameList.getItem(order - 1).equals(""))
			throw new XmlPullParserException("There are two field with the same order. Please verify the batch file.");

		nameList.setItem(order - 1, currentField);
	}
}
