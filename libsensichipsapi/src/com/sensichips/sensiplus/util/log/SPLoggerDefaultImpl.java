package com.sensichips.sensiplus.util.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mario on 21/12/2015.
 */
public class SPLoggerDefaultImpl implements SPLoggerInterface {

    private static final String PREFIX2 = "--L2: ";
    private static final String PREFIX1 = "--\tL1: ";
    private static final String PREFIX0 = "--\t\tL0: ";
    private static final String PREFIXANY = "--\t";
    private static final String PREFIXERROR = "--ERROR: ";
    private static final String PREFIXWRITE = "";

    public static String NIX_EXPR = "nux";
    public static String NIX = "*nix";
    public static String WINDOWS = "Windows";
    public static String OP_SYSTEM = NIX;


    private PrintWriter logFile = null;
    private int lineCounter = 1;
    private int fileCounter = 1;
    private int lineCounterLimit = 200000;
    private String logFileNameBase;
    private String output;

    public SPLoggerDefaultImpl(){
        String dirName = "log";
        File f = new File(dirName);
        if (!f.exists()){
            f.mkdir();
        } else {
            if (f.isDirectory()){
                File[] files = f.listFiles();
                for(int i = 0; i < files.length; i++){
                    files[i].delete();
                }
            } else {
                f.delete();
            }
        }

        String pathSeparator = "/";
        String osName = System.getProperty("os.name");
        //System.out.println(osName);
        OP_SYSTEM = osName.contains(NIX_EXPR) ? NIX : WINDOWS;

        if (OP_SYSTEM != NIX){
            pathSeparator = "\\";
        }

        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd_hh_mm_ss");
        logFileNameBase = "." + pathSeparator + dirName + pathSeparator + "log_spwinuxtools_" + dt1.format(new Date());
        regenerateLogFile(logFileNameBase + "_" + fileCounter + ".txt");

    }

    private void regenerateLogFile(String logFileName){
        try {
            logFile = new PrintWriter(logFileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    /*
    @Override
    public void d(String msgType, String msg, int debug_level) {

        try{
            String prefix = "";
            String output = msg.replace("\r", "");

            if (output.endsWith("\n")){
                output = output.substring(0, output.length() - 1);
            }

            SPLogger.DEBUG_TH = SPLogger.DEBUG_TH | SPLoggerInterface.DEBUG_VERBOSITY_ANY;

            if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_ANY) != 0) {
                prefix = PREFIXANY;
            } else if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_L2) != 0) {
                prefix = PREFIX2;
            } else if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_L1) != 0){
                prefix = PREFIX1;
            } else if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_L0) != 0){
                prefix = PREFIX0;
            } else if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_ERROR) != 0){
                prefix = "ERROR: ";
            }
            output = prefix + msgType + ": " + output;

            output = output.replace("\n", "\n" + prefix + msgType + ": ");

            if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_ERROR) != 0){
                System.err.println(output);
            } else if ((debug_level & SPLogger.DEBUG_TH) != 0){
                System.out.println(output);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }*/

    @Override
    public synchronized void d(String msgType, String msg, int debug_level) {

        if (msg == null || msgType == null) {
            return;
        }

        //System.out.println("debug_level: " + debug_level + "; SPLogger.DEBUG_TH: " + SPLogger.DEBUG_TH);


        try {
            String prefix = "";
            output = msg.replace("\r", "");

            if (output.endsWith("\n")) {
                output = output.substring(0, output.length() - 1);
            }

            SPLogger.DEBUG_TH = SPLogger.DEBUG_TH | SPLoggerInterface.DEBUG_VERBOSITY_ANY;
            prefix = PREFIXANY;
            msgType = msgType + ": ";

            if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_ERROR) != 0) {
                prefix = PREFIXERROR;
            } else if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_L2) != 0) {
                prefix = PREFIX2;
            } else if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_L1) != 0 || (debug_level & SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE) != 0) {
                if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE) != 0){
                    prefix = PREFIXWRITE;
                    msgType = "";
                } else {
                    prefix = PREFIX1;
                }

            } else if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_L0) != 0) {
                prefix = PREFIX0;
            }


            output = prefix + msgType + output;
            output = output.replace("\n", "\n" + prefix + msgType);


            if ((debug_level & SPLoggerInterface.DEBUG_VERBOSITY_ERROR) != 0) {
                System.err.println(output);
                if (logFile != null){
                    logFile.println(output);
                    logFile.flush();
                }
            } else if ((debug_level & SPLogger.DEBUG_TH) != 0) {
                System.out.println(output);
                if (logFile != null){
                    logFile.println(output);
                    logFile.flush();
                }
            }
            lineCounter++;
            if (lineCounter % lineCounterLimit == 0){
                fileCounter++;
                regenerateLogFile(logFileNameBase + "_" + fileCounter + ".txt");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}


