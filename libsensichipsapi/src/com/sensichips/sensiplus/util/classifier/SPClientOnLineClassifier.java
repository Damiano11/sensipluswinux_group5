package com.sensichips.sensiplus.util.classifier;

import java.io.*;
import java.net.Socket;

public class SPClientOnLineClassifier {
    private Socket socketClient = null;
    private BufferedReader bufferedReader = null;
    private PrintWriter printWriter = null;

    public static int NONE = -1;
    public static int WATER = 0;
    public static int HYDROGEN_PEROXIDE = 1;
    public static int AMMONIA = 2;
    public static int AIR = 3;

    private int numOfClasses = 3;


    public static String[] CLASS_NAME = new String[]{"NONE", "WATER", "HYDROGEN PEROXIDE", "AMMONIA", "AIR"};

    public SPClientOnLineClassifier(String host, int portNumber, String chipID, int numOfClasses) throws IOException {

        //String host = "hego.unicas.it";
        //int portNumber = 43547;
        this.numOfClasses = numOfClasses;
        System.out.println("Try to connect: " + host + ", port: " + portNumber);
        socketClient = new Socket(host, portNumber);

        OutputStream outputStream = socketClient.getOutputStream();
        printWriter = new PrintWriter(outputStream, true);

        InputStream inputStream = socketClient.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        bufferedReader = new BufferedReader(inputStreamReader);

        printWriter.println("0X" + chipID);

        String ack = bufferedReader.readLine();
        System.out.println("Sending data after ack (" + ack + ") ...");


    }

    public SPClientOnLineClassifier(String host, int portNumber, int numOfClasses) throws IOException {

        //String host = "hego.unicas.it";
        //int portNumber = 43547;
        this.numOfClasses = numOfClasses;
        System.out.println("Try to connect: " + host + ", port: " + portNumber);
        socketClient = new Socket(host, portNumber);

        OutputStream outputStream = socketClient.getOutputStream();
        printWriter = new PrintWriter(outputStream, true);

        InputStream inputStream = socketClient.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        bufferedReader = new BufferedReader(inputStreamReader);

        //printWriter.println("0X" + chipID);

        //String ack = bufferedReader.readLine();
        //System.out.println("Sending data after ack (" + ack + ") ...");


    }


    public String sendDataToClassifier(String valueToSend) throws IOException {
        printWriter.println("" + valueToSend);
        String predictedClass = bufferedReader.readLine();

        System.out.println("Predicted class: " + predictedClass);

        int index = (int)Float.parseFloat(predictedClass);

        String out = "";
        index = index + 1;
        if (numOfClasses == 3){

            if (index == 0) {
                out = CLASS_NAME[0];
            } else if (index == 1){
                out = CLASS_NAME[2];
            } else if (index == 2){
                out = CLASS_NAME[3];
            } else {
                out = CLASS_NAME[4];
            }

        } else {
            if (index >= 0 && index < 5){
                out = CLASS_NAME[index];
            } else {
                out = CLASS_NAME[4];
            }
        }
        return out;
    }


    public void close() throws IOException {
        if (socketClient != null)
            socketClient.close();
    }



}
