package com.sensichips.sensiplus.util;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;

import java.io.Serializable;

public class SPDataRepresentationManager implements Serializable {

    public static final long serialVersionUID = 42L;

    private String measureUnit = ""; //Volt, Ampere, ecc.

    //private int    multiplier_index = 0;
    private double rangeMin;
    private double rangeMax;
    private double alarmThreshold;
    private int    initialMultiplierIndex = 0;
    private double initialRangeMin;
    private double initialRangeMax;
    private double initialAlarmThreshold;

    //private double th = 1;

    private int multiplier_index = 0;

    public int getIndexToReduce() {
        return indexToReduce;
    }

    public void setIndexToReduce(int indexToReduce) {
        this.indexToReduce = indexToReduce;
    }

    private int indexToReduce = 0;


    public static final String MULTIPLIER_PREFIX[] = new String[]{  "y", "z", "a", "f", "p", "n", new String(Character.toChars(0x00B5)),   "m", "", "k", "M", "G", "T", "P", "E", "Z", "Y"};
    public static final double MULTIPLIER[] = new double[]{         -24, -21, -18, -15, -12,  -9, -6,                                               -3,   0,  3,   6,   9,   12,  15,  18,  21,  24};


    public static final String MEASURE_UNIT_LABEL[] = new String[]{"O", "F", "H", "C", "%", "V", "A", "L", "time", "", "rad"};
    public static final String MEASURE_UNIT[] = new String[]{new String(Character.toChars(0x03a9)), "F", "H", new String(Character.toChars(0x00B0)) + "C", "%", "V", "A", "L", "t", "", "rad"};


    public SPDataRepresentationManager(int indexToReduce){
        this.indexToReduce = indexToReduce;
    }



    public void initForEISRawMeasurements() throws SPException{
        if (this.indexToReduce == SPMeasurementParameterEIS.CAPACITANCE){
            this.setMeasureUnit("F");
            this.setRangeMin("0.0");
            this.setRangeMax("1.0");
            this.setInitial_multiplier("-12");
            this.setDefault_alarm_threshold("0.5");
        } else if (this.indexToReduce == SPMeasurementParameterEIS.RESISTANCE){
            this.setMeasureUnit(new String(Character.toChars(0x03a9)));
            this.setRangeMin("0.0");
            this.setRangeMax("1000.0");
            this.setInitial_multiplier("0");
            this.setDefault_alarm_threshold("1000");
        } else if (this.indexToReduce == SPMeasurementParameterEIS.INDUCTANCE){
            this.setMeasureUnit("H");
            this.setRangeMin("0.0");
            this.setRangeMax("1.0");
            this.setInitial_multiplier("-3");
            this.setDefault_alarm_threshold("1.0");
        } else if (this.indexToReduce == SPMeasurementParameterEIS.IN_PHASE){
            this.setMeasureUnit("O");
            this.setRangeMin("0.0");
            this.setRangeMax("33000");
            this.setInitial_multiplier("0");
            this.setDefault_alarm_threshold("15000");
        } else if (this.indexToReduce == SPMeasurementParameterEIS.QUADRATURE){
            this.setMeasureUnit("rad");
            this.setRangeMin("0.0");
            this.setRangeMax(Math.PI/2+"");
            this.setInitial_multiplier("0");
            this.setDefault_alarm_threshold(Math.PI/4+"");
        } else if (this.indexToReduce == SPMeasurementParameterEIS.MODULE){
            this.setMeasureUnit("O");
            this.setRangeMin("0.0");
            this.setRangeMax("1000.0");
            this.setInitial_multiplier("0");
            this.setDefault_alarm_threshold("500");

        } else if (this.indexToReduce == SPMeasurementParameterEIS.PHASE){
            this.setMeasureUnit("rad");
            this.setRangeMin("0.0");
            this.setRangeMax(""+(Math.PI/2));
            this.setInitial_multiplier("0");
            this.setDefault_alarm_threshold(Math.PI/4+"");
        }else if (this.indexToReduce == SPMeasurementParameterEIS.CONDUCTANCE){
            this.setMeasureUnit("S");
            this.setRangeMin("1E-8");
            this.setRangeMax("1E-7");
            this.setInitial_multiplier("0");
            this.setDefault_alarm_threshold("0.6E-7");

        } else if (this.indexToReduce == SPMeasurementParameterEIS.SUSCEPTANCE){
            this.setMeasureUnit("S");
            this.setRangeMin("0.0");
            this.setRangeMax("1000.0");
            this.setInitial_multiplier("0");
            this.setDefault_alarm_threshold("500");
        } else if (this.indexToReduce == SPMeasurementParameterEIS.VOLTAGE){
            this.setMeasureUnit("V");
            this.setRangeMin("0.0");
            this.setRangeMax("1000.0");
            this.setInitial_multiplier("-3");
            this.setDefault_alarm_threshold("500");
        } else if (this.indexToReduce == SPMeasurementParameterEIS.CURRENT){
            this.setMeasureUnit("A");
            this.setRangeMin("0.0");
            this.setRangeMax("1000.0");
            this.setInitial_multiplier("-3");
            this.setDefault_alarm_threshold("500");
        }
        else if (this.indexToReduce == SPMeasurementParameterEIS.DELTA_V_APPLIED){
            this.setMeasureUnit("V");
            this.setRangeMin("0.0");
            this.setRangeMax("1000.0");
            this.setInitial_multiplier("-3");
            this.setDefault_alarm_threshold("500");
        }

        else if (this.indexToReduce == SPMeasurementParameterEIS.CURRENT_APPLIED){
            this.setMeasureUnit("A");
            this.setRangeMin("0.0");
            this.setRangeMax("1000.0");
            this.setInitial_multiplier("-3");
            this.setDefault_alarm_threshold("500");
        }
    }
    //String measureUnit, double initialMultiplier, double rangeMin, double rangeMax, double range_default_threshold) throws SPException {
    //    this.rangeMin = rangeMin;
    //    this.rangeMax = rangeMax;
    //    this.default_alarm_threshold = range_default_threshold;
    //    this.measureUnit = measureUnit;

    public int getInitialMultiplierIndex() {
        return initialMultiplierIndex;
    }

    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    public double getRangeMin() {
        return rangeMin;
    }

    public void setRangeMin(String rangeMin)throws SPException {
        try{
            this.initialRangeMin = Double.parseDouble(rangeMin);
            this.rangeMin = initialRangeMin;
        } catch (Exception e){
            throw new SPException("rangeMin format invalid: " + rangeMin);
        }
    }

    public double getRangeMax() {
        return initialRangeMax;
    }

    public void setRangeMax(String rangeMax)throws SPException {
        try{
            this.initialRangeMax =  Double.parseDouble(rangeMax);
            this.rangeMax = initialRangeMax;
        } catch (Exception e){
            throw new SPException("rangeMax format invalid: " + rangeMax);
        }
    }

    public void setAlarmThreshold(double alarmThreshold) {
        this.alarmThreshold = alarmThreshold;
    }

    public double getAlarmThreshold() {
        return alarmThreshold;
    }


    public void setDefault_alarm_threshold(String default_alarm_threshold) throws SPException {
        try{
            this.initialAlarmThreshold = Double.parseDouble(default_alarm_threshold);
            this.alarmThreshold = initialAlarmThreshold;

        } catch (Exception e){
            throw new SPException("alarmThreshold format invalid: " + default_alarm_threshold);
        }
    }

    public String getMultiplierPrefix(){
        return MULTIPLIER_PREFIX[initialMultiplierIndex];
    }



    public void setInitial_multiplier(String multiplierString) throws SPException {
        double multiplier;
        try{
            multiplier = Double.parseDouble(multiplierString);
        } catch (Exception e){
            throw new SPException("multiplierIndex format invalid: " + multiplierString);
        }


        boolean found = false;
        int i = 0;
        while(i < MULTIPLIER.length && !found){
            found = MULTIPLIER[i] == multiplier;
            i++;
        }
        if (!found){
            String validLabels = "";
            for(int j = 0; j < MULTIPLIER.length; j++) {
                validLabels += MULTIPLIER[j] + ", ";
            }
            throw new SPException("Invalid MEASURE_UNIT label: " + multiplier + ".\nValid labels are: " + validLabels);
        }
        i--;
        initialMultiplierIndex = i;
        multiplier_index = i;
    }


    public static String getMeasureUnitCoded(String measureUnit) throws SPException {

        boolean found = false;
        int i = 0;
        while(i < MEASURE_UNIT_LABEL.length && !found){
            found = MEASURE_UNIT_LABEL[i].equalsIgnoreCase(measureUnit);
            i++;
        }
        if (!found){
            String validLabels = "";
            for(int j = 0; j < MEASURE_UNIT_LABEL.length; j++) {
                validLabels += MEASURE_UNIT_LABEL[j] + ", ";
            }
            throw new SPException("Invalid MEASURE_UNIT label: " + measureUnit + ".\nValid labels are: " + validLabels);
        }
        i--;
        return MEASURE_UNIT[i];
    }

    public double getMultiplier() {
        return multiplier;
    }

    private double multiplier = 1;
    public SPDataRepresentation adapt(double[][] values, boolean changeMultiplier){
        double k = 1000;
        double coeff = 1;
        double value = 0;
        if(changeMultiplier) {
            for (int i = 0; i < values.length; i++) {
                value += values[i][indexToReduce];
            }
            value = value / values.length;
            value = value * multiplier;
            value = Math.abs(value);
            if (value < 1 && value!=0) {
                do {
                    multiplier_index--;
                    value = value * k;
                    coeff = coeff * k;
                } while (value < 1);

            } else if (value >= 1000 && value!=0) {
                k = 1 / k;
                do {
                    multiplier_index++;
                    value = value * k;
                    coeff = coeff * k;
                } while (value >= 1000);
            }

            multiplier = multiplier * coeff;
            rangeMax *= coeff;
            rangeMin *= coeff;
            alarmThreshold *= coeff;

        }



        for (int i = 0; i < values.length; i++)
        {
            values[i][indexToReduce] *= multiplier;
        }


        return new SPDataRepresentation(measureUnit, MULTIPLIER_PREFIX[multiplier_index], rangeMin, rangeMax, alarmThreshold, values, indexToReduce, value,changeMultiplier);

    }


    public static void main(String args[]){
        try {

            String p = "1.0";
            String[] tokens = p.split("\\.");
            double initialRangeMin = 0;
            double initialRangeMax = 0.007;
            double initialAlarmThreshold = 0.006;
            int indexToReduce = 1;


            SPDataRepresentationManager spDataRepresentationManager = new SPDataRepresentationManager(indexToReduce);
            spDataRepresentationManager.setMeasureUnit("F");
            spDataRepresentationManager.setInitial_multiplier("0");
            spDataRepresentationManager.setRangeMin("" + initialRangeMin);
            spDataRepresentationManager.setRangeMax("" + initialRangeMax);
            spDataRepresentationManager.setDefault_alarm_threshold("" + initialAlarmThreshold);

            double[][] x = new double[][]{{1,0.00523456789,3,4},{2, 0.009, 3, 3},{1, 0.034232, 3, 4},{1, 0.001231, 3, 4}};
            //double multiplier = 1e-21;
            long start = System.currentTimeMillis();



            for(int i = 0; i < 1000; i++){
                System.out.print("Original: ");
                for(int k = 0; k < x.length; k++){
                    System.out.print(x[k][indexToReduce] + ", ");
                }
                System.out.println();

                SPDataRepresentation spDataRepresentation = spDataRepresentationManager.adapt(x,true);
                System.out.print("[" + spDataRepresentationManager.getMultiplierPrefix() + spDataRepresentationManager.getMeasureUnit() + "]" + ": ");
                System.out.println(spDataRepresentation);

                //spDataRepresentationManager.setRangeMin("" + initialRangeMin * 2);
                //spDataRepresentationManager.setRangeMax("" + initialRangeMax * 2);
                //spDataRepresentationManager.setDefault_alarm_threshold("" + initialAlarmThreshold * 2);
                for(int k = 0; k < x.length; k++){
                    x[k][indexToReduce] = (i + 1) * Math.random() * 0.001;
                }
            }

            System.out.println("Elapsed time: " + (System.currentTimeMillis() - start));
        } catch (SPException e) {
            e.printStackTrace();
        }
    }


}
