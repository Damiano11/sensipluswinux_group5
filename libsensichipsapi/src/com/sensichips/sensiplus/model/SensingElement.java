package com.sensichips.sensiplus.model;

import javafx.beans.property.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"idSPSensingElement", "RSense", "inGain", "outGain", "contacts", "frequency", "harmonic",
        "dcBias", "modeVI", "measureTechnique", "measureType", "filter", "phaseShiftMode", "phaseShift", "iq", "conversionRate",
        "inportADC", "NData", "measure_Unit", "name", "rangeMin", "rangeMax", "defaultAlarmThreshold", "multiplier"})
public class SensingElement {

    private String oldidSPSensingElement = null;

    private final StringProperty idSPSensingElement;
    private final IntegerProperty rSense;
    private final IntegerProperty inGain;
    private final IntegerProperty outGain;
    private final StringProperty contacts;
    private final IntegerProperty frequency;
    private final StringProperty harmonic;
    private final IntegerProperty dcBias;
    private final StringProperty modeVI;
    private final StringProperty measureTechnique;
    private final StringProperty measureType;
    private final IntegerProperty filter;
    private final StringProperty phaseShiftMode;
    private final IntegerProperty phaseShift;
    private final StringProperty iq;
    private final IntegerProperty conversionRate;
    private final StringProperty inportADC;
    private final IntegerProperty nData;
    private final StringProperty measure_Unit;
    private final StringProperty name;
    private final DoubleProperty rangeMin;
    private final DoubleProperty rangeMax;
    private final DoubleProperty defaultAlarmThreshold;
    private final IntegerProperty multiplier;


    /* Default constructor.
     */
    public SensingElement() {
        this("", Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, "", Integer.MIN_VALUE, "", Integer.MIN_VALUE,
                "", "", "", Integer.MIN_VALUE, "", Integer.MIN_VALUE,
                "", Integer.MIN_VALUE, "", Integer.MIN_VALUE, "", "", Double.NaN, Double.NaN,
                Double.NaN, Integer.MIN_VALUE);
    }

    public SensingElement(String id) {
        this(id, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, "", Integer.MIN_VALUE, "", Integer.MIN_VALUE,
                "", "", "", Integer.MIN_VALUE, "", Integer.MIN_VALUE,
                "", Integer.MIN_VALUE, "", Integer.MIN_VALUE, "", "", Double.NaN, Double.NaN,
                Double.NaN, Integer.MIN_VALUE);
    }


    public SensingElement(String idSPSensingElement, Integer rSense) {

        this(idSPSensingElement, rSense, Integer.MIN_VALUE, Integer.MIN_VALUE, "", Integer.MIN_VALUE, "", Integer.MIN_VALUE,
                "", "", "", Integer.MIN_VALUE, "", Integer.MIN_VALUE,
                "", Integer.MIN_VALUE, "", Integer.MIN_VALUE, "", "", Double.NaN, Double.NaN, Double.NaN, Integer.MIN_VALUE);
    }


    public SensingElement(String idSPSensingElement, Integer rSense, Integer inGain, Integer outGain, String contacts,
                          Integer frequency, String harmonic, Integer dcBias, String modeVI, String measureTechnique,
                          String measureType, Integer filter, String phaseShiftMode, Integer phaseShift, String iq,
                          Integer conversionRate, String inportADC, Integer nData, String measure_Unit, String name,
                          Double rangeMin, Double rangeMax, Double defaultAlarmThreshold, Integer multiplier) {

        this.idSPSensingElement = new SimpleStringProperty(idSPSensingElement);
        this.rSense = new SimpleIntegerProperty(rSense);
        this.inGain = new SimpleIntegerProperty(inGain);
        this.outGain = new SimpleIntegerProperty(outGain);
        this.contacts = new SimpleStringProperty(contacts);
        this.frequency = new SimpleIntegerProperty(frequency);
        this.harmonic = new SimpleStringProperty(harmonic);
        this.dcBias = new SimpleIntegerProperty(dcBias);
        this.modeVI = new SimpleStringProperty(modeVI);
        this.measureTechnique = new SimpleStringProperty(measureTechnique);
        this.measureType = new SimpleStringProperty(measureType);
        this.filter = new SimpleIntegerProperty(filter);
        this.phaseShiftMode = new SimpleStringProperty(phaseShiftMode);
        this.phaseShift = new SimpleIntegerProperty(phaseShift);
        this.iq = new SimpleStringProperty(iq);
        this.conversionRate = new SimpleIntegerProperty(conversionRate);
        this.inportADC = new SimpleStringProperty(inportADC);
        this.nData = new SimpleIntegerProperty(nData);
        this.measure_Unit = new SimpleStringProperty(measure_Unit);
        this.name = new SimpleStringProperty(name);
        this.rangeMin = new SimpleDoubleProperty(rangeMin);
        this.rangeMax = new SimpleDoubleProperty(rangeMax);
        this.defaultAlarmThreshold = new SimpleDoubleProperty(defaultAlarmThreshold);
        this.multiplier = new SimpleIntegerProperty(multiplier);

    }

    public String getOldidSPSensingElement() {
        return oldidSPSensingElement;
    }

    public void setoldidSPSensingElement() {
        this.oldidSPSensingElement = this.idSPSensingElement.get();
    }

    @XmlElement(required = true)
    public String getIdSPSensingElement() {
        return idSPSensingElement.get();
    }

    public StringProperty idSPSensingElementProperty() {
        return idSPSensingElement;
    }

    public void setidSPSensingElement(String idSPSensingElement) {
        this.idSPSensingElement.set(idSPSensingElement);
    }

    @XmlElement(required = true)
    public Integer getRSense() {
        if (rSense.get() == Integer.MIN_VALUE) {
            return null;
        }
        return rSense.get();
    }

    public IntegerProperty rSenseProperty() {
        return rSense;
    }

    public void setrSense(int rSense) {
        this.rSense.set(rSense);
    }

    @XmlElement(name = "InGain", required = true)
    public Integer getInGain() {
        if (inGain.get() == Integer.MIN_VALUE) {
            return null;
        }
        return inGain.get();
    }

    public IntegerProperty inGainProperty() {
        return inGain;
    }

    public void setInGain(int inGain) {
        this.inGain.set(inGain);
    }

    @XmlElement(name = "OutGain", required = true)
    public Integer getOutGain() {
        if (outGain.get() == Integer.MIN_VALUE) {
            return null;
        }
        return outGain.get();
    }

    public IntegerProperty outGainProperty() {
        return outGain;
    }

    public void setOutGain(int outGain) {
        this.outGain.set(outGain);
    }

    @XmlElement(name = "Contacts", required = true)
    public String getContacts() {
        if (getMeasureTechnique().equals("DIRECT")) {
            return null;
        }

        return contacts.get();
    }

    public String contactsProperty() {
        return contacts.get();
    }

    public void setContacts(String contacts) {
        this.contacts.set(contacts);
    }

    @XmlElement(name = "Frequency", required = true)
    public Integer getFrequency() {
        if (frequency.get() == Integer.MIN_VALUE) {
            return null;
        }

        return frequency.get();
    }

    public IntegerProperty frequencyProperty() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency.set(frequency);
    }

    @XmlElement(name = "Harmonic", required = true)
    public String getHarmonic() {
        if (getMeasureTechnique().equals("DIRECT")) {
            return null;
        }
        return harmonic.get();
    }

    public String harmonicProperty() {
        return harmonic.getName();
    }

    public void setHarmonic(String harmonic) {
        this.harmonic.set(harmonic);
    }

    @XmlElement(name = "DcBias", required = true)
    public Integer getDcBias() {
        if (dcBias.get() == Integer.MIN_VALUE) {
            return null;
        }
        return dcBias.get();
    }

    public IntegerProperty dcBiasProperty() {
        return dcBias;
    }

    public void setDcBias(int dcBias) {
        this.dcBias.set(dcBias);
    }

    @XmlElement(name = "ModeVI", required = true)
    public String getModeVI() {
        if (getMeasureTechnique().equals("DIRECT")) {
            return null;
        }
        return modeVI.get();
    }

    public String modeVIProperty() {
        return modeVI.get();
    }

    public void setModeVI(String modeVI) {
        this.modeVI.set(modeVI);
    }

    @XmlElement(name = "MeasureTechnique", required = true)
    public String getMeasureTechnique() {
        return measureTechnique.get();
    }

    public String measureTechniqueProperty() {
        return measureTechnique.get();
    }

    public void setMeasureTechnique(String measureTechnique) {
        this.measureTechnique.set(measureTechnique);
    }

    @XmlElement(name = "MeasureType", required = true)
    public String getMeasureType() {
        if (getMeasureTechnique().equals("DIRECT")) {
            return null;
        }
        return measureType.get();
    }

    public String measureTypeProperty() {
        return measureType.get();
    }

    public void setMeasureType(String measureType) {
        this.measureType.set(measureType);
    }

    @XmlElement(name = "Filter", required = true)
    public Integer getFilter() {
        return filter.get();
    }

    public IntegerProperty filterProperty() {
        return filter;
    }

    public void setFilter(int filter) {
        this.filter.set(filter);
    }

    @XmlElement(name = "PhaseShiftMode", required = true)
    public String getPhaseShiftMode() {
        if (getMeasureTechnique().equals("DIRECT")) {
            return null;
        }
        return phaseShiftMode.get();
    }

    public String phaseShiftModeProperty() {
        return phaseShiftMode.get();
    }

    public void setPhaseShiftMode(String phaseShiftMode) {
        this.phaseShiftMode.set(phaseShiftMode);
    }

    @XmlElement(name = "PhaseShift", required = true)
    public Integer getPhaseShift() {
        if (phaseShift.get() == Integer.MIN_VALUE) {
            return null;
        }
        return phaseShift.get();
    }

    public IntegerProperty phaseShiftProperty() {
        return phaseShift;
    }

    public void setPhaseShift(int phaseShift) {
        this.phaseShift.set(phaseShift);
    }

    @XmlElement(name = "IQ", required = true)
    public String getIq() {
        if (getMeasureTechnique().equals("DIRECT")) {
            return null;
        }
        return iq.get();
    }

    public String iqProperty() {
        return iq.get();
    }

    public void setIq(String iq) {
        this.iq.set(iq);
    }

    @XmlElement(name = "ConversionRate", required = true)
    public Integer getConversionRate() {
        return conversionRate.get();
    }

    public IntegerProperty conversionRateProperty() {
        return conversionRate;
    }

    public void setConversionRate(int conversionRate) {
        this.conversionRate.set(conversionRate);
    }

    @XmlElement(name = "InPortADC", required = true)
    public String getInportADC() {
        return inportADC.get();
    }

    public StringProperty inportADCProperty() {
        return inportADC;
    }

    public void setInportADC(String inportADC) {
        this.inportADC.set(inportADC);
    }

    @XmlElement(name = "NData", required = true)
    public Integer getNData() {
        return nData.get();
    }

    public IntegerProperty nDataProperty() {
        return nData;
    }

    public void setnData(int nData) {
        this.nData.set(nData);
    }

    @XmlElement(name = "MEASURE_UNIT", required = true)
    public String getMeasure_Unit() {
        return measure_Unit.get();
    }

    public StringProperty measure_UnitProperty() {
        return measure_Unit;
    }

    public void setMeasure_Unit(String measure_Unit) {
        this.measure_Unit.set(measure_Unit);
    }

    @XmlElement(name = "NAME", required = true)
    public String getName() {
        if (getMeasureTechnique().equals("DIRECT")) {
            return null;
        }
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    @XmlElement(name = "RANGE_MIN", required = true)
    public Double getRangeMin() {
        return rangeMin.get();
    }

    public DoubleProperty rangeMinProperty() {
        return rangeMin;
    }

    public void setRangeMin(double rangeMin) {
        this.rangeMin.set(rangeMin);
    }

    @XmlElement(name = "RANGE_MAX", required = true)
    public Double getRangeMax() {
        return rangeMax.get();
    }

    public DoubleProperty rangeMaxProperty() {
        return rangeMax;
    }

    public void setRangeMax(double rangeMax) {
        this.rangeMax.set(rangeMax);
    }

    @XmlElement(name = "DEFAULT_ALARM_THRESHOLD", required = true)
    public Double getDefaultAlarmThreshold() {
        return defaultAlarmThreshold.get();
    }

    public DoubleProperty defaultAlarmThresholdProperty() {
        return defaultAlarmThreshold;
    }

    public void setDefaultAlarmThreshold(double defaultAlarmThreshold) {
        this.defaultAlarmThreshold.set(defaultAlarmThreshold);
    }

    @XmlElement(name = "MULTIPLIER", required = true)
    public Integer getMultiplier() {
        return multiplier.get();
    }

    public IntegerProperty multiplierProperty() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier.set(multiplier);
    }
}
