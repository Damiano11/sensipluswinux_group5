package com.sensichips.sensiplus.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(propOrder={"idSPChip","seOnChipList"})
public class Chip {
    private final StringProperty idSPChip;
    private final StringProperty spFamily_idSPFamily;
    private String oldidSPChip = "";
    private String oldspFamily_idSPFamily = null;
    private List<SensingElementOnChip> seOnChipList;

    //il costruttore con in cui gli passiamo soltanto idSPChip, non deve essere implementato perche abbiamo sempre bisogno
    //di inserire SPFamily_idSPFamily in quanto chiave esterna
    public Chip() {
        this("", "");
    }

    public Chip(String idSPChip, String spFamily_idSPFamily) {
        this.idSPChip = new SimpleStringProperty(idSPChip);
        this.spFamily_idSPFamily = new SimpleStringProperty(spFamily_idSPFamily);

    }

    @XmlElement(name = "SENSING_ELEMENT_ONCHIP")
    public List<SensingElementOnChip> getSeOnChipList() {
        return seOnChipList;
    }

    public void setSeOnChipList(List<SensingElementOnChip> seOnChipList) {
        this.seOnChipList = seOnChipList;
    }

    public String getOldidSPChip() {
        return oldidSPChip;
    }

    public void setOldidSPChip() {
        this.oldidSPChip = this.idSPChip.get();
    }

    public String getOldspFamily_idSPFamily() {
        return oldspFamily_idSPFamily;
    }

    public void setOldspFamily_idSPFamily() {
        this.oldspFamily_idSPFamily = this.spFamily_idSPFamily.get();
    }

    @XmlElement(name = "IdChip")
    public String getIdSPChip() {
        return idSPChip.get();
    }

    public StringProperty idSPChipProperty() {
        return idSPChip;
    }

    public void setIdSPChip(String idSPChip) {
        this.idSPChip.set(idSPChip);
    }

    @XmlTransient
    public String getSpFamily_idSPFamily() {
        return spFamily_idSPFamily.get();
    }

    public StringProperty spFamily_idSPFamilyProperty() {
        return spFamily_idSPFamily;
    }

    public void setSpFamily_idSPFamily(String spFamily_idSPFamily) {
        this.spFamily_idSPFamily.set(spFamily_idSPFamily);
    }
}
