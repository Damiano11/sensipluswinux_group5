package com.sensichips.sensiplus.model;

import javafx.beans.property.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"seAssociated", "name", "internal"})
public class Port {

    private IntegerProperty idSPPort;
    private BooleanProperty internal;
    private StringProperty name;
    private StringProperty seAssociated = new SimpleStringProperty("");

    public Port() {
        this(Integer.MIN_VALUE, false, "");
    }

    public Port(Integer idSPPort, Boolean internal, String name) {
        this.idSPPort = new SimpleIntegerProperty(idSPPort);
        this.internal = new SimpleBooleanProperty(internal);
        this.name = new SimpleStringProperty(name);
    }

    public Integer getIdSPPort() {
        return idSPPort.get();
    }

    public IntegerProperty idSPPortProperty() {
        return idSPPort;
    }

    public void setIdSPPort(int idSPPort) {
        this.idSPPort.set(idSPPort);
    }

    @XmlElement(name = "INTERNAL", required = true)
    public boolean isInternal() {
        return internal.get();
    }

    public BooleanProperty internalProperty() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal.set(internal);
    }

    @XmlElement(name = "SENSING_ELEMENT_PORT", required = true)
    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    @XmlElement(name = "SENSING_ELEMENT_ID", required = true)
    public String getSeAssociated() {
        return seAssociated.get();
    }

    public StringProperty seAssociatedProperty() {
        return seAssociated;
    }

    public void setSeAssociated(String seAssociated) {
        this.seAssociated.set(seAssociated);
    }

}
