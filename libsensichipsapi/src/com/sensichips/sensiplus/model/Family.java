package com.sensichips.sensiplus.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(propOrder = {"idSPFamily", "id", "hwVersion", "sysclock", "osctrim", "measureTechniqueList", "portList"})
//@XmlAccessorType(XmlAccessType.FIELD)
public class Family {

    private String oldidSPFamily = null;
    private final StringProperty idSPFamily;
    private final StringProperty id;
    private final StringProperty hwVersion;
    private final StringProperty sysclock;
    private final StringProperty osctrim;
    private List<Port> portList;
    private List<MeasureTechnique> measureTechniqueList;

    public Family() {
        this("", "", "", "", "");
    }

    public Family(String idSPFamily) {
        this(idSPFamily, "", "", "", "");
    }

    public Family(String idSPFamily, String id, String hwVersion, String sysclock, String osctrim) {
        this.idSPFamily = new SimpleStringProperty(idSPFamily);
        this.id = new SimpleStringProperty(id);
        this.hwVersion = new SimpleStringProperty(hwVersion);
        this.sysclock = new SimpleStringProperty(sysclock);
        this.osctrim = new SimpleStringProperty(osctrim);

    }



    @XmlElement(name = "MEASURE_TYPE")
    public String[] getMeasureTechniqueList() {
        String[] m = new String[measureTechniqueList.size()];
        for (int i =0; i<measureTechniqueList.size(); i++){
            m[i] = measureTechniqueList.get(i).getType();
        }
        return m;
    }

    public void setMeasureTechniqueList(List<MeasureTechnique> measureTechniqueList) {
        this.measureTechniqueList = measureTechniqueList;
    }

    @XmlElement(name = "SENSING_ELEMENT_ONFAMILY")
    public List<Port> getPortList() {
        return portList;
    }

    public void setPortList(List<Port> portList) {
        this.portList = portList;
    }

    public String getOldidSPFamily() {
        return oldidSPFamily;
    }

    public void setOldidSPFamily() {
        this.oldidSPFamily = this.idSPFamily.get();
    }

    @XmlElement(name = "FAMILY_NAME")
    public String getIdSPFamily() {
        return idSPFamily.get();
    }

    public StringProperty idSPFamilyProperty() {
        return idSPFamily;
    }

    public void setIdSPFamily(String idSPFamily) {
        this.idSPFamily.set(idSPFamily);
    }

    @XmlElement(name = "FAMILY_ID")
    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    @XmlElement(name = "HW_VERSION")
    public String getHwVersion() {
        return hwVersion.get();
    }

    public StringProperty hwVersionProperty() {
        return hwVersion;
    }

    public void setHwVersion(String hwVersion) {
        this.hwVersion.set(hwVersion);
    }

    @XmlElement(name = "SYS_CLOCK")
    public String getSysclock() {
        return sysclock.get();
    }

    public StringProperty sysclockProperty() {
        return sysclock;
    }

    public void setSysclock(String sysclock) {
        this.sysclock.set(sysclock);
    }

    @XmlElement(name = "OSC_TRIM")
    public String getOsctrim() {
        return osctrim.get();
    }

    public StringProperty osctrimProperty() {
        return osctrim;
    }

    public void setOsctrim(String osctrim) {
        this.osctrim.set(osctrim);
    }
}
