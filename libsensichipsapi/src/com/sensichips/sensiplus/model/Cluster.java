package com.sensichips.sensiplus.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(propOrder={"idCluster","idSPFamily", "chipList"})
public class Cluster {
    private final StringProperty idCluster;
    private final StringProperty idSPFamily;
    private String oldidCluster="";
    private List<Chip> chipList;

    @XmlElement(name = "CHIP")
    public List<Chip> getChipList() {
        return chipList;
    }

    public void setChipList(List<Chip> chipList) {
        this.chipList = chipList;
    }

    public Cluster(){this("","");}

    public Cluster (String idCluster){
        this(idCluster,"");
    }

    public Cluster(String idCluster, String idSPFamily) {
        this.idCluster = new SimpleStringProperty(idCluster);
        this.idSPFamily = new SimpleStringProperty(idSPFamily);
    }

    @XmlElement(name = "CLUSTER_ID")
    public String getIdCluster() {
        return idCluster.get();
    }

    public StringProperty idClusterProperty() {
        return idCluster;
    }

    public void setIdCluster(String idCluster) {
        this.idCluster.set(idCluster);
    }

    public String getIdSPFamily() {
        return idSPFamily.get();
    }

    public StringProperty idSPFamilyProperty() {
        return idSPFamily;
    }

    public void setIdSPFamily(String idSPFamily) {
        this.idSPFamily.set(idSPFamily);
    }

    public String getOldidCluster() {
        return oldidCluster;
    }

    public void setOldidCluster() {
        this.oldidCluster = this.getIdCluster();
    }
}
