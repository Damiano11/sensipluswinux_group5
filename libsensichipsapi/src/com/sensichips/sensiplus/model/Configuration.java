//Abbiamo bisogno di una model class per tenere le informazioni sulle persone nella nostra Configurazione.
//Con JavaFx è usuale usare le Properties per tutti i campi di una model class. Le Property ci permettono, per esempio, di avere
//una notifica automatica quando variamo il valore di una delle variabili. questo ci permette di tenere sincronizzato
//ciò che è visualizzato con i dati.

package com.sensichips.sensiplus.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"driver","hostController","apiOwner","mcu","protocol","addressingType", "idCluster"})
public class Configuration {
    private final IntegerProperty idSPConfiguration;
    private final StringProperty driver;
    private final StringProperty hostController;
    private final StringProperty apiOwner;
    private final StringProperty mcu;
    private final StringProperty protocol;
    private final StringProperty addressingType;
    private final StringProperty idCluster;
    private Integer oldidSPConfiguration=null;

    public Configuration(){
        this(Integer.MIN_VALUE,"","","","","","","");
    }
    public Configuration(Integer idSPConfiguration, String idCluster){
        this(idSPConfiguration,"","","","","","",idCluster);
    }

    public Configuration(Integer idSPConfiguration, String driver, String hostController, String apiOwner, String mcu, String protocol, String addressingType, String idCluster) {
        this.idSPConfiguration = new SimpleIntegerProperty(idSPConfiguration);
        this.driver = new SimpleStringProperty(driver);
        this.hostController = new SimpleStringProperty(hostController);
        this.apiOwner = new SimpleStringProperty(apiOwner);
        this.mcu = new SimpleStringProperty(mcu);
        this.protocol=new SimpleStringProperty(protocol);
        this.addressingType = new SimpleStringProperty(addressingType);
        this.idCluster = new SimpleStringProperty(idCluster);
    }

    public Integer getIdSPConfiguration() {
        return idSPConfiguration.get();
    }

    public IntegerProperty idSPConfigurationProperty() {
        return idSPConfiguration;
    }

    public void setIdSPConfiguration(int idSPConfiguration) {
        this.idSPConfiguration.set(idSPConfiguration);
    }

    @XmlElement(name = "DRIVER")
    public String getDriver() {
        return driver.get();
    }

    public StringProperty driverProperty() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver.set(driver);
    }

    @XmlElement(name = "HOST_CONTROLLER")
    public String getHostController() {
        return hostController.get();
    }

    public StringProperty hostControllerProperty() {
        return hostController;
    }

    public void setHostController(String hostController) {
        this.hostController.set(hostController);
    }

    @XmlElement(name = "API_OWNER")
    public String getApiOwner() {
        return apiOwner.get();
    }

    public StringProperty apiOwnerProperty() {
        return apiOwner;
    }

    public void setApiOwner(String apiOwner) {
        this.apiOwner.set(apiOwner);
    }

    @XmlElement(name = "MCU")
    public String getMcu() {
        return mcu.get();
    }

    public StringProperty mcuProperty() {
        return mcu;
    }

    public void setMcu(String mcu) {
        this.mcu.set(mcu);
    }

    @XmlElement(name = "ADDRESSING_TYPE")
    public String getAddressingType() {
        return addressingType.get();
    }

    public StringProperty addressingTypeProperty() {
        return addressingType;
    }

    public void setAddressingType(String addressingType) {
        this.addressingType.set(addressingType);
    }

    @XmlElement(name = "CLUSTER_ID")
    public String getIdCluster() {
        return idCluster.get();
    }

    public StringProperty idClusterProperty() {
        return idCluster;
    }

    public void setIdCluster(String idCluster) {
        this.idCluster.set(idCluster);
    }

    public Integer getOldidSPConfiguration() {
        return oldidSPConfiguration;
    }

    //questa variabile ci è utile quando modichiamo l'id della configurazione.
    public void setOldidSPConfiguration() {
        this.oldidSPConfiguration = this.getIdSPConfiguration();
    }

    @XmlElement(name = "PROTOCOL")
    public String getProtocol() {
        return protocol.get();
    }

    public StringProperty protocolProperty() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol.set(protocol);
    }
}
