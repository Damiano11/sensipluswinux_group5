package com.sensichips.sensiplus.model.dao;

public class DAOException extends Exception {
    public DAOException(String message){
        super(message);
        printStackTrace();
    }
}