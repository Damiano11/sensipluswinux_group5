package com.sensichips.sensiplus.model.dao;

import java.util.List;

public interface DAOSensingElement<T,C, F> extends DAO<T>{
    T selectSensingElementOnChip(C c) throws DAOException;
    List<T> selectSensingElementOnFamily(F f) throws DAOException;
}
