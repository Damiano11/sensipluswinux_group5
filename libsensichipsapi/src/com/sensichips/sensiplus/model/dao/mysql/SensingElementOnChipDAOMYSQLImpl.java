package com.sensichips.sensiplus.model.dao.mysql;

import com.sensichips.sensiplus.model.SensingElementOnChip;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.DAOSensignElementOnChip;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class SensingElementOnChipDAOMYSQLImpl implements DAOSensignElementOnChip<SensingElementOnChip> {

    @Override
    public List<SensingElementOnChip> selectAll(SensingElementOnChip a) throws DAOException {
        if (a == null){
            a =  new SensingElementOnChip();
        }

        ArrayList<SensingElementOnChip> lista = new ArrayList<SensingElementOnChip>();
        try{

            if (a == null || a.getIdSPPort() == null
                    || a.getIdSPChip() == null || a.getIdSensingElementOnFamily() == null || a.getIdFamily() == null){
                throw new DAOException("In select: any field can be null or negative");
            }

            Statement st = DAOMySQLSettings.getStatement();
            String sql = "select * from SPSensingElementOnChip where SPChip_idSPChip like '";
            sql += a.getIdSPChip() + "%' and SPCluster_idCluster like '" + a.getIdSPCluster() + "%' and SPSensingElementOnFamily_idSPSensingElementOnFamily like '"
                    + (a.getIdSensingElementOnFamily() == Integer.MIN_VALUE ? "" : a.getIdSensingElementOnFamily()) + "%';";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                lista.add(new SensingElementOnChip(rs.getString("SPChip_idSPChip"), rs.getInt("SPSensingElementOnFamily_idSPSensingElementOnFamily"),
                        rs.getString("SPCluster_idCluster"), rs.getInt("M"), rs.getInt("N")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq){
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }

    private SensingElementOnChipDAOMYSQLImpl() {
    }

    private static DAOSensignElementOnChip dao = null;
    private static Logger logger = null;

    public static DAOSensignElementOnChip getInstance() {
        if (dao == null) {
            dao = new SensingElementOnChipDAOMYSQLImpl();
            logger = Logger.getLogger(SensingElementOnChipDAOMYSQLImpl.class.getName());
        }
        return dao;
    }

    @Override
    public List<SensingElementOnChip> select(SensingElementOnChip a) throws DAOException {

        if (a == null){
            a =  new SensingElementOnChip();
        }

        ArrayList<SensingElementOnChip> lista = new ArrayList<SensingElementOnChip>();
        try{

            if (a == null || a.getIdSPPort() == null
                    || a.getIdSPChip() == null || a.getIdSensingElementOnFamily() == null || a.getIdFamily() == null){
                throw new DAOException("In select: any field can be null or negative");
            }

            Statement st = DAOMySQLSettings.getStatement();
            String sql = "select * from SPSensingElementOnChip where SPChip_idSPChip like '";
            sql += a.getIdSPChip() + "%' and SPCluster_idCluster like '" + a.getIdSPCluster() + "%' and SPSensingElementOnFamily_idSPSensingElementOnFamily like " +
                    "(select idSPSensingElementOnFamily from SPSensingElementOnFamily where spsensingelement_idspsensingelement like '" +
                    a.getSeAssociated() + "' and SPFamilyTemplate_idSPFamilyTemplate like (select idSPFamilyTemplate from SPFamilyTemplate where spfamily_idspfamily like '" +
                    a.getIdFamily() + "' and spport_idspport like " + a.getIdSPPort() + "))";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                lista.add(new SensingElementOnChip(rs.getString("SPChip_idSPChip"), rs.getInt("SPSensingElementOnFamily_idSPSensingElementOnFamily"),
                        rs.getString("SPCluster_idCluster"), rs.getInt("M"), rs.getInt("N")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq){
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }

    @Override
    public void update(SensingElementOnChip a) throws DAOException {
        if (a == null || a.getIdSPCluster() == null || a.getIdSensingElementOnFamily().equals("") ||
                a.getIdSPChip().equals("") ) {
            throw new DAOException("In update: any field can be null");
        }

        String query = "UPDATE SPSensingElementOnChip SET m = '" + a.getM() + "', n = '" + a.getN()
                + "' WHERE SPChip_idSPChip = '" + a.getIdSPChip() + "' AND SPCluster_idCluster = '"
                + a.getIdSPCluster() +"' AND SPSensingElementOnFamily_idSPSensingElementOnFamily = (SELECT idSPSensingElementOnFamily FROM SPSensingElementOnFamily WHERE SPSensingElement_idSPSensingElement LIKE '" + a.getSeAssociated()
                + "' AND SPFamilyTemplate_idSPFamilyTemplate LIKE  (select idSPFamilyTemplate from SPFamilyTemplate where SPPort_idSPPort=(SELECT idSPPort FROM SPPort WHERE idSPPort LIKE '"
                + a.getIdSPPort() + "') and SPFamily_idSPFamily=(SELECT  SPFamily_idSPFamily FROM SPChip WHERE idSPChip LIKE '" + a.getIdSPChip() + "')));";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In update: " + e.getMessage());
        }
    }

    @Override
    public void insert(SensingElementOnChip a) throws DAOException {
        if (a == null || a.getIdSPCluster() == null || a.getIdSPPort() == Integer.MIN_VALUE ||
                a.getIdSPChip().equals("") || a.getSeAssociated().equals("")) {
            throw new DAOException("In insert: any field can be null");
        }

        String query = "INSERT INTO SPSensingElementOnChip(SPChip_idSPChip, m, n, SPSensingElementOnFamily_idSPSensingElementOnFamily, SPCluster_idCluster) VALUES ('"
                + a.getIdSPChip() + "', '" + (a.getM() == Integer.MIN_VALUE ? 1 : a.getM()) + "', '" + (a.getN() == Integer.MIN_VALUE ? 0 : a.getN()) + "', " +
                "(SELECT idSPSensingElementOnFamily FROM SPSensingElementOnFamily WHERE SPSensingElement_idSPSensingElement LIKE '" + a.getSeAssociated()
                + "' AND SPFamilyTemplate_idSPFamilyTemplate LIKE  (select idSPFamilyTemplate from SPFamilyTemplate where SPPort_idSPPort=(SELECT idSPPort FROM SPPort WHERE idSPPort LIKE '"
                + a.getIdSPPort() + "') and SPFamily_idSPFamily=(SELECT  SPFamily_idSPFamily FROM SPChip WHERE idSPChip LIKE '" + a.getIdSPChip() + "'))), '" + a.getIdSPCluster() + "');";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In insert: " + e.getMessage());
        }
    }

    @Override
    public void delete(SensingElementOnChip a) throws DAOException {
        if (a == null || a.getIdSPChip() == null
                || a.getIdSensingElementOnFamily() == null || a.getIdSPCluster() == null){
            throw new DAOException("In delete: any field can be null");
        }

        String query = "DELETE FROM SPSensingElementOnChip WHERE SPChip_idSPChip LIKE '" + a.getIdSPChip()
                + "%' AND SPSensingElementOnFamily_idSPSensingElementOnFamily LIKE '" + (a.getIdSensingElementOnFamily() == Integer.MIN_VALUE ? "" : a.getIdSensingElementOnFamily())
                + "%' AND SPCluster_idCluster LIKE '" + a.getIdSPCluster() + "%';";
        logger.info("SQL: " + query);

        Statement st = null;
        try {
            st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In delete: " + e.getMessage());
        }

    }
}
