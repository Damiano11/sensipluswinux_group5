package com.sensichips.sensiplus.model.dao.mysql;

import com.sensichips.sensiplus.model.Chip;
import com.sensichips.sensiplus.model.Cluster;
import com.sensichips.sensiplus.model.dao.DAOCluster;
import com.sensichips.sensiplus.model.dao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ClusterDAOMYSQLImpl implements DAOCluster<Cluster, Chip> {
    private ClusterDAOMYSQLImpl() {
    }

    private static DAOCluster dao = null;
    private static Logger logger = null;

    public static DAOCluster getInstance() {
        if (dao == null) {
            dao = new ClusterDAOMYSQLImpl();
            logger = Logger.getLogger(ConfigurationDAOMYSQLImpl.class.getName());
        }
        return dao;
    }


    @Override
    public List<Cluster> select(Cluster c) throws DAOException {

        if (c == null) {
            c = new Cluster();
        }
        if (c == null || c.getIdCluster() == null) {
            throw new DAOException("In select: any field can be null");
        }

        ArrayList<Cluster> lista = new ArrayList<Cluster>();
        try {
            Statement st = DAOMySQLSettings.getStatement();

            String sql = "select * from SPCluster where idCluster like '";
            sql += c.getIdCluster() + "%';";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new Cluster(rs.getString("idCluster"), rs.getString("SPFamily_idSPFamily")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }


    @Override
    public void insert(Cluster c) throws DAOException {

        if (c == null || c.getIdCluster() == null) {
            throw new DAOException("In insert: any field can be null");
        }

        //INSERT INTO SPCluster( idCluster, SPCalibration_idSPCalibration) VALUES ('uauau', '2');
        String query = "INSERT INTO SPCluster (idCluster, SPFamily_idSPFamily) VALUES ('"
                + c.getIdCluster() + "', '" + c.getIdSPFamily() + "');";

        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In insert: " + e.getMessage());
        }
    }

    @Override
    public void delete(Cluster c) throws DAOException {
        if (c == null || c.getIdCluster() == null || c.getIdSPFamily() == null) {
            throw new DAOException("In delete: any field can be null");
        }
        //DELETE FROM SPCluster WHERE idCluster='uauau' and SPCalibration_idSPCalibration='2';
        String query = "DELETE FROM SPCluster WHERE idCluster='" + c.getIdCluster() + "';";
        logger.info("SQL: " + query);
        Statement st = null;
        try {
            st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In delete: " + e.getMessage());
        }
    }

    @Override
    public void update(Cluster c) throws DAOException {
        if (c == null || c.getIdCluster() == null || c.getIdSPFamily() == null) {
            throw new DAOException("In update: any field can be null");
        }

        //UPDATE SPCluster SET idCluster='paperina', SPCalibration_idSPCalibration='2' WHERE idCluster='q' and SPCalibration_idSPCalibration='1';

        String query = "UPDATE SPCluster SET idCluster='" + c.getIdCluster() + "', SPFamily_idSPFamily='"
                + c.getIdSPFamily() + "' WHERE idCluster='" + c.getOldidCluster() + "';";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In update: " + e.getMessage());
        }
    }

    @Override
    public List<Chip> selectChiptIn(Cluster c) throws DAOException {

        if (c == null) {
            c = new Cluster();
        }

        ArrayList<Chip> lista = new ArrayList<Chip>();

        try {
            if (c == null || c.getIdCluster() == null || c.getIdSPFamily() == null) {
                throw new DAOException("In Select: any field can be null");
            }

            Statement st = DAOMySQLSettings.getStatement();
            //SELECT * FROM SPChip WHERE SPFamily_idSPFamily = 1 AND idSPChip NOT IN (SELECT SPChip_idSPChip FROM SPSensingElementOnChip WHERE SPCluster_idCluster ='h')  ;
            String sql = "SELECT * FROM SPChip WHERE SPFamily_idSPFamily = '" + c.getIdSPFamily()
                    + "' AND idSPChip IN (SELECT SPChip_idSPChip FROM SPSensingElementOnChip WHERE SPCluster_idCluster ='"
                    + c.getIdCluster() + "');";


            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new Chip(rs.getString("idSPChip"), rs.getString("SPFamily_idSPFamily")));

            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }

    @Override
    public List<Chip> selectChipNotIn(Cluster c) throws DAOException {

        if (c == null) {
            c = new Cluster();
        }

        ArrayList<Chip> lista = new ArrayList<Chip>();

        try {
            if (c == null || c.getIdCluster() == null || c.getIdSPFamily() == null) {
                throw new DAOException("In Select: any field can be null");
            }

            Statement st = DAOMySQLSettings.getStatement();
            //SELECT * FROM SPChip WHERE SPFamily_idSPFamily = 1 AND idSPChip NOT IN (SELECT SPChip_idSPChip FROM SPSensingElementOnChip WHERE SPCluster_idCluster ='h')  ;
            String sql = "SELECT * FROM SPChip WHERE SPFamily_idSPFamily = '" + c.getIdSPFamily()
                    + "' AND idSPChip NOT IN (SELECT SPChip_idSPChip FROM SPSensingElementOnChip WHERE SPCluster_idCluster ='"
                    + c.getIdCluster() + "');";


            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new Chip(rs.getString("idSPChip"), rs.getString("SPFamily_idSPFamily")));

            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }


    /*@Override
    public void insertChip(Cluster c, Chip chip) throws DAOException {

        if (chip == null || chip.getIdSPMeasureTechnique() == null
                || chip.getType() == null || c == null || c.getIdSPFamily() == null) {
            throw new DAOException("In insertMeasureTechnique: any field can be null");
        }
        //INSERT INTO `sensidb`.`SPSensingElementOnChip` (`SPChip_idSPChip`, `m`, `n`, `SPSensingElementOnFamily_idSPSensingElementOnFamily`, `SPCluster_idCluster`) VALUES ('Chip1', '1', '0', '6', 'h');
        String query = "INSERT INTO SPFamily_has_SPMeasureTechnique (SPFamily_idSPFamily, SPMeasureTechnique_idSPMeasureTechnique) VALUES ('"
                + c.getIdSPFamily() + "', '" + chip.getIdSPMeasureTechnique() + "');";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In insertMeasureTechnique: " + e.getMessage());
        }
    }*/


}
