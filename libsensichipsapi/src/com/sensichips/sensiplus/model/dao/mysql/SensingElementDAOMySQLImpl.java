package com.sensichips.sensiplus.model.dao.mysql;

import com.sensichips.sensiplus.model.*;
import com.sensichips.sensiplus.model.dao.DAOException;
import com.sensichips.sensiplus.model.dao.DAOSensingElement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class SensingElementDAOMySQLImpl implements DAOSensingElement<SensingElement, SensingElementOnChip, Family> {

    private SensingElementDAOMySQLImpl() {
    }

    private static DAOSensingElement dao = null;
    private static Logger logger = null;

    public static DAOSensingElement getInstance() {
        if (dao == null) {
            dao = new SensingElementDAOMySQLImpl();
            logger = Logger.getLogger(SensingElementDAOMySQLImpl.class.getName());

        }
        return dao;
    }

    @Override
    public SensingElement selectSensingElementOnChip(SensingElementOnChip c) throws DAOException {

        SensingElement se = new SensingElement();
        try {

            if (c == null || c.getIdSensingElementOnFamily() == null) {
                throw new DAOException("In select: any field can be null or negative");
            }

            Statement st = DAOMySQLSettings.getStatement();
            String sql = "select * from SPSensingElement where idSPSensingElement in " +
                    "(select SPSensingElement_idSPSensingElement from SPSensingElementOnFamily where idSPSensingElementOnFamily like '"
                    + c.getIdSensingElementOnFamily() + "');";


            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                se = new SensingElement(rs.getString("idSPSensingElement"), rs.getInt("Rsense"),
                        rs.getInt("InGain"), rs.getInt("OutGain"), rs.getString("Contacts"),
                        rs.getInt("Frequency"), rs.getString("Harmonic"), rs.getInt("DCBias"),
                        rs.getString("ModeVI"), rs.getString("MeasureTechnique"), rs.getString("MeasureType"),
                        rs.getInt("Filter"), rs.getString("PhaseShiftMode"), rs.getInt("PhaseShift"),
                        rs.getString("IQ"), rs.getInt("ConversionRate"), rs.getString("InPortADC"),
                        rs.getInt("NData"), rs.getString("measureUnit"), rs.getString("name"),
                        rs.getDouble("rangeMin"), rs.getDouble("rangeMax"), rs.getDouble("defaultAlarmThreshold"),
                        rs.getInt("multiplier"));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return se;
    }

    @Override
    public List<SensingElement> selectSensingElementOnFamily(Family f) throws DAOException {
        List<SensingElement> list = new ArrayList<>();
        try {

            if (f == null || f.getIdSPFamily() == null) {
                throw new DAOException("In select: any field can be null or negative");
            }

            Statement st = DAOMySQLSettings.getStatement();
            String sql = "SELECT * FROM SPSensingElement where idSPSensingElement IN " +
                    "(SELECT SPSensingElement_idSPSensingElement FROM SPSensingElementOnFamily where SPFamilyTemplate_idSPFamilyTemplate IN " +
                    "(SELECT idSPFamilyTemplate FROM SPFamilyTemplate WHERE SPFamily_idSPFamily LIKE '" + f.getIdSPFamily() + "'));";


            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                list.add(new SensingElement(rs.getString("idSPSensingElement"), rs.getInt("Rsense"),
                        rs.getInt("InGain"), rs.getInt("OutGain"), rs.getString("Contacts"),
                        rs.getInt("Frequency"), rs.getString("Harmonic"), rs.getInt("DCBias"),
                        rs.getString("ModeVI"), rs.getString("MeasureTechnique"), rs.getString("MeasureType"),
                        rs.getInt("Filter"), rs.getString("PhaseShiftMode"), rs.getInt("PhaseShift"),
                        rs.getString("IQ"), rs.getInt("ConversionRate"), rs.getString("InPortADC"),
                        rs.getInt("NData"), rs.getString("measureUnit"), rs.getString("name"),
                        rs.getDouble("rangeMin"), rs.getDouble("rangeMax"), rs.getDouble("defaultAlarmThreshold"),
                        rs.getInt("multiplier")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return list;
    }

    @Override
    public List<SensingElement> select(SensingElement a) throws DAOException {

        if (a == null) {
            a = new SensingElement();
        }

        ArrayList<SensingElement> lista = new ArrayList<SensingElement>();
        try {

            if (a == null) {
                throw new DAOException("In select: any field can be null or negative");
            }

            Statement st = DAOMySQLSettings.getStatement();
            String sql = "select * from SPSensingElement where idSPSensingElement like '";
            sql += a.getIdSPSensingElement() + "%' and Rsense like '" + ((a.getRSense() == null) ? "" : a.getRSense());
            sql += "%' and InGain like '" + ((a.getInGain() == null ? "" : a.getInGain()));
            sql += "%' and OutGain like '" + ((a.getOutGain() == null ? "" : a.getOutGain()));
            sql += "%' and Contacts like '" + (a.getContacts() == null ? "" : a.getContacts());
            sql += "%' and Frequency like '" + ((a.getFrequency() == null ? "" : a.getFrequency()));
            sql += "%' and Harmonic like '" + (a.getHarmonic() == null ? "" : a.getHarmonic());
            sql += "%' and DCBias like '" + ((a.getDcBias() == null ? "" : a.getDcBias()));
            sql += "%' and ModeVI like '" + (a.getModeVI() == null ? "" : a.getModeVI());
            sql += "%' and MeasureTechnique like '" + a.getMeasureTechnique();
            sql += "%' and MeasureType like '" + (a.getMeasureType() == null ? "" : a.getMeasureType());
            sql += "%' and Filter like '" + ((a.getFilter() == Integer.MIN_VALUE ? "" : a.getFilter()));
            sql += "%' and PhaseShiftMode like '" + (a.getPhaseShiftMode() == null ? "" : a.getPhaseShiftMode());
            sql += "%' and PhaseShift like '" + ((a.getPhaseShift() == null ? "" : a.getPhaseShift()));
            sql += "%' and IQ like '" + (a.getIq() == null ? "" : a.getIq());
            sql += "%' and ConversionRate like '" + ((a.getConversionRate() == Integer.MIN_VALUE ? "" : a.getConversionRate()));
            sql += "%' and InPortADC like '" + a.getInportADC();
            sql += "%' and NData like '" + ((a.getNData() == Integer.MIN_VALUE ? "" : a.getNData()));
            sql += "%' and measureUnit like '" + a.getMeasure_Unit();
            sql += "%' and name like '" + (a.getName() == null ? "" : a.getName());
            sql += "%' and rangeMin like '" + ((a.getRangeMin().isNaN() ? "" : a.getRangeMin()));
            sql += "%' and rangeMax like '" + ((a.getRangeMax().isNaN() ? "" : a.getRangeMax()));
            sql += "%' and defaultAlarmThreshold like '" + ((a.getDefaultAlarmThreshold().isNaN() ? "" : a.getDefaultAlarmThreshold()));
            sql += "%' and multiplier like '" + ((a.getMultiplier() == Integer.MIN_VALUE ? "" : a.getMultiplier())) + "%'";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new SensingElement(rs.getString("idSPSensingElement"), rs.getInt("Rsense"),
                        rs.getInt("InGain"), rs.getInt("OutGain"), rs.getString("Contacts"),
                        rs.getInt("Frequency"), rs.getString("Harmonic"), rs.getInt("DCBias"),
                        rs.getString("ModeVI"), rs.getString("MeasureTechnique"), rs.getString("MeasureType"),
                        rs.getInt("Filter"), rs.getString("PhaseShiftMode"), rs.getInt("PhaseShift"),
                        rs.getString("IQ"), rs.getInt("ConversionRate"), rs.getString("InPortADC"),
                        rs.getInt("NData"), rs.getString("measureUnit"), rs.getString("name"),
                        rs.getDouble("rangeMin"), rs.getDouble("rangeMax"), rs.getDouble("defaultAlarmThreshold"),
                        rs.getInt("multiplier")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }


    @Override
    public void update(SensingElement a) throws DAOException {

        if (a == null) {
            throw new DAOException("In update: any field can be null");
        }

        String query = "UPDATE SPSensingElement SET Rsense = '" + (a.getRSense() == null ? Integer.MIN_VALUE : a.getRSense())
                + "', InGain = '" + (a.getInGain() == null ? Integer.MIN_VALUE : a.getInGain()) + "', OutGain = '"
                + (a.getOutGain() == null ? Integer.MIN_VALUE : a.getOutGain()) + "', Frequency = '" + (a.getFrequency() == null ? Integer.MIN_VALUE : a.getFrequency())
                + "', DCBias = '" + (a.getDcBias() == null ? Integer.MIN_VALUE : a.getDcBias()) + "', Measuretechnique = '" + a.getMeasureTechnique()
                + "', MeasureType = '" + (a.getMeasureType() == null ? "IN-PHASE" : a.getMeasureType()) + "', Filter = '" + a.getFilter()
                + "', PhaseShiftMode = '" + (a.getPhaseShiftMode() == null ? "QUADRANTS" : a.getPhaseShiftMode())
                + "', PhaseShift = '" + (a.getPhaseShift() == null ? Integer.MIN_VALUE : a.getPhaseShift()) + "', IQ = '" + (a.getIq() == null ? "IN-PHASE" : a.getIq())
                + "', ConversionRate = '" + a.getConversionRate() + "', InPortADC = '" + a.getInportADC() + "', Ndata = '" + a.getNData() + "', measureUnit = '" + a.getMeasure_Unit()
                + "', name = '" + (a.getName() == null ? "" : a.getName()) + "', rangeMin = '" + a.getRangeMin() + "', rangeMax = '" + a.getRangeMax() + "', defaultAlarmThreshold = '" + a.getDefaultAlarmThreshold()
                + "', multiplier = '" + a.getMultiplier() + "', IdSPSensingElement = '" + a.getIdSPSensingElement() + "', Contacts = '" + (a.getContacts() == null ? "TWO" : a.getContacts())
                + "', Harmonic = '" + (a.getHarmonic() == null ? "FIRST_HARMONIC" : a.getHarmonic()) + "', modeVI = '" + (a.getModeVI() == null ? "VOUT_IIN" : a.getModeVI()) + "'";
        query = query + " WHERE idSPSensingElement = '" + a.getOldidSPSensingElement() + "';";

        logger.info("SQL: " + query);

        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In update: " + e.getMessage());
        }
    }


    @Override
    public void insert(SensingElement a) throws DAOException {

        if (a == null) {
            throw new DAOException("In insert: any field can be null");
        }

        String query = "INSERT INTO SPSensingElement (idSPSensingElement, Rsense, InGain, OutGain, Contacts, Frequency, Harmonic, DCBias, " +
                "ModeVI, Measuretechnique, MeasureType, Filter, PhaseShiftMode, PhaseShift, IQ, ConversionRate, InPortADC, Ndata, measureUnit, " +
                "name, rangeMin, rangeMax, defaultAlarmThreshold, multiplier) VALUES  ('" + a.getIdSPSensingElement() + "', '" + (a.getRSense() == null ? Integer.MIN_VALUE : a.getRSense())
                + "', '" + (a.getInGain() == null ? Integer.MIN_VALUE : a.getInGain()) + "', '" + (a.getOutGain() == null ? Integer.MIN_VALUE : a.getOutGain())
                + "', '" + (a.getContacts() == null ? "TWO" : a.getContacts()) + "', '" + (a.getFrequency() == null ? Integer.MIN_VALUE : a.getFrequency()) + "', '" + (a.getHarmonic() == null ? "FIRST_HARMONIC" : a.getHarmonic()) + "', '"
                + (a.getDcBias() == null ? Integer.MIN_VALUE : a.getDcBias()) + "', '" + (a.getModeVI() == null ? "VOUT_IIN" : a.getModeVI()) + "', '" + a.getMeasureTechnique() + "', '" + (a.getMeasureType() == null ? "IN-PHASE" : a.getMeasureType())
                + "', '" + a.getFilter() + "', '" + (a.getPhaseShiftMode() == null ? "QUADRANTS" : a.getPhaseShiftMode()) + "', '" + (a.getPhaseShift() == null ? Integer.MIN_VALUE : a.getPhaseShift())
                + "', '" + (a.getIq() == null ? "IN-PHASE" : a.getIq()) + "', '" + a.getConversionRate() + "', '" + a.getInportADC() + "', '" + a.getNData()
                + "', '" + a.getMeasure_Unit() + "', '" + (a.getName() == null ? "" : a.getName()) + "', '" + a.getRangeMin() + "', '" + a.getRangeMax() + "', '"
                + a.getDefaultAlarmThreshold() + "', '" + a.getMultiplier() + "')";

        logger.info("SQL: " + query);

        try {
            Statement st = DAOMySQLSettings.getStatement();

            /*
            String query = "INSERT INTO SPSensingElement (idSPSensingElement, rSense, inGain, outGain, contacts, frequency, harmonic, DCBias, " +
                    "modeVI, measureTechnique, measureType, filter, phaseShiftMode, phaseShift, IQ, conversionRate, inPortADC, nData, measureUnit, " +
                    "name, rangeMin, rangeMax, defaultAlarmThreshold, multiplier) VALUES  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement pstmt = st.getConnection().prepareStatement(query);
            pstmt.setString(1, "ggwp");
            pstmt.setNull(2, java.sql.Types.INTEGER);
            pstmt.setNull(3, java.sql.Types.INTEGER);
            pstmt.setInt(4, 11);
            pstmt.setNull(5, java.sql.Types.INTEGER);
            pstmt.setNull(6, java.sql.Types.INTEGER);
            pstmt.setNull(7, java.sql.Types.INTEGER);
            pstmt.setNull(8, java.sql.Types.INTEGER);
            pstmt.setNull(9, java.sql.Types.INTEGER);
            pstmt.setNull(10, java.sql.Types.INTEGER);
            pstmt.setNull(11, java.sql.Types.INTEGER);
            pstmt.setNull(12, java.sql.Types.INTEGER);
            pstmt.setNull(13, java.sql.Types.INTEGER);
            pstmt.setNull(14, java.sql.Types.INTEGER);
            pstmt.setNull(15, java.sql.Types.INTEGER);
            pstmt.setNull(16, java.sql.Types.INTEGER);
            pstmt.setNull(17, java.sql.Types.INTEGER);
            pstmt.setNull(18, java.sql.Types.INTEGER);
            pstmt.setNull(19, java.sql.Types.INTEGER);
            pstmt.setNull(20, java.sql.Types.INTEGER);
            pstmt.setNull(21, java.sql.Types.INTEGER);
            pstmt.setNull(22, java.sql.Types.INTEGER);
            pstmt.setNull(23, java.sql.Types.INTEGER);
            pstmt.setNull(24, java.sql.Types.INTEGER);

            pstmt.executeUpdate();
            */

            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In insert: " + e.getMessage());
        }
    }

    @Override
    public void delete(SensingElement a) throws DAOException {

        if (a == null) {
            throw new DAOException("In delete: any field can be null");
        }

        String query = "DELETE FROM SPSensingElement WHERE idSPSensingElement='" + a.getIdSPSensingElement() + "';";
        logger.info("SQL: " + query);

        Statement st = null;
        try {
            st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In delete(): " + e.getMessage());
        }

        //  DELETE FROM `sensichipsdb`.`SPSensingElement` WHERE `idSPSensingElement`='788';

    }
}
