package com.sensichips.sensiplus.model.dao.mysql;

import com.sensichips.sensiplus.model.Chip;
import com.sensichips.sensiplus.model.Cluster;
import com.sensichips.sensiplus.model.Family;
import com.sensichips.sensiplus.model.Port;
import com.sensichips.sensiplus.model.dao.DAOChip;
import com.sensichips.sensiplus.model.dao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ChipDAOMYSQLImpl implements DAOChip<Chip,Family, Port, Cluster> {

    private ChipDAOMYSQLImpl(){}

    private static DAOChip dao = null;
    private static Logger logger = null;
    public static DAOChip getInstance(){
        if (dao == null){
            dao = new ChipDAOMYSQLImpl();
            logger = Logger.getLogger(ChipDAOMYSQLImpl.class.getName());
        }
        return dao;
    }

    @Override
    public List<Chip> selectChipOfCluster(Cluster c) throws DAOException {

        if (c == null || c.getIdCluster() == null) {
            throw new DAOException("In select: any field can be null");
        }

        ArrayList<Chip> lista = new ArrayList<Chip>();
        try {
            Statement st = DAOMySQLSettings.getStatement();

            String sql = "select * from SPChip where idSPChip in (select SPChip_idSPChip from SPSensingElementOnChip " +
                    "where SPCluster_idCluster like '" + c.getIdCluster() + "')";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new Chip(rs.getString("idSPChip"), rs.getString("SPFamily_idSPFamily")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }

    @Override
    public List<Chip> select(Chip c) throws DAOException {

        if (c == null) {
            c = new Chip();
        }
        if (c == null || c.getIdSPChip() == null
                || c.getSpFamily_idSPFamily() == null) {
            throw new DAOException("In select: any field can be null");
        }

        ArrayList<Chip> lista = new ArrayList<Chip>();
        try {
            Statement st = DAOMySQLSettings.getStatement();

            String sql = "select * from SPChip where idSPChip like '";
            sql += c.getIdSPChip() + "%' and SPFamily_idSPFamily like '" + c.getSpFamily_idSPFamily()+ "%'";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                lista.add(new Chip(rs.getString("idSPChip"), rs.getString("SPFamily_idSPFamily")));
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }
        return lista;
    }

    @Override
    public void insert(Chip c) throws DAOException {

        if (c == null || c.getIdSPChip() == null
                || c.getSpFamily_idSPFamily() == null) {
            throw new DAOException("In insert: any field can be null");
        }
        //INSERT INTO SPChip(idSPChip, SPFamily_idSPFamily) VALUES ('chip uno', '1');
        String query = "INSERT INTO SPChip(idSPChip, SPFamily_idSPFamily) VALUES ('"
                +c.getIdSPChip()+"', '"+c.getSpFamily_idSPFamily()+"');";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In insert: " + e.getMessage());
        }
    }

    @Override
    public void delete(Chip c) throws DAOException {

        if (c == null || c.getIdSPChip() == null
                || c.getSpFamily_idSPFamily() == null) {
            throw new DAOException("In delete: any field can be null");
        }
        //DELETE FROM SPChip WHERE idSPChip='chip uno';
        String query = "DELETE FROM SPChip WHERE idSPChip='" + c.getIdSPChip() + "';";
        logger.info("SQL: " + query);

        Statement st = null;
        try {
            st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);

        } catch (SQLException e) {
            throw new DAOException("In delete: " + e.getMessage());
        }
    }


    @Override
    public void update(Chip c) throws DAOException {

        if (c == null || c.getIdSPChip() == null
                || c.getSpFamily_idSPFamily() == null) {
            throw new DAOException("In update: any field can be null");
        }
        //UPDATE SPChip SET idSPChip='chip uno1', SPFamily_idSPFamily='2' WHERE idSPChip='chip uno';
        String query = "UPDATE SPChip SET idSPChip='"+c.getIdSPChip()+"', SPFamily_idSPFamily='"+c.getSpFamily_idSPFamily()
        +"' WHERE idSPChip='"+c.getOldidSPChip()+"';";
        logger.info("SQL: " + query);
        try {
            Statement st = DAOMySQLSettings.getStatement();
            int n = st.executeUpdate(query);
            logger.info("Row affected: " + n + ".");
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException e) {
            throw new DAOException("In update: " + e.getMessage());
        }
    }

    @Override
    public String selectMN(Chip c, Family f, Port p) throws DAOException {
        if (c == null || c.getIdSPChip() == null
                || c.getSpFamily_idSPFamily() == null || f == null || p ==null) {
            throw new DAOException("In select: any field can be null");
        }

        String response = "";
        try {
            Statement st = DAOMySQLSettings.getStatement();

            String sql = "select m,n from SPSensingElementOnChip where spchip_idspchip like '";
            sql += c.getIdSPChip() + "' and spsensingelementonfamily_idspsensingelementonfamily like " +
                    "(select idspsensingelementonfamily from spsensingelementonfamily where spsensingelement_idspsensingelement like '" +
                    p.getSeAssociated() + "' and spfamilytemplate_idspfamilytemplate like (select idspfamilytemplate from spfamilytemplate where spfamily_idspfamily like " +
                    f.getIdSPFamily() + " and spport_idspport like " + p.getIdSPPort() + "))";

            logger.info("SQL: " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                response = rs.getInt("M") + ", " + rs.getInt("N");
            }
            DAOMySQLSettings.closeStatement(st);
        } catch (SQLException sq) {
            throw new DAOException("In select: " + sq.getMessage());
        }

        return response;
    }
}
