package com.sensichips.sensiplus.model.dao;

import java.util.List;

//S=Chip e T=Cluster
public interface DAOCluster<T,S> extends DAO<T> {
    List<S> selectChiptIn(T t) throws DAOException;
    List<S> selectChipNotIn(T t) throws DAOException;
    //void insertChip(T f, S s) throws DAOException;
}
