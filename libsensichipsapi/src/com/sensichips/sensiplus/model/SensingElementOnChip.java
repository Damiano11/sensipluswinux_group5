package com.sensichips.sensiplus.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"seAssociated","m", "n"})
public class SensingElementOnChip {

    private String oldidSPCluster = null;
    private final StringProperty idSPChip;
    private final IntegerProperty idSensingElementOnFamily;
    private final StringProperty idSPCluster;
    private final IntegerProperty idSPPort;
    private final IntegerProperty m;
    private final IntegerProperty n;
    private final StringProperty idFamily;
    private StringProperty portName;
    private StringProperty seAssociated;

    public SensingElementOnChip() {
        this("", Integer.MIN_VALUE, "", Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, "");
    }

    public SensingElementOnChip (String idSPChip){
        this(idSPChip, Integer.MIN_VALUE, "", Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, "");
    }

    public SensingElementOnChip(String idSPChip, Integer idSensingElementOnFamily, String idSPCluster, Integer m, Integer n) {
        this(idSPChip, idSensingElementOnFamily, idSPCluster, Integer.MIN_VALUE, m, n, "");

    }

    public SensingElementOnChip(String idSPChip, Integer idSensingElementOnFamily, String idSPCluster,
                                Integer idSPPort, Integer m, Integer n, String idFamily) {
        this.idSPChip = new SimpleStringProperty(idSPChip);
        this.idSensingElementOnFamily = new SimpleIntegerProperty(idSensingElementOnFamily);
        this.idSPCluster = new SimpleStringProperty(idSPCluster);
        this.idSPPort = new SimpleIntegerProperty(idSPPort);
        this.m = new SimpleIntegerProperty(m);
        this.n = new SimpleIntegerProperty(n);
        this.idFamily = new SimpleStringProperty(idFamily);
        this.portName = new SimpleStringProperty();
        this.seAssociated = new SimpleStringProperty();

    }


    @XmlTransient
    public Integer getIdSensingElementOnFamily() {
        return idSensingElementOnFamily.getValue();
    }

    public IntegerProperty idSensingElementOnFamilyProperty() {
        return idSensingElementOnFamily;
    }

    public void setIdSensingElementOnFamily(int idSensingElementOnFamily) {
        this.idSensingElementOnFamily.set(idSensingElementOnFamily);
    }

    @XmlTransient
    public String getIdSPCluster() {
        return idSPCluster.get();
    }

    public StringProperty idSPClusterProperty() {
        return idSPCluster;
    }

    public void setIdSPCluster(String idSPCluster) {
        this.idSPCluster.set(idSPCluster);
    }

    @XmlTransient
    public Integer getIdSPPort() {
        return idSPPort.get();
    }

    public IntegerProperty idSPPortProperty() {
        return idSPPort;
    }

    public void setIdSPPort(int idSPPort) {
        this.idSPPort.set(idSPPort);
    }

    @XmlElement(name = "M", required = true)
    public Integer getM() {
        return m.get();
    }

    public StringProperty mProperty() {
        return (new SimpleStringProperty(m.getValue() == Integer.MIN_VALUE ? "" : m.getValue().toString()));
    }

    public void setM(int m) {
        this.m.set(m);
    }

    @XmlElement(name = "N", required = true)
    public Integer getN() {
        return n.get();
    }

    public StringProperty nProperty() {
        return (new SimpleStringProperty(n.getValue() == Integer.MIN_VALUE ? "" : n.getValue().toString()));
    }

    public void setN(int n) {
        this.n.set(n);
    }

    @XmlTransient
    public String getIdSPChip() {
        return idSPChip.get();
    }

    public StringProperty idSPChipProperty() {
        return idSPChip;
    }

    public void setIdSPChip(String idSPChip) {
        this.idSPChip.set(idSPChip);
    }

    @XmlTransient
    public String getIdFamily() {
        return idFamily.get();
    }

    public StringProperty idFamilyProperty() {
        return idFamily;
    }

    public void setIdFamily(String idFamily) {
        this.idFamily.set(idFamily);
    }

    @XmlTransient
    public String getOldidSPCluster() {
        return oldidSPCluster;
    }

    public void setOldidSPCalibration() {
        this.oldidSPCluster = this.idSPCluster.getValue();
    }

    @XmlElement(name = "SENSING_ELEMENT_ID", required = true)
    public String getSeAssociated() {
        return seAssociated.get();
    }

    public void setSeAssociated(String seAssociated) {
        this.seAssociated.set(seAssociated);
    }

    public StringProperty seAssociatedProperty() {
        return seAssociated;
    }

    @XmlTransient
    public String getPortName() {
        return portName.get();
    }

    public StringProperty portNameProperty() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName.set(portName);
    }

    public void clearOldidSPCalibration(){
        this.oldidSPCluster = null;
    }
}
