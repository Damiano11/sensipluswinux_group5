package com.sensichips.sensiplus.config;


import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.configuration_handler.*;
import com.sensichips.sensiplus.datareader.queue.SPChartElement;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.*;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.math.BigInteger;
import java.util.*;

/**
 * Created by Marco Ferdinandi on 01/03/2018.
 */
public class SPLayoutConfigurationHandler {



    public static void updateLayoutConfigurationAfterReading(LAYOUTCONFIGURATION layoutconfiguration){

    }


    public static void generateAndStoreInstalledChips(List<SPChip> spChipList, SPCluster spCluster, LAYOUTCONFIGURATION layoutconfiguration, File fileXML, File fileXSD) throws SPException{

        if(spChipList.size()>0){
            updateLayoutConfigurationBeforeWriting(layoutconfiguration);

            layoutconfiguration.setINSTALLEDCHIPS(new InstalledChips());
            layoutconfiguration.getINSTALLEDCHIPS().setCLUSTERID(spCluster.getClusterID());
            layoutconfiguration.getINSTALLEDCHIPS().setMULTICASTCLUSTER(spCluster.getMulticastAddress());

            int k=0;
            while(k<layoutconfiguration.getCLUSTER().size() && !layoutconfiguration.getCLUSTER().get(k).getCLUSTERID().equals(spCluster.getClusterID())){
                k++;
            }
            if(k==layoutconfiguration.getCLUSTER().size()){
                throw new SPException("Cluster "+spCluster.getClusterID()+" not found in the XML");
            }
            for (int i = 0; i < spChipList.size(); i++) {
                int j = 0;
                while (j < layoutconfiguration.getCLUSTER().get(k).getCHIP().size() &&
                        !layoutconfiguration.getCLUSTER().get(k).getCHIP().get(j).getSERIALNUMBER().equals(spChipList.get(i).getSerialNumber())){
                    j++;
                }
                if(j==layoutconfiguration.getCLUSTER().get(k).getCHIP().size()){
                    throw new SPException("CHIP "+spChipList.get(i).getSerialNumber()+" not found in xml");
                }
                layoutconfiguration.getINSTALLEDCHIPS().getCHIP().add(layoutconfiguration.getCLUSTER().get(k).getCHIP().get(j));

            }
            try {

                SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                Schema schema = sf.newSchema(fileXSD);

                JAXBContext context = JAXBContext.newInstance(LAYOUTCONFIGURATION.class);


                Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marshaller.setSchema(schema);



                marshaller.marshal(layoutconfiguration, fileXML);

                SPConfigurationManager.updateLayoutConfigurationAfterReading(layoutconfiguration);
            }catch (Exception e){
                throw new SPException("Error in updating the XML with the installed chips: "+e.getMessage());
            }

        }else{
            throw new SPException("No chips in the input list");
        }

    }


    public static void overwriteXMLWithJAXB(LAYOUTCONFIGURATION layoutconfiguration,File fileXML,File fileXSD)throws SPException{
        try {
            updateLayoutConfigurationBeforeWriting(layoutconfiguration);

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(fileXSD);

            JAXBContext context = JAXBContext.newInstance(LAYOUTCONFIGURATION.class);

            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setSchema(schema);
            marshaller.marshal(layoutconfiguration, fileXML);

            SPConfigurationManager.updateLayoutConfigurationAfterReading(layoutconfiguration);
        }catch (Exception e){
            throw new SPException("Error in rewriting the XML : "+e.getMessage());
        }
    }



    public static void updateLayoutConfigurationBeforeWriting(LAYOUTCONFIGURATION layoutconfiguration){
        for(int i=0; i<layoutconfiguration.getCONFIGURATION().size(); i++){
            layoutconfiguration.getCONFIGURATION().get(i).setCluster(null);
        }
        for(int i=0; i<layoutconfiguration.getCLUSTER().size(); i++){
            for(int j=0; j<layoutconfiguration.getCLUSTER().get(i).getCHIP().size(); j++){
                layoutconfiguration.getCLUSTER().get(i).getCHIP().get(j).setSpFamily(null);
                for(int k=0; k<layoutconfiguration.getCLUSTER().get(i).getCHIP().get(j).getSENSINGELEMENTONCHIP().size(); k++){
                    layoutconfiguration.getCLUSTER().get(i).getCHIP().get(j).getSENSINGELEMENTONCHIP().get(k).setSensingElementOnFamily(null);
                }
            }
        }
        for(int i=0; i<layoutconfiguration.getFAMILY().size(); i++){
            for(int j=0; j<layoutconfiguration.getFAMILY().get(i).getSENSINGELEMENTONFAMILY().size(); j++){
                layoutconfiguration.getFAMILY().get(i).getSENSINGELEMENTONFAMILY().get(j).setSensingElement(null);
            }
        }



    }




    public static Hashtable<String, SPConfiguration> generateAvailableConfiguration(LAYOUTCONFIGURATION layoutconfiguration) throws SPException{
        Hashtable<String,SPConfiguration> spConfigurationTable = new Hashtable<>();
        Hashtable<String, SPSensingElement> spSensingElementTable = new Hashtable<>();
        Hashtable<String, SPSensingElementOnFamily> spSensingElementOnFamilyTable = new Hashtable<>();
        Hashtable<String, SPFamily> spFamilyTable = new Hashtable<>();
        Hashtable<String, SPCluster> spClusterTable = new Hashtable<>();

        for(int i=0; i<layoutconfiguration.getSENSINGELEMENT().size(); i++){
            SensingElement sensingElement = layoutconfiguration.getSENSINGELEMENT().get(i);

            SPSensingElement spSensingElement = new SPSensingElement();
            spSensingElement.setName(sensingElement.getNAME());
            spSensingElement.setConversionRate( sensingElement.getConversionRate());
            spSensingElement.setNData(sensingElement.getNData());
            spSensingElement.setMeasureTechnique(sensingElement.getMeasureTechnique());
            spSensingElement.setFilter(Integer.toString(sensingElement.getFilter()));
            spSensingElement.getSpDataRepresentationManager().setRangeMin(Double.toString(sensingElement.getRANGEMIN()));
            spSensingElement.getSpDataRepresentationManager().setRangeMax(Double.toString(sensingElement.getRANGEMAX()));
            spSensingElement.getSpDataRepresentationManager().setDefault_alarm_threshold(Double.toString(sensingElement.getDEFAULTALARMTHRESHOLD()));
            spSensingElement.getSpDataRepresentationManager().setInitial_multiplier(Double.toString(sensingElement.getMULTIPLIER()));
            spSensingElement.getSpDataRepresentationManager().setMeasureUnit(sensingElement.getMEASUREUNIT());

            if(!spSensingElement.getMeasureTechnique().equals("DIRECT")){
                spSensingElement.setRsense(Integer.toString(sensingElement.getRsense()));
                spSensingElement.setInGain(Integer.toString(sensingElement.getInGain()));
                spSensingElement.setOutGain(Integer.toString(sensingElement.getOutGain()));
                spSensingElement.setContacts(sensingElement.getContacts());
                spSensingElement.setFrequency(sensingElement.getFrequency());
                spSensingElement.setHarmonic(sensingElement.getHarmonic());
                spSensingElement.setDCBiasP(sensingElement.getDCBiasP());
                spSensingElement.setDCBiasN(sensingElement.getDCBiasN());
                spSensingElement.setModeVI(sensingElement.getModeVI());
                spSensingElement.setMeasureType(sensingElement.getMeasureType());
                spSensingElement.setPhaseShiftMode(sensingElement.getPhaseShiftMode());
                spSensingElement.setPhaseShift(sensingElement.getPhaseShift());
                spSensingElement.setIq(sensingElement.getIQ());
                spSensingElement.setInportADC(sensingElement.getInPortADC());
            }

            spSensingElementTable.put(spSensingElement.getName(),spSensingElement);

        }

        for(int i=0; i<layoutconfiguration.getFAMILY().size(); i++){
            Family family = layoutconfiguration.getFAMILY().get(i);
            SPFamily spFamily = new SPFamily();

            spFamily.setName(family.getFAMILYNAME());
            spFamily.setId(family.getFAMILYID());
            spFamily.setHW_VERSION(family.getHWVERSION());
            spFamily.setSYS_CLOCK(family.getSYSCLOCK());

            for(int j=0; j<family.getMEASURETYPE().size(); j++)
                spFamily.addMeasureType(family.getMEASURETYPE().get(j));

            spFamily.setMulticastDefaultAddress(family.getMULTICASTDEFAULT());
            spFamily.setBroadcastAddress(family.getBROADCAST());
            spFamily.setBroadcastSlowAddress(family.getBROADCASTSLOW());

            for(int j=0; j<family.getSENSINGELEMENTONFAMILY().size(); j++) {
                SensingElementOnFamily sensingElementOnFamily = family.getSENSINGELEMENTONFAMILY().get(j);
                SPSensingElementOnFamily spSensingElementOnFamily = new SPSensingElementOnFamily();
                spSensingElementOnFamily.setID(sensingElementOnFamily.getSENSINGELEMENTID());
                spSensingElementOnFamily.setSensingElement(spSensingElementTable.get(sensingElementOnFamily.getSENSINGELEMENTNAME()));
                spSensingElementOnFamily.setPort(new SPPort(sensingElementOnFamily.getSENSINGELEMENTPORT()));

                spSensingElementOnFamilyTable.put(spSensingElementOnFamily.getID(),spSensingElementOnFamily);
                spFamily.addSPSensingElementOnFamily(spSensingElementOnFamily);
            }

            for(int j=0; j<family.getANALYTE().size(); j++) {
                Analyte analyte = family.getANALYTE().get(j);
                SPAnalyte spAnalyte = new SPAnalyte();

                spAnalyte.setMeasureUnit(analyte.getANALYTEMEASUREUNIT());
                spAnalyte.setName(analyte.getANALYTENAME());

                spFamily.getAnalyteList().add(spAnalyte);
            }
            spFamilyTable.put(spFamily.getId(),spFamily);
        }

        for(int i=0; i<layoutconfiguration.getCLUSTER().size(); i++){
            Cluster cluster = layoutconfiguration.getCLUSTER().get(i);
            SPCluster spCluster = new SPCluster();

            spCluster.setClusterID(cluster.getCLUSTERID());
            spCluster.setMulticastAddress(cluster.getMULTICASTCLUSTER());

            spCluster.setBroadcastAddressI2C("0x00");

            for(int j=0; j<cluster.getCHIP().size(); j++){
                Chip chip = cluster.getCHIP().get(j);
                SPChip spChip = new SPChip();
                String familyID = chip.getFAMILYID().toUpperCase().replace("0X","");
                spChip.setSpFamily(spFamilyTable.get(familyID));

                if(j==0){
                    spCluster.setFamilyOfChips(spFamilyTable.get(familyID));
                }

                spChip.setSerialNumber(chip.getSERIALNUMBER());
                spChip.setI2CAddress(chip.getI2CADDRESS());

                if(chip.getOSCTRIM()==null){
                    spChip.setOscTrimOverride(spFamilyTable.get(familyID).getOSC_TRIM());
                }
                else{
                    spChip.setOscTrimOverride(chip.getOSCTRIM());
                }

                //TODO: AGGIUNGERE OSC TRIM OVERRIDE

                for(int k=0; k<chip.getSENSINGELEMENTONCHIP().size(); k++){
                    SensingElementOnChip sensingElementOnChip = chip.getSENSINGELEMENTONCHIP().get(k);
                    SPSensingElementOnChip spSensingElementOnChip = new SPSensingElementOnChip();
                    spSensingElementOnChip.setSensingElementOnFamily(spSensingElementOnFamilyTable.get(sensingElementOnChip.getSENSINGELEMENTID()));
                    SPCalibrationParameters spCalibrationParameters = new SPCalibrationParameters();
                    spCalibrationParameters.setM(sensingElementOnChip.getCALIBRATIONPARAMETER().getM());
                    spCalibrationParameters.setN(sensingElementOnChip.getCALIBRATIONPARAMETER().getN());
                    if(sensingElementOnChip.getCALIBRATIONPARAMETER().getThreshold()!=null)
                        spCalibrationParameters.setThreshold(sensingElementOnChip.getCALIBRATIONPARAMETER().getThreshold());
                    spSensingElementOnChip.setCalibrationParameters(spCalibrationParameters);

                    spChip.getSensingElementOnChipList().add(spSensingElementOnChip);

                }

                spCluster.add(spChip);

            }


            SPConfigurationManager.getSPClusterList().add(spCluster);
            spClusterTable.put(spCluster.getClusterID(),spCluster);
        }

        for(int i=0; i<layoutconfiguration.getCONFIGURATION().size(); i++){
            SPConfiguration spConfiguration = new SPConfiguration();
            Configuration conf = layoutconfiguration.getCONFIGURATION().get(i);

            spConfiguration.setDescription(conf.getDESCRIPTION());
            spConfiguration.addDriverParam(conf.getDRIVER().getPARAM1());
            spConfiguration.addDriverParam(conf.getDRIVER().getPARAM2());
            spConfiguration.setDriverName(conf.getDRIVER().getDRIVERNAME());
            spConfiguration.setHostController(conf.getHOSTCONTROLLER());
            spConfiguration.setApiOwner(conf.getAPIOWNER());
            spConfiguration.setMcu(conf.getMCU());
            spConfiguration.setProtocol(conf.getPROTOCOL());
            spConfiguration.setAddressingType(conf.getADDRESSINGTYPE());
            spConfiguration.setVCC(conf.getVCC());
            spConfiguration.setCluster(spClusterTable.get(conf.getCLUSTERID()));

            spConfigurationTable.put(spConfiguration.getConfigurationName(),spConfiguration);
        }


        if(layoutconfiguration.getINSTALLEDCHIPS()!=null) {
            String clusterID = layoutconfiguration.getINSTALLEDCHIPS().getCLUSTERID();
            String multicastCluster = layoutconfiguration.getINSTALLEDCHIPS().getMULTICASTCLUSTER();
            List<SPChip> spChipList = new ArrayList<>();
            List<Chip> chipList = layoutconfiguration.getINSTALLEDCHIPS().getCHIP();
           /* for (int i = 0; i < chipList.size(); i++){
                String chipSerialNumber = chipList.get(i).getSERIALNUMBER();

            }*/


            for(int j=0; j<chipList.size(); j++){
                Chip chip = chipList.get(j);
                SPChip spChip = new SPChip();
                String familyID = chip.getFAMILYID().toUpperCase().replace("0X","");
                spChip.setSpFamily(spFamilyTable.get(familyID));

                /*if(j==0){
                    spCluster.setFamilyOfChips(spFamilyTable.get(familyID));
                }*/

                spChip.setSerialNumber(chip.getSERIALNUMBER());
                spChip.setI2CAddress(chip.getI2CADDRESS());

                if(chip.getOSCTRIM()==null){
                    spChip.setOscTrimOverride(spFamilyTable.get(familyID).getOSC_TRIM());
                }
                else{
                    spChip.setOscTrimOverride(chip.getOSCTRIM());
                }

                //TODO: AGGIUNGERE OSC TRIM OVERRIDE

                for(int k=0; k<chip.getSENSINGELEMENTONCHIP().size(); k++){
                    SensingElementOnChip sensingElementOnChip = chip.getSENSINGELEMENTONCHIP().get(k);
                    SPSensingElementOnChip spSensingElementOnChip = new SPSensingElementOnChip();
                    spSensingElementOnChip.setSensingElementOnFamily(spSensingElementOnFamilyTable.get(sensingElementOnChip.getSENSINGELEMENTID()));
                    SPCalibrationParameters spCalibrationParameters = new SPCalibrationParameters();
                    spCalibrationParameters.setM(sensingElementOnChip.getCALIBRATIONPARAMETER().getM());
                    spCalibrationParameters.setN(sensingElementOnChip.getCALIBRATIONPARAMETER().getN());
                    if(sensingElementOnChip.getCALIBRATIONPARAMETER().getThreshold()!=null)
                        spCalibrationParameters.setThreshold(sensingElementOnChip.getCALIBRATIONPARAMETER().getThreshold());
                    spSensingElementOnChip.setCalibrationParameters(spCalibrationParameters);

                    spChip.getSensingElementOnChipList().add(spSensingElementOnChip);

                }

                spChipList.add(spChip);

            }

            Collection<SPConfiguration> collection = spConfigurationTable.values();
            for (Iterator iterator = collection.iterator(); iterator.hasNext();) {
                SPConfiguration val = (SPConfiguration) iterator.next();
                val.setSpInstalledChips(spChipList);
            }


        }

        return spConfigurationTable;



    }


    public static List<SPConfiguration> getSPConfigurationListFromLayoutConfiguration(LAYOUTCONFIGURATION layoutconfiguration)throws SPException{
        List<SPConfiguration> output = new ArrayList<>();
        for(int i=0; i<layoutconfiguration.getCONFIGURATION().size(); i++){
            SPConfiguration spConfiguration = new SPConfiguration();
            Configuration conf = layoutconfiguration.getCONFIGURATION().get(i);

            spConfiguration.setDescription(conf.getDESCRIPTION());
            spConfiguration.addDriverParam(conf.getDRIVER().getPARAM1());
            spConfiguration.addDriverParam(conf.getDRIVER().getPARAM2());
            spConfiguration.setDriverName(conf.getDRIVER().getDRIVERNAME());
            spConfiguration.setHostController(conf.getHOSTCONTROLLER());
            spConfiguration.setApiOwner(conf.getAPIOWNER());
            spConfiguration.setMcu(conf.getMCU());
            spConfiguration.setProtocol(conf.getPROTOCOL());
            spConfiguration.setAddressingType(conf.getADDRESSINGTYPE());
            spConfiguration.setVCC(conf.getVCC());

            //spConfiguration.setCluster(spClusterTable.get(conf.getCLUSTERID()));


            output.add(spConfiguration);
        }

        return output;
    }


    public static SPCluster getSPCluster(Cluster cluster) throws SPException {
        SPCluster clusterOut = new SPCluster();

        clusterOut.setClusterID(cluster.getCLUSTERID());
        clusterOut.setBroadcastAddressI2C("0x00");
        clusterOut.setMulticastAddress(cluster.getMULTICASTCLUSTER());


       return clusterOut;
    }


}
