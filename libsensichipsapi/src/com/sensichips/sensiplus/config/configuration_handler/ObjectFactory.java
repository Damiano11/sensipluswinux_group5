//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.config.configuration_handler;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the configuration_handler package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: configuration_handler
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LAYOUTCONFIGURATION }
     * 
     */
    public LAYOUTCONFIGURATION createLAYOUTCONFIGURATION() {
        return new LAYOUTCONFIGURATION();
    }

    /**
     * Create an instance of {@link SensingElement }
     * 
     */
    public SensingElement createSensingElement() {
        return new SensingElement();
    }

    /**
     * Create an instance of {@link Family }
     * 
     */
    public Family createFamily() {
        return new Family();
    }

    /**
     * Create an instance of {@link Cluster }
     * 
     */
    public Cluster createCluster() {
        return new Cluster();
    }

    /**
     * Create an instance of {@link Configuration }
     * 
     */
    public Configuration createConfiguration() {
        return new Configuration();
    }

    /**
     * Create an instance of {@link InstalledChips }
     * 
     */
    public InstalledChips createInstalledChips() {
        return new InstalledChips();
    }

    /**
     * Create an instance of {@link ConfigurationDriver }
     * 
     */
    public ConfigurationDriver createConfigurationDriver() {
        return new ConfigurationDriver();
    }

    /**
     * Create an instance of {@link Analyte }
     * 
     */
    public Analyte createAnalyte() {
        return new Analyte();
    }

    /**
     * Create an instance of {@link CalibrationParameter }
     * 
     */
    public CalibrationParameter createCalibrationParameter() {
        return new CalibrationParameter();
    }

    /**
     * Create an instance of {@link SensingElementOnChip }
     * 
     */
    public SensingElementOnChip createSensingElementOnChip() {
        return new SensingElementOnChip();
    }

    /**
     * Create an instance of {@link SensingElementOnFamily }
     * 
     */
    public SensingElementOnFamily createSensingElementOnFamily() {
        return new SensingElementOnFamily();
    }

    /**
     * Create an instance of {@link Chip }
     * 
     */
    public Chip createChip() {
        return new Chip();
    }

}
