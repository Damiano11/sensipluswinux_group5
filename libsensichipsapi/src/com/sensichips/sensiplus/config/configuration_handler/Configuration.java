//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.config.configuration_handler;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Configuration complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Configuration">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DRIVER" type="{}ConfigurationDriver"/>
 *         &lt;element name="HOST_CONTROLLER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="API_OWNER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MCU" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PROTOCOL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ADDRESSING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CLUSTER_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cluster" type="{}Cluster" minOccurs="0"/>
 *         &lt;element name="VCC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Configuration", propOrder = {
    "description",
    "driver",
    "hostcontroller",
    "apiowner",
    "mcu",
    "protocol",
    "addressingtype",
    "clusterid",
    "cluster",
    "vcc"
})
public class Configuration {

    @XmlElement(name = "DESCRIPTION", required = true)
    protected String description;
    @XmlElement(name = "DRIVER", required = true)
    protected ConfigurationDriver driver;
    @XmlElement(name = "HOST_CONTROLLER", required = true)
    protected String hostcontroller;
    @XmlElement(name = "API_OWNER", required = true)
    protected String apiowner;
    @XmlElement(name = "MCU", required = true)
    protected String mcu;
    @XmlElement(name = "PROTOCOL", required = true)
    protected String protocol;
    @XmlElement(name = "ADDRESSING_TYPE", required = true)
    protected String addressingtype;
    @XmlElement(name = "CLUSTER_ID", required = true)
    protected String clusterid;
    protected Cluster cluster;
    @XmlElement(name = "VCC", required = true)
    protected String vcc;

    /**
     * Recupera il valore della propriet� description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRIPTION() {
        return description;
    }

    /**
     * Imposta il valore della propriet� description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRIPTION(String value) {
        this.description = value;
    }

    /**
     * Recupera il valore della propriet� driver.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationDriver }
     *     
     */
    public ConfigurationDriver getDRIVER() {
        return driver;
    }

    /**
     * Imposta il valore della propriet� driver.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationDriver }
     *     
     */
    public void setDRIVER(ConfigurationDriver value) {
        this.driver = value;
    }

    /**
     * Recupera il valore della propriet� hostcontroller.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHOSTCONTROLLER() {
        return hostcontroller;
    }

    /**
     * Imposta il valore della propriet� hostcontroller.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHOSTCONTROLLER(String value) {
        this.hostcontroller = value;
    }

    /**
     * Recupera il valore della propriet� apiowner.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPIOWNER() {
        return apiowner;
    }

    /**
     * Imposta il valore della propriet� apiowner.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPIOWNER(String value) {
        this.apiowner = value;
    }

    /**
     * Recupera il valore della propriet� mcu.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMCU() {
        return mcu;
    }

    /**
     * Imposta il valore della propriet� mcu.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMCU(String value) {
        this.mcu = value;
    }

    /**
     * Recupera il valore della propriet� protocol.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROTOCOL() {
        return protocol;
    }

    /**
     * Imposta il valore della propriet� protocol.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROTOCOL(String value) {
        this.protocol = value;
    }

    /**
     * Recupera il valore della propriet� addressingtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDRESSINGTYPE() {
        return addressingtype;
    }

    /**
     * Imposta il valore della propriet� addressingtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDRESSINGTYPE(String value) {
        this.addressingtype = value;
    }

    /**
     * Recupera il valore della propriet� clusterid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLUSTERID() {
        return clusterid;
    }

    /**
     * Imposta il valore della propriet� clusterid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLUSTERID(String value) {
        this.clusterid = value;
    }

    /**
     * Recupera il valore della propriet� cluster.
     * 
     * @return
     *     possible object is
     *     {@link Cluster }
     *     
     */
    public Cluster getCluster() {
        return cluster;
    }

    /**
     * Imposta il valore della propriet� cluster.
     * 
     * @param value
     *     allowed object is
     *     {@link Cluster }
     *     
     */
    public void setCluster(Cluster value) {
        this.cluster = value;
    }

    /**
     * Recupera il valore della propriet� vcc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVCC() {
        return vcc;
    }

    /**
     * Imposta il valore della propriet� vcc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVCC(String value) {
        this.vcc = value;
    }

}
