//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.03.02 alle 12:34:11 PM CET 
//


package com.sensichips.sensiplus.config.configuration_handler;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per SensingElementOnChip complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="SensingElementOnChip">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sensingElementOnFamily" type="{}SensingElementOnFamily" minOccurs="0"/>
 *         &lt;element name="SENSING_ELEMENT_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CALIBRATION_PARAMETER" type="{}CalibrationParameter"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SensingElementOnChip", propOrder = {
    "sensingElementOnFamily",
    "sensingelementid",
    "calibrationparameter"
})
public class SensingElementOnChip {

    protected SensingElementOnFamily sensingElementOnFamily;
    @XmlElement(name = "SENSING_ELEMENT_ID", required = true)
    protected String sensingelementid;
    @XmlElement(name = "CALIBRATION_PARAMETER", required = true)
    protected CalibrationParameter calibrationparameter;

    /**
     * Recupera il valore della propriet� sensingElementOnFamily.
     * 
     * @return
     *     possible object is
     *     {@link SensingElementOnFamily }
     *     
     */
    public SensingElementOnFamily getSensingElementOnFamily() {
        return sensingElementOnFamily;
    }

    /**
     * Imposta il valore della propriet� sensingElementOnFamily.
     * 
     * @param value
     *     allowed object is
     *     {@link SensingElementOnFamily }
     *     
     */
    public void setSensingElementOnFamily(SensingElementOnFamily value) {
        this.sensingElementOnFamily = value;
    }

    /**
     * Recupera il valore della propriet� sensingelementid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSENSINGELEMENTID() {
        return sensingelementid;
    }

    /**
     * Imposta il valore della propriet� sensingelementid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSENSINGELEMENTID(String value) {
        this.sensingelementid = value;
    }

    /**
     * Recupera il valore della propriet� calibrationparameter.
     * 
     * @return
     *     possible object is
     *     {@link CalibrationParameter }
     *     
     */
    public CalibrationParameter getCALIBRATIONPARAMETER() {
        return calibrationparameter;
    }

    /**
     * Imposta il valore della propriet� calibrationparameter.
     * 
     * @param value
     *     allowed object is
     *     {@link CalibrationParameter }
     *     
     */
    public void setCALIBRATIONPARAMETER(CalibrationParameter value) {
        this.calibrationparameter = value;
    }

}
