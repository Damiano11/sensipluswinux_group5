package com.sensichips.sensiplus.datareader;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfigurationManager;
import com.sensichips.sensiplus.datareader.queue.SPChartElement;
import com.sensichips.sensiplus.datareader.queue.SPChartQueue;
import com.sensichips.sensiplus.datareader.queue.SPMainQueue;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.chip.SPSensingElementOnFamily;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementMetaParameter;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementOutput;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterActuator;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterPOT;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterSENSORS;
import com.sensichips.sensiplus.util.SPDataRepresentation;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

import java.util.List;

/**
 * Created by mario on 07/03/17.
 */

public class SPDataReader extends Thread {
    private static String LOG = "SPDataReader";
    private SPDataReaderConf readerConf;
    private SPMeasurement spMeasurement;
    private SPMainQueue spMainQueue;
    private boolean pauseReader = false;
    private boolean stopReader = false;
    private int counter = 0;
    private int numOfMeasures; // Numero di misure per ogni chip
    private int numOfChips;
    private List<SPChip> chipList;
    private SPMeasurement spMeasurementTemp;
    private int typeOfTempMeasure = 0;
    private int setIntervalTempMeasure = 10;
    private int counterIntervalTempMeasure = 0;


    public static final int TEMPMEASURE_LOOP = -1;
    public static final int TEMPMEASURE_ONE = 1;
    public static final int TEMPMEASURE_STOP = 0;

    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L2;

    public SPDataReader(SPDataReaderConf readerConf, SPMeasurement measurement){
        this.readerConf = readerConf;
        this.spMainQueue = new SPMainQueue(readerConf.getMaxDimQueue());
        this.spMeasurement = measurement;
        this.pauseReader = true; // The reader start in pause mode
        this.stopReader = false;
        this.numOfMeasures = readerConf.getNumOfMeasures();
        this.chipList = readerConf.getSpConf().getCluster().getActiveSPChipList();
        this.numOfChips = chipList.size();
        this.typeOfTempMeasure = readerConf.getTEMP_MEASUREMENT_MODE();
        try{
            this.spMeasurementTemp = readerConf.getSpConf().getManagedSPMeasurement("ONCHIP_TEMPERATURE");
        } catch(SPException e) {
            SPLogger.getLogInterface().d(LOG, "ONCHIP_TEMPERATURE sensing element unavailable in this configuration!!", SPLoggerInterface.DEBUG_VERBOSITY_L2);
        }
        start();
    }

    public SPMainQueue getSPMainQueue(){
        return spMainQueue;
    }


    public synchronized void ledOnchipsGreen(SPCluster temporary, boolean turnOn) throws SPException {
        SPMeasurementParameterActuator actuator = new SPMeasurementParameterActuator(SPConfigurationManager.getSPConfigurationDefault());
        actuator.setPort(SPMeasurementParameterActuator.ACTUATOR_PORT_RDY);
        actuator.setTemporaryCluster(temporary);
        int op = SPMeasurementParameterActuator.OFF;
        if (turnOn){
            op = SPMeasurementParameterActuator.ON;
        }
        actuator.setMode(op);
        spMeasurement.setACTUATOR(actuator);
    }



    public synchronized void ledOnchipsRed(SPCluster temporary, int mode) throws SPException {
        SPMeasurementParameterActuator actuator = new SPMeasurementParameterActuator(SPConfigurationManager.getSPConfigurationDefault());
        actuator.setPort(SPMeasurementParameterActuator.ACTUATOR_PORT_INT);
        actuator.setTemporaryCluster(temporary);
        actuator.setMode(mode);

        spMeasurement.setACTUATOR(actuator);
    }


    public SPMeasurement getSpMeasurement() {
        return spMeasurement;
    }

    public void setPauseReader(boolean pauseReader){
        this.pauseReader = pauseReader;
        /*if(pauseReader) {
            while (!paused) {
                SPDelay.delay(1);
            }
        }*/
    }

    public void stopReader(){
        stopReader = true;
        while(!stopped)
        {
            SPDelay.delay(1);
        }
    }

    public synchronized void subscribe(SPChartQueue q){
        spMainQueue.subscribe(q);
    }

    public synchronized void unsubscribe(SPChartQueue q){
        spMainQueue.unSubscribe(q);
    }

    private int totMeasure = 0;
    public int getTotMeasure(){
        return totMeasure;
    }

    private long startTime;
    private long currentTime;
    public float getMeasForSec(){
        return totMeasure/((currentTime - startTime)/(float)1000);
    }

    private boolean stopped = false;
    private boolean paused = false;
    public void run(){
        stopped  = false;
        paused = false;
        startTime = System.currentTimeMillis();
        while(!stopReader){
            if (!pauseReader){
                totMeasure++;
                counter++;
                // 1. Generate a measure
                SPChartElement item = null;
                try {
                    item = getNextChartElement(counter==1);
                } catch (SPException e) {
                    item = new SPChartElement(0, null, null, 0, 0,null);
                    item.setERROR(true);
                    item.setERROR_MESSAGE(e.getMessage());
                }
                // 2. insert the measure into the queue
                if (item != null)
                    spMainQueue.put(item);
            }
            paused = pauseReader;

            // 3. wait for pause ms
            try {
                sleep(readerConf.getDelay());
            } catch (InterruptedException e) {
                SPLogger.getLogInterface().d(LOG, e.getMessage(), DEBUG_LEVEL);
            }
            currentTime = System.currentTimeMillis();
        }
        stopped = true;
    }

    public synchronized void setNumTempMeasure(int numTempMeasure){
        this.typeOfTempMeasure = numTempMeasure;
        counterIntervalTempMeasure = 0;
    }

    public boolean isStopped(){
        return stopped;
    }
    public boolean isPaused(){
        return paused;
    }
    private int timeStamp = 0;

    private synchronized SPChartElement getNextChartElement(boolean firstElement) throws SPException{

        double[][] yValues;
        double[] temperature = null;
        double xValue = 0;
        boolean restoreMeasurement = false;

        /*SPMeasurementMetaParameter meta = new SPMeasurementMetaParameter();
        meta.setRenewBuffer(false);
        meta.setFillBufferBeforeStart(false);
        meta.setBurst(1);*/

        if (spMeasurementTemp != null){
            if (typeOfTempMeasure == TEMPMEASURE_LOOP && (counterIntervalTempMeasure % setIntervalTempMeasure) == 0 ||
                    typeOfTempMeasure == TEMPMEASURE_ONE && counterIntervalTempMeasure == 0 ){
                SPSensingElementOnFamily sensingElementoOnFamily = readerConf.getSpConf().getCluster().getFamilyOfChips().getSPSensingElementOnFamily("ONCHIP_TEMPERATURE");
                SPMeasurementParameterSENSORS spMeasurementParameterSENSORS  = (SPMeasurementParameterSENSORS)sensingElementoOnFamily.getSPMeasurementParameter(readerConf.getSpConf());
                spMeasurementParameterSENSORS.getSPMeasurementMetaParameter().setRenewBuffer(false);
                spMeasurementParameterSENSORS.getSPMeasurementMetaParameter().setFillBufferBeforeStart(false);
                spMeasurementParameterSENSORS.getSPMeasurementMetaParameter().setBurst(1);
                SPMeasurementOutput out = new SPMeasurementOutput();
                spMeasurementTemp.setSENSOR(spMeasurementParameterSENSORS, out);
                restoreMeasurement = true;

                double[][] appo = spMeasurementTemp.getSENSOR(spMeasurementParameterSENSORS, out);



                temperature = new double[numOfChips];
                for(int i = 0; i < numOfChips; i++){
                    temperature[i] = appo[i][0];
                }
            }
            counterIntervalTempMeasure++;
        }

        SPMeasurementOutput outMsg = new SPMeasurementOutput();
        if (readerConf.getSPMeasurementParameter() instanceof SPMeasurementParameterSENSORS) {

            if (restoreMeasurement){
                spMeasurement.setSENSOR((SPMeasurementParameterSENSORS) readerConf.getSPMeasurementParameter(), outMsg);
            }

            yValues = new double[numOfChips][];
            for(int i = 0; i < numOfChips; i++){
                yValues[i] = new double[numOfMeasures];
            }
            double[][] appo = null;
            xValue = timeStamp++;
            appo = spMeasurement.getSENSOR((SPMeasurementParameterSENSORS) readerConf.getSPMeasurementParameter(), outMsg);

            int indextToWithdraw = 0;
            if(((SPMeasurementParameterSENSORS) readerConf.getSPMeasurementParameter()).getParamInternalEIS()!=null){
                indextToWithdraw = ((SPMeasurementParameterSENSORS) readerConf.getSPMeasurementParameter()).getParamInternalEIS().getMeasure();
            }

            for (int i = 0; i < chipList.size(); i++) {
                yValues[i][0] = appo[i][indextToWithdraw];
            }
            restoreMeasurement = false;
                //throw new SPException("getNextChartElement to be implemented!!!");
        } else if (readerConf.getSPMeasurementParameter() instanceof SPMeasurementParameterEIS){
            if (restoreMeasurement){
                spMeasurement.setEIS((SPMeasurementParameterEIS) readerConf.getSPMeasurementParameter(), outMsg);
            }

            double appo[][] = spMeasurement.getEIS((SPMeasurementParameterEIS) readerConf.getSPMeasurementParameter(), outMsg);
            xValue = timeStamp++;

            yValues = new double[numOfChips][];
            for(int i = 0; i < numOfChips; i++){
                yValues[i] = new double[numOfMeasures];
                System.arraycopy(appo[i], 0, yValues[i], 0, numOfMeasures);
            }

        } else if (readerConf.getSPMeasurementParameter() instanceof SPMeasurementParameterPOT) {
            if (restoreMeasurement){
                spMeasurement.setPOT((SPMeasurementParameterPOT) readerConf.getSPMeasurementParameter(), outMsg);
            }

            yValues = spMeasurement.getPOT((SPMeasurementParameterPOT) readerConf.getSPMeasurementParameter(), outMsg);
            if (((SPMeasurementParameterPOT) readerConf.getSPMeasurementParameter()).getTypeVoltammetry().equals(SPMeasurementParameterPOT.CURRENT)
                    || ((SPMeasurementParameterPOT) readerConf.getSPMeasurementParameter()).getTypeVoltammetry().equals(SPMeasurementParameterPOT.POTENTIOMETRIC)){
                xValue = timeStamp++;
            } else {
                xValue = yValues[0][0];
            }
        } else {
            throw new SPException("Type of measure unavailable: control the SPDataReader!");
            //for(int i = 0; i < numOfChips; i++) {
            //    for (int j = 0; j < numOfMeasures; j++) {
            //        double step = Math.PI / 10;
            //        yValues[i][j] = (30 * Math.sin(counter * step + step * i));
            //    }
            //}
        }

        SPDataRepresentation appo = readerConf.getSpDataRepresentationManager().adapt(yValues,firstElement);
        readerConf.getSpDataRepresentationManager().setAlarmThreshold(appo.alarm_threshold);
        SPChartElement item = new SPChartElement(xValue, yValues, temperature, numOfChips, numOfMeasures,appo);
        if(outMsg.ADCOverflow || outMsg.ADCUnderflow){
            String errorMessage="";
            if(outMsg.ADCOverflow)
            errorMessage = "ADC Overflow ";
            else{
                errorMessage = "ADC Underflow ";
            }
            item.setERROR(true);
            if(spMeasurement.isAutomaticADCRangeAdjustment()){
                errorMessage = errorMessage + " - Range adjusted automatically";
            }
            item.setERROR_MESSAGE(errorMessage);
        }
        return item;
    }
}