package com.sensichips.sensiplus.datareader.queue;

import com.sensichips.sensiplus.datareader.SPDataReader;
import com.sensichips.sensiplus.util.SPDataRepresentation;

public class SPChartElement {


    private double yValues[][];
    private double xValue = 0;
    private double temperature[];

    private boolean ERROR = false;
    private String ERROR_MESSAGE = "";

    private double xRange[] = null;
    private double yRange[] = null;

    private int numOfChips;
    private int numOfMeasures;

    public SPDataRepresentation getSpDataRepresentation() {
        return spDataRepresentation;
    }

    public void setSpDataRepresentation(SPDataRepresentation spDataRepresentation) {
        this.spDataRepresentation = spDataRepresentation;
    }

    private SPDataRepresentation spDataRepresentation;

    public SPChartElement(double xValue, double yValues[][], double temperature[], int numOfChips, int numOfMeasures, SPDataRepresentation spDataRepresentation){
        this.yValues = yValues;
        this.xValue = xValue;
        this.numOfChips = numOfChips;
        this.numOfMeasures = numOfMeasures;
        this.temperature = temperature;
        this.spDataRepresentation = spDataRepresentation;
    }

    public double[] getTemperature(){
        return temperature;
    }

    public boolean isERROR() {
        return ERROR;
    }

    public void setERROR(boolean ERROR) {
        this.ERROR = ERROR;
    }

    public String getERROR_MESSAGE() {
        return ERROR_MESSAGE;
    }

    public void setERROR_MESSAGE(String ERROR_MESSAGE) {
        this.ERROR_MESSAGE = ERROR_MESSAGE;
    }

    public double[] getyRange(){
        return yRange;
    }

    public void setyRange(double[] yRange){
        this.yRange = yRange;
    }

    public int getNumOfMeasures(){
        return numOfMeasures;
    }

    public int getNumOfChips(){
        return numOfChips;
    }


    public double[][] getyValues(){
        return yValues;
    }

    public void setxValue(int xValue){
        this.xValue = xValue;
    }

    public double getxValue(){
        return xValue;
    }

    public double[] getxRange(){
        return xRange;
    }

    public void setxRange(double xRange[]){
        this.xRange = xRange;
    }

    public String toString(){

        String out = "SPChartElement\n";

        out += "xValues: " + xValue + "; yValues: ";

        for(int i = 0;  i < yValues.length; i++){
            for(int j = 0; j < yValues[i].length; j++){
                out += (j == 0 ? "" : ", ") + yValues[i][j];
            }
            out += "\n";
        }

        out += "\nchips: " + numOfChips;
        out += "\nnumOfMeasures: " + numOfMeasures;
        out += "\ntemperature: " + temperature;
        out += "\nspDataRepresentation: " + spDataRepresentation;
        out += "\n";
        return out;
    }

}
