package com.sensichips.sensiplus.datareader.queue;


import com.sensichips.sensiplus.datareader.queue.SPChartElement;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;
import javafx.application.Platform;

public class SPChartQueue {
    private int maxDim = 100;
    private int start = 0;
    private int end = 0;
    private int riemp = 0;
    private SPChartElement data[];
    private int numOfChips;
    private int numOfMeasures;
    private double timeRange[];
    private double valueRange[][][] = null;
    private int totalElement = 0;
    private boolean blockingWay = false;
    private boolean chipMask[];


    public int getRiemp() {
        return riemp;
    }

    public SPChartQueue(int maxDim, boolean blockingWait, boolean chipMask[]){
        this.maxDim = maxDim;
        data = new SPChartElement[maxDim];
        start = 0;
        end = 0;
        this.blockingWay = blockingWait;
        this.chipMask = chipMask;
    }

    public void clear(){
        start = 0;
        end = 0;
        riemp = 0;
    }

    public int size(){
        return maxDim;
    }


    public synchronized double[] getTimeRange(){
        return timeRange;
    }

    public synchronized void put(SPChartElement r){

        if (valueRange == null) {
            numOfChips = r.getNumOfChips();
            numOfMeasures = r.getNumOfMeasures();
            valueRange = new double[numOfChips][][];
            for (int i = 0; i < numOfChips; i++) {
                valueRange[i] = new double[numOfMeasures][];
            }
        }

        for (int i = 0; i < numOfChips; i++) {
            for(int k = 0; k < numOfMeasures; k++){
                valueRange[i][k] = new double[]{Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY};
            }
        }

        if (riemp < maxDim){
            riemp++;
        } else {
            start = (start + 1) % maxDim;
        }
        data[end] = r;
        end = (end + 1) % maxDim;

        totalElement++;
        totalElement = totalElement > maxDim ? maxDim : totalElement;

        //System.out.println("totalElement: " + totalElement);

        int startIndex = end - totalElement;
        if (startIndex < 0){
            startIndex = maxDim + startIndex;
        }
        timeRange = new double[]{Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY};
        int index = startIndex;
        for(int m = 0; m < totalElement; m++) {
            if (data[index].getxValue() < timeRange[0]) {
                timeRange[0] = data[index].getxValue();
            }
            if (data[index].getxValue() > timeRange[1]) {
                timeRange[1] = data[index].getxValue();
            }
            index = (index + 1) % maxDim;
        }

        // Update min e max nella coda...
        for(int i = 0; i < numOfChips; i++){
            for(int k = 0; k < numOfMeasures; k++){
                index = startIndex;
                for(int m = 0; m < totalElement; m++){
                    if (data[index].getyValues()[i][k] < valueRange[i][k][0]){
                        valueRange[i][k][0] = data[index].getyValues()[i][k];
                    }
                    if (data[index].getyValues()[i][k] > valueRange[i][k][1]){
                        valueRange[i][k][1] = data[index].getyValues()[i][k];
                    }
                    index = (index + 1) % maxDim;
                }
            }
        }
        for(int i = 0; i < numOfChips; i++){
            for (int k = 0; k < numOfMeasures; k++) {
                SPLogger.getLogInterface().d("SPChartQueue", "" + valueRange[i][k][0] + ", " + valueRange[i][k][1], SPLoggerInterface.DEBUG_VERBOSITY_L2);
            }
        }
        //System.out.println("riemp: " + riemp + "; start: " + start + "-" + end);

        notify();
    }

    public synchronized double[] getValueRange(int measureIndex){
        double[] margins = new double[]{Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY};
        for(int i = 0; i < numOfChips; i++){
            if (chipMask[i]){
                if (valueRange[i][measureIndex][0] < margins[0]){
                    margins[0] = valueRange[i][measureIndex][0];
                }
                if (valueRange[i][measureIndex][1] > margins[1]){
                    margins[1] = valueRange[i][measureIndex][1];
                }
            }
        }
        return margins;
    }

    public synchronized SPChartElement get(int measureIndex){

        if (blockingWay && riemp == 0){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else  if (riemp == 0){
            try {
                int TIMEOUT = 5;
                wait(TIMEOUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (riemp == 0){
            return null;
        }

        SPChartElement spChartElement = data[start];
        riemp--;

        start = (start + 1) % maxDim;

        spChartElement.setxRange(getTimeRange());
        spChartElement.setyRange(getValueRange(measureIndex));

        return spChartElement;
    }

    public static final int MIN_INDEX = 0;
    public static final int MAX_INDEX = 1;
    public static final int MEAN_INDEX = 2;
    public static final int STD_INDEX = 3;
    double[][] output;

    public synchronized double[][] getStatistics(int indexToSave){
        int numOfStatistics = 4;
        double[][] output = new double[numOfChips][numOfStatistics];

        for(int i = 0; i < numOfChips; i++){
            output[i][MIN_INDEX] = Double.MAX_VALUE;
            output[i][MAX_INDEX] = Double.MIN_VALUE;
        }

        double somma = 0;
        for(int i = 0; i < numOfChips; i++){
            for(int j = 0; j < totalElement; j++){
                somma += data[j].getyValues()[i][indexToSave];
                if (output[i][MIN_INDEX] > data[j].getyValues()[i][indexToSave]){
                    output[i][MIN_INDEX] = data[j].getyValues()[i][indexToSave];
                }
                if (output[i][MAX_INDEX] < data[j].getyValues()[i][indexToSave]){
                    output[i][MAX_INDEX] = data[j].getyValues()[i][indexToSave];
                }
            }
// Media
            output[i][MEAN_INDEX] = somma/totalElement;

            somma = 0;
            for(int j = 0; j < totalElement; j++){
                somma += Math.pow(data[j].getyValues()[i][indexToSave] - output[i][MEAN_INDEX], 2);
            }
            output[i][STD_INDEX] = Math.sqrt(somma/totalElement);

        }
        return output;
    }

}
