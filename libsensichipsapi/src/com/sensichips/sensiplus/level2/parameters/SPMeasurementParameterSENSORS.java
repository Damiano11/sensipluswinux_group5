package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.chip.SPSensingElement;
import com.sensichips.sensiplus.level1.chip.SPSensingElementOnChip;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterSENSORS extends SPMeasurementParameterADC {

    private String sensorName;
    private String filter;
    private SPMeasurementParameterEIS paramInternalEIS = null;

    public SPMeasurementParameterSENSORS(SPConfiguration spConfiguration, String sensorName) throws SPException {
        super(spConfiguration);
        this.sensorName = sensorName;

        SPSensingElementOnChip spSensingElementOnChip = spConfiguration.searchSPSensingElementOnChip(this.getSensorName());
        SPSensingElement spSensingElement = spSensingElementOnChip.getSensingElementOnFamily().getSensingElement();


        if(spSensingElement.getMeasureTechnique().equals("EIS")) {
            this.paramInternalEIS = (SPMeasurementParameterEIS) spSensingElementOnChip.getSensingElementOnFamily().
                    getSPMeasurementParameter(spConfiguration);
            this.paramInternalEIS.setSPMeasurementMetaParameter(this.getSPMeasurementMetaParameter());
        }


    }


    public SPMeasurementParameterEIS getParamInternalEIS() {
        return paramInternalEIS;
    }

    public void setParamInternalEIS(SPMeasurementParameterEIS paramInternalEIS) {
        this.paramInternalEIS = paramInternalEIS;
    }


    public String getSensorName(){
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getPort(){
        return spPort.getPortValue();
    }

}
