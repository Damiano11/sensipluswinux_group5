package com.sensichips.sensiplus.level2.parameters;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementOutput {
    public String out = "";
    public boolean ADCOverflow = false;
    public boolean ADCUnderflow = false;

}
