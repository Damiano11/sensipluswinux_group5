package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;

public class SPMeasurementParameterActuator extends SPMeasurementParameterPort {

    private String Port = null;
    public static String ACTUATOR_PORT_INT = "INT";
    public static String ACTUATOR_PORT_RDY = "RDY";
    //private String PortLabel = null;


    public final static int ON = 1;
    public final static int HIGH = 2;
    public final static int MEDIUM = 3;
    public final static int LOW = 4;
    public final static int OFF = 5;


    private static int MAX_INTERNAL_INDEX = 12;

    public static final String[] portLabels = new String[]{"PORT0", "PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7", "PORT8", "PORT9", "PORT10", "PORT11", "PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3"};
    public static final String[] portValues = new String[]{"00000", "00001", "00010", "00011", "00100", "00101", "00110", "00111", "01000", "01001", "01010",  "01011",  "11100",   "11101",     "11110",     "11111"};

    private int Mode = -1;


    public SPMeasurementParameterActuator(SPConfiguration spConfiguration){
        super(spConfiguration);
    }

    @Override
    public void setPort(String port) throws SPException {
        try{
            super.setPort(port);
        } catch(SPException e){
            if(port == null || (!port.equals(ACTUATOR_PORT_INT) && !port.equals(ACTUATOR_PORT_RDY))){
                throw new SPException("Wrong port in SPMeasurementParameterActuator...");
            }
            this.Port = port;
        }
    }


    public String getPort(){
        return Port;
    }


    public boolean isValid(){
        boolean output = true;
        output = output && Port != null;
        output = output && Mode >= 0;
        return output;
    }

    public int getMode(){
        return Mode;
    }

    public void setMode(int Mode){
        this.Mode = Mode;
    }


}
