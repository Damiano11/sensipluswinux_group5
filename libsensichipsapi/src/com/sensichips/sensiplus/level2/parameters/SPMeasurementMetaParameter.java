package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

import java.util.ArrayList;

/**
 * Created by mario on 10/06/17.
 */
public class SPMeasurementMetaParameter {

    //protected SPConfiguration conf;
    protected Boolean fillBufferBeforeStart = false;
    protected Boolean renewBuffer = true;
    protected Integer burst = 1;
    protected Boolean restartBandGap = true;


    // Marco FERDINANDI
    // ADC autoscale compensation
    private double[][] previousMeasurements = null;
    private double[][] delta = null;
    private boolean deltaActive = false;
    private boolean adcOverflowReached = false;
    private boolean adcUnderflowReached = false;

    private boolean deltaComeBackActive = false;
    private double[][] deltaComeBack = null;

    public ArrayList<double[][]> getListLIFOdelta() {
        return listLIFOdelta;
    }

    ArrayList<double[][]> listLIFOdelta = new ArrayList<double[][]>();


    public SPMeasurementMetaParameter() {
        this.delta = null;
    }
    public void setNumOfChips(int numOfChips){
        this.delta = new double[numOfChips][SPMeasurementParameterEIS.NUM_OUTPUT];
    }


    public SPMeasurementMetaParameter(int numOfChip) {
        this.delta = new double[numOfChip][SPMeasurementParameterEIS.NUM_OUTPUT];
    }

    public void updateDelta(double measures[][]) throws SPException {
        if(delta != null) {
            if (adcOverflowReached) {
                double[][] appo = new double[measures.length][measures[0].length];

                for (int i = 0; i < measures.length; i++) {
                    for (int j = 0; j < measures[i].length; j++) {
                        appo[i][j] = previousMeasurements[i][j] - measures[i][j];
                    }
                }
                listLIFOdelta.add(appo);
                delta = getTotalDelta(1);

                //TODO:remove this print. Added to test the alghoritm
                //printDoubleMatrix(delta,"overflow");

                adcOverflowReached = false;
            } else if (adcUnderflowReached) {
                if (listLIFOdelta.size() > 0) {
                    listLIFOdelta.remove(listLIFOdelta.size() - 1);
                }

                delta = getTotalDelta(1);


                if (delta == null) {
                    delta = new double[measures.length][measures[0].length];
                }


                //TODO:remove this print. Added to test the alghoritm
                //printDoubleMatrix(delta,"underflow");


                adcUnderflowReached = false;
            }
            deltaActive = true;
        }
        else{
            throw new SPException(" set num of chips in SPMeasurementMetaParameter");
        }
    }


    public void printDoubleMatrix(double[][] matrix, String title){
        System.out.println(title);
        for(int i=0; i<matrix.length; i++){
            System.out.println("Chip "+i);
            for(int j=0; j<matrix[i].length; j++){
                System.out.print(matrix[i][j]+" ; ");
            }
        }



    }

    double[][] getTotalDelta(int sign){
        double[][] output = null;
        if(listLIFOdelta.size()>0){
            int rows = listLIFOdelta.get(0).length; //Number of chips
            int columns = listLIFOdelta.get(0)[0].length; //Number of measurements for each chip
            output = new double[rows][columns];

            for(int i=0; i<listLIFOdelta.size(); i++){
                double[][] current = listLIFOdelta.get(i);
                for(int r=0; r<rows;r++){
                    for(int c=0; c<columns; c++){
                        output[r][c] = output[r][c] + sign*current[r][c];
                    }
                }
            }
        }

        return output;
    }






    public boolean isAdcUnderflowReached() {
        return adcUnderflowReached;
    }

    public void setAdcUnderflowReached(boolean adcUnderflowReached) {
        this.adcUnderflowReached = adcUnderflowReached;
    }


    public double[][] getPreviousMeasurements() {
        return previousMeasurements;
    }

    public void setPreviousMeasurements(double[][] previousMeasurements) {
        if(previousMeasurements.length>0) {
            this.previousMeasurements = new double[previousMeasurements.length][previousMeasurements[0].length];

            for(int i=0; i<previousMeasurements.length; i++){
                for(int j=0; j<previousMeasurements[i].length; j++){
                    this.previousMeasurements[i][j] = previousMeasurements[i][j];
                }
            }


        }
    }

    public double[][] getDelta() {
        return delta;
    }

    public void setDelta(double[][] delta) {
        this.delta = delta;
        this.deltaActive = true;
    }

    public boolean isDeltaActive() {
        return deltaActive;
    }

    public void setDeltaActive(boolean deltaActive) {
        this.deltaActive = deltaActive;
    }

    public boolean isAdcOverflowReached() {
        return adcOverflowReached;
    }

    public void setAdcOverflowReached(boolean adcOverflowReached) {
        this.adcOverflowReached = adcOverflowReached;
    }


    public Boolean getRestartBandGap(){
        return restartBandGap;
    }

    public void setRestartBandGap(Boolean restartBandGap){
        this.restartBandGap = restartBandGap;
    }

    public Boolean getRenewBuffer(){
        return renewBuffer;
    }

    public void setRenewBuffer(Boolean renewBuffer){
        this.renewBuffer = renewBuffer;
    }

    public Boolean getFillBufferBeforeStart() {
        return fillBufferBeforeStart;
    }

    public void setFillBufferBeforeStart(Boolean fillBufferBeforeStart) {
        this.fillBufferBeforeStart = fillBufferBeforeStart;
    }

    public Integer getBurst() {
        return burst;
    }

    public void setBurst(Integer burst) {
        this.burst = burst;
    }
}
