package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.chip.SPCluster;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameter implements Serializable {

    public static final long serialVersionUID = 42L;

    protected SPConfiguration spConfiguration;
    protected SPCluster temporaryCluster;

    //public int upperADCThreshold = (int)(0.70*Math.pow(2,15));
    //public int lowerADCThreshold;

    //public static int final DIAGNOSYS_LEVEL
    private SPMeasurementMetaParameter metaParameter;


    public String[] getRegistersList() {
        return registersList;
    }

    public void setRegistersList(String[] registersList) {
        this.registersList = registersList;
    }

    private String[] registersList = new String[0];




    public SPMeasurementParameter(SPConfiguration spConfiguration){
        this.spConfiguration = spConfiguration;
        this.metaParameter = new SPMeasurementMetaParameter(this.spConfiguration.getCluster().getActiveSPChipList().size());
    }

    public SPMeasurementParameter(){
        this.spConfiguration = null;
        this.metaParameter = new SPMeasurementMetaParameter();
    }

    public SPMeasurementMetaParameter getSPMeasurementMetaParameter(){
        return metaParameter;
    }

    public void setSPMeasurementMetaParameter(SPMeasurementMetaParameter metaParameter){
        this.metaParameter = metaParameter;
    }

    public static Object searchValue(String keys[], Object values[], String label, String message) throws SPException {

        if (label == null || keys == null || values == null) {
            throw new SPException((message == null ? "" : message) + ": Null values are refused!");
        }
        if (keys.length != values.length) {
            throw new SPException((message == null ? "" : message) + ": labels.length should be equal to values.length!");
        }


        int count = 0;
        while (count < keys.length) {
            if (label.equalsIgnoreCase(keys[count])) {
                return values[count];
            }
            count++;
        }
        throw new SPException(message + " " + label + " is not in: " + Arrays.toString(keys));
    }


    public SPCluster getTemporaryCluster() {
        return temporaryCluster;
    }

    public void setTemporaryCluster(SPCluster temporaryCluster) {
        this.temporaryCluster = temporaryCluster;
    }

    protected static boolean inRange(int min, int max, Integer value) {
        return value >= min && value <= max;
    }

    protected static boolean inRange(double min, double max, double value) {
        return value >= min && value <= max;
    }

    public void reduceADCRange() throws SPException  {
        throw new SPException("The reduceADCRange method can't be called on the SPMeasurementParameter class ");
    }

    public void increaseADCRange() throws SPException  {
        throw new SPException("The increaseADCRange method can't be called on the SPMeasurementParameter class ");
    }


}

