package com.sensichips.sensiplus.level2.parameters;


import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterPOT extends SPMeasurementParameterPort {


    //private SPConfiguration conf;

    private Integer initialPotential;
    private Integer finalPotential;
    private Integer step;
    private Integer pulsePeriod;
    private Integer pulseAmplitude;
    private Integer typeVoltammetry;
    private Boolean alternativeSignal;
    private String rsense;
    private String InGain;

    private String Port = null;
    protected String PortLabel = null;
    private String Contacts = null;
    protected String ContactsLabel = null;

    public final static int POTENTIOMETRIC = 0;
    public final static int CURRENT = 1;
    public final static int LINEAR_SWEEP_VOLTAMMETRY = 2;
    public final static int STAIRCASE_VOLTAMMETRY = 3;
    public final static int SQUAREWAVE_VOLTAMMETRY = 4;
    public final static int NORMAL_PULSE_VOLTAMMETRY = 5;
    public final static int DIFFERENTIAL_PULSE_VOLTAMMETRY = 6;

//    public final static int PORT1 = 7;
//    public final static int CURRENT = 1;
//    public final static int LINEAR_SWEEP_VOLTAMMETRY = 2;
//    public final static int STAIRCASE_VOLTAMMETRY = 3;
//    public final static int SQUAREWAVE_VOLTAMMETRY = 4;
//    public final static int NORMAL_PULSE_VOLTAMMETRY = 5;
//    public final static int DIFFERENTIAL_PULSE_VOLTAMMETRY = 6;

    public static final String[] PortLabels = new String[]{"PORT1", "PORT2", "PORT3", "PORT4", "PORT5", "PORT6", "PORT7",
            "PORT8", "PORT9", "PORT10", "PORT11", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3", "PORT_HP"};
    public static final String[] PortValues = new String[] {"0000", "0001", "0010", "0100", "1000", "0011", "0101", "1001",
            "0110", "1100", "1010", "1111", "0111", "1011", "1101"};
//    public static final Integer[] PortValues = new Integer[] {PORT1};

    public static final String[] ContactsLabels = new String[]{"TWO", "THREE"};
    public static final String[] ContactsValues = new String[]{"1", "0"};

    public static final String[] rsenseLabels = new String[]{"50000", "5000", "500", "50"};
    public static final String[] rsenseValues = new String[]{"00", "01", "10", "11"};

    public static final String[] ingainLabels = new String[]{"1", "12", "20", "40"};
    public static final String[] ingainValues = new String[]{"11", "10", "01", "00"};

    public static final String[] typeLabels = new String[]{"LINEAR_SWEEP", "STAIRCASE",
            "SQUAREWAVE", "NORMAL_PULSE", "DIFFERENTIAL_PULSE", "POTENTIOMETRIC", "CURRENT"};
    public static final Integer[] typeValues = new Integer[]{LINEAR_SWEEP_VOLTAMMETRY, STAIRCASE_VOLTAMMETRY, SQUAREWAVE_VOLTAMMETRY,
            NORMAL_PULSE_VOLTAMMETRY, DIFFERENTIAL_PULSE_VOLTAMMETRY, POTENTIOMETRIC, CURRENT};


    // PAY ATTENTION
    //if gcVCC>=1.8V then VREF- = -650,  VREF+ = +650
    //if gcVCC>=2.5V then VREF- = -1000, VREF+ = +1000
    //if gcVCC>=3.3V then VREF- = -1400, VREF+ = +1400



    public static Integer Vref_minus;
    public static Integer Vref_plus;


    public static Integer initialPotentialMin = Vref_minus;
    public static Integer initialPotentialMax = Vref_plus;

    public static Integer finalPotentialMin = Vref_minus;
    public static Integer finalPotentialMax = Vref_plus;

    public static Integer stepMin = Vref_minus;
    public static Integer stepMax = Vref_plus;

    public static Integer pulsePeriodMin = Vref_minus;
    public static Integer pulsePeriodMax = Vref_plus;

    public static Integer pulseAmplitudeMin = Vref_minus;
    public static Integer pulseAmplitudeMax = Vref_plus;


    public SPMeasurementParameterPOT(SPConfiguration spConfiguration){
        super(spConfiguration);

        if (spConfiguration.getVCC() >= SPConfiguration.VCC_Th1
                && spConfiguration.getVCC() < SPConfiguration.VCC_Th2) {
            Vref_plus = (SPConfiguration.VCC_Th1 - 500)/2;
        } else if (spConfiguration.getVCC() >= SPConfiguration.VCC_Th2 && spConfiguration.getVCC() < SPConfiguration.VCC_Th3) {
            Vref_plus = (SPConfiguration.VCC_Th2 - 500)/2;
        } else if (spConfiguration.getVCC() >= SPConfiguration.VCC_Th3) {
            Vref_plus = (SPConfiguration.VCC_Th3 - 500)/2;
        }
        Vref_minus = -Vref_plus;

        initialPotentialMin = Vref_minus;
        initialPotentialMax = Vref_plus;

        finalPotentialMin = Vref_minus;
        finalPotentialMax = Vref_plus;

        stepMin = Vref_minus;
        stepMax = Vref_plus;

        pulsePeriodMin = Vref_minus;
        pulsePeriodMax = Vref_plus;

        pulseAmplitudeMin = Vref_minus;
        pulseAmplitudeMax = Vref_plus;
        
    }


    public SPMeasurementParameterPOT(SPConfiguration spConfiguration, String InOutPort, String TypeVoltammetry, Integer InitialPotential, Integer FinalPotential,
                                     Integer Step, Integer PulsePeriod, Integer PulseAmplitude, Boolean AlternativeSignal,
                                     String RSense, String InGain, String contacts) throws SPException {
        this(spConfiguration);

        setType(TypeVoltammetry);
        setFinalPotential(FinalPotential);
        setInitialPotential(InitialPotential);
        setStep(Step);
        setPulsePeriod(PulsePeriod);
        setPulseAmplitude(PulseAmplitude);
        setRsense(RSense);
        setInGain(InGain);
        setAlternativeSignal(AlternativeSignal);
        setPort(InOutPort);
        setContacts(contacts);
    }

    public SPMeasurementParameterPOT() throws SPException{

        if(!(ContactsLabels.length==ContactsValues.length)){
            throw new SPException("WARNING - array lent mismatch between measure labels, measure values and unit measures");
        }
//        if(!(PortLabels.length==PortValues.length)){
//            throw new SPException("WARNING - array lent mismatch between measure labels, measure values and unit measures");
//        }

    }

    public boolean isValid() {
        boolean valid = !(getInitialPotential() == null
                || getFinalPotential() == null
                || getStep() == null
                || getPulsePeriod() == null
                || getPulseAmplitude() == null
                || getTypeVoltammetry() == null && getAlternativeSignal() == null
                && getRsense() == null && getInGain() == null);

        return valid;

    }

    public boolean equals(SPMeasurementParameterPOT param) {
        return param.getTypeVoltammetry().equals(typeVoltammetry)
                && param.getFinalPotential().equals(finalPotential)
                && param.getInitialPotential().equals(initialPotential) && param.getStep().equals(step)
                && param.getPulsePeriod().equals(pulsePeriod) && param.getPulseAmplitude().equals(pulseAmplitude)
                && param.getPort().equals(getPort()) && param.getAlternativeSignal() == getAlternativeSignal()
                && param.getRsense().equals(getRsense()) && param.getInGain().equals(getInGain());
    }


    public String toString() {
        return "POT parameters:\n"
                + "\nPort: " + getPort()
                + "\nType: " + getTypeVoltammetry()
//                + "\nPort: " + getPort()
                + "\nInitialPotential: " + getInitialPotential()
                + "\nFinalPotential: " + getFinalPotential()
                + "\nStep: " + getStep()
                + "\nPulseAmplitude: " + getPulseAmplitude()
                + "\nalternativeSignal: " + getAlternativeSignal()
                + "\nRsense: " + getRsense()
                + "\nInGain: " + getInGain();
    }

    public String getPort() {
        return Port;
    }

    public void setPort(String InOutPort) throws SPException {
        Port = (String) searchValue(PortLabels, PortValues, InOutPort, "Port");
    }

    public void setContacts(String contacts) throws SPException {
        this.ContactsLabel = contacts;
        Contacts = (String) searchValue(ContactsLabels, ContactsValues, contacts, "Contacts");
    }

    public String getContacts() {
        return Contacts;
    }


    public void setInGain(String ingain) throws SPException {
        InGain = (String) searchValue(ingainLabels, ingainValues, ingain, "InGain");
    }

    public String getInGain() {
        return InGain;
    }

    public Boolean getAlternativeSignal() {
        return alternativeSignal;
    }


    public void setAlternativeSignal(Boolean alternative) {
        this.alternativeSignal = alternative;
    }


    public Integer getInitialPotential() {
        return initialPotential;
    }


    public void setInitialPotential(Integer initialPotential) throws SPException {
//        if (initialPotential != null && inRange((int)initialPotentialMin, (int)initialPotentialMax, initialPotential))
            this.initialPotential = initialPotential;
//        else
//            throw new SPException("initialPotential: " + initialPotential + " out of range: [" + initialPotentialMin + ", " + initialPotentialMax + "]");
    }


    public Integer getFinalPotential() {
        return finalPotential;
    }


    public void setFinalPotential(Integer finalPotential) throws SPException {
//        if (finalPotential != null && inRange((int)finalPotentialMin, (int)finalPotentialMax, finalPotential))
            this.finalPotential = finalPotential;
//        else
//            throw new SPException("finalPotential: " + finalPotential + " out of range: [" + finalPotentialMin + ", " + finalPotentialMax + "]");
    }


    public Integer getStep() {
        return step;
    }


    public void setStep(Integer step) throws SPException {
//        if (step != null && inRange((int)stepMin, (int)stepMax, step))
            this.step = step;
//        else
//            throw new SPException("step " + step + " out of range: [" + stepMin + ", " + stepMax + "]");
    }


    public Integer getPulsePeriod() {
        return pulsePeriod;
    }


    public void setPulsePeriod(Integer pulsePeriod) throws SPException {
//        if (pulsePeriod != null && inRange((int)pulsePeriodMin, (int)pulsePeriodMax, pulsePeriod))
            this.pulsePeriod = pulsePeriod;
//        else
//            throw new SPException("pulsePeriod: " + pulsePeriod + " out of range: [" + pulsePeriodMin + ", " + pulsePeriodMax + "]");
    }


    public Integer getPulseAmplitude() {
        return pulseAmplitude;
    }


    public void setPulseAmplitude(Integer pulseAmplitude) throws SPException {
//        if (pulseAmplitude != null && inRange((int)pulseAmplitudeMin, (int)pulseAmplitudeMax, pulseAmplitude))
            this.pulseAmplitude = pulseAmplitude;
//        else
//            throw new SPException("pulseAmplitude: " + pulseAmplitude + " out of range: [" + pulseAmplitudeMin + ", " + pulseAmplitudeMax + "]");
    }


    public Integer getTypeVoltammetry() {
        return typeVoltammetry;
    }


    public void setType(String type) throws SPException {
        this.typeVoltammetry = (Integer) searchValue(typeLabels, typeValues, type, "type");
    }


    public String getRsense() {
        return rsense;
    }

    public void setRsense(String rsense) throws SPException {
        this.rsense = (String) searchValue(rsenseLabels, rsenseValues, rsense, "Rsense");
    }


}
