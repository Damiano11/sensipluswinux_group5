package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterSENSORS_OLD extends SPMeasurementParameterADC {

    private String Port = null;
    private String inGain = null;
    private String rsense = null;
    private String sensorType;

    //														"0x1C",  "0x1D",  "0x1E",  "0x1F"};
    public static final String[] portLabels = new String[]{"PORT_HP", "PORT_EXT1", "PORT_EXT2", "PORT_EXT3"};
    public static final String[] portValues = new String[]{"11100", "11101", "11110", "11111"};

    // Generale
    private String Filter;
    protected String FilterLabel;
    public static final String[] filterLabels = new String[]{"1", "4", "8", "16", "32", "64", "128", "256"};
    public static final String[] filterValues = new String[]{"1", "4", "8", "16", "32", "64", "128", "256"};

    public static final int POTENTIOMETRIC = 0,
            CURRENT = 1,
            ONCHIP_TEMPERATURE = 2,
            ONCHIP_VOLTAGE = 3,
            OFFCHIP_ESLI = 4,
            OFFCHIP_BEND = 5,
            OFFCHIP_VOC = 6,
            OFFCHIP_CARBONDIOXIDE = 7,
            OFFCHIP_FIREDETECTOR = 8;
    // Parameter defined by set of labels and values
    public static final String[] sensorTypeLabels = new String[]{"POTENTIOMETRIC", "CURRENT", "ONCHIP_TEMPERATURE", "ONCHIP_VOLTAGE", "OFFCHIP_ESLI", "OFFCHIP_BEND",
            "OFFCHIP_VOC", "OFFCHIP_CARBONDIOXIDE", "OFFCHIP_FIREDETECTOR"};
    //public static final  String[] sensorTypeValues = new String[]{"1100", "1101", "", "", "", ""}; // 0xC e 0XD


    public SPMeasurementParameterSENSORS_OLD(SPConfiguration spConfiguration) throws SPException {
        super(spConfiguration);
    }

    public void setInGain(String inGain) throws SPException {
        this.inGain = (String) searchValue(SPMeasurementParameterQI.ingainLabels, SPMeasurementParameterQI.ingainValues, inGain, "InGain");
    }

    public String getInGain() {
        return inGain;
    }

    public void setRsense(String rsense) throws SPException {
        this.rsense = (String) searchValue(SPMeasurementParameterQI.rsenseLabels, SPMeasurementParameterQI.rsenseValues, rsense, "Rsense");
    }

    public String getRsense() {
        return rsense;
    }


    public void setPort(String port) throws SPException {
        Port = (String) searchValue(portLabels, portValues, port, "Port");
    }

    public String getPort() {
        return Port;
    }


    public boolean equals(SPMeasurementParameterSENSORS_OLD param) {
        return param.getSensorType().equalsIgnoreCase(sensorType) && param.getNData() == getNData() && param.getPort().equalsIgnoreCase(this.getPort());
    }

    public String toString() {
        return "SensorType: " + getSensorType() + ", NData: " + getNData() + ", Port: " + getPort();
    }

    public boolean isValid() {
        boolean notValid = getNData() == null || getSensorType() == null;
        notValid = notValid || (getPort() == null &&
                (getSensorType().equals(SPMeasurementParameterSENSORS_OLD.sensorTypeLabels[SPMeasurementParameterSENSORS_OLD.OFFCHIP_ESLI]) ||
                        getSensorType().equals(SPMeasurementParameterSENSORS_OLD.sensorTypeLabels[SPMeasurementParameterSENSORS_OLD.OFFCHIP_BEND]) ||
                        getSensorType().equals(SPMeasurementParameterSENSORS_OLD.sensorTypeLabels[SPMeasurementParameterSENSORS_OLD.OFFCHIP_VOC]) ||
                        getSensorType().equals(SPMeasurementParameterSENSORS_OLD.sensorTypeLabels[SPMeasurementParameterSENSORS_OLD.OFFCHIP_CARBONDIOXIDE])||
                        getSensorType().equals(SPMeasurementParameterSENSORS_OLD.sensorTypeLabels[SPMeasurementParameterSENSORS_OLD.OFFCHIP_FIREDETECTOR])));

        notValid = notValid || (getPort() == null || getInGain() == null || getRsense() == null) &&
                getSensorType().equals(SPMeasurementParameterSENSORS_OLD.sensorTypeLabels[SPMeasurementParameterSENSORS_OLD.CURRENT]);


        notValid = notValid || (getPort() == null || getInGain() == null) &&
                getSensorType().equals(SPMeasurementParameterSENSORS_OLD.sensorTypeLabels[SPMeasurementParameterSENSORS_OLD.POTENTIOMETRIC]);

        return !notValid;

    }


    public void setSensorType(String st) throws SPException {
        int i = 0;
        while (i < sensorTypeLabels.length && !sensorTypeLabels[i].equalsIgnoreCase(st)) {
            i++;
        }
        if (i >= sensorTypeLabels.length) {
            throw new SPException("Sensor Type value not found: " + st);
        }
        sensorType = sensorTypeLabels[i];
        //this.sensorType = (String) searchValue(sensorTypeLabels, sensorTypeValues, st, "SensorType");
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setFilter(String filter) throws SPException {
        this.FilterLabel = filter;
        this.Filter = (String) searchValue(filterLabels, filterValues, filter, "Filter");
    }


    public String getFilter() {
        return Filter;
    }

    public String getFilterLabel(){
        return FilterLabel;
    }


}
