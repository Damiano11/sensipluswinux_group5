package com.sensichips.sensiplus.level2.parameters;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPMeasurementParameterPPR extends SPMeasurementParameterADC {

    public static final String MODE_COUNTER = "0";
    public static final String MODE_DOSIMETRY = "1";

    private String Electrodes = null;
    private String ElectrodesLabel = null;

    private String DetectorType = null;
    private String DetectorTypeLabel = null;

    private String InPort = null;
    private String InPortLabel = null;

    private Integer Timer = null;
    private Integer EnergyThreshold = null;
    private Integer HitsThreshold = null;

    private String Mode = null;
    private String ModeLabel = null;

    public static final String[] electrodesLabels= new String[]{"3_ELECTRODES", "2_ELECTRODES"};
    public static final String[] electrodesValues = new String[]{"1", "0"};

    public static final String[] detectorTypeLabels = new String[]{"SCINTILLATOR", "SOLIDSTATE"};
    public static final String[] detectorTypeValues = new String[]{"1", "0"};

    public static final String[] portLabels = new String[]{"PORT0", "PORT1", "PORT_EXT1", "PORT_EXT2"};
    public static final String[] portValues = new String[]{"00", "01", "10", "11"};

    public static final Integer timerMin = 0;
    public static final Integer timerMax = 255;

    public static final Integer energyThresholdMin = 0;
    public static final Integer energyThresholdMax = 255;


    public static final Integer hitscountThresholdMin = 0;
    public static final Integer hitscountThresholdMax = 65535;

    public static final String[] modeLabels = new String[]{"Counter", "Dosimetry"};
    public static final String[] modeValues = new String[]{MODE_COUNTER, MODE_DOSIMETRY};

    public SPMeasurementParameterPPR(SPConfiguration conf){
        super(conf);
    }

    public SPMeasurementParameterPPR(SPConfiguration conf, String Electrodes, String DetectorType, String InPort,
                                     Integer Timer, Integer EnergyThreshold, Integer HitsThreshold, String Mode, String DAO) throws SPException {
        super(conf);
        setElectrodes(Electrodes);
        setDetectorType(DetectorType);
        setInPort(InPort);
        setTimer(Timer);
        setEnergyThreshold(EnergyThreshold);
        setHitsThreshold(HitsThreshold);
        setMode(Mode);
    }
    public boolean isValid() {
        boolean valid = !(getElectrodes() == null
                || getDetectorType() == null
                || getInPort() == null
                || getTimer() == null
                || getEnergyThreshold() == null
                || getHitsThreshold() == null
                || getMode() == null);

        return valid;

    }

    public String toString() {
        return "PPR parameters: "
                + "\nElectrodesLabel: " + ElectrodesLabel + ", DetectorType: " + getElectrodes()
                + "\nDetectorTypeLabel: " + DetectorTypeLabel + ", DetectorType: " + getDetectorType()
                + "\nInPortLabel: " + InPortLabel + ", InPort: " + getInPort()
                + "\nTimer: " + Timer
                + "\nEnergyThreshold: " + EnergyThreshold
                + "\nHitsThreshold: " + HitsThreshold
                + "\nModeLabel: " + ModeLabel + ", Mode: " + getMode();
    }

    public void setElectrodes(String Electrodes)  throws SPException {
        this.ElectrodesLabel = Electrodes;
        this.Electrodes = (String) searchValue(electrodesLabels, electrodesValues, Electrodes, "Electrodes");
    }
    public String getElectrodes(){
        return Electrodes;
    }

    public void setDetectorType(String DetectorType) throws SPException {
        this.DetectorTypeLabel = DetectorType;
        this.DetectorType = (String) searchValue(detectorTypeLabels, detectorTypeValues, DetectorType, "DetectorType");
    }
    public String getDetectorType(){
        return DetectorType;
    }

    public void setTimer(Integer Timer) throws SPException {
        if (inRange((int)timerMin, (int)timerMax, Timer)){
            this.Timer = Timer;
        } else
            throw new SPException("Timer " + Timer + " out of range: [" + timerMin + ", " + timerMax + "]");
    }

    public Integer getTimer(){
        return Timer;
    }



    public void setEnergyThreshold(Integer EnergyThreshold) throws SPException {

        if (inRange((int)energyThresholdMin, (int)energyThresholdMax, EnergyThreshold))
            this.EnergyThreshold = EnergyThreshold;
        else
            throw new SPException("EnergyThreshold " + EnergyThreshold + " out of range: [" + energyThresholdMin + ", " + energyThresholdMax + "]");
    }

    public Integer getEnergyThreshold(){
        return EnergyThreshold;
    }

    public void setHitsThreshold(Integer HitsThreshold) throws SPException {
        if (inRange((int)hitscountThresholdMin, (int)hitscountThresholdMax, HitsThreshold))
            this.HitsThreshold = HitsThreshold;
        else
            throw new SPException("HitsThreshold " + HitsThreshold + " out of range: [" + hitscountThresholdMin + ", " + hitscountThresholdMax + "]");

    }

    public Integer getHitsThreshold(){
        return HitsThreshold;
    }


    public void setMode(String mode) throws SPException {
        this.Mode = (String) searchValue(modeLabels, modeValues, mode, "Mode");
    }

    public String getMode() {
        return Mode;
    }


    public void setInPort(String port) throws SPException {
        InPort = (String) searchValue(portLabels, portValues, port, "InPort");
    }

    public String getInPort() {
        return InPort;
    }


}
