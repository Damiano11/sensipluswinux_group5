package com.sensichips.sensiplus.level2;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.chip.SPSensingElement;
import com.sensichips.sensiplus.level1.chip.SPSensingElementOnChip;
import com.sensichips.sensiplus.level2.parameters.*;
import com.sensichips.sensiplus.level2.status.SPMeasurementStatusIQ;
import com.sensichips.sensiplus.level2.status.SPMeasurementStatusPOT;
import com.sensichips.sensiplus.level2.status.SPMeasurementStatusSENSOR;
import com.sensichips.sensiplus.level2.util.SPMeasurementUtil;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.log.SPLogger;

import java.util.ArrayList;
import java.util.List;


/**
 * The High Level part of the library presents to the programmers all the methods available for different types
 * of analytical measurements.  Alle the classes of this level are contained in com.sensichips.android.level2.
 * In particular SPMeasurement is the abstract class with all the expected methods while SPMeasurementRUN4
 * is the specialized class for the Run4 of the chip (2016). In addition the com.sensichips.android.level2.parameter
 * contains a set of classes created to manage the input parameters of the measurement methods.
 * In this module (in particulare in the SPMeasurementRUN4 class) it   is stored the complete state of the current measure.
 * In order to define the current measure type a call to the corresponding setMeasureType method is needed. For each
 * type of measurement there are two methods: setMeasureType and getMeasureType.
 * The setMeasureType sets the initial chip state and internal method parameters that will be used by
 * subsequent getMeasureType calls. The initial stte then evolves at each getMeasureType according to the
 * type of action that it's being performed. For each setMeasureType its possible to have 0 or more
 * getMeasureType subsequently calls. Each getMeasureType request to the chip one or more
 * measurements, it returns to the caller the corresponding numeric values and update the internal state of the
 * method.
 * Therefore the setMeasureType:
 * - parses the input parameters in order to identify errors and make some computation on it (for example in
 * order to identify the frequency available on the chip with respect to the frequency requested by the caller)
 * - send a sequence of WRITE instruction the the chip by mean of the L1 library;
 * - define the initial state of the L2 module.
 * <p>
 * The getMeasureType will then be called a number of times according to the performed measurement:
 * - each call will interact with the chip with WRITE and READ accesses, to read required data
 * - it will update the API module state and prepare for subsequent reads
 * <p>
 * For example:
 * setEIS/getEIS (Electrical Impedance Spectroscopy)
 * - setEIS writes into the chip registers that are involved in this type of measure by preparaing the stimulus
 * <p>
 * and the measurement section;
 * - consequently the getEIS call implements two measurements: in quadrature and in phase. By means of
 * this two value the methods computes Resistance, Capacitance and Inductance.
 * <p>
 * setPOT/getPOT (Potentiostat)
 * - setPOT sets the chip for the subsequent reads and initializes parameters within the API, i.e. the type of
 * <p>
 * ramp to be generated among the choices presented to the used,
 * - back-to-back getPOT calls will set the chip to the output stimulus according to the selected ramp and read
 * the sensor according to the user specified timing.
 * <p>
 * Methods of Level 2 throws SPException in case of errors with SPMeasurement API or
 * SPException in case of errors in the input parameters.
 * <p>
 * <p>
 * Property: Sensichips s.r.l.
 *
 * @author: Mario Molinara, mario.molinara@sensichips.com, m.molinara@unicas.it
 */
public class SPMeasurementRUN5_PC extends SPMeasurementRUN4 {

    public SPMeasurementRUN5_PC(SPConfiguration conf, String logDirectory, String measure_type) throws SPException {
        super(conf, logDirectory, measure_type);
    }


    @Override
    public synchronized void setSENSOR(SPMeasurementParameterSENSORS param, SPMeasurementOutput m)
            throws SPException {

        SPSensingElementOnChip spSensingElementOnChip = spConfiguration.searchSPSensingElementOnChip(param.getSensorName());
        SPSensingElement spSensingElement = spSensingElementOnChip.getSensingElementOnFamily().getSensingElement();

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        Double conversionRate50 = (double)50;
        String InPortADC = SPMeasurementParameterADC.INPORT_IA;
        Integer NDATA = 1;

        generateCalibrationParameters_new(param.getSensorName());

        if(param.getParamInternalEIS()!=null){

            SPMeasurementOutput m2 = new SPMeasurementOutput();
            //SPMeasurementParameterEIS paramInternal = (SPMeasurementParameterEIS)spSensingElementOnChip.getSensingElementOnFamily().getSPMeasurementParameter(spConfiguration);
            //paramInternal.setSPMeasurementMetaParameter(param.getSPMeasurementMetaParameter());


            SPMeasurementParameterEIS paramInternal = param.getParamInternalEIS();

            // Effettua la ricerca inversa: posseggo il valore, ritrovo la label
            setEIS(paramInternal, m2);
            ref.log += m2.out;
            m2 = new SPMeasurementOutput();
            setADC(NDATA, conversionRate50, InPortADC, true, m2);
            ref.log += m2.out;

        } else if (spSensingElement.getMeasureTechnique().equals("DIRECT")) {

            /*ref.instructionToSend = "WRITE DAS_CONFIG M 0x07FF";
            sendSequence(ref);*/

            if (spSensingElement.getName().equals("ONCHIP_TEMPERATURE") || spSensingElement.getName().equals("ONCHIP_VOLTAGE")) {

                String instruction = "WRITE CHA_SELECT S 0X";
                String instructionNum = "110" + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                ref.instructionToSend = instruction;
                sendSequence(ref);

                ref.instructionToSend = "WRITE DSS_SELECT M 0x0020"; //  -- disconnect DAS from IA
                sendSequence(ref);


                if (spSensingElement.getName().equals("ONCHIP_VOLTAGE")){
                    ref.instructionToSend = "WRITE CHA_CONDIT M 0x031F";
                } else {
                    ref.instructionToSend = "WRITE CHA_CONDIT M 0x011F"; // InGain = 40 => 001F; InGain = 20 => 011F, InGain = 12 => 021F; InGain = 1 => 031F
                }
                sendSequence(ref);

                ref.instructionToSend = "WRITE CHA_FILTER M 0x9200";   //ref.instructionToSend = "WRITE CHA_FILTER+1 S 0xFE"; da verificare con Simone: F6 o FE
                sendSequence(ref);

                ref.instructionToSend = "WRITE ANA_CONFIG S 0x1B";
                sendSequence(ref);


                SPMeasurementOutput m2 = new SPMeasurementOutput();
                setADC(NDATA, conversionRate50, InPortADC, true, m2);
                ref.log += m2.out;

                if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null) {
                    status = new SPMeasurementStatusSENSOR(this, param, spProtocol, "");
                }

            } else if (spSensingElement.getName().equals("ONCHIP_LIGHT") || (spSensingElement.getName().equals("ONCHIP_DARK"))){

                ref.instructionToSend = "WRITE DSS_DIVIDER M 0x0002"; //  -- disconnect DAS from IA
                sendSequence(ref);

                String instruction = "WRITE CHA_SELECT S 0X";
                String instructionNum = "010" + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                ref.instructionToSend = instruction;
                sendSequence(ref);

                instruction = "WRITE DSS_SELECT M 0X"; //  -- disconnect DAS from IA
                instructionNum = "00000000001" + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                ref.instructionToSend = instruction;
                sendSequence(ref);

                ref.instructionToSend = "WRITE CHA_FILTER M 0x9200";
                sendSequence(ref);

                ref.instructionToSend = "WRITE CHA_CONDIT M 0x001F"; // 40 Ingain --
                sendSequence(ref);

                ref.instructionToSend = "WRITE CHA_FILTER+1 S 0xB2";
                sendSequence(ref);

                ref.instructionToSend = "WRITE ANA_CONFIG S 0x3F";
                sendSequence(ref);

                SPMeasurementOutput m2 = new SPMeasurementOutput();
                setADC(NDATA, conversionRate50, InPortADC, true, m2);
                ref.log += m2.out;

                if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null) {
                    status = new SPMeasurementStatusSENSOR(this, param, spProtocol, "");
                }
            } else {
                throw new SPException("In setSENSOR. Sensing element unavailable: " + spSensingElement.getMeasureTechnique());
            }
        } else {
            throw new SPException("Measure technique unavailable: " + spSensingElement.getMeasureTechnique());
        }
    }


    public void restartBandGap(SPMeasurementParameterSENSORS param, boolean disableChipsBandgapProblems) throws SPException {

        SPMeasurementOutput out = new SPMeasurementOutput();

        double upperLimitVoltage = 3900;
        double lowerLimitVoltage = 1500;

        setSENSOR(param, out);

        SendInstructionInOut ref;
        SPCluster temporaryCluster = null;
        boolean onchip_voltage_ok = false;
        int maxTryal = 10;
        int counter = 0;
        int i = 0;
        String row = "";
        while(!onchip_voltage_ok && counter < maxTryal){
            // Restart bandgap
            ref = new SendInstructionInOut(temporaryCluster);
            ref.instructionToSend = spConfiguration.getSPBandGapRATIOMETRIC();
            sendSequence(ref);
            ref = new SendInstructionInOut(temporaryCluster);
            ref.instructionToSend = spConfiguration.getSPBandGapFromVCC();
            sendSequence(ref);

            //Verify if chips are started
            double[][] output = getSENSOR(param, out);
            i = 0;
            onchip_voltage_ok = true;
            while(i < numOfChips){
                onchip_voltage_ok = onchip_voltage_ok && output[i][0] < upperLimitVoltage && output[i][0] > lowerLimitVoltage;
                i++;
            }
            counter++;

            row = "";
            for(int k = 0; k < numOfChips; k++){
                //row += "" + output[k][0] + ((k < (numOfChips - 1)) ? ", ":"");
                cluster.getActiveSPChipList().get(k).setLastVBandGap((float)output[k][0]);
                row += "" + k + ") " + output[k][0] + " (" + cluster.getSPActiveSerialNumbers().get(k) + ")\n";
            }
            System.out.println("\nRestartBandGap repetition: " + counter + " Vcc:\n" + row);
        }

        if (!onchip_voltage_ok){
            if(disableChipsBandgapProblems) {
                List<SPChip> chipsToDisable = new ArrayList<>();
                List<SPChip> activeChips = new ArrayList<>();
                for (int k = 0; k < numOfChips; k++) {
                    double currentVBandgap = cluster.getActiveSPChipList().get(k).getLastVBandGap();
                    if(!(currentVBandgap<upperLimitVoltage && currentVBandgap>lowerLimitVoltage)) {
                        chipsToDisable.add(cluster.getActiveSPChipList().get(k));
                    }else{
                        activeChips.add(cluster.getActiveSPChipList().get(k));
                    }
                }
                cluster.setSPChipAllDisactive();
                cluster.setSPChipActive(activeChips);
                cluster.setChipWithBandgapProblem(chipsToDisable);

            }else {
                throw new SPException("Bandgap problem: impossible to restart!");
            }
        }
    }


    @Override
    public synchronized double[][] getSENSOR(SPMeasurementParameterSENSORS param, SPMeasurementOutput m)
            throws SPException {

        SPSensingElementOnChip spSensingElementOnChip = spConfiguration.searchSPSensingElementOnChip(param.getSensorName());
        SPSensingElement spSensingElement = spSensingElementOnChip.getSensingElementOnFamily().getSensingElement();

        //TODO: Understand it
        //??????????generateCalibrationParameters_new(param.getSensorName());


        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());

        double output[][] = null;
        Integer NDATA = 1;

        if(param.getParamInternalEIS()!=null){

            output = new double[cluster.getActiveSPChipList().size()][SPMeasurementParameterEIS.NUM_OUTPUT ];

            //SPMeasurementOutput m2 = new SPMeasurementOutput();
            //SPMeasurementParameterEIS paramInternal = (SPMeasurementParameterEIS) spSensingElementOnChip.getSensingElementOnFamily().getSPMeasurementParameter(spConfiguration);
            //paramInternal.setSPMeasurementMetaParameter(paramInternal.getSPMeasurementMetaParameter());

            SPMeasurementParameterEIS paramInternal = param.getParamInternalEIS();

            double buffer[][] = getEIS(paramInternal, m);

            for(int i = 0; i < buffer.length; i++) {
               /* output[i][0] = buffer[i][paramInternal.getMeasure()];
                output[i][0] = N[i] + M[i] * output[i][0];

                output[i][SPMeasurementParameterEIS.IN_PHASE + 1]       = buffer[i][SPMeasurementParameterEIS.IN_PHASE];
                output[i][SPMeasurementParameterEIS.QUADRATURE + 1]     = buffer[i][SPMeasurementParameterEIS.QUADRATURE];
                output[i][SPMeasurementParameterEIS.CONDUCTANCE + 1]         = buffer[i][SPMeasurementParameterEIS.CONDUCTANCE];
                output[i][SPMeasurementParameterEIS.SUSCEPTANCE + 1]          = buffer[i][SPMeasurementParameterEIS.SUSCEPTANCE];
                output[i][SPMeasurementParameterEIS.MODULE + 1]         = buffer[i][SPMeasurementParameterEIS.MODULE];
                output[i][SPMeasurementParameterEIS.PHASE + 1]          = buffer[i][SPMeasurementParameterEIS.PHASE];
                output[i][SPMeasurementParameterEIS.RESISTANCE + 1]     = buffer[i][SPMeasurementParameterEIS.RESISTANCE];
                output[i][SPMeasurementParameterEIS.CAPACITANCE + 1]    = buffer[i][SPMeasurementParameterEIS.CAPACITANCE];
                output[i][SPMeasurementParameterEIS.INDUCTANCE + 1]     = buffer[i][SPMeasurementParameterEIS.INDUCTANCE];

                */


               for(int j=0; j<SPMeasurementParameterEIS.measureLabels.length; j++){
                   if(j==paramInternal.getMeasure())
                       output[i][j] = M[i]*buffer[i][j]+N[i];
                   else
                    output[i][j] = buffer[i][j];
               }

            }

            //ADDED BY MARCO FERDINANDI
            //Se è stato rilevato l'ADC Overflow e è settato a true l'automaticADCRangeAdjustment,
            //la get Sensor aggiorna i parametri nei sensingelement. Per questo motivo, per riportarsi alle condizioni
            //originali, bisogna solo ricaricare l'xml di configurazione (per il momento equivale a riavviare l'applicazione).
           /* if(m.ADCOverflow && this.isAutomaticADCRangeAdjustment()){
                spSensingElement.setRsense(paramInternal.getRsenseLabel());
                spSensingElement.setInGain(paramInternal.getInGainLabel());
            }*/

            ref.log += m.out;

        } else if (spSensingElement.getMeasureTechnique().equals("DIRECT")) {

            if (spSensingElement.getName().equals("ONCHIP_TEMPERATURE") || spSensingElement.getName().equals("ONCHIP_VOLTAGE")) {

                double k = 1;
                if (spSensingElement.getName().equals("ONCHIP_VOLTAGE")){
                    k = 3 * spConfiguration.getVREF() / NORMALIZATION_FACTOR;
                }

                output = new double[cluster.getActiveSPChipList().size()][1];

                if(this.isSystemLevelChopperActive()) {
                    ref.instructionToSend = ((SPMeasurementStatusSENSOR) status).getInstructionToggleCHAFILTERPCI();
                    sendSequence(ref);
                }

                int[][] buffer = getDATA(param,NDATA, true, true, m);

                if(this.isSystemLevelChopperActive()) {
                    if (status.getSystemLevelChopper().isPCIBitHigh()) {
                        for (int i = 0; i < buffer.length; i++) {
                            for (int j = 0; j < buffer[i].length; j++) {
                                buffer[i][j] = buffer[i][j] * -1;
                            }
                        }
                    }

                    double[][] appo = new double[buffer.length][buffer[0].length];
                    for (int i = 0; i < buffer.length; i++) {
                        for (int j = 0; j < buffer[i].length; j++) {
                            appo[i][j] = buffer[i][j];
                        }
                    }


                    status.updateSystemLevelChopper(appo);


                    for (int i = 0; i < numOfChips; i++) {
                        output[i][0] = N[i] + (M[i] * appo[i][0] * k);// * SPConfiguration.VREF_1); // / NORMALIZATION_FACTOR;
                        //output[i][0] = (1 * buffer[i][0] * k);// * SPConfiguration.VREF_1); // / NORMALIZATION_FACTOR;

                    }
                }else{
                    for (int i = 0; i < numOfChips; i++) {
                        output[i][0] = N[i] + (M[i] * buffer[i][0] * k);// * SPConfiguration.VREF_1); // / NORMALIZATION_FACTOR;
                        //output[i][0] = (1 * buffer[i][0] * k);// * SPConfiguration.VREF_1); // / NORMALIZATION_FACTOR;

                    }
                }

                status.updateStatus(output);

                ref.log += m.out;

                //ref.instructionToSend = spConfiguration.getSPBandGapFromVCC();
                //sendSequence(ref);


            } else if (spSensingElement.getName().equals("ONCHIP_LIGHT") ||  spSensingElement.getName().equals("ONCHIP_DARK")){


                output = new double[cluster.getActiveSPChipList().size()][1];

                if(this.isSystemLevelChopperActive()) {
                    ref.instructionToSend = ((SPMeasurementStatusSENSOR) status).getInstructionToggleCHAFILTERPCI();
                    sendSequence(ref);
                }

                int[][] buffer = getDATA(param,NDATA, true, true, m);

                if(this.isSystemLevelChopperActive()) {
                    if (status.getSystemLevelChopper().isPCIBitHigh()) {
                        for (int i = 0; i < buffer.length; i++) {
                            for (int j = 0; j < buffer[i].length; j++) {
                                buffer[i][j] = buffer[i][j] * -1;
                            }
                        }
                    }

                    double[][] appo = new double[buffer.length][buffer[0].length];
                    for (int i = 0; i < buffer.length; i++) {
                        for (int j = 0; j < buffer[i].length; j++) {
                            appo[i][j] = buffer[i][j];
                        }
                    }

                    status.updateSystemLevelChopper(appo);

                    for (int i = 0; i < numOfChips; i++) {
                        output[i][0] += N[i] + (M[i] * appo[i][0]);
                    }
                }else{
                    for (int i = 0; i < numOfChips; i++) {
                        output[i][0] += N[i] + (M[i] * buffer[i][0]);
                    }
                }

                status.updateStatus(output);
                ref.log += m.out;

            } else {
                throw new SPException("In getSENSOR. Sensing element unavailable: " + spSensingElement.getMeasureTechnique());
            }



        }  else {
            throw new SPException("Measure technique unavailable: " + spSensingElement.getMeasureTechnique());
        }
        return output;
    }


    /**
     * @param param
     * @param m     log messages
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)

    @Override
    public synchronized  void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m)
            throws SPException {

        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setADC", DEBUG_LEVEL);

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {

            Integer  FNP, OSR_DIVIDER, CICW, SAM_DIVIDER_int, SAM_DIVIDER_fraz, SAM_DIVIDER, Conversion_Rate_int, CCO, FRD = 0;
            Double Conversion_Rate_fraz, Conversion_Rate;

            String instruction, instructionNum;
            ArrayList<String> instructionList = new ArrayList<>();

            Integer cutOffMin = 50;
            Integer cutOffMax = 100;

            // True if the conversion rate should be evaluated internally
            if (param.getConversion_Rate() == null){
                if (lastParameter == null) {
                    throw new SPException("Any setXXX called before setADC");
                }
                if (lastParameter instanceof SPMeasurementParameterEIS){
                    // Evaluate the conversion rate
                    Conversion_Rate = AvailableFrequencies.evaluateADCConversionRate(((SPMeasurementParameterEIS) lastParameter).getFrequency(), cutOffMin, cutOffMax);
                    FRD = ((SPMeasurementStatusIQ) status).getFRD();
                    ((SPMeasurementStatusIQ)status).setConversionRate(Conversion_Rate);
                    ((SPMeasurementStatusIQ)status).setConversionRate(25); // TODO: Verify with Simone Del Cesta
                } else {
                    //Throws and exception: any other alternatives are defined
                    throw new SPException("Any suitable setXXX called before setADC");
                }

            } else {
                Conversion_Rate = param.getConversion_Rate();
            }

            if (Conversion_Rate.intValue() <= 1000) {
                FNP = 0; //-- enabled ADC precision mode
            } else {
                FNP = 1; //-- enabled ADC fast mode
            }


            if (Conversion_Rate <= clock/2016.0){
                SAM_DIVIDER = 63;
                double f_osr = 2 * Conversion_Rate * 2016;
                if (FRD == 1){
                    f_osr = f_osr * 2 / 3;
                }
                OSR_DIVIDER = (int) Math.round(clock / f_osr);
            } else {

                OSR_DIVIDER = 2;
                SAM_DIVIDER_int = (int) Math.round(clock/ ((64 * OSR_DIVIDER.intValue() * Conversion_Rate))); // 8 bit integer
                Conversion_Rate_int = (int) (clock / (64 * OSR_DIVIDER.intValue() * SAM_DIVIDER_int.intValue()));

                SAM_DIVIDER_fraz = (int)  Math.round((clock / (64 * OSR_DIVIDER.intValue() * Conversion_Rate)) * 2 / 3);
                Conversion_Rate_fraz = (double) ((clock / (64 * OSR_DIVIDER.intValue() * SAM_DIVIDER_fraz.intValue())) * 2 / 3);
                if (Math.abs(Conversion_Rate_int - Conversion_Rate) <= Math.abs(Conversion_Rate_fraz - Conversion_Rate)) {
                    SAM_DIVIDER = SAM_DIVIDER_int;
                    FRD = 0;
                } else {
                    SAM_DIVIDER = SAM_DIVIDER_fraz;
                    FRD = 1;
                }
            }


            if (param.getNData().intValue() == 1)
                CCO = 0;
            else
                CCO = 1;


            if (CCO == 0) {
                if (SAM_DIVIDER <= 2) {
                    CICW = 0;
                } else if (SAM_DIVIDER <= 4) {
                    CICW = 3;
                } else if (SAM_DIVIDER <= 8) {
                    CICW = 6;
                } else if (SAM_DIVIDER <= 16) {
                    CICW = 9;
                } else if (SAM_DIVIDER <= 32) {
                    CICW = 12;
                } else { //if (SAM_DIVIDER <= 63){
                    CICW = 15;
                }
            } else {
                if (SAM_DIVIDER > 16) {
                    SAM_DIVIDER = 16;
                    FRD = 1;
                }

                if (SAM_DIVIDER <= 1) {
                    CICW = 3;
                } else if (SAM_DIVIDER <= 2) {
                    CICW = 6;
                } else if (SAM_DIVIDER <= 4) {
                    CICW = 9;
                } else if (SAM_DIVIDER <= 8) {
                    CICW = 12;
                } else { //if  (SAM_DIVIDER <= 16){
                    CICW = 15;
                }

            }

            instruction = "WRITE COMMAND S 0x04";
            instructionList.add(instruction);


            instruction = "WRITE COMMAND S 0x06";
            instructionList.add(instruction);


            instruction = "WRITE CHA_DIVIDER M 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(SAM_DIVIDER.intValue(), 6);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(OSR_DIVIDER.intValue(), 9);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(FRD, 1);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_SELECT+1 S 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(CICW.intValue(), 4) + "00" + param.getInPort();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_CONFIG S 0x";
            instructionNum = "" + CCO + "" + FNP + "1";
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(param.getNData().intValue(), 5);


            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            sendSequences(instructionList, ref, "setADC");

            if (m != null) {
                m.out = ref.log;
            }
            //lastParameter = param;

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setADC", DEBUG_LEVEL);

    }*/

    /**
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
    */
    @Override
    public synchronized  void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m)
            throws SPException {

        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setADC", DEBUG_LEVEL);

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {

            Integer FNP, OSR_DIVIDER, CICW, SAM_DIVIDER_int, SAM_DIVIDER_fraz, SAM_DIVIDER, Conversion_Rate_int, CCO, FRD = 0;
            //Double Conversion_Rate_fraz;
            Double Conversion_Rate, OSR;

            String instruction, instructionNum;
            ArrayList<String> instructionList = new ArrayList<>();

            //Integer cutOffMin = 20;
            //Integer cutOffMax = 100;

            Conversion_Rate = (12.5);
            OSR = (double)512;

            if (Conversion_Rate <= 1000) {
                FNP = 0; //-- enabled ADC precision mode
            } else {
                FNP = 1; //-- enabled ADC fast mode
            }


            if (Conversion_Rate <= clock/511.0){
                SAM_DIVIDER = 32;
                double f_osr = Conversion_Rate * OSR;
                if (FRD == 1){
                    OSR_DIVIDER = (int)Math.round((clock*2/3)/f_osr);
                } else {
                    OSR_DIVIDER = (int)Math.round(clock/f_osr);
                }
                //  OSR_DIVIDER = (int)Math.round(clock / f_osr);
            } else {

                throw new SPException("Not yet implemented. setADC() Conversion_Rate > clock/511.0 = " + clock/511);
            }


            if (param.getNData().intValue() == 1)
                CCO = 0;
            else
                CCO = 1;

            CICW = (int)(15 - 3 * (Math.log(1008.0 / OSR) / Math.log(2.0)));



            //instruction = "WRITE COMMAND S 0x04";
            //instructionList.add(instruction);


            //instruction = "WRITE COMMAND S 0x06";
            //instructionList.add(instruction);


            instruction = "WRITE CHA_DIVIDER M 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(SAM_DIVIDER.intValue(), 6);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(OSR_DIVIDER.intValue(), 9);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(FRD, 1);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_SELECT+1 S 0X";
            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(CICW.intValue(), 4) + "00" + param.getInPortADC();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_CONFIG S 0x";
            instructionNum = "" + CCO + "" + FNP + "1";
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(param.getNData().intValue(), 5);


            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            sendSequences(instructionList, ref, "setADC");

            if (m != null) {
                m.out = ref.log;
            }

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setADC", DEBUG_LEVEL);

    }



    @Override
    protected void getQ(SendInstructionInOut ref, double output[][],SPMeasurementParameter param, SPMeasurementOutput m) throws SPException {
        /// Leggo QUADRATURA
        long start = System.currentTimeMillis();

        SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;
        ref.instructionToSend = iqStatus.getInstructionQ_Prepared();

        GET_Q_ELAPSED += (System.currentTimeMillis() - start);

        if(this.isSystemLevelChopperActive()) {
            ref.instructionToSend = iqStatus.getInstructionToggleCHAFILTERPCI();
            sendSequence(ref);
        }

        //sendSequence(ref);

        start = System.currentTimeMillis();

        GET_Q_ELAPSED += (System.currentTimeMillis() - start);

        int[][] dataRead = getDATA(param,1, true, false, m);

        if(this.isSystemLevelChopperActive()) {
            if (iqStatus.getSystemLevelChopper().isPCIBitHigh()) {
                for (int i = 0; i < dataRead.length; i++) {
                    for (int j = 0; j < dataRead[i].length; j++) {
                        dataRead[i][j] = dataRead[i][j] * -1;
                    }
                }
            }
        }

        start = System.currentTimeMillis();

        ref.log += m.out;

        for(int i = 0; i < cluster.getActiveSPChipList().size(); i++) {
            output[i][SPMeasurementParameterQI.QUADRATURE] = dataRead[i][0];
        }
        GET_Q_ELAPSED += (System.currentTimeMillis() - start);

    }

    @Override
    protected void getI(SendInstructionInOut ref, double output[][],SPMeasurementParameter param,SPMeasurementOutput m) throws SPException {

        long start = System.currentTimeMillis();

        SPMeasurementStatusIQ iqStatus = (SPMeasurementStatusIQ) status;

        /// Leggo FASE
        //ref.instructionToSend = iqStatus.getInstructionI_Prepared();
        GET_I_ELAPSED += (System.currentTimeMillis() - start);


        if(this.isSystemLevelChopperActive()) {
            ref.instructionToSend = iqStatus.getInstructionToggleCHAFILTERPCI();
            sendSequence(ref);
        }



        //sendSequence(ref);

        start = System.currentTimeMillis();



        GET_I_ELAPSED += (System.currentTimeMillis() - start);

        int[][] dataRead = getDATA(param,1, true, true, m);

        if(this.isSystemLevelChopperActive()) {
            if (iqStatus.getSystemLevelChopper().isPCIBitHigh()) {
                for (int i = 0; i < dataRead.length; i++) {
                    for (int j = 0; j < dataRead[i].length; j++) {
                        dataRead[i][j] = dataRead[i][j] * -1;
                    }
                }
            }
        }

        start = System.currentTimeMillis();

        ref.log += m.out;

        for(int i = 0; i < cluster.getActiveSPChipList().size(); i++) {
            output[i][SPMeasurementParameterQI.IN_PHASE] = dataRead[i][0];
        }
        GET_I_ELAPSED += (System.currentTimeMillis() - start);

    }

    /**
     * @param param
     * @param m     log messages
     * @return
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    @Override
    protected byte[][] getDATA_BYTE(SPMeasurementParameterADC param, Boolean inPhase, SPMeasurementOutput m) throws SPException {

        // inPhase: is not used in RUN4

        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start getDATA", DEBUG_LEVEL);

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        byte[][] output;// = new byte[param.getNData() * 2];

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());

        try {

            ref.log = "-----------------------------\ngetData:\n";

            if (param.getCommandX20()){
                ref.instructionToSend = "WRITE COMMAND S 0x2";    // Trigger per acquisizione

                if (inPhase){
                    ref.instructionToSend += "0";
                } else {
                    ref.instructionToSend += "2";
                }

                sendSequence(ref);
                //SPDelay.delay(15); // Wait for data ready on ADC
                SPDelay.delay(spProtocol.getADCDelay()); // Wait for data ready on ADC
            }

            //ref.instructionToSend = "READ CHA_FIFOL M " + param.getNData() * 2;
            //ref.instructionToSend = "READ CHA_CONFIG M " + param.getNData() * 2;
            //sendSequence(ref);

            ref.instructionToSend = "READ CHA_FIFOL M " + param.getNData() * 2;
            sendSequence(ref);

            //ref.instructionToSend = "READ CHA_FIFOH S 1";
            //sendSequence(ref);

            output = new byte[ref.recValues.length][];
            SPLogger.getLogInterface().d(LOG_MESSAGE, "ref.recValues.length" + ref.recValues.length, DEBUG_LEVEL);

            for(int k = 0; k < ref.recValues.length; k++) {
                output[k] = responseTokenizerByte(ref.recValues[k]);
                SPLogger.getLogInterface().d(LOG_MESSAGE, "output[k]: " + output[k][0] + " " + output[k][1], DEBUG_LEVEL);
            }

            if (status != null){
                status.updateLog(output);
            }

            ref.log += "END getData.\n-----------------------------\n";

            if (m != null) {
                m.out = ref.log;
            }

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End getDATA", DEBUG_LEVEL);

        return output;
    }



    /**
     * @param param
     * @param m     log messages
     * @throws SPException if there are parameters error: missing or out of range
     */
    @Override
    public synchronized  void setPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m)
            throws SPException {

        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setPOT", DEBUG_LEVEL);
        ArrayList<String> instructionList = new ArrayList<>();

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {


            Double conversionRate = (double)50;
            String instruction, instructionNum;

            String CES = "0";
            if (param.portIsInternal()) {
                CES = "1";    // TODO: perfezionare confronto per individuare se porta interna o esterna
            }

            instruction = "WRITE CHA_SELECT S 0x";
            instructionNum = "01" + CES + param.getPort();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE CHA_CONDIT M 0x";
            param.setInGain("1");
            instructionNum = "000000" + param.getInGain() + param.getRsense() + "011111";
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            String FSC_DAS = param.getContacts();

            instruction = "WRITE DSS_SELECT M 0x";
            instructionNum = "0000011100" + FSC_DAS + param.getPort();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE DSS_DIVIDER M 0x0014";
            instructionList.add(instruction);

            instruction = "WRITE CHA_FILTER+1 S 0xB2";
            instructionList.add(instruction);

            sendSequences(instructionList, ref, "setPOT");

            if (m != null) {
                m.out = ref.log;
            }

            SPMeasurementOutput mADC = new SPMeasurementOutput();
            setADC(1, conversionRate, SPMeasurementParameterADC.INPORT_IA, true, mADC);
            //instructionList.add(instruction);

            if (m != null) {
                m.out += mADC.out;
            }


            instructionList = new ArrayList<>();
            instruction = "WRITE ANA_CONFIG S 0x3F";
            instructionList.add(instruction);

            //if (param.getSPMeasurementMetaParameter().getRestartBandGap()){
            //    restartBandGap();
            //}


            if (param.getTypeVoltammetry().equals(SPMeasurementParameterPOT.POTENTIOMETRIC)){
                instruction = "WRITE CHA_SELECT S 0x";
                instructionNum = "11" + CES + param.getPort();
                instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
                instructionList.add(instruction);
            }

            sendSequences(instructionList, ref, "setPOT");

            status = new SPMeasurementStatusPOT(this, spConfiguration, param, spProtocol, "" + logDirectory + "/Download/getPOT_dump.txt");


            if (m != null) {
                m.out += ref.log;
            }
            lastParameter = param;

        } catch (SPException  e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setPOT", DEBUG_LEVEL);

    }


    public synchronized void setACTUATOR(SPMeasurementParameterActuator param, SPCluster temporaryCluster) throws SPException {

    }


    public synchronized void setACTUATOR(SPMeasurementParameterActuator param) throws SPException{
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setACTUATOR", DEBUG_LEVEL);
        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }

        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        ArrayList<String> instructionList = new ArrayList<>();
        String instruction = null;

        if (param.getPort().equals(SPMeasurementParameterActuator.ACTUATOR_PORT_INT)){
            if (param.getMode() == SPMeasurementParameterActuator.ON){
                instruction = "WRITE INT_CONFIG S 0x01";
                instructionList.add(instruction);

                instruction = "WRITE DIG_CONFIG S 0x04";
                instructionList.add(instruction);
            } else {
                instruction = "WRITE DIG_CONFIG S 0x00";
                instructionList.add(instruction);
            }
            /*if (param.getMode() == SPMeasurementParameterActuator.ON){
                instruction = "WRITE INT_CONFIG S 0x05";
                instructionList.add(instruction);

                instruction = "WRITE DIG_CONFIG S 0x05";
                instructionList.add(instruction);
            } else {
                instruction = "WRITE INT_CONFIG S 0x00";
                instructionList.add(instruction);
            }*/
        } else if (param.getPort().equals(SPMeasurementParameterActuator.ACTUATOR_PORT_RDY)){

            if (param.getMode() == SPMeasurementParameterActuator.ON){
                instruction = "WRITE INT_CONFIG+1 S 0x02";
                instructionList.add(instruction);
            } else {
                instruction = "WRITE INT_CONFIG+1 S 0x00";
                instructionList.add(instruction);
            }
        }

        sendSequences(instructionList, ref, "setACTUATOR");
    }

    /**
     * The setEIS method allows setting all required parameters to perform Electric Impedance Spectroscopy
     * (EIS) measurements.
     *
     * @param param @see SPMeasurementParameterEIS
     * @param m     log messages
     * @throws SPException if there are parameters error: missing or out of range
     * @throws SPException          if there is some kind of error during this call (for example a getXXX called before setXXX)
     * @throws SPException              if there is errors in translating mnemonic in sequence of byte with respect to the low level protocol selected
     * @throws SPException               if there is errors in low level communication (disconnection, timeout, etc.)
     */
    @Override
    public synchronized  void setEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m)
            throws SPException {


        SPLogger.getLogInterface().d(LOG_MESSAGE, "Start setEIS", DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE, param.toString(), DEBUG_LEVEL);

        int chopCutOff = 10000;

        if (param == null || !param.isValid()) {
            throw new SPException("Parameters problem: " + param);
        }



        SendInstructionInOut ref = new SendInstructionInOut(param.getTemporaryCluster());
        try {
            ArrayList<String> instructionList = new ArrayList<>();
            lastParameter = param;
            float OSM;
            int FRD, DSS_DIVIDER;

            AvailableFrequencies frequencyCalculus = new AvailableFrequencies();

            String instruction, instructionNum;

            instruction = "WRITE CHA_SELECT S 0x";
            instructionNum = param.getModeVI().substring(1, 2) + param.getContacts() + "0" + param.getInPort();
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);


            instruction = "WRITE DSS_SELECT M 0x";
            if(param.getModeVI().substring(0,1).equals("1")) {
                if(param.getContactsLabel().equals(SPMeasurementParameterEIS.ContactsLabels[0])){
                    //TWO CONTACTS
                    instructionNum = "1" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "0"+param.getModeVI().substring(1,2) + "1" + param.getOutPort();
                }else if(param.getContactsLabel().equals(SPMeasurementParameterEIS.ContactsLabels[1])){
                    //FOUR CONTACTS
                    instructionNum = "1" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "00" + "1" + param.getOutPort();
                }

                //instructionNum = "1" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "00" + "1" + param.getOutPort();
            }
            else {
                if(param.getContactsLabel().equals(SPMeasurementParameterEIS.ContactsLabels[0])){
                    //two contacts
                    instructionNum = "0" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "0"+param.getModeVI().substring(1,2) + param.getContacts() + param.getOutPort();
                }else if(param.getContactsLabel().equals(SPMeasurementParameterEIS.ContactsLabels[1])){
                    //four contacts
                    instructionNum = "0" + param.getModeVI().substring(0, 1) + "000" + param.getOutGain() + "00" + param.getContacts() + param.getOutPort();
                }

            }
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE DSS_DIVIDER M 0x";
            AvailableFrequencies.availableFrequencies(spConfiguration, param.getFrequency(), frequencyCalculus);
            OSM = frequencyCalculus.OSM;
            DSS_DIVIDER = frequencyCalculus.DSS_DIVIDER;
            FRD = frequencyCalculus.FRD;

            instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(DSS_DIVIDER, 15) + Integer.toString(FRD);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE DAS_CONFIG M 0x";
            instructionNum = "0";
            int OSM_INT = (int) OSM;
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(OSM_INT, 3);
            int numBit = 12;
            int modSignDCBias = bitSignDCBias(param.getDCBiasP(), numBit);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBias, numBit);
            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE CHA_FILTER M 0x";
            String chopperValue = "";
            if (frequencyCalculus.availableFrequency <= chopCutOff) {
                chopperValue = "12"; //only ADc choppers enabled (IA chopper causes error EIS in measurements)
            } else {
                chopperValue = "12"; //only ADc choppers enabled (IA chopper causes error EIS in measurements)
            }

            if(frequencyCalculus.availableFrequency == 0){
                chopperValue = "B2";
            }

            String HE3 = "0";
            if (param.getHarmonic().equals(SPMeasurementParameterEIS.harmonicLabels[2])) {
                HE3 = "1";
            }
            instructionNum = "000" + param.getQI() + HE3;

            if (param.getFrequency() == 0){
                instructionNum += "00";
            } else {
                instructionNum +=  param.getHarmonic();
            }
            if (instructionNum.length() != 8)
                throw new SPException("lunghezza dati numerici sbagliata in instruction: " + instruction); //  numeric substring after 'OX' have to have lenght 8 bit

            String hexValueToFillWithZero = SPMeasurementUtil.zeroFill(Integer.toHexString(Integer.parseInt(instructionNum, 2)), 2);
            hexValueToFillWithZero = chopperValue + hexValueToFillWithZero;
            instruction += SPMeasurementUtil.zeroFill(hexValueToFillWithZero, 4);
            instructionList.add(instruction);

            instruction = "WRITE CHA_CONDIT M 0x";

            //String VSCM_BIAS = "011111";
            if (param.getPhaseShiftMode() == SPMeasurementParameterEIS.QUADRANT){
                instructionNum = "000000";
            } else {
                instructionNum = SPMeasurementUtil.fromIntegerToBinWithFill(param.getPhaseShift(), 5);
                if (param.getPhaseShiftMode() == SPMeasurementParameterEIS.COARSE){
                    instructionNum += "1";
                } else if (param.getPhaseShiftMode() == SPMeasurementParameterEIS.FINE){
                    instructionNum += "0";
                }
            }

            instructionNum += param.getInGain() + param.getRsense();
            int numBitDCBiasN = 6;
            int modSignDCBiasN = bitSignDCBias(param.getDCBiasN(), numBitDCBiasN);
            instructionNum += SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBiasN, numBitDCBiasN);

            instruction = SPMeasurementUtil.fromBitToHexWithConcat(instruction, instructionNum);
            instructionList.add(instruction);

            instruction = "WRITE ANA_CONFIG S 0x3F";
            instructionList.add(instruction);

            //instruction = "WRITE DAS_CONFIG M 0X0FFF";
            //instructionList.add(instruction);

            sendSequences(instructionList, ref, "setEIS");

            // "" + logDirectory + "/Download/" + measureLogFile

            String nomeFileDump = "" + logDirectory + "/Download/" + "getEIS_dump.txt";
            if (param.getSPMeasurementMetaParameter().getRenewBuffer() || status == null){
                status = new SPMeasurementStatusIQ(this, param, spProtocol, SPMeasurementParameterEIS.NUM_OUTPUT,
                        frequencyCalculus, FRD, nomeFileDump);
            }
            if (m != null) {
                m.out = ref.log;
            }

        } catch (  SPException e) {
            throw e;
        } catch (Exception e) {
            ref.exceptionMessage += e.getMessage() + "\n";
            throw new SPException(ref.exceptionMessage);
        }
        SPLogger.getLogInterface().d(LOG_MESSAGE, "End setEIS", DEBUG_LEVEL);

    }

    protected static int bitSignDCBias(int x, int numBit) {
        int toAdd = (int) Math.pow(2, numBit - 1);
        return x + toAdd;
    }




}
