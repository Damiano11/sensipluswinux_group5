package com.sensichips.sensiplus.level2.util;

import com.sensichips.sensiplus.SPException;

/**
 * Created by mario on 16/09/16.
 */
public class SPMeasurementUtil {
    public static String fromBitToHexWithConcat(String instruction, String bitString) throws SPException {

        int numBit = 0;
        if (instruction.endsWith("S 0x") || instruction.endsWith("S 0X")) {
            numBit = 8;
        } else if (instruction.endsWith("M 0x") || instruction.endsWith("M 0X")) {
            numBit = 16;
        } else {
            throw new SPException("SPDecoderGenericInstruction must contain S 0x or M 0x. Problem in: " + instruction); //  numeric substring after 'OX' have to have lenght 8 bit
        }

        if (bitString.length() != numBit)
            throw new SPException("Error in number of bit in: " + bitString + " relative to instruction " + bitString); //  numeric substring after 'OX' have to have lenght 8 bit
        // Conveerti in intero i bit in bitString, converti poi in esadecimale, fill del valore esadecimale
        return (instruction + zeroFill(Integer.toHexString(Integer.parseInt(bitString, 2)), numBit / 4)).toUpperCase();
    }

    public static String zeroFill(String toFill, int lenghTotal) {
        // method which fill to left with zeros until a specified lengh
        // e. g.: zeroFill ("1234", 6) return "001234"

        // warning: in case of negative number, this method return a substring (it does a TRUNCATION!!!!)

        if (toFill.length() <= lenghTotal) {
            while (toFill.length() < lenghTotal) {
                toFill = "0" + toFill;
            }
            return toFill;
        } else {

            return toFill.substring(toFill.length() - lenghTotal, toFill.length());
        }
    }

    public static String zeroFill(String hexToFill) throws SPException {
        // overload previous method. It's used for hexadecimal string, with fixed lenght 2 or 4.
        // e.g. zeroFill ("ABC") return "0ABC"; zeroFill ("AB") return "AB"; zeroFill ("A") return "0A"

        if (hexToFill.length() == 1 || hexToFill.length() == 2)
            return zeroFill(hexToFill, 2);
        else if (hexToFill.length() == 3 || hexToFill.length() == 4)
            return zeroFill(hexToFill, 4);
        else
            throw new SPException("wrong hexadecimal. data length");

    }


    public static String fromIntegerToBinWithFill(Integer value, int numBit) {
        return SPMeasurementUtil.zeroFill(Integer.toBinaryString(value), numBit);
    }

    public static int sarchIndex(Object[] elenco, Object elem){
        int pos = -1;
        for(int i = 0; i < elenco.length; i++){
            if (elenco[i].equals(elem)){
                pos = i;
                break;
            }
        }
        return pos;
    }

    public static Object searchItem(Object[] elenco, Object elem){
        int pos = -1;
        for(int i = 0; i < elenco.length; i++){
            if (elenco[i].equals(elem)){
                pos = i;
                break;
            }
        }
        return elenco[pos];
    }

}
