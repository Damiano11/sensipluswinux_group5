package com.sensichips.sensiplus.level2;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.drivermanager.SPDriverManager;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.protocols.esp8266.SPProtocolESP8266_SENSIBUS;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacket;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacketExtended;
import com.sensichips.sensiplus.level2.parameters.*;
import org.apache.commons.codec.binary.Hex;

import java.nio.ByteBuffer;

public class SPMeasurementRUN5_MCU extends SPMeasurementRUN5_PC {


    public SPMeasurementRUN5_MCU(SPConfiguration conf, String logDirectory, String MEASURE_TYPE) throws SPException, SPException {
        super(conf, logDirectory, MEASURE_TYPE);
    }

    /**
     * @param ModeTimer
     * @param Timer
     * @param message
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setTimer(String ModeTimer, Integer Timer, SPMeasurementOutput message) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setULTRASOUND(SPMeasurementParameterULTRASOUND param, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public double[][] getULTRASOUND(SPMeasurementParameterULTRASOUND param, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m) throws SPException {
        SPProtocolPacketExtended packet = param.getPacketForSetEIS();
        byte[] received = spProtocol.sendPacket(packet);
        System.out.println("setEIS MCU effettuata!!!");
        // received.length == 0 expected
    }

    /**
     * @param param
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
   @Override
    public double[][] getEIS(SPMeasurementParameterEIS param, SPMeasurementOutput m) throws SPException {
       double out[][];
       out = getSingleEIS(param, isAutomaticADCRangeAdjustment(), m);
       return out;
    }



    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setADC(SPMeasurementParameterADC param, SPMeasurementOutput m) throws SPException {
        SPProtocolPacketExtended packet = param.getPacketForSetADC();
        System.out.println("SetADC MCU effettuata!");
        byte[] received = spProtocol.sendPacket(packet);
    }

    @Override
    public double[][] getSingleEIS(SPMeasurementParameterEIS param, boolean ADCAdjustment, SPMeasurementOutput m) throws SPException {
        SPProtocolPacketExtended packet = param.getPacketForGetEIS();
        byte[] received = spProtocol.sendPacket(packet);
        byte[] appo = new byte[8];
        double out[][] = new double[2][SPMeasurementParameterEIS.NUM_OUTPUT];

        // Genero double
        int j = 0;
        int cont = 0;
        for (int i = 0; i < ((packet.getDataLengthExpected()-2)/8); i++) {
            System.arraycopy(received, (i * 8), appo, 0, 8);
            if(cont == 9){
                cont = 0;
                j++;
            }
            out[j][cont] = ByteBuffer.wrap(appo).getDouble();
            //System.out.println(out[j][cont]);
            cont++;
        }

        return out;
    }


    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param InOutPort
     * @param Type
     * @param InitialPotential
     * @param FinalPotential
     * @param step
     * @param PulsePeriod
     * @param PulseAmplitude
     * @param alternativeSignal
     * @param rsense
     * @param InGain
     * @param message
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public double[][] getPOT(String InOutPort, String Type, Integer InitialPotential, Integer FinalPotential, Integer step, Integer PulsePeriod, Integer PulseAmplitude, Boolean alternativeSignal, String rsense, String InGain, SPMeasurementOutput message) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public double[][] getPOT(SPMeasurementParameterPOT param, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param inPhase
     * @param m
     * @return
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public int[][] getDATA(SPMeasurementParameter param, Boolean inPhase, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void setPPR(SPMeasurementParameterPPR param, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");

    }

    /**
     * @param param
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public SPMeasurementOutputPPR[] getPPR(SPMeasurementParameterPPR param, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    @Override
    public void setACTUATOR(SPMeasurementParameterActuator parameterActuator) throws SPException {
        throw new SPException("Not yet implemented!");

    }

    @Override
    public void setACTUATOR(SPMeasurementParameterActuator parameterActuator, SPCluster temporaryCluster) throws SPException {
        throw new SPException("Not yet implemented!");

    }

    @Override
    public double[][] getSENSOR(SPMeasurementParameterSENSORS sensorParam, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");
    }

    @Override
    public void setSENSOR(SPMeasurementParameterSENSORS parameter, SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");

    }

    /**
     * @param m
     * @throws SPException
     * @throws SPException
     * @throws SPException
     * @throws SPException
     */
    @Override
    public void timerTest(SPMeasurementOutput m) throws SPException {
        throw new SPException("Not yet implemented!");

    }


    public static void main(String[] args) {
/*
            SPDriver driver_temp = SPDriverManager.get("WINUX_COMUSB_COM12_");
            SPConfiguration conf_temp = new SPConfiguration();
            SPCluster clust_temp = new SPCluster();
            SPProtocolESP8266_SENSIBUS prot_temp = new SPProtocolESP8266_SENSIBUS(driver_temp, clust_temp, 0);
*/
            int FIRSTBYTE_MASK = 255; // 11111111
            byte[] dataPacket = new byte[2];
            int dcBiasP = -1348;
            int appo = dcBiasP + 2048;
            dataPacket[0] = (byte)(appo >> 8);
            dataPacket[1] = (byte)(appo & FIRSTBYTE_MASK);
            System.out.println(Hex.encodeHexString(dataPacket));

    }

}