package com.sensichips.sensiplus.level2.status;

import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterSENSORS;

/**
 * Created by mario on 14/06/17.
 */
public class SPMeasurementStatusSENSOR extends SPMeasurementStatus {
    private boolean firstMeasure = true;
    private int counter = 0;
    private double[][][] buffer;
    private int numOutput = 1;
    int dimBuffer = 0;
    String bandGapToRestore = "";
    String bandGapMin = "";



    public String getInstructionToggleCHAFILTERPCI() {
        if(((SPMeasurementParameterSENSORS)this.spParameter).getSensorName().equals("ONCHIP_TEMPERATURE")||
                ((SPMeasurementParameterSENSORS)this.spParameter).getSensorName().equals("ONCHIP_VOLTAGE")) {
            if (systemLevelChopper.isPCIBitHigh())
                instructionToggleCHAFILTERPCI = "WRITE CHA_FILTER M 0xD200";
            else
                instructionToggleCHAFILTERPCI = "WRITE CHA_FILTER M 0x9200";
        }
        else if(((SPMeasurementParameterSENSORS)this.spParameter).getSensorName().equals("ONCHIP_LIGHT")||
                ((SPMeasurementParameterSENSORS)this.spParameter).getSensorName().equals("ONCHIP_DARK")){
            if (systemLevelChopper.isPCIBitHigh())
                instructionToggleCHAFILTERPCI = "WRITE CHA_FILTER M 0xF200";
            else
                instructionToggleCHAFILTERPCI = "WRITE CHA_FILTER M 0xB200";

        }


        return instructionToggleCHAFILTERPCI;
    }

    private String instructionToggleCHAFILTERPCI = null;


    public SPMeasurementStatusSENSOR(SPMeasurement spMeasurement, SPMeasurementParameterSENSORS spParameter, SPProtocol spProtocol, String measureLogFile){
        super(spMeasurement, spParameter, spProtocol, measureLogFile);
        if(spMeasurement.isSystemLevelChopperActive()) {
            this.systemLevelChopper = new SystemLevelChopper(spMeasurement.getClusterSize(), 1);
        }
        this.dimBuffer = Integer.parseInt(spParameter.getFilter());
        buffer = new double[spMeasurement.getClusterSize()][numOutput][dimBuffer];
        //spParameter.get



    }

    public void updateStatus(double[][] output){
        //TODO:Take care at this threashold-> Temporary testing
        int backgroundThreshold = 50;
        counter++;
        // Media sugli ultimi dim_buffer con buffer a scorrimento
        if (firstMeasure) {
            //Inizializzazione
            firstMeasure = false;
            for(int i = 0; i < spMeasurement.getClusterSize(); i++) {
                for (int j = 0; j < numOutput; j++) {
                    for (int k = 0; k < dimBuffer; k++) {
                        buffer[i][j][k] = output[i][j];
                    }
                }
            }
        } else {
            if (dimBuffer == Integer.parseInt(SPMeasurementParameterEIS.filterValues[SPMeasurementParameterEIS.filterValues.length - 1])
                    && counter == backgroundThreshold) {
                int endIndex = (dimBuffer - counter) > 0 ? (dimBuffer - counter) : 0;
                int startIndex = endIndex - backgroundThreshold;
                counter = dimBuffer;
                while (startIndex >= 0) {
                    for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                        for (int j = 0; j < numOutput; j++) {
                            for (int k = startIndex; k < endIndex; k++) {
                                buffer[i][j][k] = buffer[i][j][k + backgroundThreshold];
                            }
                        }
                    }
                    endIndex -= backgroundThreshold;
                    startIndex -= backgroundThreshold;
                }

            } else {
                for (int i = 0; i < spMeasurement.getClusterSize(); i++) {
                    for (int j = 0; j < numOutput; j++) {
                        for (int k = 0; k < dimBuffer - 1; k++) {
                            buffer[i][j][k] = buffer[i][j][k + 1];
                        }
                        buffer[i][j][dimBuffer - 1] = output[i][j];
                        output[i][j] = 0;
                        for (int k = 0; k < dimBuffer; k++) {
                            output[i][j] += buffer[i][j][k];
                        }
                        output[i][j] = output[i][j] / dimBuffer;
                    }
                    //output[i][j] = output[i][j] / dimBuffer;
                }
            }
        }
    }

}
