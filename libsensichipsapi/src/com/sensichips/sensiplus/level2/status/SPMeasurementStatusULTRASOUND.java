package com.sensichips.sensiplus.level2.status;

import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterULTRASOUND;

/**
 * Created by mario on 01/01/17.
 */

public class SPMeasurementStatusULTRASOUND extends SPMeasurementStatusIQ {


    public SPMeasurementStatusULTRASOUND(SPMeasurement spMeasurement, SPMeasurementParameterULTRASOUND spParam, SPProtocol spProtocol, int numOutput,
                                         SPMeasurement.AvailableFrequencies frequencyCalculus,
                                         int FRD, String measureLogFile) throws SPException

    {
        super(spMeasurement, spParam, spProtocol, numOutput, frequencyCalculus, FRD, measureLogFile);
    }

    public void updateStatus(double[][] output) {
        for (int k = 0; k < spMeasurement.getClusterSize(); k++) {
            // Media sugli ultimi dim_buffer con buffer a scorrimento
            if (firstMeasure) {
                // Inizializzazione
                firstMeasure = false;
                for (int j = 0; j < SPMeasurementParameterULTRASOUND.NUM_OUTPUT; j++) {
                    for (int i = 0; i < dimBuffer; i++) {
                        buffer[k][j][i] = output[k][j];
                    }
                    output[k][j] = output[k][j] / dimBuffer;
                }
            } else {
                // Ciclo normale
                for (int j = 0; j < SPMeasurementParameterULTRASOUND.NUM_OUTPUT; j++) {
                    for (int i = 0; i < dimBuffer - 1; i++) {
                        buffer[k][j][i] = buffer[k][j][i + 1];
                    }
                    buffer[k][j][dimBuffer - 1] = output[k][j];

                    output[k][j] = 0;
                    for (int i = 0; i < dimBuffer; i++) {
                        output[k][j] += buffer[k][j][i];
                    }

                    output[k][j] = output[k][j] / dimBuffer;
                }
            }

            // Crea log su file delle misure
            if (measureLog != null) {
                for (int i = 0; i < SPMeasurementParameterULTRASOUND.NUM_OUTPUT; i++) {
                    measureLog.print("" + output[k][i] + "; ");
                }
                measureLog.println();
                measureLog.flush();
            }

        }
    }
}
