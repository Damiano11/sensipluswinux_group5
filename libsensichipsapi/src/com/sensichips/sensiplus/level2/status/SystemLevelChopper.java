package com.sensichips.sensiplus.level2.status;

import java.util.Scanner;

/**
 * Created by Marco Ferdinandi on 09/01/2018.
 */
public class SystemLevelChopper {

    private int maxDim = 3;
    private int start = 0;
    private int end = 0;
    private int riemp = 0;
    private int sign = 1;


    public boolean isPCIBitHigh() {
        return PCIBitHigh;
    }

    private boolean PCIBitHigh = false;

    private int numberOfChips;

    public int getNumberOfElementsForChip() {
        return numberOfElementsForChip;
    }

    private int numberOfElementsForChip;

    private int data[][][] = null;

    private int windowDim = 2;


    public SystemLevelChopper(int numberOfChips, int numberOfElementsForChip ){
        this.numberOfChips = numberOfChips;
        this.numberOfElementsForChip = numberOfElementsForChip;

        data = new int[maxDim][numberOfChips][numberOfElementsForChip];

    }

    public double[][] updateAndMean(int[][] adcValues){
        double[][] output = new double[numberOfChips][numberOfElementsForChip];

        insert(adcValues);

        meanByWindow(output);

        return output;
    }



    public void insert(int[][] adcValues){
        PCIBitHigh = !PCIBitHigh;
        if (riemp < windowDim){
            riemp++;
        } else {
            start = (start + 1) % maxDim;
        }


        for(int i=0; i<adcValues.length; i++) {//for each chip
            for(int j=0; j<numberOfElementsForChip; j++){//for each component
                data[end][i][j] = adcValues[i][j];
            }
        }
        end = (end + 1) % maxDim;
    }

    public void meanByWindow(double[][] output){
        for(int i=0; i<numberOfChips; i++){
            for(int j=0; j<numberOfElementsForChip; j++){
                int k=start;
                int cont = 0;


                while(k!=end){
                    output[i][j] = output[i][j]+data[k][i][j];
                    k = k+1;
                    if(k>=maxDim){
                        k = k%maxDim;
                    }
                    cont ++;

                    //sign = sign *-1;
                }
                if(cont>=windowDim)
                    cont = windowDim;
                output[i][j] = output[i][j]/cont;
            }
        }
    }


    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        int numOfChip = 2;
        int numOfElements = 2;
        SystemLevelChopper systemLevelChopper = new SystemLevelChopper(numOfChip,numOfElements);

        int[][] adcVal = new int[numOfChip][numOfElements];

        while(true){
            for(int i=0; i<numOfChip; i++){
                for(int j=0; j<numOfElements; j++){
                    System.out.println("chip"+i+" element"+j+"=");
                    adcVal[i][j] = sc.nextInt();
                }
            }

            double[][] output = systemLevelChopper.updateAndMean(adcVal);


            System.out.println("\n\n");
            for(int i=0; i<output.length; i++){
                for(int j=0; j<numOfElements; j++){
                    System.out.println("chip"+i+" Mean element"+j+"= "+output[i][j]);
                }
            }



        }



    }







}
