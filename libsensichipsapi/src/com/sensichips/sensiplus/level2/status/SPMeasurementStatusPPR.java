package com.sensichips.sensiplus.level2.status;

import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterPPR;

import java.io.PrintWriter;

/**
 * Created by mario on 29/09/16.
 */
public class SPMeasurementStatusPPR extends SPMeasurementStatus {

    public SPMeasurementStatusPPR(SPMeasurement spMeasurement, SPMeasurementParameterPPR spParameter, SPProtocol spProtocol, String measureLogFile){
        super(spMeasurement, spParameter, spProtocol, measureLogFile);
        flagTIIHigh = new boolean[spMeasurement.getClusterSize()];
        flagThresholdExceeded = new boolean[spMeasurement.getClusterSize()];
        for(int i = 0; i < flagThresholdExceeded.length; i++){
            flagThresholdExceeded[i] = true;
        }
    }


    public boolean flagTIIHigh[];
    public boolean flagThresholdExceeded[];

    @Override
    public void updateStatus(double[][] output){

    }

}
