package com.sensichips.sensiplus.level2.status;

import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameter;

import java.io.File;
import java.io.PrintWriter;

/**
 * Created by mario on 16/09/16.
 */
public abstract class SPMeasurementStatus {
    protected PrintWriter measureLog;
    protected SPProtocol spMnemonic;
    protected SPMeasurementParameter spParameter;
    protected SPMeasurement spMeasurement;

    protected SystemLevelChopper systemLevelChopper = null;
    private int lastPosADC = 0;
    private boolean pipelineFull = false;
    private int ADCPipelineMaxDim = 3;


    // "" + logDirectory + "/Download/" + measureLogFile
    public SPMeasurementStatus(SPMeasurement spMeasurement, SPMeasurementParameter spParameter, SPProtocol spMnemonic, String measureLogFile){

        try {
            File mPath = new File(measureLogFile);
            measureLog = new PrintWriter(mPath);
        } catch (Exception e) {
            measureLog = null;
        }
        this.spMnemonic = spMnemonic;
        this.spParameter = spParameter;
        this.spMeasurement = spMeasurement;
    }


    public SystemLevelChopper getSystemLevelChopper() {
        return systemLevelChopper;
    }


    public abstract void updateStatus(double[][] output);

    public void updateLog(byte[][] output){

        if (measureLog != null) {
            for(int k = 0; k < spMeasurement.getClusterSize(); k++){
                for (int i = 0; i < output[k].length; i++) {
                    measureLog.print("" + output[k][i] + " ");
                }
                measureLog.print("; ");
                measureLog.flush();
            }
        }
    }


    //The input parameter "currentMeasure" contains rxc elements,
    //where:
    //r is the number of chip
    //c is the number measures for each chip.
    //This method is transparent to the measurement type.
    //It changes the values contained in the input vector making the mean with the precedent ADCvalues
    public void updateSystemLevelChopper(double[][] currentMeasure){

        int[][]ADCValues = new int[currentMeasure.length][systemLevelChopper.getNumberOfElementsForChip()];
        for(int i=0; i<currentMeasure.length; i++){
            for(int j = 0; j< systemLevelChopper.getNumberOfElementsForChip(); j++){
                ADCValues[i][j] = (int)currentMeasure[i][j];
            }
        }


        double[][] result = systemLevelChopper.updateAndMean(ADCValues);
        for(int i=0; i<result.length; i++){
            for(int j = 0; j< systemLevelChopper.getNumberOfElementsForChip(); j++){
                currentMeasure[i][j] = result[i][j];
            }
        }

    }




}