package com.sensichips.sensiplus.level2.status;

import com.sensichips.sensiplus.config.SPConfiguration;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementMetaParameter;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterEIS;
import com.sensichips.sensiplus.level2.parameters.SPMeasurementParameterPOT;
import com.sensichips.sensiplus.level2.util.SPMeasurementUtil;
import com.sensichips.sensiplus.util.SPDelay;

import java.io.PrintWriter;

/**
 * Created by mario on 16/09/16.
 */
public class SPMeasurementStatusPOT extends SPMeasurementStatus {
    private double FinalPotential;
    private double InitialPotential;
    private double Step;
    private double Stimulus;
    private double PulseAmplitude;
    private double PulsePeriod;
    private double RSense;
    private double InGain;
    private double kADC;
    private int TypeVoltammetry, count, totStepUp, totStepDown;
    private Boolean up, alternativeSignal;
    private SPConfiguration spConfiguration;

    public SPMeasurementStatusPOT(SPMeasurement spMeasurement, SPConfiguration spConiguration, SPMeasurementParameterPOT spParameter, SPProtocol spProtocol, String measureLogFile) {
        super(spMeasurement, spParameter, spProtocol, measureLogFile);

        this.FinalPotential = spParameter.getFinalPotential();
        this.InitialPotential = spParameter.getInitialPotential();
        this.Step = spParameter.getStep();
        this.TypeVoltammetry = spParameter.getTypeVoltammetry();
        this.Stimulus = spParameter.getInitialPotential();
        this.PulseAmplitude = spParameter.getPulseAmplitude();
        this.PulsePeriod = spParameter.getPulsePeriod();
        this.alternativeSignal = spParameter.getAlternativeSignal();
        this.spConfiguration = spConiguration;

        //this.up = Up;
        this.count = 0;

        this.kADC = (SPMeasurementParameterPOT.Vref_plus - SPMeasurementParameterPOT.Vref_minus) / SPMeasurement.NORMALIZATION_FACTOR;
        this.totStepUp = 0;
        this.totStepDown = 0;

        this.up = (spParameter.getFinalPotential() - spParameter.getInitialPotential()) > 0;

        int i = 0;
        while (i < SPMeasurementParameterEIS.ingainValues.length
                && !SPMeasurementParameterEIS.ingainValues[i].equalsIgnoreCase(spParameter.getInGain())) {
            i++;
        }
        this.InGain = Integer.parseInt(SPMeasurementParameterEIS.ingainLabels[i]);

        i = 0;
        while (i < SPMeasurementParameterEIS.rsenseValues.length
                && !SPMeasurementParameterEIS.rsenseValues[i].equalsIgnoreCase(spParameter.getRsense())) {
            i++;
        }
        this.RSense = Integer.parseInt(SPMeasurementParameterEIS.rsenseLabels[i]) / (double) 1000; // valore in KOhm


    }

    @Override
    public void updateStatus(double[][] output){

    }

    public String getStim2Bit() {
        // convert status.NextStimulus
        int numBit = 12;
        int modSignDCBias = bitSignPOT((int) Stimulus, numBit);
        return SPMeasurementUtil.fromIntegerToBinWithFill(modSignDCBias, numBit);
    }

    public double normalizeVoltage(double ADC) {
        return (ADC * kADC) / (RSense * InGain);
    }


    public void updateStimulus() {

        restartStimulus();
        if (TypeVoltammetry == SPMeasurementParameterPOT.LINEAR_SWEEP_VOLTAMMETRY) {
            Stimulus += up ? 1 : -1;

        } else if (TypeVoltammetry == SPMeasurementParameterPOT.POTENTIOMETRIC) {

        } else if (TypeVoltammetry == SPMeasurementParameterPOT.CURRENT) {

        } else if (TypeVoltammetry == SPMeasurementParameterPOT.STAIRCASE_VOLTAMMETRY) {
            Stimulus += Step;

        } else if (TypeVoltammetry == SPMeasurementParameterPOT.SQUAREWAVE_VOLTAMMETRY) {
            if (count == 0) {
                Stimulus += PulseAmplitude / 2;
            } else if (count % 2 == 1) {  // DISPARI
                Stimulus -= PulseAmplitude;
            } else {        // PARI
                Stimulus += PulseAmplitude + Step;
            }

        } else if (TypeVoltammetry == SPMeasurementParameterPOT.NORMAL_PULSE_VOLTAMMETRY) {
            if (up && count % 2 == 0 || !up && count % 2 == 1) {     // PARI
                if (up) {
                    totStepUp++;
                    Stimulus = InitialPotential + totStepUp * Step;
                } else {
                    totStepDown++;
                    Stimulus = InitialPotential + Step * (totStepUp - totStepDown);
                }
            } else {        // DISPARI
                Stimulus = InitialPotential;
            }
        } else if (TypeVoltammetry == SPMeasurementParameterPOT.DIFFERENTIAL_PULSE_VOLTAMMETRY) {
            if (count % 2 == 0) { // PARI
                Stimulus += PulseAmplitude;
            } else {        // DISPARI
                Stimulus -= (PulseAmplitude - Step);
            }
        }
        //if (up) {
        Stimulus = (Stimulus > SPMeasurementParameterPOT.Vref_plus ? SPMeasurementParameterPOT.Vref_plus : Stimulus);
        //} else {
        Stimulus = (Stimulus < SPMeasurementParameterPOT.Vref_minus ? SPMeasurementParameterPOT.Vref_minus : Stimulus);
        //}

        count++;
    }

    public boolean restartStimulus() {
        boolean out = false;
        if (TypeVoltammetry == SPMeasurementParameterPOT.NORMAL_PULSE_VOLTAMMETRY) {
            if (up && Step * totStepUp >= (FinalPotential - InitialPotential)
                    || !up && (Step * (totStepUp - totStepDown)) <= 0) {
                out = true;
                count = 0;
                up = !up;
                if (up) {
                    totStepUp = 0;
                } else {
                    totStepDown = 0;
                }
            }
        } else {
            if (this.TypeVoltammetry != SPMeasurementParameterPOT.NORMAL_PULSE_VOLTAMMETRY && (up && Stimulus >= FinalPotential || !up && Stimulus <= FinalPotential)) {

                out = true;
                // Azzerare count
                count = 0;
                // invertire up
                if (alternativeSignal) {
                    // Invertire final e initial
                    double temp = FinalPotential;
                    FinalPotential = InitialPotential;
                    InitialPotential = temp;
                    // Invertire up
                    up = !up;
                    // Invertire step
                    Step = -Step;
                } else {
                    Stimulus = InitialPotential;
                }
            }
        }
        return out;
    }

    public void readingDelay() {
        if (TypeVoltammetry == SPMeasurementParameterPOT.LINEAR_SWEEP_VOLTAMMETRY) {
            // NO DELAY
        } else if (TypeVoltammetry == SPMeasurementParameterPOT.STAIRCASE_VOLTAMMETRY) {
            SPDelay.delay((long) PulsePeriod);
        } else if (TypeVoltammetry == SPMeasurementParameterPOT.SQUAREWAVE_VOLTAMMETRY ||
                TypeVoltammetry == SPMeasurementParameterPOT.NORMAL_PULSE_VOLTAMMETRY ||
                TypeVoltammetry == SPMeasurementParameterPOT.DIFFERENTIAL_PULSE_VOLTAMMETRY) {
            SPDelay.delay((long) PulsePeriod / 2);

        }
    }


    private int bitSignPOT(int x, int numBit) {
        int sign = (int) Math.pow(2, numBit - 1); //RUN4 (numBit - 1)
        if (spConfiguration.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)){
            sign = (int) Math.pow(2, numBit);
        }
        double m = (double) sign / (SPMeasurementParameterPOT.Vref_plus - SPMeasurementParameterPOT.Vref_minus);
        int out = (int) (m * x);
        int _7FF = 0;
        if (spConfiguration.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)){
            _7FF  = (int) Math.pow(2, numBit - 1) - 1; // NON sommare per RUN4
        }
        out = out + _7FF;
        if (spConfiguration.getCluster().getFamilyOfChips().getHW_VERSION().equals(SPFamily.RUN5)){
            out = out >= sign ? (sign - 1) : out;
            out = out < 0 ? 0 : out;
        } else {
            sign = out < 0 ? sign : 0;
            out = Math.abs(out);
            out = out | sign;
        }
        return out;
    }

    public double getStimulus(){
        return Stimulus;
    }

    public double getkADC(){
        return kADC;
    }


    public void updateLog(double[][] output,int[][] read){
        if (measureLog != null) {
            for(int k = 0; k < spMeasurement.getClusterSize(); k++){
                measureLog.println("" + getStimulus() + "; " + getStim2Bit() + "; " +
                        read[k][0] + "; " + output[k][1] + "; " + getkADC() + ";");
                measureLog.flush();
            }
        }
    }
}