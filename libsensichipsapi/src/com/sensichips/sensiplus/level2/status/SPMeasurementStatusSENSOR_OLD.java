package com.sensichips.sensiplus.level2.status;


import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level2.SPMeasurement;
import com.sensichips.sensiplus.level2.parameters.*;

public class SPMeasurementStatusSENSOR_OLD extends SPMeasurementStatus {
    private SPMeasurementParameterEIS subParam = null;
    private SPProtocol spProtocol;
    private SPMeasurementStatus subStatus; // Per getEIS innestate in getSENSOR
    private double[][][] buffer;
    private int dimBuffer = 1;
    private int actualBufferSize = 0;
    private boolean bufferFULL = false;
    private int numOutput = 0;

    public SPMeasurementStatusSENSOR_OLD(SPMeasurement spMeasurement, SPMeasurementParameterSENSORS spParameter, SPProtocol spProtocol, int numOutput,
                                         String measureLogFile, SPMeasurementParameterEIS paramEIS,
                                         SPMeasurementStatus subStatus) {
        super(spMeasurement, spParameter, spProtocol, measureLogFile);
        this.dimBuffer = Integer.parseInt(spParameter.getFilter());;

        this.buffer = new double[spMeasurement.getClusterSize()][numOutput][dimBuffer];
        this.spProtocol = spProtocol;
        this.subParam = paramEIS;
        this.subStatus = subStatus;
        this.numOutput = numOutput;

    }

    public void updateStatus(double[][] output){


        // POST PROCESSING
        actualBufferSize++;
        if (actualBufferSize > dimBuffer) {
            bufferFULL = true;
            actualBufferSize = dimBuffer;
        }

        for(int i = 0; i < spMeasurement.getClusterSize(); i++){
            for (int j = 0; j < numOutput; j++) {
                double sum = 0;
                for (int k = 0; k < actualBufferSize - 1; k++) {
                    if (bufferFULL) {
                        buffer[i][j][k] = output[i][j];
                    }
                    sum += buffer[i][j][k];
                }
                buffer[i][j][actualBufferSize - 1] = output[i][j];
                sum += output[i][j];

                output[i][j] = sum / actualBufferSize;

                if (measureLog != null) {
                    measureLog.println("" + output + "; ");
                    for (int k = 0; k < actualBufferSize; k++) {
                        measureLog.print(buffer[i][j][k] + "; ");
                    }
                    measureLog.println();
                    measureLog.flush();
                }

            }
        }

    }


    public SPMeasurementParameterEIS getSubParam(){
        return subParam;
    }

    public SPMeasurementStatus getSubStatus(){
        return subStatus;
    }

}