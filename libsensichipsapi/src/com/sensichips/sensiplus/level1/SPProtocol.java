package com.sensichips.sensiplus.level1;


import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level0.SPDriverListenerAdapter;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.chip.SPFamily;
import com.sensichips.sensiplus.level1.chip.SPRegisterDump;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.decoder.SPDecoderGenericInstruction;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacket;
import com.sensichips.sensiplus.level1.protocols.util.SPProtocolCache;
import com.sensichips.sensiplus.level1.protocols.util.SPProtocolOut;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


/**
 * LEVEL 0 contains two sublevels: SPProtocol and SPDriver. SPProtocol control the interaction with the bridge connected
 * to the SENSIPLUS. Two known bridges are used at moment: the USB based buspirate v.3.and BLE based TI cc2541.
 * This protocol initialize the bridge in order to communicate with the chip and
 * adapt the general format of the instructions [0x03 r r r r] or [0x48 0x03] into a suitable send and receive byte
 * commands available on the SPDriver services.
 *
 * Property: Sensichips s.r.l.
 *
 * @author: Mario Molinara, mario.molinara@sensichips.com, m.molinara@unicas.it
 */

public abstract class SPProtocol extends SPDriverListenerAdapter {

    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L0;
    public static long SPPROTOCOL_ELAPSED = 0;
    public static int GLOBAL_SENT_COUNTER = 0;
    public static long SPMNEMONIC_ELAPSED = 0;

    protected int queueSize = 4096;
    //protected boolean GENERAL_ERROR = false;
    protected boolean isInitialized = false;



    // Protocols
    public static int SPI = 0;
    public static int I2C = 1;
    public static int SENSIBUS = 2;
    public static int BUSPIRATE = 3;
    public static String PROTOCOL_NAMES[] = {"SPI", "I2C", "SENSIBUS", "BUSPIRATE"};


    public static final int NO_ADDRESS = 0;
    public static final int SHORT_ADDRESS= 1;
    public static final int FULL_ADDRESS = 2;
    public static final String ADDRESSING_TYPES[] = {"NoAddress","ShortAddress","FullAddress"};

    public static final String GENERAL_ERROR_MESSAGE = "Communication error\nPlease, power down usb bridge\n(disconnect and riconnect)\n";
    public static final String LOG_MESSAGE = "SPProtocol";

    protected long timeOutFromDriver = 1000;

    protected String protocolName = "SPProtocol_";

    //protected int addressingType = SPProtocol.NO_ADDRESS;
    protected int addressingMode = SPProtocol.FULL_ADDRESS;

    protected boolean multicastActivated = false;

    protected SPDriver spDriver;
    //private SPConfiguration spConfiguration;
    protected SPProtocolListener spProtocolListener;
    private SPDecoder spDecoder;
    protected SPCluster spCluster;

    protected byte firmwareVersion;

    protected boolean isCacheActivated = true;

    public static ArrayList<String> SPEEDS = new ArrayList<>();
    protected static byte[] SPEED_COMMANDS;

    static{
        SPEEDS.add("1x");
        SPEEDS.add("2x");
        SPEEDS.add("4x");
        SPEEDS.add("8x");
        SPEEDS.add("16x");
        SPEEDS.add("32x");
        SPEED_COMMANDS = new byte[]{(byte)0xFF, (byte)0x80, (byte)0x40, (byte)0x20, (byte)0x10, (byte)0x08};
    }


    public SPProtocol(SPDriver spDriver, SPCluster spCluster, int addressingMode) throws SPProtocolException, SPDriverException {
        this.spDriver = spDriver;
        this.timeOutFromDriver = spDriver.getSPDriverTimeOut();
        this.spDecoder = getSPDecoder();
        this.spCluster = spCluster;
        this.spCluster.setSPChipAllActive();
        init(addressingMode);
    }

    public void sendChipListToMCU() throws SPDriverException, SPProtocolException{}

    public void setSPProtocolListener(SPProtocolListener spProtocolListener){
        this.spProtocolListener = spProtocolListener;
    }

    public SPDriver getSPDriver() {
        return spDriver;
    }

    public int getAddressingMode(){
        return addressingMode;
    }


    public abstract String setSpeed(String speed, SPCluster spCluster) throws SPProtocolException, SPDriverException;

    public abstract void setTk32xToDelete(byte b) throws SPProtocolException, SPDriverException;

    public abstract String testMCU() throws SPProtocolException, SPDriverException;

    public abstract long getADCDelay();

    /**
     * This method read last String received from the attached bus
     *
     * @return the received String
     * @throws SPProtocolException
     */
    //public abstract String read() throws SPProtocolException;

    /**
     * This method allow to send a sequence of bytes through the selected device.
     *
     * @param instruction A sequence of bytes represented as a String like "0x0A 0x03"
     * @param spChip
     * @throws SPProtocolException if any transmission error occur
     */
    public abstract String send(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException, SPDriverException;



    public abstract String resetChips(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException, SPDriverException;

    /**
     * This method initialize the driver defining the
     * used protocol with the chip (SPI, I2C, Sensibus, etc)
     *
     * @throws SPProtocolException if any transmission error occur
     */
    public abstract void init(int addressingMode) throws SPProtocolException, SPDriverException;

    public abstract boolean isResettable();


    /**
     * This method reset the selected driver in order to restart or change with another
     *
     * @return a message produced during reset.
     * @throws SPProtocolException if any transmission error occur
     */
    public abstract String reset() throws SPProtocolException, SPDriverException;

    /**
     * Return true if the init has been invoked. Return false also if reset has been invoked.
     *
     * @return
     */
    public abstract boolean isInitialized() throws SPProtocolException, SPDriverException;

    /**
     * Return the protocol's name.
     *
     * @return The name of the driver.
     * @throws SPProtocolException
     */
    public abstract String getProtocolName() throws SPProtocolException, SPDriverException;

    /**
     * Close all, also low level connection
     *
     * @throws SPProtocolException if some low level problem occur
     */
    public abstract void closeAll() throws SPProtocolException, SPDriverException;




    public abstract void updateSPDriverListener() throws SPProtocolException;


    public abstract String setAddressingType(int mod) throws SPProtocolException, SPDriverException;

    /*
    public void setSPConfiguration(SPConfiguration spConfiguration) throws SPProtocolException {
        if (spConfiguration == null){
            throw new SPProtocolException("SPConfiguration is null!");
        }
        this.spConfiguration = spConfiguration;
    }

    public void setSPCluster(SPCluster spCluster) throws SPProtocolException{
        if (spCluster == null){
            throw new SPProtocolException("spCluster is null!");
        }
        this.spCluster = spCluster;
    }*/


    /**
     * Created by mario on 03/12/2015.
     */
    public class SCProtocol_InitOperation {

        private int expectedLines;
        private long delayBeforeOp;
        private long delayAftereOp;
        private String messageToSend;
        private String responseContains;

        public SCProtocol_InitOperation(String messageToSend, int expectedBytes, long delayBeforeOp, long delayAftereOp, String responseContains) {
           this.messageToSend = messageToSend;
           this.delayBeforeOp = delayBeforeOp;
           this.delayAftereOp = delayAftereOp;
           this.expectedLines = expectedBytes;
           this.responseContains = responseContains;
        }

        public int getExpectedLines() {
   return expectedLines;
        }

        public long getDelayBeforeOp() {
   return delayBeforeOp;
        }

        public long getDelayAftereOp() {
   return delayAftereOp;
        }

        public String getMessageToSend() {
   return messageToSend;
        }

        public String getResponseContains() {
   return responseContains;
        }
    }

    public void activateMulticast() throws SPProtocolException {

        List<String> devices = spCluster.getSPSerialNumbers();

        SPCluster clusterForChangeMulticast = spCluster.generateTempCluster(devices);

        String dataToDIGTest = generateDataToDIGTest();

        sendInstruction("WRITE DIG_CONFIG+1 S " + dataToDIGTest, clusterForChangeMulticast);

        multicastActivated = true;
    }


    public void filterOutUnavailableChipsNew() throws SPProtocolException {
        // Verify CHIP availability

        /*if (!multicastActivated){
            throw new SPProtocolException("Chips detection available only after multicast is activated!");
        }*/

        String dataToDIGTest = generateDataToDIGTest();

        String instruction = "READ DEVICE_ID0 M 6";
        SPProtocolOut out = sendInstruction(instruction);

        List<SPChip> list = spCluster.getActiveSPChipList();
        ArrayList<String> chipToExclude = new ArrayList<>();
        String reverseTokens[] = null;
        String reverse = null;
        for(int i = 0; i < out.recValues.length; i++){
            reverseTokens = out.recValues[i].replace("[","").replace("]", "").
                    toUpperCase().replace("0X", "").trim().split(" ");
            reverse = "";
            for(int j = (reverseTokens.length - 2); j >= 0; j--){
                reverse += reverseTokens[j];
            }

            if (addressingMode == SPProtocol.NO_ADDRESS){
                list.get(0).setSerialNumber("0X" + reverse);
            } else {

                if(reverse.equalsIgnoreCase("FFFFFFFFFF")){
                    chipToExclude.add(list.get(i).getSerialNumber());
                } else if (!reverse.equalsIgnoreCase(list.get(i).getSerialNumber().toUpperCase().replace("0X", ""))){
                    System.out.println("Id found: " + out.recValues[i] + ", id expected: " + list.get(i).getSerialNumber());
                }
            }
        }

        spCluster.setSPChipActivationList(chipToExclude, false);

        if (spCluster.getSPActiveSerialNumbers().size() == 0){
            throw new SPProtocolException("No chip detected. Please verify the cable.");
        }
    }

    public void blinkLEDOnChip(SPChip spchip, long blinkTime)throws SPProtocolException {


        List<String> tempChipList = new ArrayList<>();
        tempChipList.add(spchip.getSerialNumber());
        SPCluster temporary = spCluster.generateTempCluster(tempChipList);
        sendInstruction("WRITE INT_CONFIG+1 S 0x02", temporary, 0);
        SPDelay.delay(blinkTime);
        sendInstruction("WRITE INT_CONFIG+1 S 0x00", temporary, 0);
    }


    public SPChip findChipOnTheCable(List<SPChip> alreadyInstalledChips) throws SPProtocolException {
        // Verify CHIP availability

        if (!multicastActivated){
            //throw new SPProtocolException("Activate multicast before searching chips on the cable");
        }

        //String dataToDIGTest = generateDataToDIGTest();

        String instruction = "READ DEVICE_ID0 M 6";
        SPProtocolOut out = sendInstruction(instruction);
        List<SPChip> list = spCluster.getChipList();

        String reverseTokens[] = null;
        String reverse = null;

        SPChip foundChip = null;

        for(int i = 0; i < out.recValues.length; i++){
            reverseTokens = out.recValues[i].replace("[","").replace("]", "").
                    toUpperCase().replace("0X", "").trim().split(" ");
            reverse = "";
            for(int j = (reverseTokens.length - 2); j >= 0; j--){
                reverse += reverseTokens[j];
            }

            if(!reverse.equalsIgnoreCase("FFFFFFFFFF")){
                System.out.println(reverse);
                boolean alreadyFound = false;
                for(int j=0; j<alreadyInstalledChips.size(); j++){
                    if(list.get(i).getSerialNumber().toUpperCase().replace("0X","").equalsIgnoreCase(alreadyInstalledChips.get(j).getSerialNumber().toUpperCase().replace("0X", ""))){
                        alreadyFound = true;
                        break;
                    }
                }
                if(!alreadyFound) {
                    foundChip = list.get(i);
                    if (!reverse.equalsIgnoreCase(list.get(i).getSerialNumber().toUpperCase().replace("0X", ""))) {
                        System.out.println("Id found: " + out.recValues[i] + ", id expected: " + list.get(i).getSerialNumber());

                    }
                    break;
                }
                if (!reverse.equalsIgnoreCase(list.get(i).getSerialNumber().toUpperCase().replace("0X", ""))) {
                    System.out.println("Id found: " + out.recValues[i] + ", id expected: " + list.get(i).getSerialNumber());

                }
            }
        }

        return foundChip;
    }



    /*
    public void filterOutUnavailableChips() throws SPProtocolException {
        // Verify CHIP availability

        if (addressingMode == SPProtocol.NO_ADDRESS){
            return;
        }

        if (!multicastActivated){
            throw new SPProtocolException("Unavailable chips detection available only after multicast activation.");
        }

        String dataToDIGTest = generateDataToDIGTest();

        String instruction = "READ DIG_CONFIG+1 S 1";
        SPProtocolOut out = sendInstruction(instruction);
        List<SPChip> list = spCluster.getActiveSPChipList();
        ArrayList<String> chipToExclude = new ArrayList<>();
        for(int i = 0; i < out.recValues.length; i++){
            out.recValues[i] = out.recValues[i].replace("[","").replace("]", "").toUpperCase();
            if(!out.recValues[i].equalsIgnoreCase(dataToDIGTest)){
                chipToExclude.add(list.get(i).getSerialNumber());
            }
        }

        spCluster.setSPChipActivationList(chipToExclude, false);

        if (spCluster.getSPActiveSerialNumbers().size() == 0){
            throw new SPProtocolException("No chip detected. Please verify the cable.");
        }
    }*/

    private String generateDataToDIGTest() throws SPProtocolException {
        String dataToDIGTest = null;

        String multicastAddress = spCluster.getMulticastChip().getAddress(SPProtocol.SHORT_ADDRESS, SPProtocol.PROTOCOL_NAMES[SPProtocol.SENSIBUS]);
        int MCA_to_DIG_CONFIG = Integer.parseInt(multicastAddress, 16) - 2;

        // Zero is the default value.
        if (MCA_to_DIG_CONFIG != 0) {
            dataToDIGTest = Integer.toHexString(MCA_to_DIG_CONFIG).toUpperCase();

            if (dataToDIGTest.length() == 1){
                dataToDIGTest = "0" + dataToDIGTest;
            }
            dataToDIGTest = "0X" + dataToDIGTest;
        }

        return dataToDIGTest;
    }

    public abstract float elapsedStartComResponse() throws SPProtocolException, SPDriverException;



    /**
     * Generate the suitable decoder
     * @return
     */
    public abstract SPDecoder getSPDecoder() throws SPProtocolException;

    public void clearCache(){
        SPProtocolCache.clearCache();
    }


    public void setCacheActivated(boolean activateCache){
        isCacheActivated = activateCache;
    }

    public SPProtocolOut sendInstruction(String mnemonic) throws SPProtocolException {
        return sendInstruction(mnemonic, spCluster, 0);
    }

    public SPProtocolOut sendInstruction(String mnemonic, SPCluster cluster) throws SPProtocolException {
        return sendInstruction(mnemonic, cluster, 0);
    }

    public abstract byte[] sendPacket(SPProtocolPacket packet) throws SPDriverException, SPProtocolException;

    public synchronized SPProtocolOut sendInstruction(String mnemonic, SPCluster cluster, long delayBetweeninstruction) throws SPProtocolException {
        // called for single mnemonic instruction

        boolean writeMulticast = cluster == null;

        //System.out.println(mnemonic);

        if (cluster == null){
            cluster = spCluster;
        }

        long start = System.currentTimeMillis();

        if (cluster == null){
            throw new SPProtocolException("Any chip defined in spChipID into SPMnemonic");
        }
        List<SPChip> spChipList = cluster.getActiveSPChipList();
        SPProtocolOut output = new SPProtocolOut();
        output.mnemonicInstruction = mnemonic;

        SPChip chipForMulticast = null;
        if (multicastActivated){
            chipForMulticast = cluster.getMulticastChip();
        } else {
            chipForMulticast = cluster.getBroadcastSlowChip();
        }
        try {
            // In I2C l'indirizzo è codificato nell'istruzione
            SPDecoderGenericInstruction instruction = null;
            instruction = spDecoder.decodeSingleInstruction(mnemonic, chipForMulticast);

            if ((SPLogger.DEBUG_TH & SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE) != 0){
                SPLogger.getLogInterface().d(LOG_MESSAGE, mnemonic, SPLoggerInterface.DEBUG_VERBOSITY_READ_WRITE);
            } else {
                SPLogger.getLogInterface().d(LOG_MESSAGE, mnemonic, DEBUG_LEVEL);
            }

            if (instruction.isWrite() && writeMulticast){
                // In SENSIBUS l'indirizzo è utilizzato dal protocol (I2C e SPI lo ignoreranno)
                SPPROTOCOL_ELAPSED += (System.currentTimeMillis() - start);

                GLOBAL_SENT_COUNTER++;
                //protocol.send(instruction, cluster.getMulticastChip());

                String response = null; //
                if (isCacheActivated){
                    response = SPProtocolCache.checkInstruction(instruction.getInstruction());
                }

                if (response == null){
                    response = send(instruction, chipForMulticast);
                } else {
                    SPLogger.getLogInterface().d(LOG_MESSAGE, "Cache hit for: " + response, DEBUG_LEVEL);
                }
                output.recValues = new String[]{response};
                SPLogger.getLogInterface().d(LOG_MESSAGE, output.recValues[0], DEBUG_LEVEL);

                start = System.currentTimeMillis();
                output.sentInstructions =  new String[]{instruction.getDecodedInstruction()};

            } else {
                // MNEMONIC UNICAST READ OR WRITE IS THE SAME
                SPPROTOCOL_ELAPSED += (System.currentTimeMillis() - start);

                SPDecoderGenericInstruction instructions[] = spDecoder.decodeMultipleInstructions(mnemonic.toUpperCase(), cluster);

                start = System.currentTimeMillis();

                output.recValues = new String[spChipList.size()];
                output.sentInstructions = new String[spChipList.size()];
                for(int i = 0; i < spChipList.size(); i++){
                    SPPROTOCOL_ELAPSED += (System.currentTimeMillis() - start);

                    GLOBAL_SENT_COUNTER++;
                    output.recValues[i] = send(instructions[i], spChipList.get(i));
                    //output.recValues[i] = protocol.read();

                    SPDelay.delay(delayBetweeninstruction);

                    SPLogger.getLogInterface().d(LOG_MESSAGE, output.recValues[i], DEBUG_LEVEL);

                    start = System.currentTimeMillis();
                    output.sentInstructions[i] = instructions[i].getDecodedInstruction();
                }

            }

            SPPROTOCOL_ELAPSED += (System.currentTimeMillis() - start);

        } catch (SPProtocolException e) {
            throw e;
        } catch (Exception e) {
            throw new SPProtocolException("Generic exception: " + e.getMessage());
        }

        return output;
    }


    public SPRegisterDump getSPRegisterDump(SPCluster spCluster) throws SPProtocolException {

        if (spCluster == null){
            throw new SPProtocolException("SPMnemonic.getSPRegisterDump invalid SPCluster null value!");
        }

        if (spCluster.getSPActiveSerialNumbers().size() != 1){
            throw new SPProtocolException("SPMnemonic.getSPRegisterDump is possible with cluster with size equal to one!");
        }


        SPRegisterDump dump = new SPRegisterDump(spCluster.getFamilyOfChips().getHW_VERSION());
        String[] instruction = dump.generateInstructionToRefresh(SPFamily.RUN5);
        String tokens[], lsb, msb;
        for(int i = 0; i < instruction.length; i++)  {
            SPProtocolOut out = sendInstruction(instruction[i], spCluster);
            out.recValues[0] = out.recValues[0].replace("[", "").replace("]", "");
            tokens = out.recValues[0].split(" ");
            dump.getSPRegister(spCluster.getFamilyOfChips().getHW_VERSION(), instruction[i].split(" ")[1].trim()).setLSB(tokens[0]);
            if (tokens.length > 1){
                dump.getSPRegister(spCluster.getFamilyOfChips().getHW_VERSION(), instruction[i].split(" ")[1].trim()).setMSB(tokens[1]);
            }
        }
        return dump;
    }




    /**
     * Sistema Produttore consumatore per l'interfacciamento verso l'USB.
     *
     * @param <T> Generico elemento del buffer sincronizzato.
     */
    public static class SCDriver_ReceiverQueue<T> {
        private T v[];
        private int contatore = 0, preleva = 0, inserisci = 0;
        private boolean interrupt = false;
        private Class<T> typeOfElement;
        private int queueSize;


        public SCDriver_ReceiverQueue(Class<T> c, int queueSize) {
            //v = new String[SCDriverFT232RL_Proxy.MAX_SIZE_BUFFER];
            this.queueSize = queueSize;
            v = (T[]) Array.newInstance(c, queueSize);
            typeOfElement = c;
            //v = (T[]) new Object[];
        }

        public synchronized void setInterrupt(boolean flag) {
            this.interrupt = flag;
            notify();
        }

        public synchronized boolean getAndResetInterrupt() {
            boolean output = interrupt;
            if (interrupt) {
                reset();
                interrupt = false;
            }
            return output;
        }


        public synchronized void reset() {
            contatore = 0;
            preleva = 0;
            inserisci = 0;
        }

        public synchronized void put(T o) throws SPProtocolException {
            while (contatore == queueSize) { // Queue is full
                try {
                    wait();
                } catch (InterruptedException e) {
                    //SPLogger.getLogInterface().d(LOG_MESSAGE, "InterruptedException in inserisci(T o) ", DEBUG_LEVEL);
                    //GENERAL_ERROR = true;
                    throw new SPProtocolException(e.getMessage());
                }
            }
            //if (!interrupt){
            v[inserisci] = o;
            inserisci = (inserisci + 1) % queueSize;
            contatore++;
            notify();
            //}
        }

        /**
         * @param timeout,
         * @return
         * @throws InterruptedException
         */
        public synchronized T get(long timeout) throws SPProtocolException {
            long elapsedTime = System.currentTimeMillis();

            if (contatore == 0) { // Empty queue
                try {

                    wait(timeout);
                    elapsedTime = System.currentTimeMillis() - elapsedTime;
                } catch (InterruptedException e) {
                    SPLogger.getLogInterface().d(LOG_MESSAGE, "InterruptedException in T preleva(long timeout) " + timeout, DEBUG_LEVEL);
                    //annullaGeneralError = stato;
                    //GENERAL_ERROR = true;
                    throw new SPProtocolException("InterruptedException T preleva(long timeout), " + e.getMessage());
                }
                if (elapsedTime >= timeout) {
                    //SPLogger.d(TAG_SERVICE_GENERAL_ERROR, "Timeout exception in preleva(): " + timeout);
                    //annullaGeneralError = stato;
                    //GENERAL_ERROR = true;
                    //    System.out.println("Timeout in communication ... ");
                    throw new SPProtocolException("Timeout in communication: (" + (timeout / 1000) + "s)");
                }
                if (contatore == 0)
                    return null;
            }

            T out = v[preleva];
            preleva = (preleva + 1) % queueSize;
            contatore--;
            notify();
            return out;
        }
    }
}
