package com.sensichips.sensiplus.level1.chip;


import com.sensichips.sensiplus.level1.SPProtocolException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marco Ferdinandi on 27/02/2017.
 */
public class SPFamily implements Serializable {

    public static final long serialVersionUID = 42L;
    private String name;
    private String id;

    public final static String RUN4 = "RUN4";
    public final static String RUN5 = "RUN5";

    public static final int MEASURE_TYPE_EIS = 0;
    public static final int MEASURE_TYPE_POT = 1;
    public static final int MEASURE_TYPE_ULTRASOUND = 2;
    public static final int MEASURE_TYPE_ENERGY_SPECTROSCOPY = 3;
    public final static String[] MEASURE_TYPES_AVAILABLE = new String[]{"EIS", "POT", "ULTRASOUND", "ENERGY_SPECTROSCOPY"};


    private String HW_VERSION = RUN4;
    private int SYS_CLOCK = 10000000; // -- [Hz]
    private String OSC_TRIM = "0x06"; // -- trim the internal Oscillator

    private String broadcastAddress;
    private String broadcastSlowAddress;
    private String multicastDefaultAddress;

    private List<SPSensingElementOnFamily> sensingElementOnFamilyList;

    public void addSPSensingElementOnFamily(SPSensingElementOnFamily item){
        sensingElementOnFamilyList.add(item);
    }


    private ArrayList<String> measureTypes = new ArrayList<>();
    private List<SPAnalyte> analyteList;/*Analytes which this family is able to detect*/


    public SPFamily() throws SPProtocolException {
        this.name = null;
        this.id = null;
        this.sensingElementOnFamilyList = new ArrayList<SPSensingElementOnFamily>();
        analyteList = new ArrayList<>();
    }

    public void setMulticastDefaultAddress(String address){
        multicastDefaultAddress = address;
    }

    public void setBroadcastAddress(String address){
        this.broadcastAddress = address;
    }

    public void setBroadcastSlowAddress(String address){
        this.broadcastSlowAddress = address;
    }

    public String getBroadcastAddress() {
        return broadcastAddress;
    }

    public String getBroadcastSlowAddress() {
        return broadcastSlowAddress;
    }

    public ArrayList<String> getMeasureTypes(){
        return measureTypes;
    }

    public static boolean isMeasureTypeCorrect(String measureType){
        int i = 0;
        boolean found = false;
        while(i < MEASURE_TYPES_AVAILABLE.length && !found){
            if (!(found = MEASURE_TYPES_AVAILABLE[i].equals(measureType))){
                i++;
            }
        }
        return found;
    }

    public void addMeasureType(String measureType) throws SPProtocolException {

        if (!isMeasureTypeCorrect(measureType)){
            String message = "SPFamily: wrong measure type: " + measureType + "\nMessage type available: ";
            for(int j = 0; j < MEASURE_TYPES_AVAILABLE.length; j++){
                message += MEASURE_TYPES_AVAILABLE[j] + ";";
            }
            throw new SPProtocolException(message);
        }
        measureTypes.add(measureType);

    }


    public void setSYS_CLOCK(int SYS_CLOCK){
        this.SYS_CLOCK = SYS_CLOCK;
    }

    public void setHW_VERSION(String HW_VERSION) throws SPProtocolException {
        if (!(HW_VERSION == null || HW_VERSION.equals(RUN4) || HW_VERSION.equals(RUN5))){
            throw new SPProtocolException("HW_VERSION " + HW_VERSION + " unavailable!\nVersion available: " + RUN4);
        }
        this.HW_VERSION = HW_VERSION;
    }


    public String getHW_VERSION(){
        return HW_VERSION;
    }

    public int getSYS_CLOCK(){
        return SYS_CLOCK;
    }

    public String getOSC_TRIM() throws SPProtocolException {
        Hex_Verifier(OSC_TRIM);
        return OSC_TRIM;
    }
    public void setOSC_TRIM(String OSC_TRIM){
        this.OSC_TRIM = OSC_TRIM;
    }




    public List<SPAnalyte> getAnalyteList() {
        return analyteList;
    }

    public void setAnalyteList(List<SPAnalyte> analyteList) {
        this.analyteList = analyteList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) throws SPProtocolException{
        id = id.toUpperCase();
        try{
            Hex_Verifier(id);
        } catch(SPProtocolException e){
            throw new SPProtocolException("Wrong Family id format: " + id + "\nHex expected.");
        }
        this.id = id.replace("0X", "");
    }

    public List<SPSensingElementOnFamily> getSensingElementOnFamilyList() {
        return sensingElementOnFamilyList;
    }

    public SPSensingElementOnFamily getSPSensingElementOnFamily(String sensingElement) throws SPProtocolException {
        int i = 0;
        boolean found = false;
        while(i < sensingElementOnFamilyList.size() && !found){
            if(!(found = sensingElementOnFamilyList.get(i).getID().equalsIgnoreCase(sensingElement))){
                i++;
            }
        }
        if (!found){
            throw new SPProtocolException("SENSING_ELEMENT UNAIVALABLE: " + sensingElement);
        }
        return sensingElementOnFamilyList.get(i);
    }

    public void setSensingElementOnFamilyList(List<SPSensingElementOnFamily> sensingElementOnFamilyList) {
        this.sensingElementOnFamilyList = sensingElementOnFamilyList;
    }

    public static void Hex_Verifier(String id) throws SPProtocolException {
        if (id == null){
            throw new SPProtocolException("Wrong Family id format: " + id);
        }
        id = id.toUpperCase();
        if (!id.startsWith("0X") || id.length() != 4) {
            throw new SPProtocolException("Wrong Family id format: " + id);
        }
        id = id.replace("0X","");

        try {
            Integer.parseInt(id, 16); // To verify if is an hexadecimal value
        } catch (Exception e){
            //System.out.println(e.getMessage());
            throw new SPProtocolException("Wrong Family id format: " + id);
        }
    }


    public String toString() {
        String out = "";
        out += "Family name: " + name + ", id: " + id;

        return out;
    }
}
