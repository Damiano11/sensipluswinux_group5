package com.sensichips.sensiplus.level1.chip;


import com.sensichips.sensiplus.level1.SPProtocolException;

import java.io.Serializable;

/**
 * Created by mario on 10/01/17.
 */
public class SPRegisterDumpItem implements Serializable {

    public static final long serialVersionUID = 42L;

    private SPRegisterListItem spRegisterListItem;
    private String MSB = "";
    private String LSB = "";

    public SPRegisterDumpItem(SPRegisterListItem spRegisterListItem, String MSB, String LSB) throws SPProtocolException {
        this.spRegisterListItem = spRegisterListItem;
        this.MSB = MSB.toUpperCase();
        this.LSB = LSB.toUpperCase();

        if (MSB == null || !SPRegisterDump.HexVerifier(MSB)){
            throw new SPProtocolException("SPRegisterDumpItem constructor. Wrong format for MSB: " + MSB);
        } else if (!SPRegisterDump.HexVerifier(LSB)){
            throw new SPProtocolException("SPRegisterDumpItem constructor. Wrong format for LSB: " + LSB);
        }

    }

    public SPRegisterListItem getSpRegisterListItem() {
        return spRegisterListItem;
    }

    public void setSpRegisterListItem(SPRegisterListItem spRegisterListItem) {
        this.spRegisterListItem = spRegisterListItem;
    }

    public String getMSB() {
        return MSB;
    }

    public void setMSB(String MSB) {
        this.MSB = MSB;
    }

    public String getLSB() {
        return LSB;
    }

    public void setLSB(String LSB) {
        this.LSB = LSB;
    }

}
