package com.sensichips.sensiplus.level1.chip;

import java.io.Serializable;

/**
 * Created by Marco Ferdinandi on 20/02/2017.
 */
public class SPCalibrationParameters implements Serializable {

    public static final long serialVersionUID = 42L;

    /*
     * Linear Transformation:
     * Y=X*M+N
     */

    private double m;
    private double n;
    private double threshold;

    public SPCalibrationParameters(){
        this.m = 1;
        this.n = 0;
        this.threshold = (m-n)/2;
    }

    public SPCalibrationParameters(double m, double n, double threshold){
        this.m = m;
        this.n = n;
        this.threshold = threshold;
    }

    public double getM() {
        return m;
    }

    public void setM(double m) {
        this.m = m;
    }

    public double getN() {
        return n;
    }

    public void setN(double n) {
        this.n = n;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public String toString(){
        String out = "";
        out += "\t\t\tm: " + m + "\n";
        out += "\t\t\tn: " + n + "\n";
        return out;
    }

}
