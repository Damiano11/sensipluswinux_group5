package com.sensichips.sensiplus.level1.chip;

import com.sensichips.sensiplus.util.SPDataRepresentationManager;

import java.io.Serializable;

/**
 * Created by Marco on 01/02/2017.
 */
public class SPSensingElement implements Serializable {

    public static final long serialVersionUID = 42L;

    //public static final String[] sensorTypeLabels = new String[]{"POTENTIOMETRIC", "CURRENT", "ONCHIP_TEMPERATURE", "ONCHIP_VOLTAGE",
    //        "OFFCHIP_ESLI", "OFFCHIP_BEND","OFFCHIP_VOC", "OFFCHIP_CARBONDIOXIDE", "OFFCHIP_FIREDETECTOR"};

    //eis param
    private String rsense = "";
    private String name = "";
    private String inGain = "";
    private String outGain = "";
    private String contacts = "";
    private float frequency = 0;
    private String Harmonic = "";
    private int DCBiasP = 0;
    private int DCBiasN = 0;
    private String modeVI = "";
    private String measureTechnique = "";
    private String measureType = "";
    private String filter = "";
    private String phaseShiftMode = "";
    private int phaseShift = 0;
    private String iq = "";
    //adc param
    private int conversionRate = 0;
    private int NData = 0;
    private String inportADC = "";

    // TODO: evaluate indexToReduce
    private SPDataRepresentationManager spDataRepresentationManager = new SPDataRepresentationManager(0);

    public String getMeasureTechnique() {
        return measureTechnique;
    }

    public void setMeasureTechnique(String measureTechnique) {
        this.measureTechnique = measureTechnique;
    }


    public String getRsense() {
        return rsense;
    }

    public void setRsense(String rsense) {
        this.rsense = rsense;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInGain() {
        return inGain;
    }

    public void setInGain(String inGain) {
        this.inGain = inGain;
    }

    public String getOutGain() {
        return outGain;
    }

    public void setOutGain(String outGain) {
        this.outGain = outGain;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public float getFrequency() {
        return frequency;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    public String getHarmonic() {
        return Harmonic;
    }

    public void setHarmonic(String harmonic) {
        Harmonic = harmonic;
    }

    public String getModeVI() {
        return modeVI;
    }

    public void setModeVI(String modeVI) {
        this.modeVI = modeVI;
    }

    public int getDCBiasP() {
        return DCBiasP;
    }

    public void setDCBiasP(int DCBiasP) {
        this.DCBiasP = DCBiasP;
    }


    public int getDCBiasN() {
        return DCBiasN;
    }

    public void setDCBiasN(int DCBiasN) {
        this.DCBiasN = DCBiasN;
    }


    public String getMeasureType() {
        return measureType;
    }

    public void setMeasureType(String measureType) {
        this.measureType = measureType;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getPhaseShiftMode() {
        return phaseShiftMode;
    }

    public void setPhaseShiftMode(String phaseShiftMode) {
        this.phaseShiftMode = phaseShiftMode;
    }

    public int getPhaseShift() {
        return phaseShift;
    }

    public void setPhaseShift(int phaseShift) {
        this.phaseShift = phaseShift;
    }

    public String getIq() {
        return iq;
    }

    public void setIq(String iq) {
        this.iq = iq;
    }

    public int getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(int conversionRate) {
        this.conversionRate = conversionRate;
    }

    public int getNData() {
        return NData;
    }

    public void setNData(int NData) {
        this.NData = NData;
    }

    public String getInportADC() {
        return inportADC;
    }

    public void setInportADC(String inportADC) {
        this.inportADC = inportADC;
    }


    public SPDataRepresentationManager getSpDataRepresentationManager() {
        return spDataRepresentationManager;
    }

    public String toString(){
        String out = "";
        out += "\t\t\t\trsense:            " + rsense + "\n";
        out += "\t\t\t\tname:              " + name + "\n";
        out += "\t\t\t\tinGain:            " + inGain + "\n";
        out += "\t\t\t\toutGain:           " + outGain + "\n";
        out += "\t\t\t\tcontacts:          " + contacts + "\n";
        out += "\t\t\t\tfrequency:         " + frequency + "\n";
        out += "\t\t\t\tHarmonic:          " + Harmonic + "\n";
        out += "\t\t\t\tDCBiasP:            " + DCBiasP + "\n";
        out += "\t\t\t\tmodeVI:            " + modeVI + "\n";
        out += "\t\t\t\tmeasureTechnique:   " + measureTechnique + "\n";
        out += "\t\t\t\tmeasureType:       " + measureType + "\n";
        out += "\t\t\t\tfilter:            " + filter + "\n";
        out += "\t\t\t\tphaseShiftMode:    " + phaseShiftMode + "\n";
        out += "\t\t\t\tphaseShift:        " + phaseShift + "\n";
        out += "\t\t\t\tiq:                " + iq + "\n";
        out += "\t\t\t\tconversionRate:    " + conversionRate + "\n";
        out += "\t\t\t\tNData:             " + NData + "\n";
        out += "\t\t\t\tinportADC:         " + inportADC + "\n";
        return out;
    }
}
