package com.sensichips.sensiplus.level1.decoder;

import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.chip.SPRegisterList;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;


/**
 * Property: Sensichips s.r.l.
 * This class work on "menonic" SENSIPLUS instruction like:
 * WRITE CHA1_DIVIDER 	M 0xFEDC
 * WRITE CHA1_CONFIG 	M 0x3210
 * WRITE CHA1_CONFIG 	M 0x7654
 * WRITE CHA1_CONDIT 	M 0xBA98
 * WRITE CHA1_FILTER 	M 0xFEDC
 * WRITE CHA1_SELECT 	M 0x3210
 *
 * producing a generalized representation based on SPDecoderGenericInstruction class.
 * The conversion from generalized representation and the sequence of bytes suitable for the selected protocol (SPI, ...)
 * has to be included in the extension of this Class by overriding the Instruction2String method.
 *
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public abstract class SPDecoder {


    public static String LOG_MESSAGE = "SPDecoder";
    public static String DELIM_COMMENTS = "--";

    public static long DECODE_SINGLE_INSTRUCTION_ELAPSED = 0;

    private final static int INSTRUCTION = 1;
    //private final static int DEVICE_ADDRESS = 2;
    private final static int REGISTER_ADDRESS = 3;
    private final static int MULTIPLE_ADDRESS = 4;
    private final static int DATA_OR_READ_NUMBER = 5;


    //private static Hashtable<String, String> tabRegister = new Hashtable<String, String>();
    //private static Hashtable<String, String> tabDevices = new Hashtable<String, String>();


    protected final static String NOT_FOUND = "NOT_FOUND";


    public SPDecoder() throws SPProtocolException {

    }

    /**
     * Overriding this method is possible to define the transcoding rule between SPDecoderGenericInstruction generic representation
     * of the mnemonic instruction and
     *
     * @param instruction is the generic representation of the mnemonic instruction
     * @return A String representing the sequence of bytes ready to send to the microcontroller that will convert into
     * suitable serial protocol (SPI, I2C, etc.)
     * @throws SPProtocolException Generated when the instruction object contains errors.
     */
    public abstract void Instruction2String(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException;


    /**
     * Starting from a batch file, this method produce a list of generalized Instruction object.
     * Example of batch file:
     * --
     * --	        SENSIPLUS RUN2
     * --
     * --     	  Test Registers Write & Read
     * --
     * --  A comment start with this sequence: --
     * WRITE COMMAND		S 0x00 	-- chip soft reset because there is an issue with the internal POR
     * --
     * WRITE INT_CONFIG 	M 0x3210
     * WRITE ANA_CONFIG 	M 0x7654
     * WRITE DIG_CONFIG 	M 0x3A98
     * --
     * WRITE CHA1_DIVIDER 	M 0xFEDC
     * WRITE CHA1_CONFIG 	M 0x3210
     * WRITE CHA1_CONFIG 	M 0x7654
     * WRITE CHA1_CONDIT 	M 0xBA98
     * WRITE CHA1_FILTER 	M 0xFEDC
     * WRITE CHA1_SELECT 	M 0x3210
     * --
     * WRITE DDS_DIVIDER 	M 0xFEDC
     * WRITE DDS_FIFO  	M 0x3210
     * WRITE DDS_CONFIG 	M 0x7654
     * WRITE DAS_CONFIG 	M 0xBA98
     * WRITE DSS_SELECT 	M 0xFEDC
     * WRITE TOF_DIVIDER 	M 0x3210
     * WRITE TIMER_DIVIDER 	M 0x7654
     * WRITE PPR_THRESH 	M 0xBA98
     * WRITE PPR_THRESH 	M 0xFEDC
     * WRITE PPR_SELECT 	M 0x3210
     * --
     * WRITE SENSIBUS_SET	M 0x5A5A
     * --
     * READ  STATUS		M     64 -- read them all back
     *
     * @param fileInstructions The batch file with "high level instruction"
     * @return A list of generic object Instruction that has to be decoded specifically for I2C, SPI, etc.
     * @throws SPProtocolException If some error occur during the file reading (file unavailable,
     *                             sintax error in the instructions, etc.)
     */
    public ArrayList<SPDecoderGenericInstruction> decodeFileAndLoad(InputStream fileInstructions, SPChip spChip) throws SPProtocolException {
        /* ******
         * read single line in a instructions file, and it calls
		 * decodeInstructionHigh*****
		 */
        //SPChip spChip = new SPChip(SPChip.ID_SENSIBUS_BROADCAST, SPInitialization.SENSIBUS);
        BufferedReader br = null;
        ArrayList<SPDecoderGenericInstruction> instructions = new ArrayList<SPDecoderGenericInstruction>();
        try {
            br = new BufferedReader(new InputStreamReader(fileInstructions,
                    "UTF-8"));

            String line;
            int numberline = 0;
            SPDecoderGenericInstruction instruction = null;

            while ((line = br.readLine()) != null) {
                line = line.toUpperCase().trim();
                //SPLogger.d("LOADING_INSTRUCTION", line);
                numberline++;

                if ((!line.startsWith(DELIM_COMMENTS)) && !line.isEmpty()) {
                    // if line starts with a comment or it's empty, have to do
                    // nothing and jump to next iteration of while

                    instruction = decodeSingleInstruction(line, numberline, spChip);
                    instructions.add(instruction);

                }

            }// end while

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return instructions;

    } // end decodeFileAndExecute


    /**
     * Decode single instruction (e.g. instruction inserted manually or
     * single line read in a instruction file via decodeFileAndExecute
     *
     * @param line Is a single line like "WRITE DSS_SELECT 	M 0xFEDC". The method produce a general
     *             structure SPDecoderGenericInstruction that will be decoded in a sequence of bytes
     * @return
     * @throws SPProtocolException
     */
    public SPDecoderGenericInstruction[] decodeMultipleInstructions(String line, SPCluster cluster) throws SPProtocolException {
        return decodeMultipleInstructions(line, -1, cluster);
    }



    public SPDecoderGenericInstruction[] decodeMultipleInstructions(String line, int lineNumber, SPCluster cluster) throws SPProtocolException {
        if (cluster == null || cluster.getActiveSPChipList().size() == 0){
            return new SPDecoderGenericInstruction[]{decodeSingleInstruction(line, lineNumber, null)};
        } else {
            SPDecoderGenericInstruction[] output = new SPDecoderGenericInstruction[cluster.getActiveSPChipList().size()];
            for(int i = 0; i < cluster.getActiveSPChipList().size(); i++){
                output[i] = decodeSingleInstruction(line, lineNumber, cluster.getActiveSPChipList().get(i));
            }
            return output;
        }
    }



    /**
     * Decode single instruction (e.g. instruction inserted manually or
     * single line read in a instruction file via decodeFileAndExecute
     *
     * @param line The line to decode (e.g. WRITE

     * @return An generic instance of SPDecoderGenericInstruction
     * @throws SPProtocolException
     */
    public SPDecoderGenericInstruction decodeSingleInstruction(String line, SPChip spChip) throws SPProtocolException {
        return decodeSingleInstruction(line, -1, spChip);
    }


    //public SPDecoderGenericInstruction decodeSingleInstruction(String line) throws SPProtocolException {
    //    return decodeSingleInstruction(line, -1, getBroadcastID());
    ///}


    public SPDecoderGenericInstruction decodeSingleInstruction(String line, int lineNumber, SPChip spChip) throws SPProtocolException {


        long start = System.currentTimeMillis();

        int counter = 0; /* token number */

        boolean fakeLineNumber = true; // used in exception messageToSend
        if (lineNumber > 0)
            fakeLineNumber = false;

        if (line.contains(DELIM_COMMENTS)) { // discards comments
            String[] linesRead = line.split(DELIM_COMMENTS);
            line = linesRead[0].trim(); // linesRead[0] it' s to LEFT (before)
            // to comment's delimitator

        }

        StringTokenizer st = new StringTokenizer(line); // split in words
        // (default delimitator=
        // whitespace)

        SPDecoderGenericInstruction instruction = new SPDecoderGenericInstruction(line);
        instruction.setWrite(false);
        instruction.setDeviceEx(null);
        instruction.setMultiple(null);
        instruction.setRegisterEx(null);

        instruction.setDati(line); // INSERT MNEMONIC COMMAND (E.G. send
        // int_config m 0x1234) as first element of
        // array list dati


        counter = 0;
        while (st.hasMoreTokens()) {
            String token = st.nextToken().trim();
            counter++;

            switch (counter) {
                case INSTRUCTION: { // send o read

                    counter++;

                    if (token.equalsIgnoreCase("WRITE")) {
                        instruction.setWrite(true);
                    } else if (token.equalsIgnoreCase("READ")) {
                        instruction.setWrite(false);
                        instruction.setDati("r");
                    } else { // error on first element of instruction (send or read
                        if (!fakeLineNumber)
                            throw new SPProtocolException("\nError on line n. "
                                    + lineNumber + " : " + line + " :\n" + token
                                    + " is an operation not identified");
                        else
                            throw new SPProtocolException("\nError on line : " + line
                                    + " :\n" + token
                                    + " is an operation not identified");
                    }
                } // end case INSTRUCTION
                break;
                case REGISTER_ADDRESS: {
                    boolean unoFlag = false;
                    if (token.endsWith("+1")) { // have to increase of one the
                        // exadecimal code's register
                        token = token.substring(0, token.length() - 2);// delete +1
                        unoFlag = true;
                    }

                    try {
                        instruction.setRegisterEx(SPRegisterList.getSPRegister(spChip.getSpFamily().getHW_VERSION(),token).getAddress());
                    } catch (SPProtocolException e) {
                        throw new SPProtocolException(e.getMessage());
                    }
                    //instruction.setRegisterEx(this.searchTable(token, tabRegister));

                    if (instruction.getRegisterEx().equals(NOT_FOUND)) {
                        if (!fakeLineNumber)
                            throw new SPProtocolException("\nError on line n. "
                                    + lineNumber + " : " + line + " :\n" + token
                                    + " is not found");
                        else
                            throw new SPProtocolException("\nError on line : " + line
                                    + " :\n" + token + " is not found");
                    } else if (unoFlag) {
                        int intero = Integer.parseInt(instruction.getRegisterEx()
                                .substring(2), 16);
                        intero++;
                        String stringIntero = Integer.toHexString(intero)
                                .toUpperCase();
                        if (stringIntero.length() == 1)
                            instruction.setRegisterEx("0x0" + stringIntero);
                        else
                            // stringIntero.length()==2
                            instruction.setRegisterEx("0x" + stringIntero);

                    }

                }
                break;
                case MULTIPLE_ADDRESS: {
                    if (token.equalsIgnoreCase("M") || token.equalsIgnoreCase("S")) { // MULTIPLE
                        // O
                        // SINGLE
                        instruction.setMultiple(token);
                    } else {
                        if (!fakeLineNumber)
                            throw new SPProtocolException(
                                    "\nError on value M/S on line n." + lineNumber
                                            + " : " + line);
                        else
                            throw new SPProtocolException(
                                    "\nError on value M/S on line : " + line);
                    }
                }
                break;
                case DATA_OR_READ_NUMBER: {

                    if (instruction.isWrite()) { // ### WRITE DATA

                        if (controlValueEx(token)) { // DATA BEGIN WITH '0X' OR NOT
                            // ?

                            String tokenData = token.substring(2); // DELETE '0X'

                            if (tokenData.length() == 2)
                                instruction.setDati("0x" + tokenData);

                            else if (tokenData.length() == 4) { // swap bit couple
                                instruction.setDati("0x"
                                        + tokenData.substring(2, 4));
                                instruction.setDati("0x"
                                        + tokenData.substring(0, 2));
                            }

                        } else { // error: instruction data NOT begin with '0x'
                            if (!fakeLineNumber)
                                throw new SPProtocolException(
                                        "\n ERROR!! Recontrol instruction data "
                                                + token + " on line n. "
                                                + lineNumber + " : " + line);
                            else
                                throw new SPProtocolException(
                                        "\n ERROR!! Recontrol instruction data "
                                                + token + " on line : " + line);
                        }

                    } else { // ### READ DATA

                        if (token.matches("\\d{1,}")) { // token= # reading = number

                            for (int cont = 1; cont < Integer.parseInt(token); cont++) {
                                instruction.setDati("r"); // insert 'r'

                            }

                        } else { // token is NOT a number
                            if (!fakeLineNumber)
                                throw new SPProtocolException(
                                        "\n ERROR!! Error on number of reading:  "
                                                + token + " on line n. "
                                                + lineNumber + " : " + line);
                            else
                                throw new SPProtocolException(
                                        "\n ERROR!! Error on number of reading: "
                                                + token + " on line : " + line);
                        }

                    }

                }// end DATA_OR_READ_NUMBER
                break;

                default: { // counter= n. of token >= 6 : it' s possible only with a
                    // WRITE OPERATION

                    if (instruction.isWrite()) { // ### WRITE DATA

                        if (controlValueEx(token)) { // DATA IS IN CORRECT FORMAT
                            // HEXADECIMAL?

                            String tokenData = token.substring(2); // DELETE 'OX'

                            if (tokenData.length() == 2)
                                instruction.setDati("0x" + tokenData);

                            else if (tokenData.length() == 4) { // swap bit couple
                                instruction.setDati("0x"
                                        + tokenData.substring(2, 4));
                                instruction.setDati("0x"
                                        + tokenData.substring(0, 2));
                            }

                        } else { // error: instruction data NOT begin with '0x'
                            if (!fakeLineNumber)
                                throw new SPProtocolException(
                                        "\n ERROR!! Recontrol instruction data "
                                                + token + " on line n. "
                                                + lineNumber + " : " + line);
                            else
                                throw new SPProtocolException(
                                        "\n ERROR!! Recontrol instruction data "
                                                + token + " on line : " + line);
                        }

                    } else { // read data, value not permissible
                        if (!fakeLineNumber)
                            throw new SPProtocolException(
                                    "\n ERROR!! duplicate number of reading:  "
                                            + token + " on line n. " + lineNumber
                                            + " : " + line);
                        else
                            throw new SPProtocolException(
                                    "\n ERROR!! duplicate on number of reading: "
                                            + token + " on line : " + line);
                    }

                }// end case default

            }// END SWITCH

        }// end while token

        if (instruction.getRegisterEx() == null) {
            if (!fakeLineNumber)
                throw new SPProtocolException(
                        "\n ERROR!! Missing register on line n. " + lineNumber
                                + " : " + line);
            else
                throw new SPProtocolException("\n ERROR!! Missing register on: "
                        + line);
        }

        if (instruction.getMultiple() == null) {
            if (!fakeLineNumber)
                throw new SPProtocolException(
                        "\n ERROR!! Missing M/S (Multiple or Single) on line n. "
                                + lineNumber + " : " + line);
            else
                throw new SPProtocolException(
                        "\n ERROR!! Missing M/S (Multiple or Single) register on: "
                                + line);
        }

        if (instruction.getDati().size() == 0) {
            if (instruction.isWrite()) {
                if (!fakeLineNumber)
                    throw new SPProtocolException(
                            "\n ERROR!! Missing data to send on line n. "
                                    + lineNumber + " : " + line);
                else
                    throw new SPProtocolException(
                            "\n ERROR!! Missing data to send on: " + line);

            }
        }

        Instruction2String(instruction, spChip);

        DECODE_SINGLE_INSTRUCTION_ELAPSED += (System.currentTimeMillis() - start);

        return instruction;
    }// end decodeSingleInstruction

    /**
     * Return the original mnemonic instruction in String format.
     *
     * @param instruction The instruction co "re-code"
     * @return The re-coded instruction
     * @throws SPProtocolException If some error occur.
     */
    public String getMnemonicCommand(SPDecoderGenericInstruction instruction) throws SPProtocolException {
        return instruction.getDati().get(0);
    }



    /**
     * Verify if a String is in the format 0xAA (i.e. represents an exadecimal value)
     *
     * @param value
     * @return True if the value is well defined
     * @throws SPProtocolException Is thrown if the value is invalid.
     */
    public static boolean controlValueEx(String value) throws SPProtocolException {
        value = value.toUpperCase();
        boolean valid = false;
        if (value.startsWith("0X") && (value.length() == 6 || value.length() == 4)) {
            try {
                Long.parseLong(value.substring(2), 16);
                valid = true;
            } catch (NumberFormatException e) {}
        }
        return valid;
    }




}// end class SPDecoder

