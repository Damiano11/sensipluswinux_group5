package com.sensichips.sensiplus.level1.decoder;

import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.chip.SPChip;

import java.util.ArrayList;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPDecoderI2C extends SPDecoder {


    public SPDecoderI2C() throws SPProtocolException {
        super();
    }

    @Override
    public void Instruction2String(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException {


        String multiple = instruction.getMultiple();
        boolean write = instruction.isWrite();
        String register = instruction.getRegisterEx();
        ArrayList<String> dati = instruction.getDati();
        String command = "[";
        String registerOp = "";

        // TODO: INDIRIZZO I2C come fare ...
        String dev_id = spChip.getAddress(SPProtocol.SHORT_ADDRESS, SPProtocol.PROTOCOL_NAMES[SPProtocol.I2C]).replace("0x", "").replace("0X", "");
        int dev_id_write = Integer.parseInt(dev_id, 16) * 2;
        int dev_id_read = Integer.parseInt(dev_id, 16) * 2 + 1;

        String read_prefix = Integer.toHexString(dev_id_read);
        String write_prefix = Integer.toHexString(dev_id_write);

        if (read_prefix.length() == 1){
            read_prefix = "0x0" + read_prefix;
        } else {
            read_prefix = "0x" + read_prefix;
        }

        if (write_prefix.length() == 1){
            write_prefix = "0x0" + write_prefix;
        } else {
            write_prefix = "0x" + write_prefix;
        }

        command += write_prefix + " ";

        int binary = Integer.parseInt(register.substring(2), 16); //conversion string - integer
        binary = binary << 2; // 2 bit shift to left

        if (multiple.equalsIgnoreCase("M")) {
            binary = binary + 2;
        } // second-last bit = 1 if there is multiple in instruction string


        String valueInt = Integer.toHexString(binary).toUpperCase();

        if (valueInt.length() == 1)
            command += "0x0" + valueInt;
        else //valueInt.lenght==2
            command += "0x" + valueInt;

        if (!write) {
            command += "\n[" + read_prefix;
        }
        command += " ";

        for (int i = 1; i < dati.size(); i++) { // DATI.SIZE(0) is mnemonic command (e.g. send int_config s 0x1234)
            command += dati.get(i);
            if (i < dati.size() - 1)
                command += " ";
        }

        command += "]";
        instruction.setDecodedInstruction(command);
    }


}// end class SPDecoder





