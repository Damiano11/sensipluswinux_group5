package com.sensichips.sensiplus.level1.decoder;

import java.util.ArrayList;

/**
 * Property: Sensichips s.r.l.
 *
 * @version 1.0
 * @author: Mario Molinara
 */
public class SPDecoderGenericInstruction {

    // setter e getter method for values of send, deviceEx, multiple, registerEx, data

    private boolean write = true;
    private String deviceEx = null;
    private String multiple = null;
    private String registerEx = null;
    private ArrayList<String> dati = new ArrayList<String>();
    private String instruction = "";
    private String decodedInstruction = "";

    public SPDecoderGenericInstruction(String i) {
        instruction = i;
    }

    public String toString(){
        return instruction;
    }

    public String getDecodedInstruction() {
        return decodedInstruction;
    }

    public void setDecodedInstruction(String decodedInstruction) {
        this.decodedInstruction = decodedInstruction;
    }

    public String getInstruction() {
        return instruction;
    }

    public boolean isWrite() {
        return write;
    }

    public void setWrite(boolean write) {
        this.write = write;
    }

    public String getDeviceEx() {
        return deviceEx;
    }

    public void setDeviceEx(String deviceEx) {
        this.deviceEx = deviceEx;
    }

    public String getMultiple() {
        return multiple;
    }

    public void setMultiple(String multiple) {
        this.multiple = multiple;
    }

    public String getRegisterEx() {
        return registerEx;
    }

    public void setRegisterEx(String registerEx) {
        this.registerEx = registerEx;
    }

    public ArrayList<String> getDati() {
        return dati;
    }


    public void setDati(String singleData) {
        this.dati.add(singleData);
    }


}
