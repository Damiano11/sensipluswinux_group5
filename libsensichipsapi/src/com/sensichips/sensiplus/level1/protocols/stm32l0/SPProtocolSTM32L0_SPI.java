package com.sensichips.sensiplus.level1.protocols.stm32l0;

import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.SPProtocolListener;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.decoder.SPDecoderGenericInstruction;
import com.sensichips.sensiplus.level1.decoder.SPDecoderSPI;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacket;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

/**
 * Created by Marco Ferdinandi on 03/08/2017.
 */

public class SPProtocolSTM32L0_SPI extends SPProtocol {
    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L0;

    SPDriver spDriver = null;

    SPProtocolListener protocolListener = null;
    private String dataReadFromDriver = null;

    public static final String MCU = "STM32L0";


    public SPProtocolSTM32L0_SPI(SPDriver spDriver, SPCluster spCluster, int addressingMode) throws SPProtocolException, SPDriverException {
        super(spDriver, spCluster, addressingMode);


        this.spDriver = spDriver;
        //this.protocolListener = callBack;
        protocolName = MCU + "_" + SPProtocol.PROTOCOL_NAMES[SPProtocol.SPI];
    }

    @Override
    public long getADCDelay(){
        return 1;
    }

    @Override
    public String send(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPDriverException, SPProtocolException {
        String result = "";

        String command = instruction.getDecodedInstruction();
        command = command.trim();
        //SPLogger.getLogInterface().d("WIFI", "Send: " + command, DEBUG_LEVEL);

        int numBytesToRead = 0;
        if (command.contains("r")){
            String[] hexString = command.split(" ");

            byte[] txBuffer = new byte[2];
            int val;

            val = Integer.decode(hexString[0]);
            txBuffer[0] = (byte) val;

            //val = Integer.decode(hexString[1]);
            val = hexString.length - 1;
            txBuffer[1] = (byte) (val);
            numBytesToRead = val;

            long startTime = System.currentTimeMillis();
            this.spDriver.write(txBuffer);
            result = getDataRead(numBytesToRead).trim();
            long endTime = System.currentTimeMillis();
            double time = (endTime-startTime);
            if(command.contains("0xC3")) {
                SPLogger.getLogInterface().d("WIFI", "Time reading FIFO: " +time, DEBUG_LEVEL);
            }


        }
        else{
            String[] hexString = command.split(" ");
            byte[] txBuffer1 = new byte[2];
            byte[] txBuffer2 = new byte[hexString.length-1];
            int val= Integer.decode(hexString[0]);
            txBuffer1[0] = (byte)val;
            numBytesToRead =hexString.length - 1;
            txBuffer1[1] = (byte)numBytesToRead;
            for(int i=1; i<hexString.length; i++) {
                val = Integer.decode(hexString[i]);
                txBuffer2[i-1] = (byte)val;
            }


            String sending1 = byteArrayToHexString(txBuffer1);
            String sending2 = byteArrayToHexString(txBuffer2);
            SPLogger.getLogInterface().d("WIFI", "Send: " + sending1+" "+sending2, DEBUG_LEVEL);
            this.spDriver.write(txBuffer1);
            getDataRead(2);
            this.spDriver.write(txBuffer2);
            result = getDataRead(numBytesToRead).trim();
        }


        return result;
    }

    /*@Override
    public String read() throws SPProtocolException {

        SPLogger.getLogInterface().d("WIFI", "Received: " + dataReadFromDriver, DEBUG_LEVEL);

        return dataReadFromDriver;
    }*/


    final private static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String bytesToHex(byte b) {
        char[] hexChars = new char[2];

        int v = b & 0xFF;
        hexChars[0] = hexArray[v >>> 4];
        hexChars[1] = hexArray[v & 0x0F];

        return new String(hexChars);
    }

    private static String byteArrayToHexString(byte[] b) {
        String s = "";
        for(int i=0;i<b.length; i++){
            s = s+bytesToHex(b[i])+ " ";
        }

        return s;
    }


    @Override
    public float elapsedStartComResponse() throws SPProtocolException, SPDriverException {
        throw new SPProtocolException("MCU: " + MCU + " don't allow the speed evaluation");
    }

    private String getDataRead(int numOfBytes) throws SPProtocolException, SPDriverException {
        int len = 0;
        byte[] data = new byte[1024];
        len = spDriver.read(data);

        String s = new String();
        String appo = "";
        if (data != null && numOfBytes > 0) {
            for (int i = 0; i < numOfBytes; i++) {
                String s_appo = bytesToHex(data[i]);
                if (s_appo.length() == 1)
                    s_appo = "0" + s_appo;
                appo = "0x" + s_appo + " ";
                s = s + appo;
            }

        }

        return s;
        //dataReadFromDriver = s;



    }
    @Override
    public void setTk32xToDelete(byte b) throws SPProtocolException, SPDriverException {
        throw new SPProtocolException("Method not implemented in " + SPProtocol.class);
    }

    public String setSpeed(String speed, SPCluster spCluster) throws SPProtocolException {
        throw new SPProtocolException("MCU: " + MCU + " don't allow change of speed");
    }

    public String testMCU() throws SPProtocolException {
        throw new SPProtocolException("MCU: " + MCU + " don't allow start communication test");
    }


    @Override
    public String resetChips(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException {
        return "";
    }

    @Override
    public void init(int addressingMode) throws SPProtocolException, SPDriverException {
        spDriver.openConnection();
        this.protocolListener.connectionUp(this);
    }

    @Override
    public boolean isResettable() {
        return false;
    }

    @Override
    public String reset() throws SPProtocolException {
        return null;
    }

    @Override
    public boolean isInitialized() throws SPProtocolException {
        return false;
    }

    @Override
    public String getProtocolName() throws SPProtocolException {
        return this.protocolName;
    }

    @Override
    public void closeAll() throws SPProtocolException {

    }

    @Override
    public void updateSPDriverListener() throws SPProtocolException {

    }

    @Override
    public String setAddressingType(int mod) throws SPProtocolException {
        return null;
    }

    @Override
    public SPDecoder getSPDecoder() throws SPProtocolException {
        return new SPDecoderSPI();
    }

    @Override
    public byte[] sendPacket(SPProtocolPacket packet) throws SPDriverException, SPProtocolException {
        throw new SPProtocolException("Not yet implemented");
    }

    @Override
    public void connectionUp() {
        this.protocolListener.connectionUp(this);
    }

    @Override
    public void message(String m) {

    }

    @Override
    public void connectionDown() {

    }

    @Override
    public void interrupt(byte interruptValue) {

    }

    @Override
    public void fatalError(String message) {

    }
}
