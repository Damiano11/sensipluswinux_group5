package com.sensichips.sensiplus.level1.protocols.cc2541;

import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.SPProtocolListener;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.decoder.SPDecoderGenericInstruction;
import com.sensichips.sensiplus.level1.decoder.SPDecoderSPI;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacket;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;


/**
 * Created by Marco on 08/03/2016.
 */
public class SPProtocolCC2541_SPI extends SPProtocol {

    private static int READBUF_SIZE = 4096;
    private String DEBUG_MSG = "SPProtocolCC2541_SPI";
    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L0;

    public static final String MCU = "CC2541";

    private SPDriver spDriver;
    private String protocolName = "CC2541_SPI";
    private SPProtocolListener protocolListener;

    public SPProtocolCC2541_SPI(SPDriver spDriver, SPCluster spCluster, int addressingMode) throws SPProtocolException, SPDriverException {
        super(spDriver, spCluster, addressingMode);
        this.spDriver = spDriver;
    }

    @Override
    public void updateSPDriverListener() throws SPProtocolException {
        spDriver.addSPDriverListener(this);
    }

    @Override
    public long getADCDelay(){
        return 50;
    }

    @Override
    public String reset() throws SPProtocolException {
        throw new SPProtocolException("Not yet implemented!");
    }

    @Override
    public String resetChips(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException {
        throw new SPProtocolException("Not yet implemented!");
    }

    /**
     * Return true if the init has been invoked. Return false also if reset has been invoked.
     *
     * @return
     */
    @Override
    public boolean isInitialized() throws SPProtocolException {
        throw new SPProtocolException("Not yet implemented!");
    }

    @Override
    public String setAddressingType(int mod) throws SPProtocolException {
        throw new SPProtocolException("Not yet implemented!");
    }

    //Execute the send operation which is indicated in the command string
    //Example, command string: "0x05 0x10 0x32" ->input
    //                 output: "0x05 0x10 0x32"
    @Override
    public String send(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException, SPDriverException {
        String command = instruction.getDecodedInstruction();


        command = command.trim();

        if (command.contains("r")) {
            String[] hexString = command.split(" ");

            byte[] b = new byte[2];
            int val;

            val = Integer.decode(hexString[0]);
            b[0] = (byte) val;

            //val = Integer.decode(hexString[1]);
            val = hexString.length - 1;
            b[1] = (byte) (val);

            int i=1;
            while(spDriver.write(b)==-1){
                if(i>=1000){
                    SPLogger.getLogInterface().d(DEBUG_MSG, "time out!!!", DEBUG_LEVEL);
                    System.out.println("timeout passed");
                    break;
                }
                try {
                    Thread.sleep(i++);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


        } else {

            String[] hexString = command.split(" ");

            byte[] b = new byte[hexString.length];
            int val;
            for(int i=0; i<hexString.length; i++) {
                val = Integer.decode(hexString[i]);
                b[i] = (byte)val;
            }
            int i=1;
            while(spDriver.write(b)==-1){
                if(i>=1000){
                    System.out.println("timeout passed");
                    break;

                }
                try {
                    Thread.sleep(i++);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


        }

        int len = 0;
        byte[] data = new byte[READBUF_SIZE];


        len = spDriver.read(data);

        /*String s = new String();
        String appo = "";
        if (data != null && len > 0) {

            for(byte b:data) {
                //String s_appo = Integer.toHexString(b).toUpperCase();
                if(b==(byte)('\n'))
                    break;
                String s_appo = bytesToHex(b);
                if (s_appo.length() == 1)
                    s_appo = "0" + s_appo;
                appo = "0x" + s_appo + " ";
                s = s + appo;
            }
        }*/
        String s = new String();
        String appo = "";
        if (data != null && len > 0) {
            int numBytePayload = data[0];
            for (int i = 1; i < numBytePayload+1; i++) {
                String s_appo = bytesToHex(data[i]);
                if (s_appo.length() == 1)
                    s_appo = "0" + s_appo;
                appo = "0x" + s_appo + " ";
                s = s + appo;
            }

        }

        return s + "\n";

    }


    //Execute the read operation which is indicated in the command string
    //Example, command string:  "0x03 r r"
    //Corresponding Buffer:  bidimensional
    //          1� element:  command
    //          2� element: number of byte to read
    //@Override
    //public String read() throws SPProtocolException {


    //    int len = 0;
    //    byte[] data = new byte[READBUF_SIZE];


    //    len = spDriver.read(data);

        /*String s = new String();
        String appo = "";
        if (data != null && len > 0) {

            for(byte b:data) {
                //String s_appo = Integer.toHexString(b).toUpperCase();
                if(b==(byte)('\n'))
                    break;
                String s_appo = bytesToHex(b);
                if (s_appo.length() == 1)
                    s_appo = "0" + s_appo;
                appo = "0x" + s_appo + " ";
                s = s + appo;
            }
        }*/
    //    String s = new String();
    //    String appo = "";
    //    if (data != null && len > 0) {
    //        int numBytePayload = data[0];
    //        for (int i = 1; i < numBytePayload+1; i++) {
    //            String s_appo = bytesToHex(data[i]);
    //            if (s_appo.length() == 1)
    //                s_appo = "0" + s_appo;
    //            appo = "0x" + s_appo + " ";
    //            s = s + appo;
    //        }

    //    }

    //    return s + "\n";

    //}

    public boolean isResettable(){
        return false;
    }

    final private static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String bytesToHex(byte b) {
        char[] hexChars = new char[2];

        int v = b & 0xFF;
        hexChars[0] = hexArray[v >>> 4];
        hexChars[1] = hexArray[v & 0x0F];

        return new String(hexChars);
    }

    @Override
    public void connectionUp() {
        protocolListener.connectionUp(this);
    }

    @Override
    public void init(int addressingMode) throws SPProtocolException, SPDriverException {
        spDriver.openConnection();
        protocolListener.connectionUp(this);

    }

    @Override
    public void setTk32xToDelete(byte b) throws SPProtocolException, SPDriverException {
        throw new SPProtocolException("Method not implemented in " + SPProtocol.class);
    }

    @Override
    public float elapsedStartComResponse() throws SPProtocolException, SPDriverException {
        throw new SPProtocolException("MCU: " + MCU + " don't allow the speed evaluation");
    }

    @Override
    public String setSpeed(String speed, SPCluster spCluster) throws SPProtocolException {
        throw new SPProtocolException("MCU: " + MCU + " don't allow change of speed");
    }

    @Override
    public String testMCU() throws SPProtocolException {
        throw new SPProtocolException("MCU: " + MCU + " don't allow start communication test");
    }


    @Override
    public String getProtocolName() throws SPProtocolException {
        return protocolName;
    }

    @Override
    public void closeAll() throws SPProtocolException, SPDriverException {


        spDriver.closeConnection();

    }



    @Override
    public SPDecoder getSPDecoder() throws SPProtocolException {
        return new SPDecoderSPI();
    }

    @Override
    public byte[] sendPacket(SPProtocolPacket packet) throws SPDriverException, SPProtocolException {
        throw new SPProtocolException("Not yet implemented");
    }
}
