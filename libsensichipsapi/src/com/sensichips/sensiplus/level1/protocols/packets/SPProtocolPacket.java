package com.sensichips.sensiplus.level1.protocols.packets;

public interface SPProtocolPacket {
    byte[] getBytes();
    int getDataLengthExpected();

}
