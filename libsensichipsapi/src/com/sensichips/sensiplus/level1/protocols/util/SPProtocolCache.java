package com.sensichips.sensiplus.level1.protocols.util;

import com.sensichips.sensiplus.SPException;

import java.util.Hashtable;

public class SPProtocolCache {

    private static Hashtable<String,String> cacheInstruction = new Hashtable<String, String>();
    public static int HIT;
    public static int MISS;
    public SPProtocolCache(){

    }

    public synchronized static String checkInstruction(String instruction) throws SPException {
        String output=null;

        instruction = instruction.toUpperCase();

        if(!instruction.contains("COMMAND"))
        {
            String[] splitted = instruction.trim().split(" ");
            String value = splitted[splitted.length - 1].trim();
            String key = "";
            for (int i = 0; i < splitted.length - 1; i++) {
                key = key + splitted[i].trim();
            }

            String valueFound = cacheInstruction.get(key);
            if (valueFound == null) {
                cacheInstruction.put(key, value);
                MISS++;
            } else {
                if(!valueFound.equals(value)) {
                    cacheInstruction.remove(key);
                    cacheInstruction.put(key, value);
                    MISS++;
                } else {
                    output = value;
                    HIT++;
                }
            }

        }
        //System.out.println("HIT  rate: " + ((float)HIT/(HIT + MISS)));
        //System.out.println("MISS rate: " + ((float)MISS/(HIT + MISS)));
        return output;
    }

    public synchronized static void clearCache(){
        cacheInstruction = new Hashtable<String, String>();
    }

}
