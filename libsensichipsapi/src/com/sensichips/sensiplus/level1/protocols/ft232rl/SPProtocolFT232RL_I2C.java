package com.sensichips.sensiplus.level1.protocols.ft232rl;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.SPProtocolListener;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.decoder.*;

import java.util.StringTokenizer;

/**
 * Created by mario on 03/12/2015.
 */
public class SPProtocolFT232RL_I2C extends SPProtocolFT232RL_BUSPIRATE {


    public SPProtocolFT232RL_I2C(SPDriver readWrite, SPCluster spCluster, int addressingMode) throws SPProtocolException, SPDriverException {
        super(readWrite, spCluster, addressingMode);
        // Define the init sequence for I2C protocol
        initSequence.add(new SCProtocol_InitOperation("m\n", 12, 0, 0, ""));
        initSequence.add(new SCProtocol_InitOperation("4\n", 7, 0, 0, ""));
        initSequence.add(new SCProtocol_InitOperation("1\n", 2, 0, 0,""));
        initSequence.add(new SCProtocol_InitOperation("PAw\n", 4, 0, 0,""));
        initSequence.add(new SCProtocol_InitOperation("W\n", 2, 250, 0,""));
        initSequence.add(new SCProtocol_InitOperation("(1)\n", 4, 0, 0, SPProtocol.PROTOCOL_NAMES[SPProtocol.I2C]));
        protocolName = MCU + "_" + SPProtocol.PROTOCOL_NAMES[SPProtocol.I2C];

    }

    @Override
    public String getProtocolName() {

        return SPProtocol.PROTOCOL_NAMES[SPProtocol.I2C];
    }

    public boolean isResettable(){
        return true;
    }

    @Override
    public String setAddressingType(int mod) throws SPProtocolException {
        if (mod != SPProtocol.SHORT_ADDRESS || mod < 0 || mod >= 3){
            mod = mod < 0 ? 0 : mod >= 3 ? 2 : mod;
            throw new SPProtocolException("Communication mode not allowed in I2C: " + SPProtocol.ADDRESSING_TYPES[mod]);
        }
        addressingMode = mod;
        return getProtocolName() + " " + SPProtocol.ADDRESSING_TYPES[mod] + " OK!";
    }

    @Override
    protected String send(String comando) throws SPProtocolException {

        String output = "";

        try {

            // I2C
            String reclI2C[] = null;
            int numElemI2C = 0;
            int totElemI2C = 0;
            int startCiclo = 0;
            int endCiclo = 0;
            String comandoAppoI2C = null;
            StringTokenizer stI2C = null;
            String nextCommandI2C = null;
            // I2C
            boolean scrittura = !comando.contains("r");

            comandoAppoI2C = comando.replace("[", "").replace("]", "");
            stI2C = new StringTokenizer(comandoAppoI2C, "\n");

            while (stI2C.hasMoreTokens()) {
                nextCommandI2C = stI2C.nextToken();
                if (nextCommandI2C.length() > 0) {
                    numElemI2C = countI2CToken(nextCommandI2C);
                    if (!nextCommandI2C.contains("r")) { // Scrittura
                        nextCommandI2C = "[" + nextCommandI2C;
                        if (scrittura) { // Scrittura prima di lettura [0x82 0xNN\n
                            nextCommandI2C = nextCommandI2C + "]";
                            totElemI2C = numElemI2C + 4;
                            startCiclo = 3;
                            endCiclo = 1;
                        } else {
                            totElemI2C = numElemI2C + 3;
                            startCiclo = 3;
                            endCiclo = 0;
                        }
                        nextCommandI2C = nextCommandI2C + "\n";

                    } else { // Lettura
                        nextCommandI2C = "[" + nextCommandI2C + "]\n";
                        totElemI2C = numElemI2C + 5;
                        startCiclo = 3;
                        endCiclo = 2;
                    }
                    reclI2C = sendL(nextCommandI2C, totElemI2C);
                    output = "";
                    for (int i = startCiclo; i < reclI2C.length - endCiclo; i++) {
                        output += reclI2C[i].replace("READ:", "").replace("WRITE:", "").replace("NACK", "").replace("ACK", "").replace("\r\n", "").trim();
                        if (i < reclI2C.length - endCiclo) {
                            output += " ";
                        }
                    }
                }
            }
            output = "[" + output.trim() + "]";
        } catch (Exception e) {
            throw new SPProtocolException(GENERAL_ERROR_MESSAGE + "Generic exception in SPProtocol.send. " + e.getMessage());
        }
        return output;
    }

    private int countI2CToken(String token) {
        StringTokenizer st = new StringTokenizer(token, " ");
        int count = 0;
        while (st.hasMoreTokens()) {
            st.nextToken();
            count++;
        }
        return count - 1;
    }

    private String setAddressingType(int modoAttuale, int mod) throws SPProtocolException {
        if (mod != SPProtocol.I2C){
            mod = mod < 0 ? 0 : mod >= 3 ? 2 : mod;
            throw new SPProtocolException("Communication mode not allowed in I2C: " + SPProtocol.ADDRESSING_TYPES[mod]);
        }

        if (modoAttuale == mod){
            return "";
        }

        String address = "";

        return "";

    }

    @Override
    public String resetChips(SPDecoderGenericInstruction instruction, SPChip spchip) throws SPProtocolException, SPDriverException {

        send(instruction, spchip);
        return setAddressingType(SPProtocol.SHORT_ADDRESS, addressingMode);
    }

    public SPDecoder getSPDecoder() throws SPProtocolException {
        return new SPDecoderI2C();
    }

}
