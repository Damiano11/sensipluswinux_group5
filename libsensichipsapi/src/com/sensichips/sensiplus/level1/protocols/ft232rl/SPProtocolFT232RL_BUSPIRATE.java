package com.sensichips.sensiplus.level1.protocols.ft232rl;

import com.sensichips.sensiplus.SPException;
import com.sensichips.sensiplus.level0.SPDriver;
import com.sensichips.sensiplus.level0.SPDriverException;
import com.sensichips.sensiplus.level1.SPProtocol;
import com.sensichips.sensiplus.level1.SPProtocolException;
import com.sensichips.sensiplus.level1.chip.SPChip;
import com.sensichips.sensiplus.level1.chip.SPCluster;
import com.sensichips.sensiplus.level1.decoder.SPDecoder;
import com.sensichips.sensiplus.level1.decoder.SPDecoderGenericInstruction;
import com.sensichips.sensiplus.level1.protocols.packets.SPProtocolPacket;
import com.sensichips.sensiplus.util.SPDelay;
import com.sensichips.sensiplus.util.log.SPLogger;
import com.sensichips.sensiplus.util.log.SPLoggerInterface;

import java.util.ArrayList;

/**
 * Created by mario on 03/12/2015.
 */
public class SPProtocolFT232RL_BUSPIRATE extends SPProtocol implements Runnable {

    //public PrintWriter dumpMnemonic;
    protected boolean GENERAL_ERROR = false;

    private static int DEBUG_LEVEL = SPLoggerInterface.DEBUG_VERBOSITY_L0;

    public static final String MCU = "FT232RL";


    protected static final int READBUF_SIZE = 4096;
    //public static final String BUSPIRATE = "BUSPIRATE";


    protected ArrayList<SCProtocol_InitOperation> initSequence = new ArrayList<SCProtocol_InitOperation>();

    // Gestione ricezione dati
    protected SCDriver_ReceiverQueue<String> receiverBufferString;
    //private SCDriver_ReceiverQueue<Byte> bufferByte = new SCDriver_ReceiverQueue<>(Byte.class);


    // Necessaria una variabile globale?
    // protected String output = "";


    //protected int stato = SCDriverFT232RL_Proxy.BUS_PIRATE_MODE;
    //protected int annullaGeneralError;


    //protected SPProtocolListener spProtocolListener;
    //protected SPDriver spDriver;


    //protected boolean transaction = false;
    //protected ArrayList<String> comandiTransaction = new ArrayList<String>();
    @Override
    public void setTk32xToDelete(byte b) throws SPProtocolException, SPDriverException {
        throw new SPProtocolException("Method not implemented in " + SPProtocol.class);
    }


    public SPProtocolFT232RL_BUSPIRATE(SPDriver spDriver, SPCluster spCluster, int addressingMode) throws SPProtocolException, SPDriverException {
        super(spDriver, spCluster, addressingMode);
        protocolName = SPProtocol.PROTOCOL_NAMES[SPProtocol.BUSPIRATE];

        //init();

    }

    @Override
    public String resetChips(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException, SPDriverException {
        return "";
    }


    public boolean isResettable(){
        return true;
    }


    @Override
    public void updateSPDriverListener() throws SPProtocolException {
        spDriver.addSPDriverListener(this);
    }


    @Override
    public String setAddressingType(int mod)  throws SPProtocolException, SPDriverException {
        return getProtocolName() + " " + SPProtocol.ADDRESSING_TYPES[mod] + " OK!";
    }


    /**
     * Close all, also low level connection
     *
     * @throws SPProtocolException if some low level problem occur
     */
    @Override
    public void closeAll() throws SPProtocolException, SPDriverException {
        reset();
        stopReceiverThread();
        //spDriver.closeConnection();

    }

    @Override
    public float elapsedStartComResponse() throws SPProtocolException, SPDriverException {
        throw new SPProtocolException("MCU: " + MCU + " don't allow the speed evaluation");
    }

    @Override
    public String setSpeed(String speed, SPCluster spCluster) throws SPProtocolException {
        throw new SPProtocolException("MCU: " + MCU + " don't allow change of speed");
    }

    @Override
    public String testMCU() throws SPProtocolException {
        throw new SPProtocolException("MCU: " + MCU + " don't allow start communication test");
    }

    /**
     * To override if the protocol define some kind of starting sequence
     */
    protected String startCommunication() throws SPProtocolException, SPDriverException {
        return "";
    }

    protected void sendID(String id) throws SPProtocolException {
    }

    /**
     * This method allow to send a sequence of bytes through the selected device.
     *
     * @param instruction A sequence of bytes represented as a String like "0x0A 0x03"
     * @param spChip
     * @throws SPProtocolException if any transmission error occur
     */
    @Override
    public String send(SPDecoderGenericInstruction instruction, SPChip spChip) throws SPProtocolException, SPDriverException {
            return write(instruction.getDecodedInstruction(), spChip, addressingMode);
    }



    @Override
    public long getADCDelay(){
        return 15;
    }

    protected String write(String instruction, SPChip spChip, int addressingType) throws SPProtocolException, SPDriverException {


        if (GENERAL_ERROR) {
            throw new SPProtocolException(GENERAL_ERROR_MESSAGE);
        }
        //SPDelay.delay(100);

        String output = null;

        String sc = "";

        int i = 0, maxTrial = 2;
        boolean failed = true;

        while (failed && i < maxTrial) {
            try {
                sc = startCommunication();

                if (sc.length() > 0){
                    SPLogger.getLogInterface().d(LOG_MESSAGE, "Start communication ok: " + sc, DEBUG_LEVEL);
                }


                String address = spChip.getAddress(addressingType, getProtocolName());

                if (address.length() > 0){
                    sendID(address);
                }

                output = send(instruction);
                failed = false;
            } catch (SPProtocolException e) {
                SPLogger.getLogInterface().d(LOG_MESSAGE, "SCDriverInterruptException in void send(String s), resend instruction " + instruction, DEBUG_LEVEL);
                i++;
            }
        }
        if (failed) {
            String mess = "SCDriverInterruptException in void send(String s). Failed to send " + instruction + " after " + maxTrial + " trial.";
            SPLogger.getLogInterface().d(LOG_MESSAGE, "WriteBus_err: " + mess, DEBUG_LEVEL);
            throw new SPProtocolException(mess);
        }
        return output;
    }


    /**
     * @param comando
     * @return
     * @throws SPProtocolException
     */
    protected String send(String comando) throws SPProtocolException, SPDriverException {
        // Default implementation for FT2323 in buspirate modality
        sendL(comando, 0);
        return "";
    }


    /*
    public void verifyCoherenceServiceBridge() throws SPProtocolException {

        stopReceiverThread();

        int lenRcv = 0;
        long timeOut = 250;
        byte[] buffer = new byte[READBUF_SIZE];
        String lastSend = "m\n";
        spDriver.write(lastSend.getBytes());
        SPDelay.delay(timeOut);
        lenRcv = spDriver.read(buffer);
        String s = new String(buffer, 0, lenRcv);

        if (!s.contains("Ready")) {
            lastSend = "x\n";
            spDriver.write(lastSend.getBytes());
            SPDelay.delay(timeOut);
            lenRcv = spDriver.read(buffer);
            s = new String(buffer, 0, lenRcv);
        }

        String expectedProtocol = null;
        // Individuare correttamente quale stato è stato individuato ... ora la condizione è negata.
        //if (getProtocolName().equals(SPInitialization.SPI) && !s.contains("no mode change")){
        //    expectedProtocol = SPInitialization.SPI;
        //} else if (getProtocolName().equals(SPInitialization.I2C) && !s.contains("no mode change")){
            //expectedProtocol = SPInitialization.I2C;
        //} else if (getProtocolName().equals(SPProtocolFT232RL_BUSPIRATE.BUSPIRATE) && !s.contains("HiZ>")) {
            //expectedProtocol = SPProtocolFT232RL_BUSPIRATE.BUSPIRATE;
        //}
        //if (expectedProtocol != null){
         //   throw new SPProtocolException(GENERAL_ERROR_MESSAGE + "\nIncoherent state: expected " + getProtocolName() + ",  found " + expectedProtocol);
        //}

        startReceiverThread();
    }*/

    /**
     * @return
     */
    @Override
    public String getProtocolName() throws SPProtocolException {
        return protocolName;
    }



    protected final String[] sendL(String command, int n) throws SPProtocolException, SPDriverException {
        return sendL(command, n, timeOutFromDriver);
    }

    protected final String[] sendL(String command, int n, long timeout) throws SPProtocolException, SPDriverException {
        String output[] = new String[n];
        SPLogger.getLogInterface().d(LOG_MESSAGE, "-------------------------------------------------", DEBUG_LEVEL);
        SPLogger.getLogInterface().d(LOG_MESSAGE, "Send: " + command + "Wait for " + n + " elements.", DEBUG_LEVEL);
        String echo = "";

        spDriver.write(command.getBytes());
        for (int i = 0; i < n; i++) {
            try {
                output[i] = receiverBufferString.get(timeout);
                echo += output[i];
            } catch (Exception e) {
                SPLogger.getLogInterface().d(LOG_MESSAGE, e.getMessage(), DEBUG_LEVEL);
                //annullaGeneralError = stato;
                GENERAL_ERROR = true;
                throw new SPProtocolException(GENERAL_ERROR_MESSAGE + e.getMessage());
            }
        }


        SPLogger.getLogInterface().d(LOG_MESSAGE, "Obtained " + n + " elements.\n" + echo, DEBUG_LEVEL);
        return output;
    }


    /**
     * A general implementation of the init method for FT232RL
     */
    @Override
    public void init(int addressingMode) throws SPProtocolException, SPDriverException {
        if (GENERAL_ERROR) {
            throw new SPProtocolException(GENERAL_ERROR_MESSAGE);
        } else if (isInitialized) {
            return;
        }

        stopReceiverThread = false;
        stoppedReceiverThread = true;
        receiverBufferString = new SCDriver_ReceiverQueue<String>(String.class, queueSize);

        try {

            spDriver.openConnection();
            startReceiverThread();


            for (int i = 0; i < initSequence.size(); i++) {
                SPDelay.delay(initSequence.get(i).getDelayBeforeOp());
                String[] resL = sendL(initSequence.get(i).getMessageToSend(), initSequence.get(i).getExpectedLines());
                if (!initSequence.get(i).getResponseContains().equals("")) {
                    boolean flag = false;
                    int j = 0;
                    while (j < resL.length && !flag) {
                        flag = (resL[j].contains(initSequence.get(i).getResponseContains()));
                        j++;
                    }
                    if (!flag) { // Error in sequence
                        GENERAL_ERROR = true;
                        throw new SPProtocolException("Problem during " + getProtocolName() + " setup. " + GENERAL_ERROR_MESSAGE);
                    }
                }
                SPDelay.delay(initSequence.get(i).getDelayAftereOp());
            }


            cleanReceiver();

            setAddressingType(addressingMode);

            this.addressingMode = addressingMode;

            isInitialized = true;

        } catch (SPProtocolException sce) {
            GENERAL_ERROR = true;
            throw new SPProtocolException(sce.getMessage());
        }

        spProtocolListener.connectionUp(this);
    }

    @Override
    public boolean isInitialized() {
        return isInitialized;
    }


    @Override
    public String reset() throws SPProtocolException, SPDriverException {
        if (GENERAL_ERROR) {
            throw new SPProtocolException(GENERAL_ERROR_MESSAGE);
        }

        try{
            if (isInitialized && !getProtocolName().equals(SPProtocol.BUSPIRATE)){
            //if (!getProtocolName().equals(BUSPIRATE)){
                String str = "#\n";
                String resp[] = sendL(str, 2);
                boolean flag = false;
                int cont = 0;
                while (cont < resp.length && !flag) {
                    flag = resp[cont].contains("Ready");
                    cont++;
                }
                if (!flag) {
                    //SPLogger.getLogInterface().d(TAG_SERVICE, "cleanReceiver()");
                    cleanReceiver();
                }

                isInitialized = false;
                GENERAL_ERROR = false;
            }
            stopReceiverThread();
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Waiting for reset ... ", DEBUG_LEVEL);
            //Thread.sleep(1000);
        } catch (Exception e){
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Reset failed", DEBUG_LEVEL);
        }
        return "Reset ok";

    }

    protected void cleanReceiver() throws SPProtocolException {

        if (receiverBufferString == null){
            return;
        }

        int maxIterClean = 20;
        long minimumWaiting = 100;
        int cont = 0;
        try {
            while (cont < maxIterClean || (receiverBufferString.get(minimumWaiting)).length() > 0)
                cont++;
        } catch (SPProtocolException e) {
            SPLogger.getLogInterface().d(LOG_MESSAGE, "Clean Receiver interrupted.", DEBUG_LEVEL);
        }
    }


    /**********************************************************************************************
     *                              END: LOW LEVEL SEND/RECEIVE ON FT232RL
     *********************************************************************************************/


    /**
     * SPDriverListener IMPLEMENTATION
    */

    @Override
    public void connectionUp() {
        spProtocolListener.connectionUp(this);
    }



    /**********************************************************************************************
     * THREAD GESTIONE LETTURA DA FT232RL
     *********************************************************************************************/


    protected boolean stopReceiverThread = false;
    protected boolean stoppedReceiverThread = true;


    protected boolean isReceiverThreadRunning() {
        return !stoppedReceiverThread;
    }

    protected void startReceiverThread() {
        if (stoppedReceiverThread) {
            // Only one thread can be active
            stopReceiverThread = false;
            stoppedReceiverThread = false;
            new Thread(this).start();
        }
    }

    public void stopReceiverThread() {
        stopReceiverThread = true;
        while (!stoppedReceiverThread) {
            SPDelay.delay(SPDelay.DELAY_LOW);
        }
    }


    /**
     * Cicla continuamente sulla porta usb. Deposita nel buffer token completi (stringhe) chiuse dalla sequenza \r\n
     * Esempio: "0x0D 0x0A\r\n"
     */
    @Override
    public void run() {
        byte bufferRec[] = new byte[READBUF_SIZE];
        StringBuilder newToken = new StringBuilder();
        int len = 0;
        int i = 0;
        boolean lf = false, cr = false;
        //SPLogger.getLogInterface().d(TAG_SERVICE,"SPProtocol receiver started.");
        stoppedReceiverThread = false;
        while (!stopReceiverThread && !GENERAL_ERROR) {

            try {
                // Produci
                lf = false;
                cr = false;
                while (!(lf && cr) && !stopReceiverThread) {
                    if (i == len) {
                        len = spDriver.read(bufferRec);
                        //SPLogger.getLogInterface().d(TAG_SERVICE,"len: " + len);
                        i = 0;
                    }
                    if (i == 0 && i == len) {
                        Thread.sleep((long) 1);
                    } else {
                        while (i < len && !(lf && cr)) {
                            if (!lf && bufferRec[i] == 0x0D) {
                                lf = true;
                            } else if (lf && !cr && bufferRec[i] == 0x0A) {
                                cr = true;
                            } else {
                                lf = false;
                            }

                            newToken.append((char) bufferRec[i]);
                            i++;
                        }
                    }
                }
                if (lf && cr && !stopReceiverThread) {
                    // TODO: Ripristinare SPLogger.getLogInterface().d seguente
                    //SPLogger.getLogInterface().d(LOG_MESSAGE, newToken.toString(), DEBUG_LEVEL);
                    receiverBufferString.put(newToken.toString());
                    newToken = new StringBuilder();
                }
            } catch (SPException ex) {
                SPLogger.getLogInterface().d(LOG_MESSAGE, ex.getMessage(), DEBUG_LEVEL);
            } catch (InterruptedException e) {
                SPLogger.getLogInterface().d(LOG_MESSAGE, e.getMessage(), DEBUG_LEVEL);
            }
        }
        stoppedReceiverThread = true;
    }



    public SPDecoder getSPDecoder() throws SPProtocolException {
        return null;
    }

    @Override
    public byte[] sendPacket(SPProtocolPacket packet) throws SPDriverException, SPProtocolException {
        throw new SPProtocolException("Not yet implemented");
    }

}

